<?php
/*
 * class-quiz.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


class Quiz
{
    public $parent_id;
    public $user_id = -1;
    
    // pour les réponses il faut savoir à quelle session les rattacher
    public $session_id;
    
    public $questions = array();
    public $reponses = array();
    
    public $sujet; // prerequis, objectifs, satisfaction, formation
    public $sujet_titre;
    public $namecode = ""; // quizpr, quizobj, quizsat,
    public $type; // Q, R
    public $occurrence = 0;
    public $quiz_id; // identifiant unique du quiz qui permet de le retrouver après modification des questions (généré en augmentant de 1 l'ID le plus élevé des quiz présents dans la base)
    
    
    private $table_suffix = "wpof_quiz";
    
    /*
     * $parent_id vaut
     * si $type == Q (questions)
     *      $formation_id ou $session_formation_id (si $formation_id n'existe pas, session unique)
     * si $type == R (réponses)
     *      $user_id
     *
     * Cependant, on passe soit un $user_id (si $type == R), soit un $session_formation_id (si $type == Q)
     * pour type == Q, on détermine si la session a son propre quiz, sinon, on va chercher celui de la formation
     *
     * MAJ : on ne passe plus le type, c'est forcément Q, mais on a plusieurs choix :
     * - soit on affiche le questionnaire pour un stagiaire : dans ce cas, le quiz_id détermine les questions (le quiz_id devre être stocké dans la session)
     * - soit on affiche juste le questionnaire de manière générale et l'on prend le quid_id le plus récent
     *
     */
    public function __construct($quiz_id = -1)
    {
        if ($quiz_id > 0)
        {
            $this->quiz_id = $quiz_id;
            
            global $wpdb;
            $table_quiz = $wpdb->prefix.$this->table_suffix;
            
            $query = $wpdb->prepare("SELECT DISTINCT subject, parent_id from $table_quiz WHERE quiz_id = '%d' AND type = 'Q';", $quiz_id);
            $result = $wpdb->get_row($query);
            
            //$this->sujet = $result->subject;
            //$this->parent_id = $result->parent_id;
            $this->set_identite($result->subject, $result->parent_id);
            
            if (get_post_type() == "session")
                $this->session_id = get_the_ID();
        }
        // sinon, on ne fait qu'allouer l'objet vierge
    }
    
    public function set_identite($sujet, $parent_id)
    {
        $this->sujet = $sujet;
        $this->sujet_titre = "";
        
        switch($sujet)
        {
            case "prerequis":
                $this->sujet_titre = __("Évaluation des compétences sur les pré-requis");
                $this->namecode = "quizpr";
                break;
            case "objectifs":
                $this->sujet_titre = __("Évaluation des compétences sur les objectifs");
                $this->namecode = "quizobj";
                break;
            case "formation":
                $this->sujet_titre = __("Évaluation de la formation");
                $this->namecode = "quizfor";
                break;
            case "satisfaction":
                $this->sujet_titre = __("Votre satisfaction à postériori");
                $this->namecode = "quizsat";
                break;
        }
        
        $this->parent_id = $parent_id;
        $this->{$this->namecode} = "";
    }
    
    // initialisation des questions à partir d'un texte (depuis la création d'une formation ou d'une session de formation)
    public function parse_text($text)
    {
        global $wpdb;
        $table_quiz = $wpdb->prefix.$this->table_suffix;
        
        $temp_questions = array();
        $title = "none";
        
        foreach (preg_split("/\n/", $text) as $q)
        {
            $q = trim($q);
            if ($q == "") continue;
            
            // On suppose que les paragraphes sont les lignes qui ne commencent pas par une balise HTML (<p> est implicite dans WordPress)
            if (substr_compare($q, "<h", 0, 2) == 0)
                $title = htmlentities(strip_tags($q), ENT_QUOTES);
            else
                $temp_questions[$title][] = htmlentities($q, ENT_QUOTES);
        }
        
        // comparaison des deux tableaux
        $diff = false;
        foreach ($temp_questions as $t => $groupe)
        {
            if ($diff) break;
            if (!isset($this->questions[$t]))
                $diff = true;
            elseif (count(array_diff_assoc($groupe, $this->questions[$t])) > 0)
                $diff = true;
        }
        
        if ($diff)
        {
            $this->questions = $temp_questions;
            $this->quiz_id = $this->last_quiz_id() + 1;
            $num = 0;
            foreach ($this->questions as $t => $groupe)
                foreach ($groupe as $q)
                {
                    $query = $wpdb->prepare("INSERT INTO $table_quiz (quiz_id, parent_id, subject, type, title, meta_key, meta_value)
                        VALUES ('%d', '%d', '%s', 'Q', '%s', '%d', '%s');",
                        $this->quiz_id, $this->parent_id, $this->sujet, $t, $num, $q);
                    $wpdb->query($query);
                    $num++;
                }
        }
    }
    
    /*
     * Réinitialise le tableau questions à partir d'une liste
     * Chaque élément de liste est un tableau contenant une clé 'type' (titre, competence) et une clé 'text' (le texte de l'entrée)
     */
    public function parse_list($list_questions = array())
    {
        if (empty($list_questions)) return;
        
        $titre = 'none';
        $this->questions = array($titre => array());
        foreach($list_questions as $ligne)
        {
            if ($ligne['type'] == 'titre')
            {
                $titre = $ligne['text'];
                $this->questions[$titre] = array();
            }
            else
                $this->questions[$titre][] = $ligne['text'];
        }
    }
    
    /*
     * Mets à jour les questions dans la base de données
     */
    public function update_questions()
    {
        global $wpdb;
        $table_quiz = $wpdb->prefix.$this->table_suffix;
        // TODO : tester si le quiz_parent_id est une formation (créer nouveau quiz_id) ou une session (garder le même quiz_id)
        
        $parent_type = get_post_type($this->parent_id);
        if ($parent_type == "session")
        {
            $query = $wpdb->prepare("DELETE FROM $table_quiz WHERE quiz_id = '%d';", $this->quiz_id);
            $wpdb->query($query);
        }
        else  // si le quiz vient d'une formation ou nouvellement créé, on en crée un nouveau avec un nouveau quiz_id
        {
            $this->quiz_id = $this->last_quiz_id() + 1;
            $this->parent_id = $this->new_parent_id;
            update_post_meta($this->parent_id, $this->namecode."_id", $this->quiz_id);
            update_post_meta($this->parent_id, $this->namecode."_parent_id", $this->parent_id);
        }
        
        $num = 0;
        foreach ($this->questions as $t => $groupe)
            foreach ($groupe as $q)
            {
                $query = $wpdb->prepare("INSERT INTO $table_quiz (quiz_id, parent_id, subject, type, title, meta_key, meta_value)
                    VALUES ('%d', '%d', '%s', 'Q', '%s', '%d', '%s');",
                    $this->quiz_id, $this->parent_id, $this->sujet, $t, $num, $q);
                $wpdb->query($query);
                $num++;
            }
    }
    
    /*
     * Charger les questions dans le quiz
     * $quiz_id permet de charger une ancienne version
     * Si $quiz_id est null, on charge la version la plus récente
     */
    public function init_questions($quiz_id = null)
    {
        global $wpdb;
        $table_quiz = $wpdb->prefix.$this->table_suffix;
        
        if (!$quiz_id)
        {
            $query = $wpdb->prepare("SELECT MAX(`quiz_id`) FROM $table_quiz WHERE `parent_id` = '%d' AND subject = '%s';", $this->parent_id, $this->sujet);
            $quiz_id = $wpdb->get_var($query);
        }
        
        $this->quiz_id = $quiz_id;
        
        $query = $wpdb->prepare("SELECT title, meta_key, meta_value from $table_quiz WHERE parent_id = '%d' AND subject = '%s' AND type = 'Q' AND quiz_id = '%d';",
            $this->parent_id, $this->sujet, $this->quiz_id);
        
        $this->questions = array();
        foreach ($wpdb->get_results($query, ARRAY_A) as $line)
        {
            $this->questions[stripslashes($line['title'])][$line['meta_key']] = stripslashes($line['meta_value']);
        }
        
        $this->{$this->namecode} = $this->get_html_questions();
    }
    
    
    public function init_reponses($user_id, $occurrence)
    {
        global $wpdb;
        $table_quiz = $wpdb->prefix.$this->table_suffix;
        
        $this->user_id = $user_id;
        
        $query = $wpdb->prepare("SELECT title, meta_key, meta_value from $table_quiz WHERE parent_id = '%d' AND subject = '%s' AND type = 'R' AND quiz_id = '%d' AND occurrence = '%d';",
            $this->user_id, $this->sujet, $this->quiz_id, $occurrence);
        
        $this->reponses = array();
        foreach ($wpdb->get_results($query, ARRAY_A) as $line)
        {
            $this->reponses[$line['meta_key']] = $line['meta_value'];
        }
    }
    
    /*
     * Mets à jour toutes les réponses présentes dans $this->reponses
     * ON DUPLICATE KEY évite que les entrées de la table soient dupliquées inutilement
     */
    public function update_reponses($user_id, $occurrence)
    {
        global $wpdb;
        $table_quiz = $wpdb->prefix.$this->table_suffix;
        
        foreach($this->reponses as $t => $groupe)
            foreach ($groupe as $meta_key => $meta_value)
            {
                $query = $wpdb->prepare
                ("INSERT INTO $table_quiz (parent_id, title, type, quiz_id, subject, occurrence, meta_key, meta_value)
                    VALUES ('%d', '%s', 'R', '%d', '%s', '%d', '%d', '%s')
                    ON DUPLICATE KEY UPDATE meta_value = '%s';",
                    $user_id, $t, $this->quiz_id, $this->sujet, $occurrence, $meta_key, $meta_value, $meta_value
                );
                $wpdb->query($query);
            }
    }
    
    /*
     * Retourne le dernier numéro de quiz depuis la base de données
     */
    public function last_quiz_id()
    {
        global $wpdb;
        $table_quiz = $wpdb->prefix.$this->table_suffix;
        
        $query = "SELECT MAX(`quiz_id`) FROM $table_quiz;";
        return $wpdb->get_var($query);
    }
    
    
    /*
    * Création du questionnaire de test des connaissances (pré-requis et objectifs)
    * $user_id : id de l'utilisateur dont il faut récupérer les réponses
    * $quiz_id : indice du quiz dans la session de formation : eval_preform ou eval_postform
    *
    * On suppose que ces questions peuvent évoluer dans le temps (si possible pas pendant une session de formation, hein, pas de blague !) donc on va aussi stocker le texte de la question et la date de réponse
    * Les réponses à ce questionnaire ne sont stockées que dans le cadre d'une session de formation, donc pas besoin de stocker l'intitulé de la formation
    *
    */
    public function get_html($user_id = 0, $quiz_id = null, $occurrence = 0)
    {
        global $wpof;
        $html = "";
        
        if ($user_exists = get_user_by('ID', $user_id))
        {
            $this->init_reponses($user_id, $occurrence);
            $session_stagiaire = get_stagiaire_by_id($this->session_id, $user_id);
        }
        
        // TODO prendre le bon quiz_id depuis la session
        
        if (count($this->questions) == 0)
            $this->init_questions($quiz_id);
        
        if ($user_exists)
        {
            $html .= hidden_input("session_id", $session_stagiaire->session_formation_id);
            $html .= hidden_input("user_id", $user_id);
        }
        $html .= hidden_input("quiz[".$this->sujet."][id]", $quiz_id);
        $html .= hidden_input("quiz[".$this->sujet."][parent_id]", $this->parent_id);
        $html .= hidden_input("quiz_sujet[]", $this->sujet);
        $html .= hidden_input("quiz[".$this->sujet."][occurrence]", $occurrence);
        $html .= hidden_input("quiz[".$this->sujet."][date]", time());

        $html .= "<table class='eval'>";
        foreach($this->questions as $titre => $groupe)
        {
            if ($titre == "none") $titre = "";
            $html .= "<tr><td><h3>$titre</h3></td><td><span class='um-faicon-minus-circle'></span> 1</td><td>2</td><td>3</td><td>4</td><td>5 <span class='um-faicon-plus-circle'></span></td></tr>\n";

            foreach($groupe as $num => $q)
            {
                $valeur = (isset($this->reponses[$num])) ? $this->reponses[$num] : "";
                $html .= "<tr><td class='quiz-question'>$q</td>";
                $i = 0;
                while ($i < 5)
                {
                    $html .= "<td class='coche'><input type='radio' name='quiz_{$this->sujet}[$titre][$num]' value='$i' ".checked($i, $valeur, false)." /></td>";
                    $i++;
                }
                $html .= "</tr>\n";
            }
        }
        $html .= "</table>";
        
        return $html;
    }
    
    public function get_html_questions()
    {
        if (count($this->questions) == 0 && $this->quiz_id > 0)
            $this->init_questions($this->quiz_id);
            
        $html = "";
        
        foreach($this->questions as $titre => $groupe)
        {
            if ($titre != "none")
                $html .= "<h3>$titre</h3>";

            foreach($groupe as $q)
                $html .= "<p>$q</p>";
        }
        
        return $html;
    }
    
    public function get_edit_questions()
    {
        if (count($this->questions) == 0 && $this->quiz_id > 0)
            $this->init_questions($this->quiz_id);
            
        $html = "";
        
        $html .= "<h2>{$this->sujet_titre}</h2>";
        $html .= "<div class='quiz {$this->namecode}' data-id='{$this->quiz_id}' data-sujet='{$this->sujet}'>";
        
        $html .= "<div>";
        $html .= "<span class='bouton quiz-enregistrer'>".__("Enregistrer le quiz")."</span> ";
        $html .= "<span class='bouton quiz-ajouter' data-type='titre'>".__("Ajouter titre")."</span> ";
        $html .= "<span class='bouton quiz-ajouter' data-type='competence'>".__("Ajouter compétence")."</span> ";
        $html .= "</div>";
        
        $html .= "<ul class='quiz-sortable'>";
        
        foreach($this->questions as $titre => $groupe)
        {
            if ($titre != "none")
                $html .= $this->get_edit_line($titre, 'titre');

            foreach($groupe as $q)
                $html .= $this->get_edit_line($q, 'competence');
        }
        
        $html .= "</ul>";
        $html .= "</div>";
        
        return $html;
    }
    
    public function get_edit_line($text, $class)
    {
        $new_line = ($text == "") ? "data-new='1'" : "";
        ob_start();
        ?>
        <li class="<?php echo $class; ?>" <?php echo $new_line; ?> data-type="<?php echo $class; ?>">
        <span class='ui-icon ui-icon-arrowthick-2-n-s'></span>
        <input type="text" value="<?php echo $text; ?>" />
        <div style='float: right;'>
            <div class="quiz-line-type"></div>
            <span class='ui-icon ui-icon-closethick quiz-supprimer'></span>
        </div>
        </li>
        <?php
        
        return ob_get_clean();
    }
}
