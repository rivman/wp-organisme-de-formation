<?php
/*
 * class-session-formation.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
require_once(wpof_path . "/class/class-formation.php");
require_once(wpof_path . "/class/class-client.php");
require_once(wpof_path . "/class/class-lieu.php");
require_once(wpof_path . "/class/class-document.php");
require_once(wpof_path . "/class/class-upload.php");
require_once(wpof_path . "/class/class-session-stagiaire.php");
require_once(wpof_path . "/class/class-quiz.php");
require_once(wpof_path . "/class/class-creneau.php");


$type_emargement_text = array
(
    'jour' => __("par jour"),
    'tous' => __("tous les stagiaires inscrits à la session"),
    'vide' => __("feuilles vierges"),
    'stagiaire' => __("par stagiaire"),
);


class SessionFormation
{
    public $numero = ""; // nomenclature interne
    public $titre_session = "";
    public $titre_formation = "";
    public $session_unique_titre = "";
    public $permalien = "";
    public $slug = "";
    public $bandeau_id = "-1"; // image mise en avant
    public $acces_session;
    
    // tableau d'ID des formateurs
    public $formateur = array();
    
    // Infos venant de la formation catalogue
    public $duree = 0;
    public $tarif_inter = 0;
    public $tarif_intra = 0;
    
    // infos effectives pour cette session
    public $tarif_heure = 0;
    public $nb_heure; // au format horaire : 3:30
    public $nb_heure_decimal = 0; // nombre d'heures au format décimal : 3,5 pour 3:30
    public $tarif_base_total = false; // détermine si le tarif de base défini pour le total (true) ou à l'heure (false)
    public $tarif_total_chiffre = 0;
    public $tarif_total_lettre = 0;
    public $tarif_total_autres_chiffre = 0;
    public $tarif_total_autres_lettre = 0;
    public $autres_frais = "";
    public $budget_global = 0; // montant de la facture ou total des factures en inter
    public $exe_comptable = array(); // les clés sont la ou les années, les valeurs sont le ou les budgets de l'année concernée
    
    public $quizpr = "";
    public $quizobj = "";
    public $quizpr_id = "";
    public $quizobj_id = "";
    
    public $dates_array = array();
    public $dates_texte = "";
    public $first_date = "";
    public $first_date_timestamp = 0;
    
    public $type_emargement = array();
    
    // type de session
    public $type_index = "";
    public $type_texte = "";
    
    // Objet lieu
    public $lieu = -1;
    public $lieu_nom = "";
    public $lieu_adresse = "";
    public $lieu_code_postal = "";
    public $lieu_ville = "";
    public $lieu_localisation = "";
    public $lieu_secu_erp = "";
    
    // Clients
    public $clients = array();
    
    // stagiaires
    public $stagiaires_min = 0;
    public $stagiaires_max = 0;
    public $inscrits = array();
    public $stagiaires = array(); // tableau de SessionStagiaire
    public $horaire_am_debut = 0;
    public $horaire_am_fin = 0;
    public $horaire_pm_debut = 0;
    public $horaire_pm_fin = 0;
    
    // documents
    public $doc_suffix;
    public $doc_necessaire = array(); // identifiant des docs globaux nécessaires pour cette session
    public $uploads = array(); // tableau des documents scannés ou extérieurs, attachés à cette session
    
    // structure bénéficiaire (si formation intra)
    public $entreprise_nom = "";
    public $entreprise_adresse = "";
    public $entreprise_cp_ville = "";
    public $entreprise_telephone = "";
    public $entreprise_contact_id = -1;
    public $nature_formation = "";
    public $nature_formation_complement = "";
    public $financement = "";
    public $financement_complement = "";
    
    // sous-traitance
    public $commanditaire_nom = "";
    public $commanditaire_contact = "";
    public $commanditaire_contact_email = "";
    public $commanditaire_num_of = "";
    public $st_nb_stagiaire = 0;
    
    public $id;
    
    // ID de la formation en doublon pour assurer la compatibilité
    public $formation_id = -1;
    public $formation = -1;
    
    public $creneaux = array();
    
    
    public function __construct($session_id = -1)
    {
        global $Formation;
        global $wpof;
        
        $this->tarif_inter = $wpof->tarif_inter;
        
        if ($session_id > 0)
        {
            $this->id = $session_id;
            $wp_post = get_post($session_id);
            $meta = get_post_meta($session_id);
            
            // caractéristiques de la formation
            $this->numero = (isset($meta['numero'][0])) ? $meta['numero'][0] : "";
            $this->specialite = (isset($meta['specialite'][0])) ? $meta['specialite'][0] : "";
            
            foreach($wpof->desc_formation->term as $k => $t)
                $this->$k = (isset($meta[$k][0])) ? $meta[$k][0] : "";
            
            // infos issues des meta données du post
            if (isset($meta['formation'][0]))
            {
                $this->formation_id = $this->formation = $meta['formation'][0];
                
                if (!isset($Formation[$this->formation]))
                    $Formation[$this->formation] = new Formation($meta['formation'][0]);

                $formation = $Formation[$this->formation];
                
                foreach($wpof->desc_formation->term as $k => $t)
                    if ($this->$k == "")
                        $this->$k = $formation->$k;
                        
                if ($this->titre_formation == "")
                    $this->titre_formation = $formation->titre;

                $this->tarif_inter = $formation->tarif;
                
                if (isset($meta['session_unique_titre'][0]) && $meta['session_unique_titre'][0] != "")
                    $this->session_unique_titre = $this->titre_formation = $meta['session_unique_titre'][0];
                else
                    $this->titre_formation = $formation->titre;
            }
            // récupération des bons identifiants de quiz et du parent
            $this->quizpr_id = (isset($meta['quizpr_id'][0])) ? $meta['quizpr_id'][0] : 0;
            $this->quizpr_parent_id = (isset($meta['quizpr_parent_id'][0])) ? $meta['quizpr_parent_id'][0] : $this->id;
            $this->quizpr = new Quiz($this->quizpr_id);
            
            $this->quizobj_id = (isset($meta['quizobj_id'][0])) ? $meta['quizobj_id'][0] : 0;
            $this->quizobj_parent_id = (isset($meta['quizobj_parent_id'][0])) ? $meta['quizobj_parent_id'][0] : $this->id;
            $this->quizobj = new Quiz($this->quizobj_id);

            $this->formateur = get_post_meta($session_id, "formateur", true); // permet de récupérer l'info sous forme de tableau
            
            // type de session de formation
            if (isset($meta['type_formation'][0]))
            {
                $this->type_index = $meta['type_formation'][0];
                $this->type_texte = $wpof->type_session[$this->type_index]['value'];
            }
            
            // Visibilité de la session
            $this->visibilite_session = (isset($meta['visibilite_session'][0])) ? $meta['visibilite_session'][0] : 'public';
            $this->acces_session = (isset($meta['acces_session'][0])) ? ($meta['acces_session'][0]) : $this->visibilite_session;

            // si session intra
            if ($this->type_index == "intra")
            {
                $this->entreprise_nom = (isset($meta['entreprise_nom'][0])) ? $meta['entreprise_nom'][0] : "";
                $this->entreprise_adresse = (isset($meta['entreprise_adresse'][0])) ? $meta['entreprise_adresse'][0] : "";
                $this->entreprise_cp_ville = (isset($meta['entreprise_cp_ville'][0])) ? $meta['entreprise_cp_ville'][0] : "";
                $this->entreprise_telephone = (isset($meta['entreprise_telephone'][0])) ? $meta['entreprise_telephone'][0] : "";
                $this->entreprise_contact_id = (isset($meta['contact'][0])) ? $meta['contact'][0] : "";
                $this->financement = (isset($meta['financement'][0])) ? $meta['financement'][0] : "";
                $this->financement_complement = (isset($meta['financement_complement'][0])) ? $meta['financement_complement'][0] : "";
                $this->nature_formation = (isset($meta['nature_formation'][0])) ? $meta['nature_formation'][0] : "";
                
                // TODO : à virer une ligne ci-dessous lorsque plus besoin
                $this->nb_heure = (isset($meta['nb_heure'][0])) ? $meta['nb_heure'][0] : 0;
                
                //$this->tarif_total_lettre = (isset($meta['tarif_total_lettre'][0])) ? $meta['tarif_total_lettre'][0] : "";
                $this->autres_frais = (isset($meta['autres_frais'][0])) ? $meta['autres_frais'][0] : "";
                $this->tarif_total_autres_chiffre = (isset($meta['tarif_total_autres_chiffre'][0])) ? $meta['tarif_total_autres_chiffre'][0] : 0;
                // $this->tarif_total_autres_lettre = (isset($meta['tarif_total_autres_lettre'][0])) ? $meta['tarif_total_autres_lettre'][0] : "";
            }
            
            if ($this->type_index == "sous_traitance")
            {
                foreach (array('commanditaire_nom', 'commanditaire_contact', 'commanditaire_contact_email', 'commanditaire_num_of') as $c)
                    $this->$c = (isset($meta[$c][0])) ? $meta[$c][0] : "";
            }
            
            // lieu
            $lieu = null;
            if (isset($meta['lieu'][0]))
            {
                $this->lieu = $meta['lieu'][0];
                $lieu = get_lieu_by_id($this->lieu);
            }
                
            foreach(array("nom", "adresse", "code_postal", "ville", "localisation", "secu_erp") as $data)
            {
                $lieu_data = 'lieu_'.$data;
                if ($lieu && isset($lieu->$data))
                    $this->$lieu_data = $lieu->$data;
                elseif (isset($meta[$lieu_data][0]))
                    $this->$lieu_data = $meta[$lieu_data][0];
            }
            $this->ville = $this->lieu_ville;

            // stagiaires
            $this->clients = get_post_meta($session_id, 'clients', true); // tableau
            if (!is_array($this->clients))
                $this->clients = array();
            $this->stagiaires_min = (isset($meta['stagiaires_min'][0])) ? $meta['stagiaires_min'][0] : "";
            $this->stagiaires_max = (isset($meta['stagiaires_max'][0])) ? $meta['stagiaires_max'][0] : "";
            $this->inscrits = get_post_meta($session_id, 'inscrits', true); // tableau
            if (!is_array($this->inscrits))
                $this->inscrits = array();
            
            // types de feuilles d'émargement
            $this->type_emargement = get_post_meta($session_id, 'type_emargement', true);
            if ($this->type_emargement == "")
            {
                global $type_emargement_text;
                $this->type_emargement = array_fill_keys(array_keys($type_emargement_text), 0);
                $this->type_emargement['jour'] = 1;
            }
            
            // documents nécessaires
            foreach($wpof->documents->term as $doc_index => $doc)
            {
                if ($doc->contexte & $wpof->doc_context->session)
                    $this->doc_necessaire[] = $doc_index;
            }
            $this->doc_suffix = $this->id;
            
            /*
            if ($this->type_index != "")
            {
                global $doc_nom;
                foreach (array_keys($doc_nom) as $doc_index)
                {
                    if ($doc_index == "pv_secu" && ($lieu) && $lieu->secu_erp == "")
                        continue;

                    if ($wpof->{$doc_index."_global_".$this->type_index} == 1)
                        $this->doc_necessaire[] = $doc_index;
                }
            }
            */
            
            // scans et cie
            $this->uploads = unserialize(get_post_meta($this->id, "uploads", true));
            if (!is_array($this->uploads))
                $this->uploads = array();
            
            $this->init_dates_creneaux();
            
            if (!empty($meta['nb_heure_estime_decimal'][0]))
                $this->nb_heure_estime_decimal = $meta['nb_heure_estime_decimal'][0];
            
            $this->calcule_temps_session();
            
            if (isset($meta['tarif_total_chiffre'][0]))
            {
                $this->tarif_total_chiffre = $meta['tarif_total_chiffre'][0];
                $this->calcule_tarif();
            }
            
            // infos issues du post
            $this->titre_session = $wp_post->post_title;
            
            // Vérification de la conformité du post_title
            //$this->check_post_title($wp_post);
            
            $this->permalien = get_the_permalink($session_id);
            $this->slug = $wp_post->post_name;
        }
    }
    
    /*
     * Qui peut modifier cette session ?
     * Renvoie true si
     * $user_id a le rôle responsable ou admin
     * $user_id fait partie des formateurs animant cette session ($this->formateur)
     * $this->formateur est vide (session non définie)
     */
    public function can_edit($user_id = -1)
    {
        global $wpof;
        
        if ($user_id == -1)
            $user_id = get_current_user_id();
        
        $role = wpof_get_role($user_id);
        $super_roles = array("admin", "um_responsable");
        
        if ($wpof->formateur_gest == 1)
            $super_roles[] = "um_formateur-trice";
        
        if ($role == "um_stagiaire" || $role == null)
            return false;
        
        if (!isset($this->formateur)
            || !is_array($this->formateur)
            || count($this->formateur) == 0
            || in_array($user_id, $this->formateur)
            || in_array($role, $super_roles))
            return true;
        
        return false;
    }
    
    /*
     * Création des sessions stagiaires
     * En effet, on n'a pas toujours besoin des sessions stagiaires lorsqu'on instancie une session formation
     * Si $user_id est définit, alors on n'instancie que la session de $user_id
     * Sinon, on instancie tous les stagiaires de la session
     */
    public function init_stagiaires($user_id = -1)
    {
        global $SessionStagiaire;

        if ($user_id > 0)
        {
            if (!isset($this->stagiaires[$user_id]))
                $this->stagiaires[$user_id] = new SessionStagiaire($this->id, $user_id);
            $SessionStagiaire[$user_id] = $this->stagiaires[$user_id];
        }
        else
            foreach ($this->inscrits as $i)
            {
            
                if (!isset($this->stagiaires[$i]))
                    $this->stagiaires[$i] = new SessionStagiaire($this->id, $i);
            
                $SessionStagiaire[$i] = new SessionStagiaire($this->id, $i);//$this->stagiaires[$i];
            }
    }
    
    public function init_clients($client_id = -1)
    {
        global $Client;
        
        if ($client_id > 0)
            $client_list = array($client_id);
        else
            $client_list = $this->clients;
        
        foreach($client_list as $cid)
            $Client[$cid] = new Client($this->id, $cid);
            
        $this->calcule_budget_global();
    }
    
    /*
     * Initialisation des dates et des créneaux
     */
    public function init_dates_creneaux()
    {
        $tmp_creneaux = get_post_meta($this->id, "creneaux", true);
        
        if (!is_array($tmp_creneaux) || empty($tmp_creneaux))
            $this->dates_array = $this->creneaux = array();
        else
        {
            foreach($tmp_creneaux as $date => $jour_creneaux)
            {
                if (is_array($jour_creneaux) && count($jour_creneaux) > 0)
                    foreach ($jour_creneaux as $c)
                    {
                        $creno = new Creneau($c);
                        if ($creno->lieu_id == -1)
                            $creno->lieu_nom = $this->lieu_nom;
                        $this->creneaux[$date][$c] = $creno;
                    }
                else
                    $this->creneaux[$date] = array();
            }
            $this->dates_array = array_keys($this->creneaux);
            $this->dates_texte = pretty_print_dates($this->dates_array);
            $this->first_date = $this->dates_array[0];
            $this->first_date_timestamp = date_create_from_format("d/m/Y", $this->first_date)->getTimestamp();
        }
    }
    
    public function check_post_title($wp_post = null)
    {
        if (!$wp_post)
            $wp_post = get_post($this->id);
        
        $concat_id = true;
        $actuel_titre_session = $this->titre_formation;
        if (!empty($this->first_date))
        {
            $actuel_titre_session .= " – ".pretty_print_dates($this->first_date);
            $concat_id = false;
        }
        if (!empty($this->lieu_ville))
        {
            $actuel_titre_session .= " – ".$this->ville;
        }
        $actuel_titre_session = strip_tags($actuel_titre_session);
        
        $slug = sanitize_title($actuel_titre_session);
        if ($concat_id)
            $slug .= "-".$this->id;
        
        if ($actuel_titre_session != $this->titre_session)
        {
            $new_data = array
            (
                'ID' => $this->id,
                'post_title' => $actuel_titre_session,
                'post_name' => $slug,
            );
            remove_action( 'post_updated', 'wp_save_post_revision' );
            $res = wp_update_post(wp_slash($new_data), true);
            add_action( 'post_updated', 'wp_save_post_revision' );
            if ($res != $this->id)
                var_dump($res);
            $this->titre_session = $new_data['post_title'];
            
            return true;
        }
        return false;
    }

    
    /*
     * Surcharge de update_post_meta
     * Peut être appelée dans un contexte où l'on veut mettre à jour une donnée sans se soucier de savoir
     * si l'on est sur une SessionFormation ou une SessionStagiaire
     */
    public function update_meta($meta_key, $meta_value = null)
    {
        if ($meta_value == null)
            $meta_value = $this->$meta_key;
        else
            $this->$meta_key = $meta_value;
            
        if ($meta_key == "creneaux")
            return $this->update_creneaux();
            
        return update_post_meta($this->id, $meta_key, $meta_value);
    }
    
    // Cas particulier des créneaux où le stocke que les index
    private function update_creneaux()
    {
        // initialisation de tmp_creneaux avec les dates triées en clés
        $tmp_creneaux = array_fill_keys(sort_dates(array_keys($this->creneaux)), array());
        
        foreach($this->creneaux as $date => $jour_creneaux)
        {
            if (is_array($jour_creneaux) && count($jour_creneaux) > 0)
            {
                foreach($jour_creneaux as $c)
                    $tmp_creneaux[$date][] = $c->id;
            }
            else
                $tmp_creneaux[$date] = array();
        }
        
        return update_post_meta($this->id, "creneaux", $tmp_creneaux);
    }
    
    public function delete()
    {
        $log = array();
        // suppression des clients et stagiaires
        foreach($this->clients as $cid)
        {
            $client = get_client_by_id($this->id, $cid);
            $client->delete();
            
            $log[] = "Suppression de ".$client->nom;
        }
        
        global $wpdb;
        
        // suppression des créneaux
        global $suffix_creneaux;
        $query = $wpdb->prepare ("DELETE FROM ".$wpdb->prefix.$suffix_creneaux." WHERE session_id = '%d';", $this->id );
        $res = $wpdb->query($query);
        $log[] = $query." → ".$res;
        
        // suppression des quiz
        global $suffix_quiz;
        $query = $wpdb->prepare ("DELETE FROM ".$wpdb->prefix.$suffix_quiz." WHERE parent_id = '%d';", $this->id );
        $res = $wpdb->query($query);
        $log[] = $query." → ".$res;
        
        // suppression des documents
        global $suffix_documents;
        $query = $wpdb->prepare ("DELETE FROM ".$wpdb->prefix.$suffix_documents." WHERE session_id = '%d';", $this->id );
        $res = $wpdb->query($query);
        $log[] = $query." → ".$res;
        
        // suppression des metas
        $query = $wpdb->prepare ("DELETE FROM ".$wpdb->prefix."postmeta WHERE post_id = '%d';", $this->id );
        $res = $wpdb->query($query);
        $log[] = $query." → ".$res;
        
        // suppression du post (sans passer par la case poubelle)
        wp_delete_post($this->id, true);
        
        return $log;
    }

    public function get_delete_bouton($texte_bouton = "Supprimer cette session")
    {
        ob_start();
        ?>
        <p class="delete-entity icone-bouton" data-objectclass="<?php echo get_class($this); ?>" data-id="<?php echo $this->id; ?>">
        <span class="dashicons dashicons-dismiss" > </span>
        <?php echo $texte_bouton; ?>
        </p>
        <?php
        return ob_get_clean();
    }

    
    /*
    * Création d'un input avec événement change qui met à jour la valeur dans la base
    */
    public function get_input_jpost($type, $name, $label = "")
    {
        $html = "<div class='input_jpost'>";
        $input_id = $name.rand();
        
        if ($label != "")
            $html .= "<label class='top input_jpost_label' for='$input_id'>$label</label>";
        $html .= "<span class='input_jpost_span'>{$this->$name}</span>";
        $step = ($type == "number") ? "step='0.01'" : "";
        $html .= "<input class='input_jpost_value' type='$type' $step id='$input_id' name='$name' value='{$this->$name}' />";
        $html .= "<input type='hidden' name='session_id' value='{$this->id}' />";
        $html .= "<input type='hidden' name='stagiaire_id' value='-1' />";
        $html .= "</div>";
        
        return $html;
    }

    /*
    * Création d'un input avec événement change qui met à jour la valeur dans la base
    */
    public function get_select_jpost($list, $name, $selected = "", $options = "", $first = null, $label = "")
    {
        $options .= " class='select_jpost_value'";
        $html = "<div class='select_jpost'>";
        $select_id = $name.rand();
        if (!isset($wpof->$name))
            init_term_list($name);
        
        if ($label != "")
            $html .= "<label class='top select_jpost_label' for='$select_id'>$label</label>";
        $html .= "<span class='select_jpost_span'>".$list->get_term($this->$name)."</span>";
        $first = (!is_array($selected) && in_array($selected, $list->term)) ? $selected." — ancien terme, à changer !!" : $first;
        $html .= $list->get_select_list($selected, $options, $first);
        $html .= "<input type='hidden' name='session_id' value='{$this->id}' />";
        $html .= "<input type='hidden' name='stagiaire_id' value='-1' />";
        $html .= "</div>";
        
        return $html;
    }

    
    // Retourne un tableau des stagiaires inscrits
    // { "id" => "Display name"}
    public function get_liste_stagiaires()
    {
        $stagiaires = array();
        foreach($this->inscrits as $stagiaire_id)
            $stagiaires[$stagiaire_id] = get_displayname($stagiaire_id, false);
        
        if ($this->entreprise_contact_id > 0)
            $stagiaires[$this->entreprise_contact_id] = get_displayname($this->entreprise_contact_id, false)." ".__("(contact client)");
        
        return $stagiaires;
    }
    
    // Crée les inputs pour définir la répartition par exercire comptable
    public function get_input_exe_comptable($exe_comptable = null)
    {
        if (!$exe_comptable) $exe_comptable = $this->exe_comptable;
        
        $html = "<div class='exe_comptable'>";
        $rand = rand();
        
        foreach($exe_comptable as $annee => $tarif)
        {
            $html .= "<label for='".$annee.$rand."'>$annee</label>";
            $html .= "<input id='".$annee.$rand."' name='$annee' type='number' step='0.01' value='$tarif' />";
        }
        $html .= "</div>";
        
        return $html;
    }
    
    
    // Renseigne le tableau des exercices comptables
    // En général ce tableau n'aura qu'une valeur (si toute la session se déroule dans la même année civile)
    public function set_exercice_comptable()
    {
        if ($this->type_index != 'inter' && !empty($this->dates_array))
        {
            $this->exe_comptable = get_post_meta($this->id, "exe_comptable", true);
            
            if (!is_array($this->exe_comptable) || count($this->exe_comptable) == 0 || in_array("", array_keys($this->exe_comptable)))
            {
                $this->exe_comptable = array();
                foreach($this->dates_array as $d)
                {
                    $date = explode('/', $d);
                    $this->exe_comptable[$date[2]] = 0;
                }
                $last_year = array_keys($this->exe_comptable);
                $last_year = $last_year[count($last_year) - 1];
                $this->exe_comptable[$last_year] = $this->tarif_total_chiffre;
                $this->update_meta("exe_comptable");
            }
            $diff = $this->tarif_total_chiffre - array_sum($this->exe_comptable);
            if ($diff != 0)
            {
                $last_year = array_keys($this->exe_comptable);
                $last_year = $last_year[count($last_year) - 1];
                $this->exe_comptable[$last_year] += $diff;
                
                $this->update_meta("exe_comptable");
            }
        }
    }
    
    // Calcule le temps (en heures) de suis de la session par ce stagiaire (somme de tous les créneaux actifs)
    public function calcule_temps_session()
    {
        $this->temps = DateTime::createFromFormat("U", "0");
            
        foreach($this->creneaux as $tab_date)
        {
            if (!empty($tab_date))
            {
                foreach($tab_date as $creno)
                {
                    $this->temps->add($creno->duree);
                }
                $h = floor($this->temps->format("U") / 3600);
                $m = ($this->temps->format("U") % 3600) / 60;
                $this->nb_heure_decimal = $this->temps->format("U") / 3600;
                $this->nb_heure = sprintf("%02d:%02d", $h, $m);
            }
        }
        
        $this->dates_array = array_unique($this->dates_array);
        $this->dates_texte = pretty_print_dates($this->dates_array);
        
        if (!isset($this->nb_heure_estime_decimal))
        {
            $this->nb_heure_estime_decimal = $this->nb_heure_decimal;
            $this->nb_heure_estime = $this->nb_heure;
        }
        else
        {
            $h = (integer) $this->nb_heure_estime_decimal;
            $m = ($this->nb_heure_estime_decimal - $h) * 60;
            $this->nb_heure_estime = sprintf("%02d:%02d", $h, $m);
        }
    }    

    // Calcule le tarif total affiché
    public function calcule_tarif()
    {
        if ($this->nb_heure_decimal > 0)
            $this->tarif_heure = sprintf("%.2f", round($this->tarif_total_chiffre / $this->nb_heure_decimal, 2));
        $this->tarif_total_lettre = num_to_letter($this->tarif_total_chiffre);
        $this->tarif_total_autres_lettre = num_to_letter($this->tarif_total_autres_chiffre);
    }
            
    public function calcule_budget_global()
    {
        global $Client;
        $this->budget_global = 0;
        foreach ($this->clients as $cid)
            $this->budget_global += get_client_meta($cid, "tarif_total_chiffre");
    }
    
    // Supprime un élément d'un tableau de sous-entité (clients, inscrits)
    function supprime_sous_entite($tab_name, $id)
    {
        if (isset($this->$tab_name) && is_array($this->$tab_name))
        {
            $key = array_search($id, $this->$tab_name);
            unset($this->$tab_name[$key]);
            $this->update_meta($tab_name, $this->$tab_name);
        }
    }
    
    /*
     * Création des documents
     * En effet, on n'a pas toujours besoin des documents lorsqu'on instancie une session formation
     */
    public function init_docs()
    {
        global $Documents;
        global $wpof;
        
        foreach ($this->doc_necessaire as $d)
        {
            $doc = new Document($d, $this->id, $wpof->doc_context->session);
            $Documents[$doc->id] = $doc;
        }
    }
    
    /*
     * Renvoie la présentation de la session
     * $lieu : affiche ou non les détails du lieu
     * $prog : affiche ou non le programme
     */
    public function get_html_presentation($lieu = true, $prog = true)
    {
        $html = "<div class='presentation'>";
            
        $html .= $this->presentation;
            
        $html .= "<div id='formation_formateur'>";
        $html .= "<p>".__("Équipe pédagogique")."</p>";
        $html .= the_liste_formateur(array('only' => $this->formateur), false);
        $html .= "</div>";
        
        $html .= "<p>".__("Dates")." : ".$this->dates_texte."</p>";
        $html .= "<p>".__("Tarif")." : ".get_tarif_formation($this->tarif_total_chiffre)." ";
        $html .= __("(par stagiaire)");
        
        // Lieu de la session
        if ($lieu)
        {
            if ($this->lieu > 0)
            {
                $html .= "<p class='openButton' data-id='blocLieu".$this->id."'>Lieu : ".$this->lieu_nom." - ".$this->lieu_ville."</p>";
                $html .= "<div id='blocLieu".$this->id."' class='blocHidden'>";
                $html .= "<p>".$this->lieu_adresse."</p>";
                $html .= wpautop($this->lieu_localisation);
                $html .= "</div>";
            }
            else
                $html .= "<p>Lieu : ".$this->ville."</p>";
        }
        
        // Programme de la session de formation
        if ($prog)
        {
            $html .= "<p class='openButton' data-id='prog-session".$this->id."'>".__("Programme de la session")."</p>";
            $html .= "<div id='prog-session".$this->id."' class='blocHidden'>".$this->programme."</div>";
        }
        
        $html .= "</div> <!-- presentation -->";
        
        return $html;
    }
    
    public function get_the_stagiaire_board_old($user_id)
    {
        global $SessionStagiaire;
        ob_start();
        ?>
        <div class='board-nom-stagiaire' id='tab-s<?php echo $user_id; ?>'>
        <?php echo get_displayname($user_id, false); ?>
        <span class='last-modif'>(<?php echo __("Dernière connexion")." ".get_last_login($user_id).")"; ?></span>
        <?php
        $SessionStagiaire[$user_id]->the_board_old();
        ?>
        </div>
        <?php
        return ob_get_clean();
    }
    
    public function the_stagiaire_board_old($user_id)
    {
        echo $this->get_the_stagiaire_board_old($user_id);
    }

    public function get_the_stagiaire_board($user_id)
    {
        global $SessionStagiaire;
        ob_start();
        ?>
        <div class='board-nom-stagiaire' id='tab-s<?php echo $user_id; ?>'>
        <?php echo get_displayname($user_id, false); ?>
        <span class='last-modif'>(<?php echo __("Dernière connexion")." ".get_last_login($user_id).")"; ?></span>
        <?php
        $SessionStagiaire[$user_id]->the_board();
        ?>
        </div>
        <?php
        return ob_get_clean();
    }
    
    public function the_stagiaire_board($user_id)
    {
        echo $this->get_the_stagiaire_board($user_id);
    }

    /*
    * Tableau de bord du formateur pour une session donnée
    * $session_id : ID de la session
    * $user_id : ID du stagiaire
    * $formation_id : ID de la formation
    * $inscrits : tableau contenant les ID des stagiaires inscrits
    */
    public function the_board()
    {
        global $doc_nom;
        global $Formation;
        global $SessionStagiaire;
        global $Documents;
        global $wpof;
        
        $user_id = get_current_user_id();
        $role = wpof_get_role($user_id);
        
        $this->init_docs();
        
        ?>
        <div id="session-formation" class="id session" data-id="<?php echo $this->id; ?>">
        <?php echo hidden_input("default_main_tab", (isset($_SESSION['main-tabs'])) ? $_SESSION['main-tabs'] : 0); ?>
        </div>
        
        <div id="main-tabs">
        <ul>
            <li><a href="#tab-client"><?php _e("Clients/stagiaires"); ?></a></li>
            <li><a href="#tab-session"><?php _e("Session"); ?></a></li>
            <li><a href="#tab-formation"><?php _e("Formation"); ?></a></li>
            <li><a href="#tab-dates"><?php _e("Dates"); ?></a></li>
            <li><a href="#tab-lieu"><?php _e("Lieu"); ?></a></li>
            <li><a href="#tab-eval"><?php _e("Évaluations"); ?></a></li>
        </ul>
        
        <div id="tab-session">
        <div class="infos-session flexrow edit-data" data-id="<?php echo $this->id; ?>">
        <fieldset>
            <legend><?php _e("Formation"); ?></legend>
        <?php
            echo get_input_jpost($this, "acces_session", array('select' => '', 'label' => __("Accès à la session (visibilité)")));
            if (!isset($wpof->formation))
                init_term_list("formation");
            echo get_input_jpost($this, "formation", array('select' => '', 'label' => __("Formation du catalogue"), 'first' => __("Session unique, sans lien avec le catalogue"), 'postprocess' => 'tabs_reload'));
            echo get_input_jpost($this, "session_unique_titre", array('input' => 'text', 'label' => __("Intitulé de session unique"), 'size' => '80'));
            echo get_input_jpost($this, "specialite", array('select' => '', 'label' => __("Spécialité"), 'first' => __("Choisissez une spécialité, la plus précise possible")));
        ?>
        </fieldset>
        
        <fieldset>
            <legend><?php _e("Équipe pédagogique"); ?></legend>
        <?php
            if (!isset($wpof->formateur))
                init_term_list("formateur");
            echo get_input_jpost($this, "formateur", array('select' => 'multiple', 'rows' => 10));
        ?>
        </fieldset>
        
        <fieldset>
            <legend><?php _e("Stagiaires"); ?></legend>
            <?php echo get_input_jpost($this, "stagiaires_min", array('input' => 'number', 'step' => 1, 'min' => 1, 'label' => __('Nombre minimum souhaité'))); ?>
            <?php echo get_input_jpost($this, "stagiaires_max", array('input' => 'number', 'step' => 1, 'min' => 1, 'label' => __('Nombre maximum souhaité'))); ?>
            <?php echo get_input_jpost($this, "tarif_total_chiffre", array('input' => 'number', 'step' => 0.01, 'min' => 0, 'label' => __('Tarif/stagiaire affiché'), 'after' => $wpof->monnaie_symbole, 'postprocess' => 'update_pour_infos_session')); ?>
        </fieldset>
        
        <div class="pour-infos">
            <?php echo $this->get_pour_infos_box(); ?>
        </div>
        
        </div>
        <fieldset>
            <legend><?php _e("Documents administratifs pour la session"); ?></legend>
            <?php echo get_gestion_docs($this); ?>
        </fieldset>
            <?php echo $this->get_televersement_fieldset(); ?>
        </div> <!-- tab-session -->
        
        <div id="tab-dates">
            <h3><?php _e("Dates et créneaux"); ?></h3>
            <p>
            <?php
                _e("Types de créneaux : ");
                foreach($wpof->type_creneau as $type => $text)
                    echo "<span class='faux-bouton creneau $type'>$text</span>";
            ?>
            </p>
            <?php echo $this->get_html_creneaux(true); ?>
            
        </div> <!-- tab-dates -->
        
        <div id="tab-lieu">
            <h3><?php _e("Lieu"); ?></h3>
        <div class="lieu-session editable">
            <div class="select edit-data">
            <?php
            init_term_list("lieu");
            echo get_input_jpost($this, "lieu", array('select' => '', 'label' => __("Lieu prédéfini"), 'first' => __("Lieu occasionnel"), 'postprocess' => 'toggle_lieu_details+tabs_reload+update_session_titre'));
            ?>
            </div>
            <?php echo $this->get_lieu_details(); ?>
        </div>
        </div> <!-- tag-lieu -->
        
        <div id="tab-formation" class="desc-session edit-data">
                <h3><?php _e("Description de la session"); ?></h3>
                <?php
                    foreach($wpof->desc_formation->term as $k => $t)
                        echo get_input_jpost($this, $k, array('editor' => '', 'label' => $t->text));
                ?>
        </div> <!-- tab-formation -->
        
        <div id="tab-client">
            <p class="icone-bouton dynamic-dialog" data-function="new_client" data-sessionid="<?php echo $this->id; ?>"><span class="dashicons dashicons-plus-alt" > </span> <?php _e("Ajouter client"); ?></p>
            
            <div id="tabs-clients">
            <?php echo $this->get_tabs_clients(); ?>
            </div>
        </div> <!-- tab-client -->
        
        <div id="tab-eval">
            <?php echo $this->get_evaluations_tab(); ?>
        </div> <!-- tab-eval -->
        
        </div> <!-- session-formation -->
        
        <?php
    }
    
    public function get_pour_infos_box()
    {
        global $wpof;
        $this->calcule_temps_session();
        $this->calcule_tarif();
        $this->calcule_budget_global();
       
        ob_start();
        ?>
            <h3><?php _e("Pour information"); ?></h3>
            <p><?php _e("Dates concernées"); ?> : <span class="dates_concernees"><?php echo $this->dates_texte; ?></span></p> 
            <p><?php _e("Tarif horaire par stagiaire (affiché)"); ?> : <span class="tarif_heure"><?php echo get_tarif_formation($this->tarif_heure); ?></span> </p>
            <p><?php _e("Nombre d'heures"); ?> : <span class="nb_heure"><?php echo $this->nb_heure; ?></span> </p>
            <p><?php _e("Tarif total par stagiaire (affiché)"); ?> : <span class="tarif_total"><?php echo get_tarif_formation($this->tarif_total_chiffre); ?></span> </p>
            <p><?php _e("Budget global"); ?> : <span class="budget_global"><?php echo get_tarif_formation($this->budget_global); ?> </span> </p>
        <?php
        return ob_get_clean();
    }
    
    public function get_lieu_details()
    {
        global $wpof;
        ob_start();
        ?>
            <div class="lieu details <?php if ($this->lieu == -1) echo "edit-data"; ?>">
            <?php
            foreach($wpof->desc_lieu->term as $key => $term)
            {
                switch ($term->type)
                {
                    case "text":
                    case "number":
                        echo get_input_jpost($this, "lieu_$key", array('input' => $key, 'label' => $term->text));
                        break;
                    case "textarea":
                    case "editor":
                    case "select":
                        echo get_input_jpost($this, "lieu_$key", array($term->type => '', 'label' => $term->text));
                        break;
                    default:
                        echo "<p>$key pas encore géré</p>";
                        break;
                }
                
            }
            ?>
            </div>
        <?php
        
        return ob_get_clean();
    }
    
    public function get_evaluations_tab()
    {
        $role = wpof_get_role(get_current_user_id());
        $html = "";
        $quizpr = new Quiz($this->quizpr_id);
        if (empty($quizpr->sujet))
            $quizpr->set_identite("prerequis", $this->id);
        $html .= $quizpr->get_edit_questions();
        $html .= ($role == 'admin') ? "<div>quizpr_id = ".$this->quizpr_id."</div>" : "";
        
        $quizobj = new Quiz($this->quizobj_id);
        if (empty($quizobj->sujet))
            $quizobj->set_identite("objectifs", $this->id);
        $html .= $quizobj->get_edit_questions();
        $html .= ($role == 'admin') ? "<div>quizobj_id = ".$this->quizobj_id."</div>" : "";
        
        return $html;
    }
    
    /*
     * Retourne l'onglet des clients
     */
    public function get_tabs_clients()
    {
        $list_tabs = "";
        $content_tabs = "";
        
        foreach($this->clients as $c)
        {
            $client = new Client($this->id, $c);
            $nom = (empty($client->nom)) ? __("Sans nom") : $client->nom;
            $list_tabs .= "<li class='tab-client client-$c'><a href='#tab-c$c'>".$nom."</a></li>";
            $content_tabs .= $client->get_the_board();
            
            if (!empty($client->stagiaires))
                foreach($client->stagiaires as $user_id)
                {
                    $stagiaire = get_stagiaire_by_id($this->id, $user_id);
                    $list_tabs .= "<li class='tab-stagiaire client-$c stagiaire-$user_id'><a href='#tab-s$user_id'>".get_displayname($user_id, false)."</a></li>";
                    $content_tabs .= $stagiaire->get_the_board();
                }
        }
        return "<ul>$list_tabs</ul>\n".$content_tabs;
    }
    
    
    /*
    * Calcul du budget total d'une session de formation en inter
    */
    protected function budget_total()
    {
        global $SessionStagiaire;
        $budget = 0;
        
        foreach($this->inscrits as $i)
        {
            $tarif = $this->stagiaires[$i]->tarif_total_chiffre;
        //    $tarif = $SessionStagiaire[$i]->tarif_total_chiffre;
            if (is_numeric($tarif))
                $budget += $tarif;
        }
        return $budget;
    }
    
    protected function get_entete_board()
    {
        $role = wpof_get_role(get_current_user_id());
        
        ob_start();
        ?>
        <p class="type_session"><?php echo __("Session")." ".lcfirst($this->type_texte); ?></p>
        <?php if (in_array($role, array("admin", "um_responsable"))) : ?>
        <div class="responsable edit-data">
        <?php echo $this->get_input_jpost("text", 'numero', __("Identifiant interne de la session")); ?>
        <p>
        <?php _e("Équipe pédagogique"); ?> : 
        <?php
            $liste_form = array();
            foreach($this->formateur as $fid)
                $liste_form[] = get_displayname($fid, false);
            echo join(", ", $liste_form);
        ?>
        </p>
        </div>
        <?php endif;
        
        return ob_get_clean();
    }
    
    protected function get_televersement_fieldset()
    {
        ?>
            <fieldset><legend><?php _e("Téléversement"); ?></legend>
            <p><?php _e("Déposez ici les documents signés et scannés ou tout autre document qui vous parait utile (devis, facture par exemple)"); ?></p>
            <p><?php echo __("Poids maximal par fichier : ").ini_get("upload_max_filesize"); ?></p>
            
            <form method="POST" name="upload" enctype="multipart/form-data">
                <input name="scan" type="file" accept="image/*,.pdf" multiple />
                <?php
                    echo hidden_input('id_span_message', 'archive-message');
                    echo hidden_input('action', 'archive_file');
                    echo hidden_input('session_id', $this->id);
                ?>
                <input class="ajax-save-file bouton" type="button" value="<?php _e("Déposer scan") ?>" />
            </form>
            <p id="archive-message" class="message"></p>
            <table class='gestion-docs-admin' id="liste-scan">
            <tr>
            <th><?php _e("Fichier"); ?></th>
            <th><?php _e("Date de dépôt"); ?></th>
            <!-- <th><?php //_e("Diffuser à"); ?></th> -->
            <th><?php _e("Supprimer"); ?></th>
            </tr>
            <?php
            foreach ($this->uploads as $u)
                echo $u->get_html('tr');
            ?>
            </table>
            
            </fieldset>
            
        <?php
    }

    /*
    * Affiche le formulaire d'inscription à une session de formation
    */
    public function get_formulaire_inscription_session($user_id)
    {
        global $wpof;
        global $SessionStagiaire;
        $session_stagiaire = $SessionStagiaire[$user_id];
        $role = wpof_get_role($user_id);
        
        // On stocke tout dans une variable texte pour afficher à la fin (ou éventuellement le retourner)
        $html = "";
        
        $html .= "<h3>".__("Identité")."</h3>";
        $html .= $session_stagiaire->user->display_name;
        $html .= hidden_input("user_id", $user_id);
        
        $html .= "<h3>".__("Statut")."</h3>";
        $first = (!in_array($session_stagiaire->statut_stagiaire, $wpof->statut_stagiaire->term)) ? __("Sélectionnez un statut") : $session_stagiaire->statut." — ancien terme, à changer !!";
        $html .= $wpof->statut_stagiaire->get_select_list($session_stagiaire->statut_stagiaire, "", $first);
        
        $html .= "<h3>".__("Informations administratives")."</h3>";
        
        
        if ($this->type_index == "inter") :   /////// INTER ONLY
        
        if ($role != 'um_stagiaire')
            $html .= "<p data-id='$user_id'>".__("Copier les infos administratives de ").select_user(array('include' => $this->inscrits), "stagiaire-a-copier", "", __("Personne"))."</p>";
        
        if ($session_stagiaire->has_employeur)
        {
            $entreprise_non_visible = "blocHidden";
            $entreprise_oui_visible = "";
        }
        else
        {
            $entreprise_oui_visible = "blocHidden";
            $entreprise_non_visible = "";
        }
        
        else:   /////// NOT INTER
        
            $entreprise_non_visible = "blocHidden";
            $entreprise_oui_visible = "";
        
        endif;   /////// INTER ONLY
        
        $html .= "<table id='entreprise$user_id'>";
        
        if ($this->type_index == "inter") :   /////// INTER ONLY
        
        $html .= "<tr><td>{$wpof->terms_question_employeur}</td><td><input type='checkbox' class='has-employeur' name='has_employeur' id='has-employeur$user_id' data-id='entreprise$user_id' value='1' ".checked($session_stagiaire->has_employeur, 1, false)." /></td></tr>";
        $html .= "<tr class='employeur-oui $entreprise_oui_visible'><td>".__("Nom de la structure")."</td><td><input type='text' name='entreprise' id='entreprise-nom$user_id' value='{$session_stagiaire->entreprise}' /></td></tr>";
        
        endif;   /////// INTER ONLY
        
        $html .= "<tr class='employeur-oui $entreprise_oui_visible'><td>".__("Votre fonction")."</td><td><input type='text' name='entreprise_fonction' id='fonction$user_id' value='{$session_stagiaire->entreprise_fonction}' /></td></tr>";
        
        
        if ($this->type_index == "inter") :   /////// INTER ONLY
        
        $html .= "<tr class='employeur-oui $entreprise_oui_visible'><td>".__("Service interlocuteur")."</td><td><input type='text' name='entreprise_service' id='entreprise-service$user_id' value='{$session_stagiaire->entreprise_service}' /></td></tr>";
        $html .= "<tr class='employeur-oui $entreprise_oui_visible'><td>".__("Nom du/de la responsable")."</td><td><input type='text' name='entreprise_responsable' id='entreprise-responsable$user_id' value='{$session_stagiaire->entreprise_responsable}' /></td></tr>";
        $html .= "<tr><td><span class='employeur-oui $entreprise_oui_visible'>".__("Adresse de votre employeur")."</span><span class='employeur-non $entreprise_non_visible'>".__("Votre adresse")."</span></td>";
        $html .= "<td><textarea name='adresse' id='adresse$user_id' cols='60' rows='4'>{$session_stagiaire->adresse}</textarea></td></tr>";
        
        $html .= "<tr><td>".__("Code postal et ville")."</td><td><input type='text' name='cp_ville' id='cp-ville$user_id' value='{$session_stagiaire->cp_ville}' /></td></tr>";
        $html .= "<tr><td>".__("Téléphone")."</td><td><input type='text' name='telephone' id='telephone$user_id' value='{$session_stagiaire->telephone}' /></td></tr>";
        
        endif;   /////// INTER ONLY
        
        
        $html .= "</table>";
        
        if ($this->type_index == "inter") :   /////// INTER ONLY
        
        $html .= "<h3>".__("Financement")."</h3>";
        $html .= "<p>".__("Principale source de financement pour cette formation")."</p>";
//        $html .= select_by_list($wpof->financement, "financement" , $session_stagiaire->financement, "", __("Sélectionnez une source de financement"));
        $first = (in_array($session_stagiaire->financement, $wpof->financement->term)) ? __("Sélectionnez le mode de financement principal") : $session_stagiaire->financement." — ancien terme, à changer !!";
        $html .= $wpof->financement->get_select_list($session_stagiaire->financement, "", $first);

        $html .= "<h3>".__("Objectif général de la prestation")."</h3>";
//        $html .= select_by_list($wpof->nature_formation, "nature_formation" , $session_stagiaire->nature_formation, "", __("Sélectionnez un objectif"));
        $first = (in_array($session_stagiaire->nature_formation, $wpof->nature_formation->term)) ? __("Sélectionnez objectif") : $session_stagiaire->nature_formation." — ancien terme, à changer !!";
        $html .= $wpof->nature_formation->get_select_list($session_stagiaire->nature_formation, "", $first);
        
        endif;   /////// INTER ONLY
        
        
        $html .= "<h3>".__("Vos besoins")."</h3>";
        $html .= "<p>".__("Quelles sont vos attentes par rapport à cette formation ? Comment allez-vous mettre en pratique ce que vous allez apprendre ?")."</p>";
        $html .= "<textarea cols='60' rows='15' name='attentes'>{$session_stagiaire->attentes}</textarea>";
        
        $html .= hidden_input("date_inscription", time());
        
        return $html;
    }
    
    /*
     * Renvoie la liste de tous les créneaux classés par dates sous forme html
     * $edit : si true on peut modifier les créneaux, si false, ils sont simplement affichés
     * $actif : tableau $session_stagiaire->creneaux contenant la liste des ID des créneaux auxquels un stagiaire est inscrit
     */
    public function get_html_creneaux($edit = false, $objet = null)
    {
        $actif = $data_objet = "";
        if ($objet)
        {
            $objet_class = get_class($objet);
            if (in_array($objet_class, array('Client', 'SessionStagiaire')))
            {
                $actif = $objet->creneaux;
                if ($objet_class == "SessionStagiaire")
                    $data_objet = "data-objetid='{$objet->user_id}' data-objet='SessionStagiaire'";
                else
                    $data_objet = "data-objetid='{$objet->id}' data-objet='$objet_class'";
            }
        }
        ob_start();
        
        if (!$edit && count($this->creneaux) == 0)
            echo "<p class='alerte'>".__("Définissez d'abord des créneaux")."</p>";
        else
        {
            ?>
            <div class='tableau-creneau' <?php echo $data_objet; ?> data-sessionid='<?php echo $this->id; ?>'>
            <?php
            /*
            if (count($this->creneaux) == 0)
                echo $this->get_html_ligne_creneaux($edit);
            else
            {*/
                foreach($this->creneaux as $date => $creneau)
                    echo $this->get_html_ligne_creneaux($edit, $date, $actif);
            //}
            if ($edit)
            {
                ?>
                    <div class="icone-bouton add-date empty-date"><span class="dashicons dashicons-plus-alt"></span><?php _e("Date"); ?></div>
                <?php
            }
            echo "</div>";
        }
        
        return ob_get_clean();
    }
    
    /*
     * Renvoie une ligne de créneaux correspondant à une date au format DateTime
     */
    public function get_html_ligne_creneaux($edit = false, $date = null, $actif = null)
    {
        ob_start();
        echo "<div class='liste-creneau' data-date='$date'>";
        
        if ($date == null)
        {
            if ($edit)
                echo '<span class="dashicons dashicons-dismiss icone del-date"></span><input class="datepicker" name="dates[]" type="text" value="" />';
        }
        else
        {
            if ($edit)
                echo '<span class="dashicons dashicons-dismiss icone del-date"></span><input class="datepicker" name="dates[]" type="text" value="'.$date.'" />';
            else
                echo "<span class='creneau-date'>$date</span>";
                
            if ($this->type_index != 'sous_traitance')
            {
                foreach ($this->creneaux[$date] as $creno)
                {
                    $creno_actif = (is_array($actif) && isset($actif[$creno->id]) && $actif[$creno->id] == 1) ? "actif" : "";
                    echo $creno->get_html($creno_actif, $edit);
                }
            }
        }
        
        if ($edit && $this->type_index != 'sous_traitance')
        {
            ?>
            <div class="icone-bouton dynamic-dialog" data-function="add_or_edit_creneau" data-crenoid="-1" data-sessionid="<?php echo $this->id; ?>">
                <span class="dashicons dashicons-plus-alt"></span>
                <?php _e("Créneau"); ?>
            </div>
            <div class="icone-bouton add-date" data-decaljour="1"><span class="dashicons dashicons-plus-alt"></span><?php _e("Copie +1 j"); ?></div>
            <div class="icone-bouton add-date" data-decaljour="7"><span class="dashicons dashicons-plus-alt"></span><?php _e("Copie +7 j"); ?></div>
            <?php
        }
        echo "</div>";
        
        return ob_get_clean();
    }
    
    public function get_sql_select_button($stagiaire_id = -1)
    {
        return "<span class='sql_select bouton' data-sessionid='".$this->id."' data-stagiaireid='$stagiaire_id'>SQL</span>";
    }
}

function get_session_by_id($id)
{
    global $SessionFormation;
    
    if (!isset($SessionFormation[$id]))
        $SessionFormation[$id] = new SessionFormation($id);
        
    return $SessionFormation[$id];
}

