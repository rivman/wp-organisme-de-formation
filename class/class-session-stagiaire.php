<?php
/*
 * class-session-stagiaire.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

require_once(wpof_path . "/class/class-document.php");
require_once(wpof_path . "/class/class-creneau.php");

class SessionStagiaire
{
    public $statut_stagiaire = "";
    public $statut_complement = "";
    public $nature_formation = "";
    public $nature_formation_complement = "";
    public $adresse = "";
    public $cp_ville = "";
    public $telephone = "";
    public $has_employeur = 1;
    public $entreprise = "";
    public $entreprise_responsable = "";
    public $entreprise_service = "";
    public $entreprise_fonction = "";
    public $financement = "";
    public $financement_complement = "";
    public $fonction = "";
    public $attentes = "";
    public $date_inscription = "";
    public $etat_session = "initial";
    public $emarge = 1;
    public $temps; // Objet DateTime
    public $nb_heure; // au format horaire : 3:30
    public $nb_heure_decimal = 0; // nombre d'heures au format décimal : 3,5 pour 3:30
    public $tarif_heure = "";
    public $tarif_total_chiffre = "";
    public $tarif_total_lettre = "";
    public $tarif_base_total = null;  // détermine si le tarif de base défini pour le total (true) ou à l'heure (false)
    public $exe_comptable = array();
    
    // Créneaux de session suivis par le stagiaire
    public $creneaux = array();
    public $dates_array = array();
    public $dates_texte = "";
    
    public $eval_prerequis = array();
    public $eval_preform = array();
    public $eval_postform = array();
    
    public $doc_necessaire = array();
    public $doc_uid = array();
    public $user;
    public $user_id = -1;
    public $session_formation_id;
    public $client_id = -1;
    public $id = -1;
    
    // table suffix
    private $table_suffix;
    


    public function __construct($session_formation_id, $user_id = -1)
    {
        global $SessionFormation;
        global $wpof;
        global $suffix_session_stagiaire;
        
        $this->table_suffix = $suffix_session_stagiaire;
        
        $this->session_formation_id = $session_formation_id;
        
        $session_formation = get_session_by_id($session_formation_id);
        
        if ($user_id > 0)
        {
            $this->id = $session_formation_id."-$user_id";
            $this->user = get_user_by("id", $user_id); // todo à modifié après avoir créé un vrai objet user
            $this->user_id = $user_id;
            
            $meta = $this->get_meta();
            
            foreach($meta as $m)
            {
                $this->{$m['meta_key']} = stripslashes($m['meta_value']);
            }
            
            // recherche du numéro de client si absent
            if ($this->client_id == -1)
            {
                $session = get_session_by_id($this->session_formation_id);
                $client_id = reset($session->clients);
                while ($client_id !== false && $this->client_id == -1)
                {
                    $client = get_client_by_id($this->session_formation_id, $client_id);
                    if (in_array($this->user_id, $client->stagiaires))
                        $this->client_id = $client_id;
                    else
                        $client_id = next($session->clients);
                }
            }
            
            if ($this->tarif_base_total == null)
            {
                $this->tarif_base_total = $session_formation->tarif_base_total;
                if ($this->tarif_base_total)
                    $this->tarif_total_chiffre = $session_formation->tarif_total_chiffre;
                else
                {
                    if (!empty($session_formation->tarif_heure))
                        $this->tarif_heure = $session_formation->tarif_heure;
                    else
                        $this->tarif_heure = $wpof->tarif_inter;
                }
            }
/*            
            if (!$this->tarif_base_total && empty($this->tarif_heure))
            {
                if (!empty($session_formation->tarif_heure))
                    $this->tarif_heure = $session_formation->tarif_heure;
                else
                    $this->tarif_heure = $wpof->tarif_inter;
            }
*/                
            
            // documents nécessaires
            foreach($wpof->documents->term as $doc_index => $doc)
            {
                if ($doc->contexte & $wpof->doc_context->stagiaire)
                    $this->doc_necessaire[] = $doc_index;
            }
            $this->doc_suffix = "s".$this->user_id;
            
            if ($session_formation->type_index != "sous_traitance")
            {
                $this->creneaux = array();
                
                // les créneaux actifs sont stockés dans un tableau plat : l'id du créneau en clé et la valeur 0 (inactif) ou 1 (actif)
                $creneaux_from_db = json_decode($this->get_meta("creneaux"), true);
                
                // on définit tous les créneaux comme actifs par défaut ou si $creneaux_from_db[id] n'est pas définit
                if (is_array($session_formation->creneaux))
                    foreach($session_formation->creneaux as $d)
                        foreach($d as $c)
                            $this->creneaux[$c->id] = (isset($creneaux_from_db[$c->id])) ? $creneaux_from_db[$c->id] : 1;

                $this->calcule_temps_session();
                
    /*            {
                    $this->tarif_total_chiffre = $meta['tarif_total_chiffre'];
                    $this->tarif_base_total = true;
                    echo "true";
                }
                else
                {
                    //$this->tarif_total_chiffre = 0;
                    $this->tarif_base_total = false;
    //                echo "false";
    //            }
    */
                /*
                if ($this->tarif_total_chiffre > 0 && $this->nb_heure_decimal > 0 && $this->tarif_heure == 0)
                {
                    $this->tarif_heure = sprintf("%.3f", round($this->tarif_total_chiffre / $this->nb_heure_decimal, 3));
                }
                    
                if ($this->tarif_heure == "")
                    $this->tarif_heure = $wpof->tarif_inter;
                */
                $this->calcule_tarif();
                $this->set_exercice_comptable();
            }
        }
    }
    
    /*
     * Création des documents
     * En effet, on n'a pas toujours besoin des documents lorsqu'on instancie une session stagiaire
     */
    public function init_docs()
    {
        global $Documents;
        global $wpof;

        foreach ($this->doc_necessaire as $d)
        {
            $doc = new Document($d, $this->session_formation_id, $wpof->doc_context->stagiaire, $this->user_id);
            $Documents[$doc->id] = $doc;
            $this->doc_uid[] = $doc->id;
        }
    }
    
    /*
     * Retourne une métadonnée (ou toutes) de session stagiaire
     * $meta_key : clé de la valeur recherchée
     *
     * Retourne la valeur recherchée ou un tableau de toutes les valeurs
     */
    public function get_meta($meta_key = null)
    {
        return get_stagiaire_meta($this->session_formation_id, $this->user_id, $meta_key);
    }

    public function update_meta($meta_key, $meta_value = null)
    {
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        if ($meta_value == null)
            $meta_value = $this->$meta_key;
        else
            $this->$meta_key = $meta_value;
            
        if (is_array($meta_value))
            $meta_value = serialize($meta_value);
        
        $query = $wpdb->prepare
        ("INSERT INTO $table (session_id, user_id, meta_key, meta_value)
            VALUES ('%d', '%d', '%s', '%s')
            ON DUPLICATE KEY UPDATE meta_value = '%s';",
            $this->session_formation_id, $this->user_id, $meta_key, $meta_value, $meta_value);
        
        return $wpdb->query($query);
    }
    
    /*
    * Création d'un input avec événement change qui met à jour la valeur dans la base
    */
    public function get_input_jpost($type, $name, $label = "")
    {
        $html = "<div class='input_jpost'>";
        $input_id = $name.rand();
        
        if ($label != "")
            $html .= "<label class='top input_jpost_label' for='$input_id'>$label</label>";
        $html .= "<span class='input_jpost_span'>{$this->$name}</span>";
        $step = ($type == "number") ? "step='0.01'" : "";
        $html .= "<input class='input_jpost_value' type='$type' $step id='$input_id' name='$name' value='{$this->$name}' />";
        $html .= "<input type='hidden' name='session_id' value='{$this->session_formation_id}' />";
        $html .= "<input type='hidden' name='stagiaire_id' value='{$this->user_id}' />";
        $html .= "</div>";
        
        return $html;
    }

    /*
    * Création d'un input avec événement change qui met à jour la valeur dans la base
    */
    public function get_select_jpost($list, $name, $selected = "", $options = "", $first = null, $label = "")
    {
        $options .= " class='select_jpost_value'";
        $html = "<div class='select_jpost'>";
        $select_id = $name.rand();
        
        if ($label != "")
            $html .= "<label class='top select_jpost_label' for='$select_id'>$label</label>";
        $html .= "<span class='select_jpost_span'>".$list->get_term($this->$name)."</span>";
        $first = (in_array($selected, $list->term)) ? $first : $selected." — ancien terme, à changer !!";
        $html .= $list->get_select_list($selected, $options, $first);
        $html .= "<input type='hidden' name='session_id' value='{$this->session_formation_id}' />";
        $html .= "<input type='hidden' name='stagiaire_id' value='{$this->user_id}' />";
        $html .= "</div>";
        
        return $html;
    }

    // Renseigne le tableau des exercices comptables
    // En général ce tableau n'aura qu'une valeur (si toute la session se déroule dans la même année civile)
    public function set_exercice_comptable()
    {
        global $SessionFormation;
        if ($SessionFormation[$this->session_formation_id]->type_index == 'inter')
        {
            $this->exe_comptable = unserialize($this->get_meta("exe_comptable"));
            
            if (!is_array($this->exe_comptable) || count($this->exe_comptable) == 0)
            {
                $this->exe_comptable = array();
                foreach($this->dates_array as $d)
                {
                    $date = explode('/', $d);
                    $this->exe_comptable[$date[2]] = 0;
                }
                $last_year = array_keys($this->exe_comptable);
                $last_year = $last_year[count($last_year) - 1];
                $this->exe_comptable[$last_year] = $this->tarif_total_chiffre;
                
                $this->update_meta("exe_comptable");
            }
            $diff = $this->tarif_total_chiffre - array_sum($this->exe_comptable);
            if ($diff != 0)
            {
                $last_year = array_keys($this->exe_comptable);
                $last_year = $last_year[count($last_year) - 1];
                $this->exe_comptable[$last_year] += $diff;
                
                $this->update_meta("exe_comptable");
            }
        }
    }
    
    
    // Calcule le temps (en heures) de suis de la session par ce stagiaire (somme de tous les créneaux actifs)
    public function calcule_temps_session()
    {
        $session_formation = get_session_by_id($this->session_formation_id);
        $client = get_client_by_id($this->session_formation_id, $this->client_id);
        
        $this->temps = DateTime::createFromFormat("U", "0");
        $this->dates_array = array();
        
        foreach($session_formation->creneaux as $date => $tab_date)
            foreach($tab_date as $creno)
            {
                if (!isset($this->creneaux[$creno->id]) || !isset($client->creneaux[$creno->id]) || $this->creneaux[$creno->id] * $client->creneaux[$creno->id] != 0)
                {
                    $this->temps->add($creno->duree);
                    $this->dates_array[] = $date;
                }
            }
        
        $this->dates_array = array_unique($this->dates_array);
        $this->dates_texte = pretty_print_dates($this->dates_array);
        
        $h = $this->temps->format("U") / 3600;
        $m = ($this->temps->format("U") % 3600) / 60;
        $this->nb_heure_decimal = $this->temps->format("U") / 3600;
        $this->nb_heure = sprintf("%02d:%02d", $h, $m);
        
        if (!isset($this->nb_heure_estime_decimal))
        {
            $this->nb_heure_estime_decimal = $this->nb_heure_decimal;
            $this->nb_heure_estime = $this->nb_heure;
        }
        else
        {
            $h = (integer) $this->nb_heure_estime_decimal;
            $m = ($this->nb_heure_estime_decimal - $h) * 60;
            $this->nb_heure_estime = sprintf("%02d:%02d", $h, $m);
        }
    }
    
    public function calcule_tarif()
    {
        if ($this->tarif_base_total == 1)
        {
            if ($this->nb_heure_decimal > 0)
                $this->tarif_heure = sprintf("%.2f", round($this->tarif_total_chiffre / $this->nb_heure_decimal, 2));
        }
        else
            $this->tarif_total_chiffre = sprintf("%.2f", round($this->tarif_heure * $this->nb_heure_decimal, 1));
        $this->tarif_total_lettre = num_to_letter($this->tarif_total_chiffre);
    }
    
    /*
     * Suppression du stagiaire pour cette session
     */
    public function delete()
    {
        // supression du client du tableau inscrits de la session TODO : supprimer ce tableau définitivement
        $session = get_session_by_id($this->session_formation_id);
        $session->supprime_sous_entite("inscrits", $this->user_id);
        
        $client = get_client_by_id($this->session_formation_id, $this->client_id);
        $client->supprime_sous_entite("stagiaires", $this->user_id);
        
        // suppression du stagiaire dans la base de données
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        $query = $wpdb->prepare
        ("DELETE FROM $table
            WHERE user_id = '%d' AND session_id = '%d';",
            $this->user_id, $this->session_formation_id
        );
        
        return $wpdb->query($query);
    }
    
    /*
     * Interface avec le stagiaire
     */
    public function the_interface()
    {
        echo $this->get_the_interface();
    }
    
    public function get_the_interface()
    {
        global $wpof;
        $session_formation = get_session_by_id($this->session_formation_id);
        
        ob_start();
        
        echo hidden_input("default_main_tab", (isset($_SESSION['main-tabs'])) ? $_SESSION['main-tabs'] : 0);
        ?>
        <div id="main-tabs">
            <ul>
                <li><a href="#tab-initial"><?php _e("Avant la formation"); ?></a></li>
                <li><a href="#tab-documents"><?php _e("Documents"); ?></a></li>
                <li><a href="#tab-apres"><?php _e("Après la formation"); ?></a></li>
            </ul>
            
            <div id="tab-initial" class="stagiaire" data-userid="<?php echo $this->user_id; ?>">
                <form>
                <h2><?php _e("Vos besoins"); ?></h2>
                <p><?php _e("Quelles sont vos attentes par rapport à cette formation ? Comment allez-vous mettre en pratique ce que vous allez apprendre ?"); ?></p>
                <textarea cols='60' rows='15' name='attentes'><?php echo $this->attentes; ?></textarea>
                
                <?php
                    hidden_input("date_inscription", time());
                    echo "<h2>{$session_formation->quizpr->sujet_titre}</h2>";
                    echo $session_formation->quizpr->get_html($this->user_id, $session_formation->quizpr_id, 0);
                    echo "<h2>{$session_formation->quizobj->sujet_titre} avant la formation</h2>";
                    echo $session_formation->quizobj->get_html($this->user_id, $session_formation->quizobj_id, 0);
                ?>
                <div class='icone-bouton stagiaire-submit'><span class='dashicons dashicons-plus-alt'></span><?php _e("Enregistrez vos modifications"); ?></div>
                </form>
                <p class="message"></p>
            </div> <!-- initial -->
            
            <div id="tab-documents">
                <?php
                global $Documents;
                $this->init_docs();
                $liste_doc = "";
                foreach ($this->doc_uid as $docuid)
                {
                    $doc = $Documents[$docuid];
                    if ($doc->visible_stagiaire)
                        $liste_doc .= "<li class='doc {$doc->type}'>{$doc->link_name}</li>";
                }
                if ($liste_doc != "") : ?>
                <ul class="liste-doc-stagiaire">
                <?php echo $liste_doc; ?>
                </ul>
                <?php else : ?>
                <p><?php echo $wpof->doc_pas_de_docs; ?></p>
                <?php endif; ?>
            </div>
        
            <div id="tab-apres">
            <form>
            <?php
                echo "<h2>{$session_formation->quizobj->sujet_titre} après la formation</h2>";
                echo $session_formation->quizobj->get_html($this->user_id, $session_formation->quizobj_id, 1);
            ?>
            <div class='icone-bouton stagiaire-submit'><span class='dashicons dashicons-plus-alt'></span><?php _e("Enregistrez vos modifications"); ?></div>
            </form>
            <p class="message"></p>
            </div>
        
        <?php
        return ob_get_clean();
    }
    
    /*
     * Tableau de bord stagiaire, pour le formateur
     */
    public function the_board()
    {
        echo $this->get_the_board();
    }
    public function get_the_board()
    {
        $current_user_id = get_current_user_id();
        $role = wpof_get_role($current_user_id);
        
        if ($role == "um_stagiaire")
            return $this->get_the_interface();
        
        $client = get_client_by_id($this->session_formation_id, $this->client_id);
        
        global $doc_nom;
        global $wpof;
        global $Documents;
        $this->init_docs();
        
        $session_formation = get_session_by_id($this->session_formation_id);
        
        ob_start();
        ?>
        <div class='board board-stagiaire edit-data client-<?php echo $this->client_id; ?> stagiaire-<?php echo $this->user_id; ?>' id='tab-s<?php echo $this->user_id; ?>'>
            <div class="infos-stagiaire flexrow">
            <?php
            ?>
                <div>
                <?php
                    /* TODO : gérer la modification des nom et prénom
                    echo get_input_jpost($this, "prenom", array('input' => 'text', 'label' => __("Prénom")));
                    echo get_input_jpost($this, "nom", array('input' => 'text', 'label' => __("Nom")));
                    */
                    echo "<p>".get_displayname($this->user_id, false)."</p>";
                    echo get_input_jpost($this, "statut_stagiaire", array('select' => '', 'label' => __("Statut du stagiaire")));
                    echo get_input_jpost($this, "nb_heure_estime_decimal", array('input' => 'number', 'label' => __("Durée estimée en heures (en décimal)"))); 
                    echo get_input_jpost($this, "emarge", array('input' => 'checkbox', 'label' => __("Signe la feuille d'émargement")));
                    if ($client->financement != "part")
                        echo get_input_jpost($this, "entreprise_fonction", array('input' => 'text', 'label' => sprintf(__("Fonction chez %s"), $client->nom)));
                ?>
                </div>
                <div class="icones">
                <p>ID : <?php echo $this->id; ?></p>
                <p>Client ID : <?php echo $this->client_id; ?></p>
                <p class="delete-entity icone-bouton" data-objectclass="<?php echo get_class($this); ?>" data-id="<?php echo $this->user_id; ?>" data-sessionid="<?php echo $this->session_formation_id; ?>" data-parent=".stagiaire-<?php echo $this->user_id; ?>">
                <span class="dashicons dashicons-dismiss" > </span>
                <?php _e("Supprimer ce stagiaire"); ?>
                </p>
                
                <?php if ($role == "admin"): ?>
                <p><span data-userid='<?php echo $this->user_id ?>' data-url='<?php echo $session_formation->permalien; ?>' class='bouton switch-user dashicons dashicons-controls-repeat'></span></p>
                <?php endif; ?>
                
                <p><span class='last-modif'><?php echo __("Dernière connexion")." ".get_last_login($this->user_id); ?></span></p>
                </div>
            </div> <!-- infos-stagiaire -->
            
            <fieldset>
                <legend><?php _e("Documents administratifs pour la session"); ?></legend>
                <?php echo get_gestion_docs($this); ?>
            </fieldset>
                
            <?php if (0) : ?>
            <fieldset><legend><?php _e("Créneaux de présence"); ?></legend>
            <?php
                if (count($session_formation->creneaux) == 0)
                    echo "<p class='alerte'>".__("Définissez d'abord des créneaux")."</p>";
                else
                    echo $session_formation->get_html_creneaux(false, $this);
            ?>
            </fieldset>
            <?php endif; ?>
        </div> <!-- board-stagiaire -->
        <?php
        return ob_get_clean();
    }
    
    /*
    * tableau de bord du stagiaire pour une session donnée
    * ancienne version
    */
    public function the_board_old()
    {
        // récupération du rôle
        //global $ultimatemember;
        //um_fetch_user(get_current_user_id());
        //$role = $ultimatemember->user()->get_role();
        $current_user_id = get_current_user_id();
        $role = wpof_get_role($current_user_id);
        
        global $etat_session;
        global $doc_nom;
        global $SessionFormation;
        global $wpof;
        global $Documents;
        $this->init_docs();
        
        $session_formation = $SessionFormation[$this->session_formation_id];
        
        /*
            Restrictions d'accès à l'onglet admin
            - permis pour responsable et admin
            - permis pour les formateurs intervenants
            - permis pour les autres formateurs si wpof_formateur_gest vaut 1
            
        $admin_session indique si la personne qui consulte est habilitée à gérer la session
        */
        $admin_session = false;
        if (in_array($role, array("um_responsable", "admin"))
            || in_array($current_user_id, $session_formation->formateur)
            || ($role == "um_formateur-trice" && $wpof->formateur_gest == 1)
            || ($current_user_id == $session_formation->entreprise_contact_id))
            $admin_session = true;
        
        // $pre_open indique si certains onglets sont pré-ouverts ou non
        $pre_open = true;
        if ($admin_session || $current_user_id == $session_formation->entreprise_contact_id)
            $pre_open = false;
        ?>
        
        <div id="wpof-menu<?php echo $this->id; ?>" class="wpof-menu <?php echo $this->etat_session; ?> famille-stagiaire" data-famille="stagiaire">
        <ul>
        <?php if ($admin_session): ?>
        <li data-id="<?php echo $this->id; ?>" id="btwpof-tableau<?php echo $this->id; ?>" class="onglet fermer-tableau dashicons-before dashicons-arrow-up-alt2"></li>
        <?php endif; ?>
        
        <?php if ($role == "um_stagiaire") : ?>
        <li data-id="presentation<?php echo $this->id; ?>" id="btpresentation<?php echo $this->id; ?>" class="onglet"><?php _e("Présentation"); ?></li>
        <?php endif; ?>
        <?php $onglet_highlight = ($pre_open && $this->etat_session == 'initial') ? "highlightButton" : ""; ?>
        <li data-id="preinscription<?php echo $this->id; ?>" id="btpreinscription<?php echo $this->id; ?>" class="onglet <?php echo $onglet_highlight; ?>"><?php _e("Formulaire d'inscription"); ?></li>
        <?php $onglet_highlight = ($pre_open && in_array($this->etat_session, array('inscrit', 'confirme'))) ? "highlightButton" : ""; ?>
        <li data-id="documents<?php echo $this->id; ?>" id="btdocuments<?php echo $this->id; ?>" class="onglet <?php echo $onglet_highlight; ?>"><?php _e("Documents administratifs"); ?></li>
        <?php $onglet_highlight = ($pre_open && in_array($this->etat_session, array('pendant', 'apres'))) ? "highlightButton" : ""; ?>
        <li data-id="eval_postform<?php echo $this->id; ?>" id="bteval_postform<?php echo $this->id; ?>" class="onglet <?php echo $onglet_highlight; ?>"><?php _e("Évaluation des connaissances"); ?></li>
        
        <?php
        if ($admin_session): ?>
        <li data-id="admin<?php echo $this->id; ?>" id="btadmin<?php echo $this->id; ?>" data-famille="stagiaire" class="onglet"><?php _e("Admin"); ?></li>
        <li data-userid="<?php echo $this->user_id; ?>" data-sessionid="<?php echo $session_formation->id; ?>" class="unsubscribe right icone-bouton" title="<?php _e("Désinscrire cette personne de cette session"); ?>"><span class="dashicons dashicons-dismiss"></span></li>
            <?php if ($role == "admin") : ?>
                <span data-userid='<?php echo $this->user_id; ?>' data-url='<?php echo $session_formation->permalien; ?>' class='right icone-bouton switch-user dashicons dashicons-controls-repeat'></span>
            <?php endif; ?>
        <?php endif; ?>
        </ul>
        
        </div>
        
        <div id="wpof-tableau<?php echo $this->id; ?>" class="wpof-tableau famille-stagiaire">
        <?php if ($role == "um_stagiaire") : ?>
            <div id="presentation<?php echo $this->id; ?>" class="tableau tableau<?php echo $this->id; ?> blocHidden">
                <?php echo $session_formation->get_html_presentation(); ?>
            </div>
        <?php endif; ?>
            
        <?php $bloc_display = ($pre_open && $this->etat_session == 'initial') ? "" : "blocHidden"; ?>
            <div id="preinscription<?php echo $this->id; ?>" class="tableau tableau<?php echo $this->id; ?> <?php echo $bloc_display; ?>">
            <?php
                echo "<form id='inscription".$this->user_id."' action='".$session_formation->permalien."' method='post'>";
                echo hidden_input("inscription", 1);
                echo $session_formation->get_formulaire_inscription_session($this->user->ID);
                echo $session_formation->quizpr->get_html($this->user_id, $session_formation->quizpr_id, 0);
                echo $session_formation->quizobj->get_html($this->user_id, $session_formation->quizobj_id, 0);
                echo "<p class='msg-validation msg'>{$wpof->terms_msg_validation_pre_inscription}</p>";
                echo "<input type='submit' value='".__("Enregistrez vos modifications")."'>";
                echo "</form>";
            ?>
            </div>
        
        <?php $bloc_display = ($pre_open && in_array($this->etat_session, array('inscrit', 'confirme'))) ? "" : "blocHidden"; ?>
            <div id="documents<?php echo $this->id; ?>" class="tableau tableau<?php echo $this->id; ?> <?php echo $bloc_display; ?>">
            <?php
                if ($this->tarif_total_chiffre != "" && $session_formation->type_index == "inter")
                {
                    echo "<p>".__("votre tarif : ");
                    if ($tarif_ttc = get_ttc_prix($this->tarif_total_chiffre))
                        printf(__("%.2f %s HT (%.2f %s TTC) soit %.2f %s HT par heure"),
                            $this->tarif_total_chiffre, $wpof->monnaie_symbole,
                            $tarif_ttc, $wpof->monnaie_symbole,
                            $this->tarif_heure, $wpof->monnaie_symbole);
                    else
                        printf(__("%.2f %s soit %.2f %s par heure"),
                            $this->tarif_total_chiffre, $wpof->monnaie_symbole,
                            $this->tarif_heure, $wpof->monnaie_symbole);
                    echo "</p>";
                }
            
                $liste_doc = "";
                foreach ($this->doc_uid as $docuid)
                {
                    $doc = $Documents[$docuid];
                    if ($doc->visible_stagiaire)
                        $liste_doc .= "<li class='doc {$doc->type}'>{$doc->link_name}</li>";
                }
                if ($liste_doc != "") : ?>
                <ul class="liste-doc-stagiaire">
                <?php echo $liste_doc; ?>
                </ul>
                <?php else : ?>
                <p><?php echo $wpof->doc_pas_de_docs; ?></p>
                <?php endif; ?>
                
            </div>
            
        <?php $bloc_display = ($pre_open && in_array($this->etat_session, array('pendant', 'apres'))) ? "" : "blocHidden"; ?>
            <div id="eval_postform<?php echo $this->id; ?>" class="tableau tableau<?php echo $this->id; ?> <?php echo $bloc_display; ?>">
                <?php
                //echo $session_formation->quizobj->get_html($this->user_id, $session_formation->quizobj_id, 1, "quiz_postform", $session_formation->permalien);
                echo $session_formation->quizobj->get_html($this->user_id, $session_formation->quizobj_id, 1);
                ?>
            </div>
            
        <?php if ($admin_session): ?>
            
            <div id="admin<?php echo $this->id; ?>" class="tableau tableau<?php echo $this->id; ?> blocHidden">
            <form id="admin-session-stagiaire<?php echo $this->id; ?>" action="<?php echo $session_formation->permalien; ?>" method='post'>
            <input type="hidden" id="admin-session-stagiaire<?php echo $this->id; ?>" name="admin-session-stagiaire" value="1" />
            <input type="hidden" id="user_id" name="user_id" value="<?php echo $this->user_id; ?>" />
            <input type="hidden" id="session_id" name="session_id" value="<?php echo $session_formation->id; ?>" />
            <input type="hidden" id="formation_id" name="formation_id" value="<?php echo $session_formation->formation_id; ?>" />
            
            <p id="etat_inscription-<?php echo $this->id; ?>"><?php echo __("Changer l'état d'inscription"); ?>
            <?php echo select_by_list($etat_session, "etat_session", $this->etat_session, "data-sessionid='$session_formation->id' data-stagiaireid='$this->user_id' class='unique-value'"); ?>
            <span class="message"></span>
            </p>
            
            <p id="signe_emargement-<?php echo $this->id; ?>">
            <?php echo __("Signe la feuille d'émargement"); ?>
            <input type="checkbox" value="1" name="emarge" data-sessionid="<?php echo $session_formation->id; ?>" data-stagiaireid="<?php echo $this->user_id; ?>" class="unique-value" <?php echo checked($this->emarge, 1); ?> />        
            <span class="message"></span>
            </p>
            </form>
            
            <?php if ($session_formation->type_index == "inter") : ////// inter only ?>
            
            <fieldset id="fieldset-stagiaire-<?php echo $this->user_id; ?>" class="metadata infosession edit-data" data-stagiaireid="<?php echo $this->user_id; ?>" data-sessionid="<?php echo $this->session_formation_id; ?>">
            <legend><?php _e("Tarif"); ?></legend>
                <div>
                <?php _e("Tarif horaire"); ?>
                <input type="number" min="0" step="0.01" name="tarif_heure_presentiel" id="tarif_heure_presentiel_<?php echo $this->id; ?>" class="notif-modif" value="<?php echo $this->tarif_heure; ?>" /> <?php echo $wpof->monnaie_symbole;
                if (!$session_formation->tarif_base_total)
                    echo " (".__("tarif de base annoncé ")."{$session_formation->tarif_heure} {$wpof->monnaie_symbole})"; ?>
                </div>
                        
                <div><?php _e("Durée en heures"); ?> <span class='nb_heure'><?php echo $this->nb_heure; ?></span></div>
                <!-- <div><?php _e("Tarif total en chiffres"); ?> <span class='tarif_total_chiffre'><?php echo $this->tarif_total_chiffre; ?></span> <?php echo $wpof->monnaie_symbole; ?></div> -->
                
                <?php echo $this->get_input_jpost('number', 'nb_heure_estime_decimal', __("Durée estimée en heures (en décimal)")); ?>
                <div><?php _e("Tarif total en chiffres"); ?> 
                <input type='number' step='0.01' min='0' name='tarif_total_chiffre' class='tarif_total_chiffre' value='<?php echo $this->tarif_total_chiffre; ?>'/> <?php echo $wpof->monnaie_symbole; ?>
                <?php if ($session_formation->tarif_base_total)
                    echo " (".__("tarif de base annoncé ")."{$session_formation->tarif_total_chiffre} {$wpof->monnaie_symbole})"; ?>
                </div>
                <div><?php _e("Tarif total en lettres"); ?> <span class='tarif_total_lettre'><?php echo $this->tarif_total_lettre; ?></span></div>
                
                <?php if (count($this->exe_comptable) > 1) : ?>
                <div class="flexrow"><p><?php _e("Répartition par exercice comptable"); ?></p>
                <?php echo $session_formation->get_input_exe_comptable($this->exe_comptable); ?>
                </div>
                <?php endif; ?>
                
              <!--  <p  data-action="enregistrer_stagiaire_tarif_input" class="bouton enregistrer-stagiaire-tarif-input"><?php _e("Enregistrer les informations administratives"); ?></p> -->
                <p class="message"></p>
            </fieldset>
            
            <?php endif; ////// inter only ?>
            
            <fieldset><legend><?php _e("Créneaux de présence"); ?></legend>
            <?php
                if (count($session_formation->creneaux) == 0)
                    echo "<p class='alerte'>".__("Définissez d'abord des créneaux")."</p>";
                else
                    echo $session_formation->get_html_creneaux(false, $this);
            ?>
            </fieldset>
            
            <?php if ($current_user_id != $session_formation->entreprise_contact_id) : ?>
            <fieldset><legend><?php _e("Documents"); ?></legend>
            
            <?php echo get_gestion_docs($this); ?>
            
            <p><span id="doc-tous<?php echo $this->user_id; ?>" data-resultat="doc-<?php echo $this->user_id; ?>-resultat" class='doc-compile doc-tous doc-bouton bouton' data-sessionid='<?php echo $this->session_formation_id; ?>' data-userid='<?php echo $this->user_id; ?>'><?php _e("Tous les PDF <strong>déjà créés</strong> en un seul"); ?></span> <span id="doc-<?php echo $this->user_id; ?>-resultat"></span></p>
            
            </fieldset>
            <?php endif; ?>
            
            </div>
            <?php endif; // fin si role = admin ou responsable, ou current_user_id est l'un des formateurs, ou tous les formateurs peuvent gérer ?>
        </div>
        
        <?php
    }
}

function get_stagiaire_meta($session_id, $stagiaire_id, $meta_key)
{
    global $wpdb;
    global $suffix_session_stagiaire;
    $table_session_stagiaire = $wpdb->prefix.$suffix_session_stagiaire;
    
    if ($meta_key)
    {
        $query = $wpdb->prepare
        ("SELECT meta_value
            from $table_session_stagiaire
            WHERE session_id = '%d'
            AND user_id = '%d'
            AND meta_key = '%s';",
            $session_id, $stagiaire_id, $meta_key);
        return $wpdb->get_var($query);
    }
    else
    {
        $query = $wpdb->prepare("SELECT meta_key, meta_value from $table_session_stagiaire WHERE session_id = '%d' AND user_id = '%d';", $session_id, $stagiaire_id);
        return $wpdb->get_results($query, ARRAY_A);
    }
}

function get_stagiaire_by_id($session_id, $user_id)
{
    global $SessionStagiaire;
    
    if (!isset($SessionStagiaire[$user_id]))
        $SessionStagiaire[$user_id] = new SessionStagiaire($session_id, $user_id);
        
    return $SessionStagiaire[$user_id];
}
