<?php
/*
 * class-formation.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


class Formation
{
    public $titre = "";
    public $permalien = "";
    public $slug = "";
    public $bandeau_id = "-1"; // image mise en avant
    
    // tableau d'ID des formateurs
    public $formateur = array();
    
    public $presentation = "";
    public $public_cible = "";
    public $objectifs = "";
    public $prerequis = "";
    public $programme = "";
    public $materiel_pedagogique = "";
    public $ressources = "";
    
    public $duree = 0;
    public $tarif = 0;
    public $quizpr = "";
    public $quizobj = "";
    public $specialite = "";
    public $id;
    
    public function __construct($formation_id = -1)
    {
        global $wpof;
        
        if ($formation_id > 0)
        {
            $this->id = $formation_id;
            $data = get_post($formation_id);
            $meta = get_post_meta($formation_id);
            
            // infos issues du post
            $this->titre = $data->post_title;
            $this->permalien = get_the_permalink($formation_id);
            $this->slug = $data->post_name;
            
            // infos issues des meta données du post
            $this->formateur = get_post_meta($formation_id, "formateur", true); // permet de récupérer l'info sous forme de tableau
            if (!is_array($this->formateur))
                $this->formateur = array();
            
            // caractéristiques de la formation
            foreach($wpof->desc_formation->term as $k => $t)
                $this->$k = (isset($meta[$k][0])) ? $meta[$k][0] : "";
            
            $this->duree = (isset($meta['duree'][0])) ? $meta['duree'][0] : "";
            
            if (isset($meta['tarif'][0]) && $meta['tarif'][0] > 0)
                $this->tarif = $meta['tarif'][0];
            else
                $this->tarif = $wpof->tarif_inter;
            $this->quizpr = (isset($meta['quizpr'][0])) ? $meta['quizpr'][0] : "";
            $this->quizpr_id = (isset($meta['quizpr_id'][0])) ? $meta['quizpr_id'][0] : "";
            $this->quizobj = (isset($meta['quizobj'][0])) ? $meta['quizobj'][0] : "";
            $this->quizobj_id = (isset($meta['quizobj_id'][0])) ? $meta['quizobj_id'][0] : "";
            $this->ressources = (isset($meta['ressources'][0])) ? $meta['ressources'][0] : "";
            
            $this->specialite = (isset($meta['specialite'][0])) ? $meta['specialite'][0] : "";
            
            if (has_post_thumbnail($formation_id))
                $this->bandeau_id = get_post_thumbnail_id($formation_id);
        }
        else
        {
            foreach($wpof->desc_formation->term as $k => $t)
                $this->$k = "";
        }
    }
    
    /*
    * Création d'un select avec événement change qui met à jour la valeur dans la base
    */
    public function get_select_jpost($list, $name, $selected = "", $options = "", $first = null, $label = "")
    {
        $options .= " class='select_jpost_formation_value'";
        $html = "<div class='select_jpost'>";
        $select_id = $name.rand();
        
        if ($label != "")
            $html .= "<label class='top select_jpost_label' for='$select_id'>$label</label>";
        $first = (!is_array($selected) && in_array($selected, $list->term)) ? $first : __("Choisissez une spécialité");
        $html .= $list->get_select_list($selected, $options, $first);
        $html .= "<input type='hidden' name='formation_id' value='{$this->id}' />";
        $html .= "</div>";
        
        return $html;
    }

}

function get_formation_by_id($id)
{
    global $Formation;
    
    if (!isset($Formation[$id]))
        $Formation[$id] = new Formation($id);
        
    return $Formation[$id];
}
