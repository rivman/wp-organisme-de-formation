<?php
/*
 * class-document.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

// Inclure dompdf installé via composer
require_once wpof_path . '/vendor/autoload.php';
use Dompdf\Dompdf;
use Dompdf\Options;

define( 'wpof_url_pdf', WP_CONTENT_URL . "/uploads/pdf");
define( 'wpof_path_pdf', WP_CONTENT_DIR . "/uploads/pdf");


/*
 * Liste des documents administratifs
 * indices utilisés dans le code
 * noms proposés par défaut mais modifiables dans les options
 */
$doc_nom = array
(
    'proposition' => __("Proposition de formation : programme détaillé"),
    'convention' => __("Convention de formation professionnelle"),
    'emargement' => __("Feuille d'émargement"),
    'reglement_interieur' => __("Réglement intérieur"),
    'attestation_formation' => __("Attestation de suivi de formation"),
    'certificat_realisation' => __("Certificat de réalisation"),
    'accord_comm' => __("Accord de communication sur le partenariat"),
    'eval_formation' => __("Évaluation de la formation"),
    'quiz_connaissances' => __("Évaluation des compétences sur les objectifs"),
    'pv_secu' => __("PV de sécurité pour les ERP"),
);



class Document
{
    /* Valeurs pour l'état de validation (signature) de chaque document
     * NEED : le document devra être signé
     * REQUEST : un message est envoyé pour que la personne signe ce document
     * DONE : le document est signé
     * Attention : REQUEST doit disparaître lorsque DONE apparaît !
     */
    const VALID_STAGIAIRE_NEED = 1;
    const VALID_STAGIAIRE_REQUEST = 2;
    const VALID_STAGIAIRE_DONE = 4;
    const VALID_CLIENT_NEED = 8;
    const VALID_CLIENT_REQUEST = 16;
    const VALID_CLIENT_DONE = 32;
    const VALID_RESPONSABLE_NEED = 64;
    const VALID_RESPONSABLE_REQUEST = 128;
    const VALID_RESPONSABLE_DONE = 256;
    const DRAFT = 512;
    const SCAN = 1024;
    
    // qui doit signer et est-ce que le document est signé (utilise les constantes ci-dessus)
    public $valid = 0;
    
    
    /* Valeurs pour les colonnes du tableau gestion-docs-admin
    */
    const COL_ALL = 0xFFFF;
    const COL_DOCUMENT = 1;
    const COL_DRAFT = 2;
    const COL_FINAL = 4;
    const COL_REQUEST = 8;
    const COL_DIFFUSER = 16;
    const COL_SCAN = 32;
    const COL_SUPPRIMER = 64;
    const COL_NOM_ENTITE = 128;

    // type de document : convention, ri, attestation de suivi, feuille d'émargement, etc.
    public $type = "";
    
    // chemin et nom de fichiers
    public $path = wpof_path_pdf;
    public $url = wpof_url_pdf;
    public $base_filename = "";
    public $html_filename = "";
    public $pdf_filename = "";
    public $text_name = "";
    public $link_name = "";
    
    // le document est-il diffusé au stagiaire
    public $visible_stagiaire = false;
    
    // date de dernière modification
    public $last_modif = 0;
    
    // est-ce que le document a une date de signature précisée ?
    // si false, on utilise la date du jour
    public $has_date = false;
    public $date = "";
    
    // entité concernée
    public $user_id = null;
    public $client_id = null;
    public $formateur_id = null;
    
    // la personne qui crée le document, peut-elle le signer ?
    public $signataire = false;
    
    // session de formation concernée
    public $session_formation_id = null;
    
    // table suffix
    private $table_suffix = WPOF_TABLE_SUFFIX_DOCUMENTS;
    
    // objet DomPDF
    private $dompdf = null;
    private $pdf_opt = array();
    
    // l'id d'un document est de la forme : type-<session_id> ou type-[csf]<contexte_id>
    // c → client, s → stagiaire, f → formateur
    public $id;
    
    public function __construct($type, $session_formation_id, $contexte, $contexte_id = -1)
    {
        global $wpdb;
        global $wpof;
        
        $table_documents = $wpdb->prefix.$this->table_suffix;
        
        $this->session_formation_id = $session_formation_id;
        $this->type = $type;
        $this->contexte = $contexte;
        $this->contexte_id = ($contexte_id == -1) ? $session_formation_id : $contexte_id;
        $this->text_name = $wpof->documents->get_term($type);
        $this->path .= "/$session_formation_id"; // chaque session a son dossier (TODO : qui pourrait être créé dès la création de la session, voir dans cpt-session-formation)
        $this->url .= "/$session_formation_id";
        $this->base_filename = "$type";
        
        switch ($contexte & $wpof->doc_context->entite)
        {
            case $wpof->doc_context->session:
                $this->id = "$type-{$this->contexte_id}";
                break;
            case $wpof->doc_context->client:
                $this->id = "$type-c{$this->contexte_id}";
                $this->client_id = $this->contexte_id;
                break;
            case $wpof->doc_context->stagiaire:
                $this->id = "$type-s{$this->contexte_id}";
                $this->user_id = $this->contexte_id;
                break;
            case $wpof->doc_context->formateur:
                $this->id = "$type-f{$this->contexte_id}";
                $this->formateur_id = $this->contexte_id;
                break;
        }
        $query = $wpdb->prepare("SELECT meta_key, meta_value from $table_documents WHERE session_id = '%d' AND contexte & '%d' AND contexte_id = '%d' AND document = '%s';",
            $this->session_formation_id,
            $this->contexte,
            $this->contexte_id,
            $this->type
            );
        $res = $wpdb->get_results($query, OBJECT);
        
        foreach($res as $row)
            $this->{$row->meta_key} = $row->meta_value;
        
        $this->valid |= $wpof->documents->term[$this->type]->signature;
        
        $this->path .= ($this->valid & Document::SCAN) ? "/scan" : "";
        $this->url .= ($this->valid & Document::SCAN) ? "/scan" : "";
        
        $this->init_link_name();
    }
    
    public function init_link_name()
    {
        if ($this->pdf_filename != "")
        {
            $this->href = "?download={$this->type}&s={$this->session_formation_id}&ci={$this->contexte_id}&c={$this->contexte}";
            $this->link_name = "<a href='{$this->href}'>".$this->text_name."</a>";
        }
        else
            $this->link_name = $this->text_name;
    }
    
    public function get_meta($meta_key)
    {
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        $query = $wpdb->prepare
        ("SELECT meta_value from $table
            WHERE contexte_id = '%d'
            AND contexte = '%d'
            AND session_id = '%d'
            AND document = '%s'
            AND meta_key = '%s';",
            $this->contexte_id, $this->contexte, $this->session_formation_id, $this->type, $meta_key);
        
        return $wpdb->get_var($query);
    }
    
    public function update_meta($meta_key, $meta_value)
    {
        // mise à jour de l'instance courante
        $this->$meta_key = $meta_value;

        // mise à jour de la base de données
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        $query = $wpdb->prepare
        ("INSERT INTO $table (session_id, contexte, contexte_id, document, meta_key, meta_value)
            VALUES ('%d', '%d', '%d', '%s', '%s', '%s')
            ON DUPLICATE KEY UPDATE meta_value = '%s';",
            $this->session_formation_id, $this->contexte, $this->contexte_id, $this->type, $meta_key, $meta_value, $meta_value);
        return $wpdb->query($query);
    }
    
    public function get_html_ligne($cols = Document::COL_ALL & ~ Document::COL_NOM_ENTITE)
    {
        global $wpof;
        
        $role = wpof_get_role(get_current_user_id());
        $formation_id = get_post_meta("formation", $this->session_formation_id, true);
        
        // si l'utilisateur est signataire, on n'affiche pas la colonne de demande de signature
        if ($this->signataire)
            $cols &= ~ Document::COL_REQUEST;
        
        $html = "<tr id='tr-{$this->id}' class='docrow' data-cols='$cols' data-sessionid='{$this->session_formation_id}' data-contexteid='{$this->contexte_id}' data-contexte='{$this->contexte}' data-typedoc='{$this->type}' data-docuid='{$this->id}'>";
        
        // Nom du document
        if ($cols & Document::COL_DOCUMENT)
        {
            $download_icon = "";
            $brouillon = "";
            if ($this->valid & Document::DRAFT)
                $brouillon = " <span class='alerte'>(".__("Brouillon").")</span> ";
            
            $entite_nom = "";
            if ($cols & Document::COL_NOM_ENTITE && $this->user_id > 0)
                $entite_nom = " – ".get_displayname($this->user_id)."(".__("stagiaire").")";
            if ($cols & Document::COL_NOM_ENTITE && $this->client_id > 0)
                $entite_nom = " – ".get_client_meta($this->client_id, "nom")."(".__("client").")";
            if ($cols & Document::COL_NOM_ENTITE && $this->formateur_id > 0)
                $entite_nom = " – ".get_displayname($this->formateur_id)."(".__("formateur externe").")";
                    
            $infos_doc = "";
            if ($this->last_modif != "")
            {
                $infos_doc = "<p class='infos_doc'><span class='last-modif'>{$wpof->doc_last_modif} : ".date_i18n("j/m/Y H:i:s", $this->last_modif)."</span>";
                $signatures = array();
                if ($this->valid & Document::VALID_RESPONSABLE_NEED)
                {
                    if ($this->valid & Document::VALID_RESPONSABLE_DONE)
                        $signatures['resp'] = "<span class='signature fait'>";
                    else
                        $signatures['resp'] = "<span class='signature alerte'>";
                    $signatures['resp'] .= __("Responsable")."</span>";
                }
                if ($this->valid & Document::VALID_CLIENT_NEED)
                {
                    if ($this->valid & Document::VALID_CLIENT_DONE)
                        $signatures['client'] = "<span class='signature fait'>";
                    else
                        $signatures['client'] = "<span class='signature alerte'>";
                    $signatures['client'] .= __("Client")."</span>";
                }
                if ($this->valid & Document::VALID_STAGIAIRE_NEED)
                {
                    if ($this->valid & Document::VALID_STAGIAIRE_DONE)
                        $signatures['stag'] = "<span class='signature fait'>";
                    else
                        $signatures['stag'] = "<span class='signature alerte'>";
                    $signatures['stag'] .= __("Stagiaire(s)")."</span>";
                }
                if (count($signatures) > 0)
                    $infos_doc .= " — ".__("Document signé par : ").join(" / ", $signatures);
                $infos_doc .= "</p>";
                
                $download_icon .= "<a class='right float' href='{$this->href}'><span class='dashicons dashicons-download'></span></a>";
            }
            
            $html .= "<td id='nom-{$this->id}'>".$download_icon.$this->link_name.$entite_nom.$brouillon.$infos_doc;
            if (debug && $role == "admin")
                $html .= "<p>".decbin($this->valid)."</p>";
            $html .= "</td>"; 
        }
        
        // Les deux premiers boutons ne sont affichés que si le doc n'est pas un scan
        // Bouton brouillon
        if ($cols & Document::COL_DRAFT)
        {
            $html .= "<td>";
            if (!($this->valid & Document::SCAN))
                $html .= "<span class='doc-creer doc-bouton'>".__("Créer")."</span>";
            $html .= "</td>";
        }
        
        // Bouton finaliser
        if ($cols & Document::COL_FINAL)
        {
            $html .= "<td>";
            if ($this->last_modif != ""
                && !($this->valid & Document::SCAN)
                && !(($this->valid & Document::VALID_RESPONSABLE_NEED) XOR $this->signataire)
                && !($this->valid & (Document::VALID_RESPONSABLE_DONE | Document::VALID_CLIENT_DONE))
                )
            {
                $html .= "<span class='doc-creer doc-bouton' data-final='valider'";
                if ($this->signataire)
                    $html .= " data-signataire='1'";
                $html .= ">".__("Créer")."</span>";
            }
            $html .= "</td>";
        }
        
        // Bouton demander la signature
        //if (!$this->signataire && $cols & Document::COL_REQUEST)
        if ($cols & Document::COL_REQUEST)
        {
            $html .= "<td>";
            if ($this->last_modif != "" && $this->valid & Document::VALID_RESPONSABLE_NEED)
            {
                if (!($this->valid & Document::VALID_RESPONSABLE_DONE))
                {
                    $etat_class = ($this->valid & Document::VALID_RESPONSABLE_REQUEST) ? "en-cours" : "";
                    $html .= "<span class='doc-demande-valid $etat_class doc-bouton' title='{$wpof->doc_demande_valid}'>".__("Demander")."</span>";
                }
            }
            $html .= "</td>";
        }
        
        // Bouton diffuser
        if ($cols & Document::COL_DIFFUSER)
        {
            $html .= "<td>";
            if ($this->last_modif != "" && !($this->valid & Document::DRAFT))
            {
                $etat_class = ($this->visible_stagiaire) ? "fait" : "";
                $html .= "<span class='doc-diffuser $etat_class doc-bouton' title='{$wpof->doc_diffuser}'>".__("Diffuser")."</span>";
            }
            $html .= "</td>";
        }
        
        // bouton uploader
        if ($cols & Document::COL_SCAN)
        {
            $html .= '<td>';
            if ($this->last_modif != ""
                && $this->valid & (Document::VALID_RESPONSABLE_NEED | Document::VALID_CLIENT_NEED | Document::VALID_STAGIAIRE_NEED)
                && !($this->valid & Document::DRAFT))
            {
                $id_span_filename = "file".rand();
                $id_span_message = "msg".rand();
                $html .= "<span class='doc-scan doc-bouton' title='{$wpof->doc_scan}'>".__("Déposer")."</span>";
                $html .= '<div class="dialog dialog-scan" style="display: none;">';
                $html .= '<form method="POST" name="upload-'.$this->id.'" enctype="multipart/form-data">';
                $html .= '<input name="scan-'.$this->id.'" type="file" accept="image/*,.pdf" />';
                $html .= '<p>';
                if ($this->valid & Document::VALID_RESPONSABLE_NEED)
                    $html .= '<label><input type="checkbox" name="signature_responsable" /> '.__("signé par le⋅la responsable").'</label><br />';
                if ($this->valid & Document::VALID_CLIENT_NEED)
                    $html .= '<label><input type="checkbox" name="signature_client" /> '.__("signé par le⋅la client⋅e").'</label>';
                if ($this->valid & Document::VALID_STAGIAIRE_NEED)
                    $html .= '<label><input type="checkbox" name="signature_stagiaire" /> '.__("signé par le⋅la⋅les stagiaire(s)").'</label>';
                $html .= '</p>';
                $html .= hidden_input('id_span_message', $id_span_message);
                $html .= hidden_input('id_span_filename', $id_span_filename);
                $html .= hidden_input('action', 'archive_file');
                $html .= hidden_input('session_id', $this->session_formation_id);
                $html .= hidden_input('cols', $cols);
                $html .= hidden_input('signataire', ($this->signataire) ? 1 : 0);
                $html .= hidden_input('ligne_id', "tr-{$this->id}");
                $html .= '</form>';
                $html .= '<p id="'.$id_span_message.'" class="message"></p>';
                $html .= '</div>';
                $html .= '<span id="'.$id_span_filename.'" class="filename"></span>';
            }
            $html .= '</td>';
        }
        
        // Bouton supprimer
        if ($cols & Document::COL_SUPPRIMER)
        {
            $html .= "<td>"; 
            if ($this->last_modif != "")
                $html .= "<span class='doc-supprimer attention doc-bouton' title='{$wpof->doc_supprimer}'>".__("Supprimer")."</span>";
                $html .= "<div style='display: none' id='supprim{$this->id}'><p>{$wpof->doc_supprimer}</p></div>";
            $html .= "</td>";
        }
        
        $html .= "</tr>";
        
        return $html;
    }
    
    
    /*
     * Supprimer un document dans la base
     * Si $file vaut true, alors on supprime aussi les fichiers
     */
    public function supprimer($file = false)
    {
        if ($file)
        {
            unlink("{$this->path}/{$this->pdf_filename}");
            unlink("{$this->path}/{$this->html_filename}");
        }
        
        // mise à jour de la base de données
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        $query = $wpdb->prepare
        ("DELETE FROM $table
            WHERE session_id = '%d'
            AND contexte_id = '%d'
            AND contexte = '%d'
            AND document = '%s';",
            $this->session_formation_id, $this->contexte_id, $this->contexte, $this->type
        );
        
        return $wpdb->query($query);
    }
    
    public function pdf_creer()
    {
        global $wpof;
        $session_formation = get_session_by_id($this->session_formation_id);
        
        $options = new Options();
        $options->set('chroot', WP_CONTENT_DIR);
        $options->set('tempDir', WP_CONTENT_DIR."/uploads");
        $options->set('isRemoteEnabled', TRUE);
        $options->set('isPhpEnabled', TRUE);
        $options->set('defaultPaperSize', 'A4');
        $options->set('dpi', 200);
        
        $this->dompdf = new Dompdf($options);
        
        // création du doc en HTML
        switch ($this->type)
        {
            case "emargement":
                $session_formation->init_stagiaires();
                $html = "";
                if ($session_formation->type_emargement['jour'] == 1)
                    $html .= $this->get_html_emargement_journee();
                if ($session_formation->type_emargement["tous"] == 1)
                    $html .= $this->get_html_emargement_journee("tous");
                if ($session_formation->type_emargement["vide"] == 1)
                    $html .= $this->get_html_emargement_journee("vide");
                if ($session_formation->type_emargement["stagiaire"] == 1)
                    $html .= $this->get_html_emargement_stagiaire();
                break;
            case "eval_formation":
                $html = $this->get_html_eval_formation();
                break;
            case "quiz_connaissances":
                $html = $this->get_html_quiz_connaissances();
                break;
            case "pv_secu":
                $html = $this->get_html_pv_secu();
                break;
            case "proposition":
                $html = $this->get_html_proposition();
                break;
            default:
                $html = $this->get_html_base($wpof->{$this->type."_id"});
                break;
        }
        
        if ($html != "")
        {
            $this->html_save($html);
            
            $html = $this->html_from_model() . $html;
            
            //$html = $this->img_url_to_path($html); // l'utilisation d'image PNG locale semble poser problème (image vide intégrée dans le PDF)
            
            // output the HTML content
            $this->dompdf->loadHtml($html);
            $this->pdf_save();
            
            return true;
        }
        else
            return false;
    }
    
    /*
     * Création du document en HTML
     * Import du modèle
     * Substitution des variables
     */
    private function get_html_base($modele_id)
    {
        $modele = get_post($modele_id);
        $html = "<h1>".$modele->post_title."</h1>";
        $html .= wpautop($modele->post_content);
        return $this->substitute_values(wpautop($html));
    }

    /*
     * Renvoie un tableau d'une ligne avec les informations identifiant la session et le stagiaire
     */
    private function get_html_cartouche()
    {
        $session_formation = get_session_by_id($this->session_formation_id);
        
        $html = "<table class='cartouche'><tr>";
        $html .= "<td>{$session_formation->titre_formation}</td>";
        $html .= "<td>{$session_formation->dates_texte}</td>";
        $html .= "<td>{$session_formation->ville}</td>";
        $html .= "<td>".get_user_meta($this->user_id, "first_name", true)." ".get_user_meta($this->user_id, "last_name", true)."</td>";
        $html .= "</tr></table>";
        
        return $html;
    }
    
    private function get_html_proposition()
    {
        global $wpof;
        $session_formation = get_session_by_id($this->session_formation_id);
        
        ob_start();
        ?>
        <h1><?php echo $wpof->documents->get_term('proposition'); ?></h1>
        <h2><?php echo $session_formation->titre_formation; ?></h2>
        <table class="proposition">
        <?php
        foreach($wpof->desc_formation->term as $k => $term)
        {
            if ($k == "programme")
                continue; // on traite le programme plus loin
            echo "<tr>";
            echo "<td>{$term->text}</td>";
            
            if (!empty($session_formation->$k))
                echo "<td>".wpautop($session_formation->$k)."</td>";
            elseif ($this->valid & Document::DRAFT)
                printf("<td style='color: red';>".__("Vous n'avez pas défini « %s ». Ce message n'apparait que dans le brouillon.")."</td>", $term->text);
            
            echo "</tr>";
        }
        ?>
        
        <tr><td><?php _e("Dates et durée"); ?></td>
        <td>
        <p><?php echo $session_formation->dates_texte; ?></p>
        <p><?php echo ($session_formation->nb_heure != $session_formation->nb_heure_estime) ? __("Durée estimée") : __("Durée"); echo " ".$session_formation->nb_heure_estime; ?></p>
        </td>
        </tr>
        
        <tr>
        <td><?php
            $pluriel = (count($session_formation->formateur) > 1) ? "s" : "";
            echo __("Intervenant(e)").$pluriel;
            ?>
        </td>
        <td>
        <?php echo join($this->get_liste_formateurs(true)); ?>
        </td>
        </tr>
        
        </table>

        <h3><?php _e("Programme"); ?>
        <div class="proposition">
        <?php echo wpautop($session_formation->programme); ?>
        </div>
        <?php 
        return ob_get_clean();
    }
    
    private function get_liste_formateurs($presentation = false)
    {
        $session_formation = get_session_by_id($this->session_formation_id);
        $liste_formateurs = array();
        $presentation_formateurs = array();
        foreach($session_formation->formateur as $f)
        {
            $display_name = get_displayname($f);
            $liste_formateurs[] = $display_name;
        
            if ($presentation)
            {
                $photo_id = get_user_meta($f, 'photo', true);
                $img_tag = "";
                if ($photo_id)
                {
                    $src = wp_get_attachment_url($photo_id);
                    $img_tag = "<img class='photo-formateur' src='".$src."' />";
                }
                
                $cv_id = get_user_meta($f, 'cv', true);
                $cv_tag = ($this->valid & Document::DRAFT) ? "<p><span style='color: red;'>CV manquant, vous devez en fournir un !!!!</span></p>" : "";
                if ($cv_id)
                {
                    $src = wp_get_attachment_url($cv_id);
                    $cv_tag = "<p>Son CV : <a href='$src'>".$src."</a></p>";
                }
                $presentation_formateurs[] = "<div class='fiche-formateur'>$img_tag<h4>".$display_name."</h4>".wpautop(get_user_meta($f, 'presentation', true)).$cv_tag."</div>";
            }
        }
        
        if ($presentation)
            return $presentation_formateurs;
        else
            return $liste_formateurs;
    }
    
    private function get_html_eval_formation()
    {
        global $wpof;
        $session_formation = get_session_by_id($this->session_formation_id);
        
        $corps = $wpof->eval_form;
        
        $html = "";
        $html .= "<h1>".__("Évaluation de la formation")."</h1>";
        $html .= $this->get_html_cartouche();
        
        $html .= "<table class='eval'>";
        
        foreach (preg_split("/\n/", $corps) as $line)
        {
            $line = explode("|", $line);
            
            $line_type = trim($line[0]);
            if (in_array($line_type, array("", "#"))) continue;
            
            switch ($line_type)
            {
                case "h":
                    $html .= "<tr><td class='td-titre'>".trim($line[1])."</td><td class='td-plusmoins'>–</td><td></td><td></td><td></td><td class='td-plusmoins'>+</td></tr>";
                    break;
                case "r":
                    $html .= "<tr><td class='td-question'>".trim($line[1])."</td>";
                    $i = 1;
                    while ($i <= 5)
                    {
                        $html .= "<td class='td-value'>$i</td>";
                        $i++;
                    }
                    $html .= "</tr>";
                    break;
                case "t":
                case "ta":
                    $html .= "<tr><td class='td-question'>".trim($line[1])."</td><td colspan='5'></td></tr>";
                    $html .= "<tr><td class='td-reponse-$line_type' colspan='6'></td></tr>";
                    break;
            }
        }
        
        $html .= "</table>";
        
        return $html;
    }

    private function get_html_quiz_connaissances()
    {
        global $wpof;
        $session_formation = get_session_by_id($this->session_formation_id);
        $quiz = new Quiz($session_formation->quizobj_id);
        $quiz->init_questions($session_formation->quizobj_id);
        
        $html = "";
        $html .= "<h1>".__("Évaluation des compétences")."</h1>";
        $html .= $this->get_html_cartouche();
        
        
        $html .= "<table class='eval'>";
        
        foreach($quiz->questions as $titre => $groupe)
        {
            if ($titre == "none") $titre = "";
            
            $html .= "<tr><td class='td-titre'><h3>$titre</h3></td><td class='td-plusmoins'>-</td><td> </td><td> </td><td> </td><td class='td-plusmoins'>+</td></tr>\n";

            foreach($groupe as $num => $q)
            {
                $html .= "<tr><td class='quiz-question td-question'>$q</td>";
                $i = 1;
                while ($i <= 5)
                {
                    $html .= "<td class='td-value'>$i</td>";
                    $i++;
                }
                $html .= "</tr>\n";
            }
        }
        $html .= "</table>";
        
        return $html;
    }


    private function get_html_pv_secu()
    {
        $session_formation = get_session_by_id($this->session_formation_id);
        
        $html = "";
        
        if (!empty($session_formation->lieu_secu_erp))
        {
            $html .= "<h1>".__("PV de commission de sécurité")."</h1>";
            $html .= wp_get_attachment_image($session_formation->lieu_secu_erp, 'fullsize', false, array('class' => 'pv-secu', 'align' => 'center'));
            return $html;
        }
        else
            return "";
    }
    
    /*
    * Création de la feuille d'émargement par jour
    * $content vaut
    * * "vide" pour créer des feuilles vierges de noms
    * * "tous" pour mentionner tous les présents, pas seulement ceux marqués comme émargeant
    */
    function get_html_emargement_journee($content = "")
    {
        global $wpof;
        global $SessionStagiaire;
        
        $session_formation = get_session_by_id($this->session_formation_id);

        $emargement = get_post($wpof->emargement_id);
        $modele_page = "<div class='emargement'>".wpautop($emargement->post_content)."</div>";
        
        $contenu = "";
        foreach($session_formation->dates_array as $d)
        {
            $nb_stagiaires = 0;
            $colonnes = array();
            $liste_stagiaires = array();
            
            $contenu .= $modele_page;
            $contenu = str_replace("{titre}", $d, $contenu);
            
            foreach($session_formation->creneaux[$d] as $c)
            {
                if (!in_array($c->type, array("foad_sync", "foad_async")))
                    $colonnes[] = "<th class='plage'>".$c->titre."<br />".$wpof->type_creneau[$c->type]."</th>";
            }
            if ($wpof->doc_emarge_recu_ri != "")
                $colonnes['ri'] = "<th>".$wpof->doc_emarge_recu_ri."</th>";
            
            $tableau_complet = "<table class='tableau-stagiaires'><tbody><tr><th class='client'>".__("Client")."</th><th class='stagiaire'>".__("Stagiaire")."</th>".join($colonnes)."</tr>";
            
            foreach($session_formation->inscrits as $stagiaire_id)
            {
                $session_stagiaire = $SessionStagiaire[$stagiaire_id];
                if (in_array($d, $session_stagiaire->dates_array) && ($session_stagiaire->emarge || $content == "tous"))
                {
                    $client = get_client_by_id($this->session_formation_id, $session_stagiaire->client_id);
                    $tableau_complet .= "<tr><td>";
                    if ($content != "vide")
                    {
                        if ($client->financement != "part" && isset($client->nom))
                            $tableau_complet .= $client->nom;
                    }
                    $tableau_complet .= "</td><td>";
                    if ($content != "vide")
                        $tableau_complet .= get_displayname($stagiaire_id);
                    $tableau_complet .= "</td>";
                        
                    foreach($session_formation->creneaux[$d] as $c)
                        if (!in_array($c->type, array("foad_sync", "foad_async")))
                        {
                            if ($session_stagiaire->creneaux[$c->id] == 1 || $content == "vide" || $content == "tous")
                                $tableau_complet .= "<td></td>";
                            else
                                $tableau_complet .= "<td class='stagiaire-absent'></td>";
                        }
                    
                    if (isset($colonnes['ri']))
                        $tableau_complet .= "<td></td>";
                    $tableau_complet .= "</tr>";
                    $nb_stagiaires ++;
                }
            }
            
            $i = $nb_stagiaires;
            while ($i <= $session_formation->stagiaires_max)
            {
                $tableau_complet .= "<tr>".str_repeat("<td></td>", count($colonnes) + 2)."</tr>"; // 2 : colonne client + colonne stagiaire
                $i++;
            }
            
            $tableau_complet .= "</tbody></table>";
            
            $contenu = str_replace("{tableau-stagiaires}", $tableau_complet, $contenu);
            $contenu .= "<div class='saut-de-page'></div>";
        }
        
        return $this->substitute_values(wpautop($contenu));
    }
    
    /*
    * Création de la feuille d'émargement par stagiaire
    */
    function get_html_emargement_stagiaire()
    {
        global $wpof;
        global $SessionStagiaire;
        
        $session_formation = get_session_by_id($this->session_formation_id);

        $emargement = get_post($wpof->emargement_id);
        $modele_page = "<div class='emargement'>".wpautop($emargement->post_content)."</div>";
        
        if ($wpof->doc_emarge_recu_ri != "")
            $ligne_valid_ri = "<tr><td colspan='2'>".$wpof->doc_emarge_recu_ri."</td><td> </td></tr>";
        
        $contenu = "";
        foreach($session_formation->inscrits as $stagiaire_id)
        {
            $session_stagiaire = $SessionStagiaire[$stagiaire_id];
            
            if ($session_stagiaire->emarge)
            {
                $contenu .= $modele_page;
                $stagiaire = get_displayname($stagiaire_id);
                if ($session_formation->type_index == "inter" && $session_stagiaire->entreprise != "")
                    $stagiaire .= " (".$session_stagiaire->entreprise.")";
                $contenu = str_replace("{titre}", $stagiaire, $contenu);
                
                $tableau_complet = "<table class='tableau-dates'><tbody>";
                $tableau_complet .= "<tr><th>".__("Date")."</th><th>".__("Créneau")." / ".__("Type")."</th><th>".__("Signature")."</th></tr>";
                
                $last_date = "";
                foreach($session_formation->creneaux as $date => $creneaux)
                {
                    foreach($creneaux as $c)
                    {
                        if ($session_stagiaire->creneaux[$c->id] == 1 && !in_array($c->type, array("foad_sync", "foad_async")))
                        {
                            $cell = "td";
                            if ($date != $last_date)
                            {
                                $last_date = $date;
                                $cell = "th";
                            }
                            $tableau_complet .= "<tr><$cell>".pretty_print_dates($c->date)."</$cell><td>".$c->titre." / ".$wpof->type_creneau[$c->type]."</td><td> </td></tr>";
                        }
                    }
                }
                
                $tableau_complet .= $ligne_valid_ri."</tbody></table>";
                $contenu = str_replace("{tableau-stagiaires}", $tableau_complet, $contenu);
                $contenu .= "<div class='saut-de-page'></div>";
            }
        }
        
        return $this->substitute_values(wpautop($contenu));
    }


    /*
     * Enregistrement d'un fichier HTML avec le contenu du document sans html_model
     * Servira à générer des documents PDF regroupant plusieurs documents
     */
    private function html_save($html)
    {
        if (!is_dir($this->path)) mkdir($this->path, 0777, true);
        
        $this->html_filename = $this->base_filename.".html";
        
        $html_file = fopen($this->path."/".$this->html_filename, "w");
        fwrite($html_file, $html);
    }

    /*
     * Création du document PDF avec son nom et son chemin
     * retourne le nom du fichier PDF
     */
    private function pdf_save()
    {
        if (!is_dir($this->path)) mkdir($this->path, 0777, true);
        
        $this->pdf_filename = $this->base_filename.".pdf";
        $this->dompdf->render();
        $pdf_content = $this->dompdf->output();
        
        $pdf_name = $this->path."/".$this->pdf_filename;
        $pdf_file = fopen($pdf_name, "w");
        fwrite($pdf_file, $pdf_content);
        fclose($pdf_file);
    }
    
    /*
    * Modèle de contenu HTML header et footer (modèle de base)
    * Comme on ne peut invoquer plusieurs fois loadHtml, on prépare une chaîne html envoyée en une seule fois
    */
    public function html_from_model()
    {
        global $wpof;

        $html = "";
        
        $html .= "<style type='text/css'>";
        $html .= "@page { margin: ".$wpof->pdf_marge_haut."mm ".$wpof->pdf_marge_droite."mm ".$wpof->pdf_marge_bas."mm ".$wpof->pdf_marge_gauche."mm; }";
        $html .= "h1.saut-page {page-break-before: always; break-before: always;}"; // force le saut de page avant les h1 de classe saut-page
        $html .= "h1.saut-page:nth-of-type(1) { page-break-before: avoid; break-before: avoid; }"; // sauf le premier
        $html .= "#header { height: ".$wpof->pdf_hauteur_header."mm; }";
        $html .= "#footer { height: ".$wpof->pdf_hauteur_footer."mm; bottom: 0mm; }";
        $html .= "body { margin: ".$wpof->pdf_hauteur_header."mm 0 ".$wpof->pdf_hauteur_footer."mm 0; }";
        $html .= $wpof->pdf_css;
        $html .= "</style>";
        $html .= "<div id='header'>".wpautop($wpof->pdf_header)."</div>";
        $html .= "<div id='footer'>".wpautop($wpof->pdf_footer)."<div class='page-number'></div></div>";
        
        return $html;
    }

    /*
    * Modèle de contenu HTML avec le footer mis dans le header
    * Comme on ne peut invoquer plusieurs fois loadHtml, on prépare une chaîne html envoyée en une seule fois
    */
    private function html_from_model_header_only()
    {
        global $wpof;

        // calcul totalement empirique : le header de ce modèle a une hauteur égale à la somme de celle du header et celle du footer définies dans les options
        $hauteur_header = $wpof->pdf_hauteur_header + $wpof->pdf_hauteur_footer;

        $html = "";
        
        $html .= "<style type='text/css'>";
        $html .= "@page { margin: ".$wpof->pdf_marge_haut."mm ".$wpof->pdf_marge_droite."mm ".$wpof->pdf_marge_bas."mm ".$wpof->pdf_marge_gauche."mm; }";
        $html .= "h1.saut-page {page-break-before: always; break-before: always;}"; // force le saut de page avant les h1 de classe saut-page
        $html .= "h1.saut-page:nth-of-type(1) { page-break-before: avoid; break-before: avoid; }"; // sauf le premier
        $html .= "#header { height: ".$hauteur_header."mm; width: 50%; }";
        $html .= "body { margin: ".$hauteur_header."mm 0 0 0; }";
        $html .= $wpof->pdf_css;
        $html .= "#footer { height: ".$hauteur_header."mm; top: 0mm; width: 100%; border: none; }";
        $html .= "#footer p { text-align: right; font-size: 0.9em; line-height: 1.1em; padding-left: 120mm; color: cmyk(0, 0, 0, 1); }";
        $html .= "</style>";
        $html .= "<div id='header'>".wpautop($wpof->pdf_header)."</div>";
        $html .= "<div id='footer'>".wpautop($wpof->pdf_footer)."</div>";
        
        return $html;
    }

    /*
     * Convertit les URL des images en chemin absolu
     * Évite la copie des images dans un dossier temporaire avant de les inclure dans le PDF
     */
    function img_url_to_path($html)
    {
        /*
        $dom = new DOMDocument("1.0","UTF-8");
        $dom->loadHTML(utf8_decode($html));

        foreach ($dom->getElementsByTagName('img') as $img)
        {
            $src = $img->getAttribute("src");
            $src = str_replace(WP_CONTENT_URL, WP_CONTENT_DIR, $src);
            $img->setAttribute("src", $src);
        }
        return $dom->saveHTML();
        */
        return preg_replace('#(<img)(.*)(src=["\'])'.WP_CONTENT_URL.'([^"\']*)(["\'][^>]*>)#', '$1$2$3'.WP_CONTENT_DIR.'$4$5', $html);
    }
    
    /*
    * Substitution des variables disséminées dans les textes de base
    */
    function substitute_values($text)
    {
        global $SessionStagiaire;
        global $wpof;
        
        $highlight = $this->valid & Document::DRAFT;
        
        $session_formation = get_session_by_id($this->session_formation_id);
        
        if ($this->contexte & $wpof->doc_context->client)
            $client = get_client_by_id($this->session_formation_id, $this->contexte_id);
        
        if ($this->contexte & $wpof->doc_context->stagiaire)
        {
            $stagiaire = get_stagiaire_by_id($this->session_formation_id, $this->contexte_id);
            $client = get_client_by_id($this->session_formation_id, $stagiaire->client_id);
        }
        
        // Rôle de celui qui génère le document
        $current_user_id = get_current_user_id();
        $role = wpof_get_role($current_user_id);
        
        // balises de surlignage
        if ($highlight)
        {
            $tagS = "<span style='color: red;'>[ ";
            $tagE = " ]</span>";
        }
        else
        {
            $tagS = "";
            $tagE = "";
        }

        // Responsable de formation
        $text = str_replace("{responsable-signature}", "", $text);
        $text = str_replace("{responsable-lu-approuve}", "", $text);
        
        $respform_id = $wpof->respform_id;
        $respform_nom = get_displayname($respform_id, false);
        $respform_genre = get_user_meta($respform_id, "genre", true);
        $text = str_replace("{respform-nom}", $tagS.$respform_nom.$tagE, $text);
        $text = str_replace("{respform-fonction}", $tagS.$wpof->respform_fonction .$tagE, $text);

        // remplacements transitoires
        $text = str_replace("{nb-jour}", $tagS."{nb-jour} à supprimer".$tagE, $text);
        $text = str_replace("{tarif-jour}", $tagS."{tarif-jour} à supprimer".$tagE, $text);

        if (isset($client))
        {
            $text = str_replace("{entreprise}", $tagS.$client->nom .$tagE, $text);
            $text = str_replace("{client}", $tagS.$client->nom .$tagE, $text);
            
            if ($client->financement == "part")
                $text = preg_replace("/{has-employeur}(.*){\/has-employeur}/", "", $text);
            else
                $text = preg_replace("/{has-employeur}(.*){\/has-employeur}/", "$1", $text);
                
            $text = str_replace("{adresse}", $tagS.nl2br($client->adresse).$tagE, $text);
            $text = str_replace("{cp-ville}", $tagS.$client->cp_ville .$tagE, $text);
            
            $text = str_replace("{nature-formation}", $tagS.$wpof->nature_formation->get_term($client->nature_formation).$tagE, $text);
            
            if ($this->contexte & $wpof->doc_context->client)
            {
                $pluriel = (count($client->dates_array) > 1) ? "s" : "";
                $text = str_replace("{pluriel-dates}", $tagS.$pluriel.$tagE, $text);
                $text = str_replace("{dates}", $tagS.$client->dates_texte .$tagE, $text);
                $text = str_replace("{duree}", $tagS.$client->nb_heure." ".__("heures").$tagE, $text);
                $text = str_replace("{nb-heure}", $tagS.$client->nb_heure .$tagE, $text);
            }
            
            // Tarif formation
            if ($tarif_heure_ttc = get_ttc_prix($client->tarif_heure))
            {
                $text = str_replace("{tarif-heure}", $tagS.$client->tarif_heure." ".$wpof->monnaie_symbole ." HT (".$tarif_heure_ttc." ".$wpof->monnaie_symbole ." TTC)".$tagE, $text);
                $text = str_replace("{tarif-chiffre}", $tagS.$client->tarif_total_chiffre." "
                    .$wpof->monnaie_symbole ." HT (".get_ttc_prix($client->tarif_total_chiffre)." ".$wpof->monnaie_symbole ." TTC)" .$tagE, $text);
            }
            else
            {
                $text = str_replace("{tarif-heure}", $tagS.$client->tarif_heure." ".$wpof->monnaie_symbole .$tagE, $text);
                $text = str_replace("{tarif-chiffre}", $tagS.$client->tarif_total_chiffre ." ".$wpof->monnaie_symbole.$tagE, $text);
            }
            $text = str_replace("{tarif-lettre}", $tagS.$client->tarif_total_lettre .$tagE, $text);

            
            // Autres frais
            if ($client->tarif_total_autres_chiffre == 0)
            {
                $text = preg_replace("/{if-autres_frais}(.*){\/if-autres_frais}/", "", $text);
            }
            else
            {
                if ($tarif_autres_ttc = get_ttc_prix_autres($client->tarif_total_autres_chiffre))
                    $text = str_replace("{tarif-total-autres-chiffre}", $tagS.$client->tarif_total_autres_chiffre." "
                        .$wpof->monnaie_symbole." HT ($tarif_autres_ttc ".$wpof->monnaie_symbole." TTC)".$tagE, $text);
                else
                    $text = str_replace("{tarif-total-autres-chiffre}", $tagS.$client->tarif_total_autres_chiffre." ".$wpof->monnaie_symbole.$tagE, $text);
                $text = str_replace("{tarif-total-autres-lettre}", $tagS.$client->tarif_total_autres_lettre .$tagE, $text);
                $text = str_replace("{autres-frais}", $tagS.$client->autres_frais .$tagE, $text);
                $text = str_replace("{if-autres_frais}", "", $text);
                $text = str_replace("{/if-autres_frais}", "", $text);
            }
        }
        if (isset($stagiaire))
        {
            $text = str_replace("{fonction}", $tagS.lcfirst($stagiaire->entreprise_fonction) .$tagE, $text);
            $text = str_replace("{Fonction}", $tagS.ucfirst($stagiaire->entreprise_fonction) .$tagE, $text);
            
            $pluriel = (count($stagiaire->dates_array) > 1) ? "s" : "";
            $text = str_replace("{pluriel-dates}", $tagS.$pluriel.$tagE, $text);
            $text = str_replace("{dates}", $tagS.$stagiaire->dates_texte .$tagE, $text);
            $text = str_replace("{duree}", $tagS.$stagiaire->nb_heure." ".__("heures").$tagE, $text);
            $text = str_replace("{nb-heure}", $tagS.$stagiaire->nb_heure .$tagE, $text);
            $text = str_replace("{dates-stagiaire}", $tagS.$stagiaire->dates_texte.$tagE, $text);
            $text = str_replace("{nb-heure-stagiaire}", $tagS.$stagiaire->nb_heure_estime .$tagE, $text);
            
            $text = str_replace("{stagiaire}", $tagS.get_displayname($stagiaire->user_id, false).$tagE, $text);
            $text = str_replace("{monsieur-madame}", $tagS.lcfirst(str_or_array(get_user_meta($stagiaire->user_id, "genre", true))) .$tagE, $text);
            $text = str_replace("{Monsieur-Madame}", $tagS.ucfirst(str_or_array(get_user_meta($stagiaire->user_id, "genre", true))) .$tagE, $text);
        }
        
        if ($this->contexte & $wpof->doc_context->session)
        {
            $text = str_replace("{duree}", $tagS.$session_formation->nb_heure." ".__("heures") .$tagE, $text);
            $text = str_replace("{dates}", $tagS.$session_formation->dates_texte .$tagE, $text);
            $text = str_replace("{duree}", $tagS.$session_formation->nb_heure." ".__("heures") .$tagE, $text);
            $text = str_replace("{nb-heure}", $tagS.$session_formation->nb_heure .$tagE, $text);
            $text = str_replace("{tarif-chiffre}", $tagS.$session_formation->tarif_total_chiffre .$tagE, $text);
        }
        
        $text = str_replace("{formation-titre}", $tagS.$session_formation->titre_formation .$tagE, $text);
        $text = str_replace("{pre-requis}", $tagS.wpautop($session_formation->prerequis) .$tagE, $text);
        $text = str_replace("{objectifs}", $tagS.$session_formation->objectifs .$tagE, $text);
        $text = str_replace("{presentation-formation}", $tagS.wpautop($session_formation->presentation) .$tagE, $text);
        $text = str_replace("{programme}", $tagS.wpautop($session_formation->programme) .$tagE, $text);
        $text = str_replace("{materiel_pedagogique}", $tagS.wpautop($session_formation->materiel_pedagogique) .$tagE, $text);
        $text = str_replace("{public}", $tagS.$session_formation->public_cible .$tagE, $text);
        
        $text = str_replace("{formateur}", $tagS.join(", ", $this->get_liste_formateurs(false)) .$tagE, $text);
        $text = str_replace("{presentation-formateur}", $tagS.join("", $this->get_liste_formateurs(true)) .$tagE, $text);
        
        $text = str_replace("{monnaie-symbole}", $tagS.$wpof->monnaie_symbole  .$tagE, $text);
        $text = str_replace("{monnaie}", $tagS.$wpof->monnaie."s"  .$tagE, $text);
        
        if ($this->has_date)
            $text = str_replace("{today}", $tagS.pretty_print_dates(date("d/m/Y", $this->date)) .$tagE, $text);
        else
            $text = str_replace("{today}", $tagS.pretty_print_dates(date("d/m/Y", time())) .$tagE, $text);
        
        $text = str_replace("{date-fin}", $tagS.pretty_print_dates(end($session_formation->dates_array)) .$tagE, $text);
        
        if ($session_formation->lieu)
            $text = str_replace("{lieu}", $tagS.$session_formation->lieu_nom."<br />".nl2br($session_formation->lieu_adresse)."<br />".$session_formation->lieu_code_postal." ".$session_formation->lieu_ville .$tagE, $text);
        else
            $text = str_replace("{lieu}", $tagS.$session_formation->ville .$tagE, $text);
        
        
        $text = str_replace("{stagiaire}", "", $text);
        $text = str_replace("{monsieur-madame}", "", $text);
        $text = str_replace("{Monsieur-Madame}", "", $text);
        
        $text = str_replace("{nom-of}", $tagS.$wpof->of_nom .$tagE, $text);
        $text = str_replace("{no-of}", $tagS.$wpof->of_noof .$tagE, $text);
        $text = str_replace("{ville-of}", $tagS.$wpof->of_ville .$tagE, $text);
        $text = str_replace("{adresse-of}", $tagS.nl2br($wpof->of_adresse)."<br />".$wpof->of_code_postal ." ".$wpof->of_ville .$tagE, $text);
        $text = str_replace("{no-siret}", $tagS.$wpof->of_siret.$tagE, $text);
        /*
        $text = str_replace("{}", $tagS. .$tagE, $text);
        */
        
        return $text;
    }

}

/*
* Crée un tableau pour gérer les documents administratifs
*/
function get_gestion_docs($objet)
{
    global $Documents;
    global $wpof;
    
    // l'utilisateur courant peut-il signer les documents
    $signataire = is_signataire();
    
    // quelle est la source de documents ?
    $doc_necessaire = $objet->doc_necessaire;
    $doc_uid_suffix = "-{$objet->doc_suffix}";
    
    // si aucun document n'est nécessaire (j'en doute), on ne fait rien
    if (count($doc_necessaire) == 0) return "";
            
    $html = "";
    
    ob_start();
    ?>
    <table class='gestion-docs-admin'><tbody>
    <tr class="tr-titre">
        <th><?php _e("Document"); ?></th>
        <th><?php _e("Brouillon"); ?></th>
        <th><?php _e("Final"); ?></th>
        <?php if (!$signataire): ?>
            <th><?php _e("Signature responsable"); ?></th>
        <?php endif; ?>
        <th><?php _e("Diffuser"); ?></th>
        <th><?php _e("Scan"); ?></th>
        <th><?php _e("Supprimer"); ?></th>
    </tr>
    <?php
    
    $html .= ob_get_clean();
    
    // note : $doc->id = $doc_uid = $doc_id . $doc_uid_suffix
    foreach($doc_necessaire as $doc_id)
    {
        $doc =& $Documents[$doc_id.$doc_uid_suffix];
        $doc->signataire = $signataire;
        
        $html .= $doc->get_html_ligne();
    }
    
    $html .= "</tbody></table>";
    
    return $html;
}
