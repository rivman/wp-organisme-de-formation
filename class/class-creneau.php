<?php
/*
 * class-creneau.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

require_once(wpof_path . "/class/class-session-formation.php");

 
class Creneau
{
    // données simples telles que saisies via un formulaire
    public $date = "";
    public $heure_debut = "";
    public $heure_fin = "";
    
    // DateTime objets
    public $debut = null;
    public $fin = null;
    
    //  objet
    public $duree;
    public $titre;
    
    public $type = 'presentiel';
    public $session_id;
    
    public $activite_id = -1;
    public $module_id = -1;
    public $lieu_id = -1;
    public $lieu = "";
    public $lieu_nom = "";
    public $salle_id = -1;
    
    public $id = -1;
    private $table_suffix = "wpof_creneaux";
    
    public function __construct($id = -1)
    {
        if ($id > 0)
        {
            $this->id = $id;
            $this->init_from_db($id);
        }
    }
    
    public function init_from_form($data = array())
    {
        // TODO mettre un test sur les champs indispensables
        if (isset($data['date']))
        {
            $this->date = $data['date'];
            
            if (isset($data['heure_debut']))
            {
                $this->heure_debut = $data['heure_debut'];
                $this->debut = DateTime::createFromFormat("d/m/Y H:i", $data['date']." ".$data['heure_debut']);
            }
            if (isset($data['heure_fin']))
            {
                $this->heure_fin = $data['heure_fin'];        
                $this->fin = DateTime::createFromFormat("d/m/Y H:i", $data['date']." ".$data['heure_fin']);
                if (isset($this->debut))
                    $this->duree = $this->debut->diff($this->fin);
            }
        }
        
        if (isset($data['type']) && $data['type'] != "")
            $this->type = $data['type'];
            
        if (isset($data['session_id']) && $data['session_id'] != "")
            $this->session_id = $data['session_id'];

        if (isset($data['user_id']) && $data['user_id'] != "")
            $this->user_id = $data['user_id'];
            
        if (isset($data['activite_id']) && $data['activite_id'] != "")
            $this->activite_id = $data['activite_id'];
        
        if (isset($data['lieu_id']) && $data['lieu_id'] != "")
            $this->lieu_id = $data['lieu_id'];
        
        if (isset($data['lieu_nom']) && $data['lieu_nom'] != "")
            $this->lieu_nom = $data['lieu_nom'];
        
        if ($this->lieu_id == -1)
            $this->lieu_nom = get_post_meta($this->session_id, "lieu_nom", true);
        
        if (isset($data['salle_id']) && $data['salle_id'] != "")
            $this->salle_id = $data['salle_id'];
        
        $this->set_titre();
    }
    
    public function init_from_db($id)
    {
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        $query = $wpdb->prepare("SELECT * from $table WHERE id = '%d';", $id);
        $result = $wpdb->get_row($query);
        
        foreach(array('session_id', 'activite_id', 'module_id', 'lieu_id', 'salle_id', 'type', 'id') as $field)
            $this->$field = $result->$field;
        
        $this->debut = DateTime::createFromFormat("Y-m-d H:i:s", $result->date_debut);
        $this->fin = DateTime::createFromFormat("Y-m-d H:i:s", $result->date_fin);
        
        $this->date = $this->debut->format("d/m/Y");
        $this->heure_debut = $this->debut->format("H:i");
        $this->heure_fin = $this->fin->format("H:i");
        
        $this->duree = $this->debut->diff($this->fin);
        
        if ($this->lieu_id == -1)
            $this->lieu_nom = get_post_meta($this->session_id, "lieu_nom", true);
            
        $this->set_titre();
    }
    
    public function set_titre()
    {
        if (isset($this->debut) && isset($this->fin))
            $this->titre = $this->debut->format("H:i")." – ".$this->fin->format("H:i")." (".$this->duree->format("%h:%I").")";
        else
            $this->titre = __("Non défini");
    }
    
    public function set_date($date)
    {
        $this->date = $date;
        
        if ($this->heure_debut != "")
        {
            $this->debut = DateTime::createFromFormat("d/m/Y H:i", $this->date." ".$this->heure_debut);
            $this->fin = DateTime::createFromFormat("d/m/Y H:i", $this->date." ".$this->heure_fin);
        }
    }
    
    public function update()
    {
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        // Si aucun id, on insère le nouveau créneau dans la base et on récupère son id à la fin
        if ($this->id < 0)
        {
            $query = $wpdb->prepare
            ("INSERT INTO $table (session_id, activite_id, module_id, date_debut, date_fin, type, lieu_id, salle_id)
                VALUES ('%d', '%d', '%d', '%s', '%s', '%s', '%d', '%d')
                ON DUPLICATE KEY UPDATE date_fin = '%s', type = '%s', activite_id = '%d', module_id = '%d';",
                $this->session_id,
                $this->activite_id,
                $this->module_id,
                $this->debut->format("Y-m-d H:i:s"),
                $this->fin->format("Y-m-d H:i:s"),
                $this->type,
                $this->lieu_id,
                $this->salle_id,
                $this->fin->format("Y-m-d H:i:s"),
                $this->type,
                $this->activite_id,
                $this->module_id);
            //echo "<p>$query</p>";
            $res = $wpdb->query($query);
            //echo "<p>$res</p>";
            if ($res !== false)
            {
                $query = $wpdb->prepare
                ("SELECT id from $table WHERE date_debut = '%s' AND session_id = '%d' AND lieu_id = '%d' AND salle_id = '%d';",
                    $this->debut->format("Y-m-d H:i:s"),
                    $this->session_id,
                    $this->lieu_id,
                    $this->salle_id);
                $this->id = $wpdb->get_var($query);
                //echo "<p>$query</p>";
            }
        }
        // sinon, on met à jour le créneau
        else
        {
            $query = $wpdb->prepare
            ("UPDATE $table SET session_id = '%d', activite_id = '%d', module_id = '%d', date_debut = '%s', date_fin = '%s', type = '%s', lieu_id = '%d', salle_id = '%d'
                WHERE id = '%d';",
                $this->session_id,
                $this->activite_id,
                $this->module_id,
                $this->debut->format("Y-m-d H:i:s"),
                $this->fin->format("Y-m-d H:i:s"),
                $this->type,
                $this->lieu_id,
                $this->salle_id,
                $this->id);
            //echo $query;
            $res = $wpdb->query($query);
            //echo "<p>$res</p>";
        }
        //echo "<p>avant return $res</p>";
        if ($res === false)
            return $res;
        else
            return $this->id;
    }
    
    public function delete()
    {
        // supression du créneau du tableau creneaux de la session
        $session = get_session_by_id($this->session_id);
        
        unset($session->creneaux[$this->date][$this->id]);
        $session->update_meta("creneaux");
        
        // suppression du client dans la base de données
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        $query = $wpdb->prepare
        ("DELETE FROM $table
            WHERE id = '%d';",
            $this->id
        );
        
        return $wpdb->query($query);
    }
    
    public function get_html($class = "", $edit = false)
    {
        global $wpof;
        ob_start(); ?>
        
        <?php $id = ($this->id > 0) ? $this->id : "tmp".rand(); ?>  
        <div class="creneau <?php echo $this->type; ?> <?php echo $class; ?>" id="creno<?php echo $id; ?>" data-id="<?php echo $id; ?>">
        <?php
        $lieu_id = $lieu_nom = "";
        if ($this->session_id > 0)
        {
            $session = get_session_by_id($this->session_id);
            $lieu_id = $session->lieu;
            $lieu_nom = ($lieu_id == "-1") ? $session->lieu_nom : "";
        }
        
        if ($lieu_nom == "")
        {
            $lieu_nom = $this->lieu_nom;
            $lieu_id = $this->lieu_id;
        }
        ?>
            <?php if ($this->heure_debut != "") 
                {
                    if (!$edit)
                        echo "<span class='select-icon'></span><span class='select-icon dashicons dashicons-yes-alt'></span>";
                    echo $this->titre;
                    //echo " [{$this->id}]";
                }
            ?>
            <?php if ($edit): ?>
            <span class="dashicons dashicons-edit dynamic-dialog" data-function="add_or_edit_creneau" data-date="<?php echo $this->date; ?>" data-sessionid="<?php echo $this->session_id; ?>" data-crenoid="<?php echo $this->id; ?>"></span>
            <span class="dashicons dashicons-dismiss del-creneau"></span>
            
            <?php if (0) : // TODO : cette partie est obsolète ! ?>
            <div class="masque"></div>
            <div class="details">
                <?php echo hidden_input("creno_id[]", $id); ?>
                <label><?php _e("Heure de début"); ?> <input type="time" name="<?php echo $id; ?>_heure_debut" value="<?php echo $this->heure_debut; ?>" /></label>
                <label><?php _e("Heure de fin"); ?> <input type="time" name="<?php echo $id; ?>_heure_fin" value="<?php echo $this->heure_fin; ?>" /></label>
                <label><?php _e("Type"); ?> <?php echo select_by_list($wpof->type_creneau, $id."_type", $this->type); ?></label>
                <!-- <label><?php _e("Module"); ?> <?php echo select_by_list(array(), $id."_module_id", $this->module_id); ?></label>
                <label><?php _e("Activité"); ?> <?php echo select_by_list(array(), $id."_activite_id", $this->activite_id); ?></label> -->
                <!-- <label class="lieu-select"><?php _e("Lieu"); ?> <?php echo select_post_by_type("lieu", $id."_lieu_id", $this->lieu_id, ($lieu_id == -1) ? $lieu_nom : null); ?></label> -->
                <!-- <label><?php _e("Salle"); ?> <?php echo select_by_list(array(), $id."_salle_id", $this->salle_id); ?></label> -->
                <label><?php echo hidden_input($id."_date", $this->date); ?></label>
                <label><?php echo hidden_input($id."_session_id", $this->session_id); ?></label>
                <div><span class="valide-creneau bouton"><?php _e("Valider"); ?></span><span class="annule-creneau bouton"><?php _e("Annuler"); ?></span></div>
            </div>
            <?php endif; ?>
            
            <?php endif; ?>
        </div>
        <?php
        return ob_get_clean();
    }
    
    public function get_creneau_form()
    {
        global $wpof;
        $session = get_session_by_id($this->session_id);
        
        $html = "";
        $html .= hidden_input("creno_id", $this->id);
        $html .= get_input_jpost($this, 'heure_debut', array('input' => 'time', 'label' => __("Heure de début")));
        $html .= get_input_jpost($this, 'heure_fin', array('input' => 'time', 'label' => __("Heure de fin")));
        $html .= '<div class="input_jpost select"><label>'.__("Type").'</label><div class="select">'.select_by_list($wpof->type_creneau, "type", $this->type).'</div></div>';
        //$html .= get_input_jpost($this, 'type', array('select' => '', 'label' => __("Type")));
        //$html .= get_input_jpost($this, 'module', array('select' => '', 'label' => __("Module")));
        //$html .= get_input_jpost($this, 'activite', array('select' => '', 'label' => __("Activité")));
        
        init_term_list("lieu");
        $html .= hidden_input("default_lieu_id", $session->lieu);
        $first_lieu = ($session->lieu_nom != "") ? $session->lieu_nom : null;
        if ($first_lieu)
            $html .= get_input_jpost($this, 'lieu', array('select' => '', 'first' => $first_lieu, 'label' => __("Lieu")));
        //$html .= get_input_jpost($this, 'salle', array('select' => '', 'label' => __("Salle")));
        $html .= hidden_input("date", $this->date);
        
        return $html;
    }
    
};

?>
