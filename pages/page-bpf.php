<?php
/*
 * wpof-bpf.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

require_once(wpof_path . "/wpof-responsable-fonctions.php");

add_filter( 'the_posts', 'generate_bpf_page', -10 );
function generate_bpf_page($posts)
{
    global $wp, $wp_query, $wpof;

    $url_slug = $wpof->url_bpf; // slug de la page du BPF

    if (!defined('BPF_PAGE') && (strtolower($wp->request) == $url_slug))
    {
        define( 'BPF_PAGE', true );

        // create a fake virtual page
        $post = new stdClass;
        $post->post_author    = 1;
        $post->post_name      = $url_slug;
        $post->guid           = home_url() . '/' . $url_slug;
        $post->post_title     = $wpof->title_bpf;
        $post->post_content   = get_bpf_content();
        $post->ID             = -999;
        $post->post_type      = 'page';
        $post->post_status    = 'static';
        $post->comment_status = 'closed';
        $post->ping_status    = 'closed';
        $post->comment_count  = 0;
        $post->post_date      = current_time( 'mysql' );
        $post->post_date_gmt  = current_time( 'mysql', 1 );
        $posts                = NULL;
        $posts[]              = $post;

        // make wpQuery believe this is a real page too
        $wp_query->is_page             = true;
        $wp_query->is_singular         = true;
        $wp_query->is_home             = false;
        $wp_query->is_archive          = false;
        $wp_query->is_category         = false;
        unset( $wp_query->query[ 'error' ] );
        $wp_query->query_vars[ 'error' ] = '';
        $wp_query->is_404 = false;
    }

    return $posts;
}

function get_bpf_content()
{
    $role = wpof_get_role(get_current_user_id());
    
    if (!in_array($role, array("um_responsable", "admin")))
        return "";
    
    $annee_defaut = get_user_meta(get_current_user_id(), "annee_comptable", true);
    if (empty($annee_defaut))
        $annee_defaut = date('Y') - 1;
    
    $html = "";
    $html .= "<div id='annee_choix' data-id='bpf'>";
    $html .= get_choix_annee_comptable($annee_defaut);
    $html .= "</div>";
    
    $html .= "<div id='bpf' data-annee='$annee_defaut'>";
    $html .= get_bpf($annee_defaut);
    $html .= "</div>";
    return $html;
}

function get_bpf($annee)
{
    global $SessionFormation;
    global $wpof;
    set_session_annee_comptable($annee);
    
    $html = "";
    
    $session_ctl = array();
    $session_ctl[0] = array();
    
    $nb_stagiaires = $nb_stagiaires_st = 0;
    foreach($SessionFormation as $s)
    {
        $s->init_stagiaires();
        if ($s->type_index == "sous_traitance")
        {
            if (count($s->stagiaires) > 0)
                $nb_stagiaires_st += count($s->stagiaires);
            else
                $nb_stagiaires_st += $s->st_nb_stagiaire;
        }
        else
            $nb_stagiaires += count($s->stagiaires);
            
        if (!in_array($s->type_index, array_keys($wpof->type_session)))
            $session_ctl[0][] = get_session_numero($s);
        else
        {
            if (empty($session_ctl[$s->type_index]))
                $session_ctl[$s->type_index] = array();
            $session_ctl[$s->type_index][] = get_session_numero($s);
        }
    }
    $html .= "<ul>";
    $html .= "<li>".__("Nombre de sessions pour")." $annee : ".count($SessionFormation)."</li>";
    $html .= "<li>Nombre stagiaires en direct : $nb_stagiaires</li>";
    $html .= "<li>Nombre de stagiaires en sous-traitance : $nb_stagiaires_st</li>";
    foreach($wpof->type_session as $k => $val)
    {
        $html .= "<li>".__("Nombre de session")." $k : ".count($session_ctl[$k])." <span class='openButton' data-id='liste-$k'>".__("détails")."</span>";
        $html .= "<div class='blocHidden' id='liste-$k'>" .join(", ", $session_ctl[$k]) ."</div>";
        $html .= "</li>";
    }
    if (count($session_ctl[0]) > 0)
        $html .= "<li class='erreur'>".__("Nombre de session sans type")." : ".count($session_ctl[0])." → ".join(", ", $session_ctl[0])."</li>";
    
    $html .= "</ul>";
    
    $html .= get_bpf_c($annee);
    $html .= get_bpf_d();
    $html .= get_bpf_e();
    $html .= get_bpf_f1();
    $html .= get_bpf_f2();
    $html .= get_bpf_f3();
    $html .= get_bpf_f4();
    $html .= get_bpf_g();
    
    return $html;
}

// Bilan financier HT : origine des produits de l'OF
function get_bpf_c($annee)
{
    global $wpof;
    global $SessionFormation;
    
    $total_produits = 0;
    $financement = array();
    foreach(array_keys($wpof->financement->term) as $k)
        $financement[$k] = 0;
    $fin_ctl = array();
    
    foreach($SessionFormation as $session)
    {
        switch ($session->type_index)
        {
            case 'inter':
                $fin_ctl[$session->id] = array();
                foreach($session->stagiaires as $stagiaire)
                {
                    if (!isset($financement[$stagiaire->financement]))
                        $financement[$stagiaire->financement] = 0;
                    $financement[$stagiaire->financement] += $stagiaire->exe_comptable[$annee];
                    $fin_ctl[$session->id][$stagiaire->user_id] = $stagiaire->financement;
                }
                break;
            case 'intra':
                if (!isset($financement[$session->financement]))
                    $financement[$session->financement] = 0;
                $financement[$session->financement] += $session->exe_comptable[$annee] - (integer) ($session->tarif_total_autres_chiffre);
                
                $financement['autres'] += (integer) ($session->tarif_total_autres_chiffre);
                $fin_ctl[$session->id] = $session->financement;
                break;
            case 'sous_traitance':
                $financement['opac'] += $session->exe_comptable[$annee];
                break;
        }
    }
    
    ob_start();
    ?>
    <h2><?php _e("C. Bilan financier hors taxes : origine des produits de l'organisme"); ?></h2>
    
    <?php
        $sous_total = $financement["prive"];
        $total_produits += $sous_total;
    ?>
    <p class="value"><?php echo $wpof->financement->get_term("prive"); ?><span class="bpf_right">1. </span><span class="bpf_value"><?php echo $sous_total; ?></span></p>
    
    <div class="bpf_group">
        <p class="groupe"><?php echo $wpof->financement->group["g1"]; ?></p>
        
        <?php
            $lettre = array('', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h');
            $total_mutu = 0;
        ?>
        <?php for($i = 1; $i <= 8; $i++) : ?>
            <?php
                $sous_total_mutu = $financement["mutu".$i];
                $total_mutu += $sous_total_mutu;
            ?>
            <p class="sous_value"><?php echo $wpof->financement->get_term("mutu".$i); ?><span class="bpf_right"><?php echo $lettre[$i]; ?>. </span><span class="bpf_value"><?php echo $sous_total_mutu; ?></span></p>
        <?php endfor; ?>
        
        <?php
            $total_produits += $total_mutu;
        ?>
        <p class="value"><?php echo __("Total")." ".$wpof->financement->group["g1"]." ".__("(total des lignes a à h)"); ?> <span class="bpf_right">2. </span><span class="bpf_value"><?php echo $total_mutu; ?></span></p>
    </div>
    
    <?php
        $sous_total = $financement["public"];
        $total_produits += $sous_total;
    ?>
    <p class="value"><?php echo $wpof->financement->get_term("public"); ?><span class="bpf_right">3. </span><span class="bpf_value"><?php echo $sous_total; ?></span></p>
    
    <div class="bpf_group">
        <p class="groupe"><?php echo $wpof->financement->group["g2"]; ?></p>
        
        <?php
            $total_pubspec = 0;
        ?>
        <?php for($i = 1; $i <= 5; $i++) : ?>
            <?php
                $sous_total_pubspec = $financement["pubspec".$i];
                $total_pubspec += $sous_total_pubspec;
            ?>
            <p class="sous_value"><?php echo $wpof->financement->get_term("pubspec".$i); ?><span class="bpf_right"><?php echo $i+3; ?>. </span><span class="bpf_value"><?php echo $sous_total_pubspec; ?></span></p>
        <?php endfor; ?>
        
        <?php
            $total_produits += $total_pubspec;
        ?>
    </div>
    
    <?php
        $sous_total = $financement["part"];
        $total_produits += $sous_total;
    ?>
    <p class="value"><?php echo $wpof->financement->get_term("part"); ?><span class="bpf_right">9. </span><span class="bpf_value"><?php echo $sous_total; ?></span></p>
    
    <?php
        $sous_total = $financement["opac"];
        $total_produits += $sous_total;
    ?>
    <p class="value"><?php echo $wpof->financement->get_term("opac"); ?><span class="bpf_right">10. </span><span class="bpf_value"><?php echo $sous_total; ?></span></p>
    
    <?php
        $sous_total = $financement["autres"];
        $total_produits += $sous_total;
    ?>
    <p class="value"><?php echo $wpof->financement->get_term("autres"); ?><span class="bpf_right">11. </span><span class="bpf_value"><?php echo $sous_total; ?></span></p>
    
    <p class="total"><?php _e("Total des produits réalisés au titre de la formation professionnelle (total des lignes 1 à 11)"); ?>
    <span class="bpf_value"><?php 
        echo $total_produits;
        if ($total_produits != array_sum($financement))
            echo "<br /><span class='erreur'>(+ ".(array_sum($financement) - $total_produits)." ".__("à réaffecter").")</span>";
    ?></span>
    </p>
    <?php
    
    // contrôle : liste par type de financement réellement indiqués dans les sessions (permet de vérifier les infos erronées)
    ?>
    <div class="bpf_ctl">
    <p><span class="openButton" data-id="liste-financement"><?php _e("Voir les erreurs d'affectation"); ?></span></p>
    <table class="blocHidden" id="liste-financement">
    <tr><td><?php _e("Mode de financement actuel"); ?></td><td><?php _e("Session ID"); ?></td><td><?php _e("Stagiaire"); ?></td></tr>
    <?php
        foreach($fin_ctl as $session => $fin) :
            if (is_array($fin))
                foreach($fin as $user_id => $f)
                {
                    if (!$wpof->financement->is_term($f)) : ?>
                        <tr>
                        <td><?php echo "[$f]"; ?></td>
                        <td><a href="<?php echo $SessionFormation[$session]->permalien; ?>"><?php echo ($SessionFormation[$session]->numero != "") ? $SessionFormation[$session]->numero : $session; ?></a> (inter)</td>
                        <td><?php echo get_displayname($user_id, false); ?></td>
                        </tr>
                    <?php endif;
                }
            else
            {
                if (!$wpof->financement->is_term($fin)) : ?>
                    <tr>
                    <td><?php echo "[$fin]"; ?></td>
                    <td><a href="<?php echo $SessionFormation[$session]->permalien; ?>"><?php echo $session; ?></a> (intra)</td>
                    <td> — </td>
                    </tr>
                <?php endif;
            }
        endforeach; ?>
    <tr class="total"><td colspan="2"><?php _e("Total mauvaise affectation"); ?></td><td><?php echo array_sum($financement) - $total_produits; ?></td></tr>
    <tr class="total"><td colspan="2"><?php _e("Total tout"); ?></td><td><?php echo array_sum($financement); ?></td></tr>
    </table>
    </div>
    <?php
    return ob_get_clean();
}

// Bilan financier HT : charges de l'OF
function get_bpf_d()
{
    ob_start();
    return ob_get_clean();
}

// Personnes dispensant des heures de formation
function get_bpf_e()
{
    global $wpof;
    global $SessionFormation;
    
    $formateur_interne = array();
    $formateur_st = array();
    
    $session_ctl = array();
    
    foreach($SessionFormation as $session)
    {
        foreach($session->formateur as $f)
        {
            if (empty(get_user_meta($f, "sous_traitant", true)))
            {
                if (isset($formateur_interne[$f]))
                    $formateur_interne[$f] += $session->nb_heure_decimal;
                else
                    $formateur_interne[$f] = $session->nb_heure_decimal;
            }
            else
            {
                if (!isset($formateur_st[$f]))
                    $formateur_st[$f] = 0;
                    
                $formateur_st[$f] += $session->nb_heure_decimal;
            }
                
            if (empty($session_ctl[$f]))
                $session_ctl[$f] = array();
            $session_ctl[$f][$session->id] = $session->nb_heure_decimal;
        }
    }
    
    ob_start();
    ?>
    <h2><?php _e("E. Personnes dispensant des heures de formation"); ?></h2>
    <p><strong>Attention</strong> lorsque qu'une session est animée par plus d'un formateur, la durée de la formation est attribuée à chacun, pouvant entraîner des comptes multiples de temps ! À corriger à la main pour l'instant</p>
    <table>
    <tr><td></td><td><?php _e("Nombre de formateurs"); ?></td><td><?php _e("Nombre d'heures de formation"); ?></td></tr>
    <tr>
    <td><?php _e("Personnes de votre organisme dispensant des heures de formation"); ?></td>
    <td><?php echo count($formateur_interne); ?></td>
    <td><?php echo array_sum($formateur_interne); ?></td>
    </tr>
    <tr>
    <td><?php _e("Personnes extérieures à votre organisme dispensant des heures de formation dans le cadre de contrats de sous-traitance"); ?></td>
    <td><?php echo count($formateur_st); ?></td>
    <td><?php echo array_sum($formateur_st); ?></td>
    </tr>
    </table>
    
    <?php
    
    // contrôle : liste des formateurs interne et externe
    ?>
    <div class="bpf_ctl">
    <p><span class="openButton" data-id="liste-formateur"><?php _e("Voir les formateurs"); ?></span></p>
    <div class="blocHidden" id="liste-formateur">
    <p><?php _e("Définissez les formateurs sous-traitants depuis la page"); ?> <a href="/equipe-pedagogique/"><?php _e("Équipe pédagogique"); ?></a></p>
    <table>
    <tr><td><?php _e("Formateur⋅trice"); ?></td><td><?php _e("Session (nb h)"); ?></td><td><?php _e("Nombre d'heures"); ?></td></tr>
    <?php foreach($formateur_interne as $user_id => $nb_heures) : ?>
    <tr><td><?php echo get_displayname($user_id, false); ?> <strong>(<?php _e("interne"); ?>)</strong></td>
    <td>
        <?php
        $liste_session_heure = array();
        foreach ($session_ctl[$user_id] as $sid => $nbh)
        {
            $link = "<a href=".$SessionFormation[$sid]->permalien.">";
            $link .= ($SessionFormation[$sid]->numero != "") ? $SessionFormation[$sid]->numero : $sid;
            $link .= "</a> ($nbh)";
            $liste_session_heure[] = $link;
        }
        echo join("<br />", $liste_session_heure);
        ?>
    </td>
    <td><?php echo $nb_heures; ?></td></tr>
    <?php endforeach; ?>
    <?php foreach($formateur_st as $user_id => $nb_heures) : ?>
    <tr><td><?php echo get_displayname($user_id, false); ?> <em>(<?php _e("externe"); ?>)</em></td>
    <td>
        <?php
        $liste_session_heure = array();
        foreach ($session_ctl[$user_id] as $sid => $nbh)
        {
            $link = "<a href=".$SessionFormation[$sid]->permalien.">";
            $link .= ($SessionFormation[$sid]->numero != "") ? $SessionFormation[$sid]->numero : $sid;
            $link .= "</a> ($nbh)";
            $liste_session_heure[] = $link;
        }
        echo join("<br />", $liste_session_heure);
        ?>
    </td>
    <td><?php echo $nb_heures; ?></td></tr>
    <?php endforeach; ?>
    </table>
    </div>
    </div>
    <?php
    return ob_get_clean();
}

// Type de stagiaire de l'OF
function get_bpf_f1()
{
    global $wpof;
    global $SessionFormation;
    
    // tableau pour remonter les durées nulles
    $erreur_duree = array();
    
    $statut_stagiaire = array();
    foreach(array_keys($wpof->statut_stagiaire->term) as $k)
        $statut_stagiaire[$k] = array();
    $statut_stagiaire["Non défini"] = array();
    $total_stagiaires = $total_heures = 0;
    
    foreach($SessionFormation as $session)
    {
        if ($session->type_index != "sous_traitance")
        {
            $session->init_stagiaires();
            foreach($session->stagiaires as $stagiaire)
            {
                if (!empty($stagiaire->statut_stagiaire))
                    $statut_stagiaire[$stagiaire->statut_stagiaire][$stagiaire->id] = $stagiaire->nb_heure_estime_decimal;
                else
                    $statut_stagiaire["Non défini"][$stagiaire->id] = $stagiaire->nb_heure_estime_decimal;
                $total_stagiaires++;
                $total_heures += $stagiaire->nb_heure_estime_decimal;
                
                if ($stagiaire->nb_heure_estime_decimal == 0)
                {
                    if (empty($erreur_duree[$session->id]))
                        $erreur_duree[$session->id] = array();
                    $erreur_duree[$session->id][] = get_displayname($stagiaire->user_id, false);
                }
            }
        }
    }
    
    $total_stagiaires_affiche = $total_heures_affiche = 0;
    ob_start();
    ?>
    <h2><?php _e("F1. Type de stagiaires de l'organisme"); ?></h2>
    
    <table>
    <tr><td></td><td><?php _e("Nombre de stagiaires ou apprentis"); ?></td><td><?php _e("Nombre d'heures de formation suivies par les stagiaires et apprentis"); ?></td></tr>
    <?php foreach(array_keys($wpof->statut_stagiaire->term) as $k) : ?>
    <tr>
    <td><?php echo $wpof->statut_stagiaire->get_term($k); ?></td>
    <td><?php $total_stagiaires_affiche += count($statut_stagiaire[$k]); echo count($statut_stagiaire[$k]); ?></td>
    <td><?php $total_heures_affiche += array_sum($statut_stagiaire[$k]); echo array_sum($statut_stagiaire[$k]); ?></td>
    </tr>
    <?php endforeach; ?>
    <tr class="total">
    <td><?php _e("Total"); ?></td><td><?php echo $total_stagiaires; ?></td><td><?php echo $total_heures; ?></td></tr>
    <?php if ($total_heures != $total_heures_affiche) : ?>
    <tr class="erreur"><td><?php _e("Dont total des mauvaises affectations (à corriger)"); ?></td><td><?php echo $total_stagiaires - $total_stagiaires_affiche; ?></td><td><?php echo $total_heures - $total_heures_affiche; ?></td></tr>
    <?php endif; ?>
    </table>
    
    <div class="bpf_ctl">
    <?php if (!empty($erreur_duree)) : ?>
    <p class="bg-erreur"><span class="openButton" data-id="erreur-duree-stagiaire"><?php _e("Attention ! certains stagiaires ont un nombre d'heures égal à 0"); ?></span></p>
    <div class="blocHidden" id="erreur-duree-stagiaire">
    <table>
    <?php foreach ($erreur_duree as $sid => $stag_zero) : ?>
    <tr><td><?php echo join(", ", $stag_zero); ?></td>
    <td><a href="<?php echo $SessionFormation[$sid]->permalien; ?>"><?php echo ($SessionFormation[$sid]->numero != "") ? $SessionFormation[$sid]->numero : $sid; ?></td>
    </tr>
    <?php endforeach; ?>
    </table>
    </div>
    <?php endif; ?>
    <p><span class="openButton" data-id="liste-type-stagiaire"><?php _e("Voir les types de stagiaire mal affectés"); ?></span></p>
    <div class="blocHidden" id="liste-type-stagiaire">
    <p><?php _e("Définissez les types de stagiaire depuis la page"); ?> <a href="<?php $wpof->url_pilote; ?>"><?php echo $wpof->title_pilote; ?></a>. <?php _e("Ouvrez les listes de stagiaires à l'aide des boutons « Stagiaires »"); ?>.</p>
    <table>
    <tr><td><?php _e("Statut invalide ou ancien"); ?></td><td><?php _e("Session ID"); ?></td><td><?php _e("Stagiaire"); ?></td></tr>
    <?php foreach($statut_stagiaire as $k => $val) : ?>
        <?php if (!$wpof->statut_stagiaire->is_term($k)) : ?>
            <?php foreach(array_keys($val) as $id) : ?>
            <?php $id = explode("-", $id); ?>
            <tr>
            <td><?php echo "[$k]"; ?></td>
            <td><a href="<?php echo $SessionFormation[$id[0]]->permalien; ?>"><?php echo ($SessionFormation[$id[0]]->numero != "") ? $SessionFormation[$id[0]]->numero : $id[0]; ?></a></td>
            <td><?php echo get_displayname($id[1], false); ?></td>
            </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    <?php endforeach; ?>
    </table>
    </div>
    </div>
    <?php
    return ob_get_clean();
}

// dont activité est sous-traitée de l'OF (formateur sous-traitant)
function get_bpf_f2()
{
    global $wpof;
    global $SessionFormation;
    
    $session_st = array();
    $total_stagiaires = $total_heures = 0;
    
    foreach($SessionFormation as $session)
    {
        foreach($session->formateur as $f)
        {
            if (!empty(get_user_meta($f, "sous_traitant", true)))
            {
                $session->init_stagiaires();
                // on fait un classement par session pour une vérif ultérieure
                if (!isset($session_st[$session->id]))
                    $session_st[$session->id] = array("stagiaire" => 0, "heure" => 0);
                foreach($session->stagiaires as $s)
                {
                    $session_st[$session->id]["heure"] += $s->nb_heure_estime_decimal;
                }
                $session_st[$session->id]["stagiaire"] = count($session->stagiaires);
                $total_stagiaires += $session_st[$session->id]["stagiaire"];
                $total_heures += $session_st[$session->id]["heure"];
            }
        }
    }

    ob_start();
    ?>
    <h2><?php _e("F2. Dont activité sous-traitée"); ?></h2>
    <table>
    <tr>
    <td></td><td><?php _e("Nombre de stagiaires ou d'apprentis"); ?></td><td><?php _e("Nombre d'heures de formation suivies par les stagiaires et apprentis"); ?></td></tr>
    <tr>
    <td><?php _e("Stagiaire ou apprentis dont l'action a été confiée par votre organisme à un autre organisme"); ?></td>
    <td><?php echo $total_stagiaires; ?></td>
    <td><?php echo $total_heures; ?></td>
    </tr>
    </table>
    
    <div class="bpf_ctl">
    <p><span class="openButton" data-id="liste-sous-traitant"><?php _e("Voir par session"); ?></span></p>
    <div class="blocHidden" id="liste-sous-traitant">
    <table>
    <tr><td><?php _e("Session ID"); ?></td><td><?php _e("Nombre de stagiaires"); ?><td><?php _e("nombre d'heures"); ?></td></tr>
    <?php foreach($session_st as $k => $v) : ?>
    <tr>
    <td><a href="<?php echo $SessionFormation[$k]->permalien ?>"><?php echo ($SessionFormation[$k]->numero != "") ? $SessionFormation[$k]->numero : $k; ?></a></td>
    <td><?php echo $v["stagiaire"]; ?></td>
    <td><?php echo $v["heure"]; ?></td>
    </tr>
    <?php endforeach; ?>
    </table>
    </div>
    </div>
    <?php
    return ob_get_clean();
}

// Objectif général des prestations dispensées
function get_bpf_f3()
{
    global $wpof;
    global $SessionFormation;
    
    $total_stagiaires = $total_heures = 0;
    $nature_formation = array();
    $nat_ctl = array();
    foreach(array_keys($wpof->nature_formation->term) as $k)
        $nature_formation[$k] = array();
    $nature_formation["Non défini"] = array();
    
    foreach($SessionFormation as $session)
    {
        $session->init_stagiaires();
        if ($session->type_index == 'inter')
        {
            $nat_ctl[$session->id] = array();
            foreach($session->stagiaires as $stagiaire)
            {
                if (!isset($stagiaire->nature_formation))
                    $session->nature_formation = "Non défini";
                $nat_ctl[$session->id][$stagiaire->user_id] = $session->nature_formation;
                
                if (!isset($nature_formation[$stagiaire->nature_formation][$stagiaire->id]))
                    $nature_formation[$stagiaire->nature_formation][$stagiaire->id] = 0;
                $nature_formation[$stagiaire->nature_formation][$stagiaire->id] += $stagiaire->nb_heure_estime_decimal;
                $total_stagiaires ++;
                $total_heures += $stagiaire->nb_heure_estime_decimal;
            }
        }
        elseif ($session->type_index == 'intra')
        {
            if (empty($session->nature_formation))
                $session->nature_formation = "Non défini";
            $nat_ctl[$session->id] = $session->nature_formation;
            
            foreach($session->stagiaires as $stagiaire)
            {
                if (!isset($nature_formation[$session->nature_formation][$stagiaire->id]))
                    $nature_formation[$session->nature_formation][$stagiaire->id] = 0;
                $nature_formation[$session->nature_formation][$stagiaire->id] += $stagiaire->nb_heure_estime_decimal;
                $total_stagiaires ++;
                $total_heures += $stagiaire->nb_heure_estime_decimal;
            }
        }
    }
  
    $total_stagiaires_affiche = $total_heures_affiche = 0;
    ob_start();
    ?>
    <h2><?php _e("F3. Objectif général des prestations dispensées"); ?></h2>
    <table>
    <tr>
    <td></td><td><?php _e("Nombre de stagiaires ou d'apprentis"); ?></td><td><?php _e("Nombre d'heures de formation suivies par les stagiaires et apprentis"); ?></td></tr>
    <?php foreach(array_keys($wpof->nature_formation->term) as $k) : ?>
    <tr>
    <td><?php echo $wpof->nature_formation->get_term($k); ?></td>
    <td><?php $total_stagiaires_affiche += count($nature_formation[$k]); echo count($nature_formation[$k]); ?></td>
    <td><?php $total_heures_affiche += array_sum($nature_formation[$k]); echo array_sum($nature_formation[$k]); ?></td>
    </tr>
    <?php endforeach; ?>
    <tr class="total">
    <td><?php _e("Total"); ?></td><td><?php echo $total_stagiaires; ?></td><td><?php echo $total_heures; ?></td></tr>
    <?php if ($total_heures_affiche != $total_heures) : ?>
    <tr class="erreur">
    <td><?php _e("Dont total des mauvaises affectations (à corriger)"); ?></td><td><?php echo $total_stagiaires - $total_stagiaires_affiche; ?></td><td><?php echo $total_heures - $total_heures_affiche; ?></td></tr>
    <?php endif; ?>
    </table>
    
    <div class="bpf_ctl">
    <p><span class="openButton" data-id="liste-nature"><?php _e("Voir également les mauvaises affectations"); ?></span></p>
    <div class="blocHidden" id="liste-nature">
    <p><?php _e("Définissez les objectifs de prestation (nature de formation) depuis la page"); ?> <a href="<?php $wpof->url_pilote; ?>"><?php echo $wpof->title_pilote; ?></a>. <?php _e("Ouvrez les listes de stagiaires à l'aide des boutons « Stagiaires »"); ?>.</p>
    <table>
    <tr><td><?php _e("Objectif de prestation invalide ou ancien"); ?></td><td><?php _e("Session ID"); ?></td><td><?php _e("Stagiaire"); ?></td></tr>
    <?php foreach($nat_ctl as $session_id => $k) : ?>
        <?php if (!$wpof->statut_stagiaire->is_term($k)) : ?>
            <?php if (is_array($k)) : ?>
                <?php foreach($k as $user_id => $key) : ?>
                <tr>
                <td><?php echo "[$key]"; ?></td>
                <td><a href="<?php echo $SessionFormation[$session_id]->permalien; ?>"><?php echo ($SessionFormation[$session_id]->numero != "") ? $SessionFormation[$session_id]->numero : $session_id; ?></a> (inter)</td>
                <td><?php echo get_displayname($user_id, false); ?></td>
                </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                <td><?php echo "[$k]"; ?></td>
                <td><a href="<?php echo $SessionFormation[$session_id]->permalien; ?>"><?php echo ($SessionFormation[$session_id]->numero != "") ? $SessionFormation[$session_id]->numero : $session_id; ?></a> (intra)</td>
                <td><?php echo "—"; ?></td>
                </tr>
            <?php endif; ?>
        <?php endif; ?>
    <?php endforeach; ?>
    </table>
    </div>
    </div>
    <?php
    return ob_get_clean();
}

// Spécialités de formation
function get_bpf_f4($tri = "heures")
{
    global $wpof;
    global $SessionFormation;
    
    $specialites = array();
    $specialites["Non défini"] = array();
    $spec_ctl = array();
    $spec_ctl["Non défini"] = array();
    
    foreach($SessionFormation as $session)
    {
        if ($session->type_index != "sous_traitance")
        {
            $spec = "";
            if ($session->formation_id > 0)
                $spec = get_post_meta($session->formation_id, "specialite", true);
            elseif (!empty($session->specialite))
                $spec = $session->specialite;
            if (empty($spec))
                $spec = "Non défini";
            
            $session->init_stagiaires();
            foreach($session->stagiaires as $stagiaire)
            {
                if (!isset($specialites[$spec][$stagiaire->id]))
                    $specialites[$spec][$stagiaire->id] = 0;
                if (!isset($spec_ctl[$spec][$session->id]))
                    $spec_ctl[$spec][$session->id] = 0;
                    
                $specialites[$spec][$stagiaire->id] += $stagiaire->nb_heure_estime_decimal;
                $spec_ctl[$spec][$session->id] += $stagiaire->nb_heure_estime_decimal;
            }
        }
    }
    
    // on trie ensuite le tableau $specialites par stagiaires et par heures
    $spec_stagiaires = array();
    $spec_heures = array();
    
    $nb_spec = 5;
    
    foreach($specialites as $k => $val)
    {
        $spec_stagiaires[$k] = count($val);
        $spec_heures[$k] = array_sum($val);
    }
    arsort($spec_stagiaires);
    arsort($spec_heures);
    ksort($spec_ctl);
    
    $total_stagiaires = array_sum($spec_stagiaires);
    $total_heures = array_sum($spec_heures);
    
    $spec_ref = ($tri == "stagiaires") ? $spec_stagiaires : $spec_heures;
    
    ob_start();
    ?>
    <h2><?php _e("F4. Spécialités de formation"); ?></h2>
    <p class="value"><?php _e("Cinq principales spécialités de formation"); ?></p>
    <table>
    <tr>
    <td></td><td><?php _e("Nombre de stagiaires ou d'apprentis"); ?></td><td><?php _e("Nombre d'heures de formation suivies par les stagiaires et apprentis"); ?></td></tr>
    <?php foreach(array_keys(array_slice($spec_ref, 0, $nb_spec, true)) as $k) : ?>
    <tr>
    <td><?php echo $wpof->specialite->get_term($k); ?></td>
    <td><?php echo $spec_stagiaires[$k]; ?></td>
    <td><?php echo $spec_heures[$k]; ?></td>
    </tr>
    <?php endforeach; ?>
    <tr>
    <td><?php _e("Autres spécialités"); ?></td>
    <td><?php echo array_sum(array_intersect_key($spec_stagiaires, array_slice($spec_ref, $nb_spec, NULL, true))); ?></td>
    <td><?php echo array_sum(array_intersect_key($spec_heures, array_slice($spec_ref, $nb_spec, NULL, true))); ?></td>
    </tr>
    <tr class="total">
    <td><?php _e("Total"); ?></td><td><?php echo $total_stagiaires; ?></td><td><?php echo $total_heures; ?></td></tr>
    </table>
    
    <div class="bpf_ctl">
    <p><span class="openButton" data-id="liste-specialite"><?php _e("Voir les autres spécialités et le détail par session"); ?></span>
    <?php if (!empty($spec_ctl["Non défini"])) : ?>
        <span class="erreur"><?php _e("Des formations et/ou des sessions n'ont pas de spécialité définie !"); ?></span>
    <?php endif; ?>
    </p>
    <div class="blocHidden" id="liste-specialite">
    <p><?php echo __("Pour les formations du catalogue, les spécialités peuvent être affectées depuis la page")." <a href='/formations/'>".__("Formations")."</a>"; ?>.</p>
    <h3><?php _e("Détails des autres spécialités"); ?></h3>
    <table>
    <tr>
    <td></td><td><?php _e("Nombre de stagiaires ou d'apprentis"); ?></td><td><?php _e("Nombre d'heures de formation suivies par les stagiaires et apprentis"); ?></td></tr>
    <?php foreach(array_keys(array_slice($spec_ref, $nb_spec, NULL, true)) as $k) : ?>
    <tr>
    <td><?php echo $wpof->specialite->get_term($k); ?></td>
    <td><?php echo $spec_stagiaires[$k]; ?></td>
    <td><?php echo $spec_heures[$k]; ?></td>
    </tr>
    <?php endforeach; ?>
    </table>
    
    <h3><?php _e("Détail par session"); ?></h3>
    <table>
    <tr>
    <td><?php _e("Spécialité"); ?></td>
    <td><?php _e("ID session ou formation"); ?></td>
    <td><?php _e("Nombre de stagiaires"); ?><td>
    <?php _e("nombre d'heures"); ?></td>
    </tr>
    <?php foreach($spec_ctl as $key => $session) : ?>
        <?php foreach($session as $id => $h) : ?>
        <tr>
        <td><?php echo $wpof->specialite->get_term($key); ?></td>
        <?php
            $session_num = "";
            if ($SessionFormation[$id]->formation_id > 0)
            {
                $url = get_edit_post_link($SessionFormation[$id]->formation_id);
                $text = "F – ".$SessionFormation[$id]->formation_id;
                $session_num .= " → ";
                $session_num .= ($SessionFormation[$id]->numero != "") ? $SessionFormation[$id]->numero : "S - ".$id;
            }
            else
            {
                $url = get_edit_post_link($id);
                $text = ($SessionFormation[$id]->numero != "") ? $SessionFormation[$id]->numero : "S - ".$id;
            }
        ?>
        <td><a href="<?php echo $url; ?>"><?php echo $text; ?></a> <em><?php echo $session_num; ?></em></td>
        <td><?php echo count($SessionFormation[$id]->inscrits); ?></td>
        <td><?php echo $h; ?></td>
        </tr>
        <?php endforeach; ?>
    <?php endforeach; ?>
    </table>
    </div>
    </div>
    <?php
    return ob_get_clean();
}

// Stagiaires dont la formation a été confiée à votre OF par un autre OF (votre OF est sous-traitant d'un autre OF)
function get_bpf_g()
{
    global $SessionFormation;
    
    $sous_traitance = array();
    
    $total_stagiaires = $total_heures = 0;
    
    foreach($SessionFormation as $s)
    {
        $nb_stagiaires = $nb_heures = 0;
        if ($s->type_index == "sous_traitance")
        {
            if (count($s->inscrits) > 0)
            {
                $s->init_stagiaires();
                $nb_stagiaires += count($s->stagiaires);
                foreach($s->stagiaires as $stagiaire)
                    $nb_heures += $stagiaire->nb_heure_decimal;
            }
            else
            {
                $nb_stagiaires = $s->st_nb_stagiaire;
                $nb_heures = $s->st_nb_stagiaire * $s->nb_heure_decimal;
            }
            
            $sous_traitance[$s->id] = array("stagiaire" => $nb_stagiaires, "heure" => $nb_heures);
        }
        $total_heures += $nb_heures;
        $total_stagiaires += $nb_stagiaires;
    }
    
    ob_start();
    ?>
    <h2><?php _e("G. Stagiaires dont la formation a été confiée à votre organisme par un autre OF"); ?></h2>
    <table>
    <tr>
    <td></td><td><?php _e("Nombre de stagiaires ou d'apprentis"); ?></td><td><?php _e("Nombre d'heures de formation suivies par les stagiaires et apprentis"); ?></td></tr>
    <tr>
    <td><?php _e("Formations confiées à votre organisme par un autre organisme de formation"); ?></td>
    <td><?php echo $total_stagiaires; ?></td>
    <td><?php echo $total_heures; ?></td>
    </tr>
    </table>
    
    <div class="bpf_ctl">
    <p><span class="openButton" data-id="liste-sous-traite"><?php _e("Voir par session"); ?></span></p>
    <div class="blocHidden" id="liste-sous-traite">
    <table>
    <tr><td><?php _e("ID session"); ?></td><td><?php _e("Nombre de stagiaires"); ?><td><?php _e("nombre d'heures"); ?></td></tr>
    <?php foreach($sous_traitance as $k => $v) : ?>
    <tr>
    <td><a href="<?php echo $SessionFormation[$k]->permalien ?>"><?php echo ($SessionFormation[$k]->numero != "") ? $SessionFormation[$k]->numero : $k; ?></a></td>
    <td><?php echo $v["stagiaire"]; ?></td>
    <td><?php echo $v["heure"]; ?></td>
    </tr>
    <?php endforeach; ?>
    </table>
    </div>
    </div>
    <?php
    return ob_get_clean();
}

?>
