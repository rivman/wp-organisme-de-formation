<?php
/*
 * page-pilote.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

require_once(wpof_path . "/wpof-responsable-fonctions.php");

$session_keys = array
(
    'id' => array('text' => __('ID')),
    'titre_session' => array('text' => __('Intitulé')),
    'dates_texte' => array('text' => __('Dates')),
    'formateur' => array('text' => __('Formateur⋅trice(s)')),
    'nb_heure_estime_decimal' => array('text' => __('Durée réelle ou estimée (h)')),
    'budget_global' => array('text' => __('Budget global')),
    'inscrits' => array('text' => __('Stagiaires')),
);

$client_keys = array
(
    'nom' => array('text' => __('Client')),
    'exe_comptable' => array('text' => __('Exercice comptable')),
    'financement' => array('text' => __('Financement')),
    'nature_formation' => array('text' => __('Objectif prestation')),
    'tarif_heure' => array('text' => __('Tarif horaire')),
    'tarif_total_chiffre' => array('text' => __('Tarif total')),
    'tarif_total_autres_chiffre' => array('text' => __('Autres frais (montant)')),
    'stagiaires' => array('text' => __("Stagiaires")),
);

$stagiaire_keys = array
(
    'user_id' => array('text' => __('Stagiaire')),
    'statut_stagiaire' => array('text' => __('Statut')),
//    'emarge' => array('text' => __('Émarge')),
    'nb_heure_estime_decimal' => array('text' => __('Durée réelle ou estimée (h)')),
);

add_filter('the_posts', 'generate_pilote_page', -10);
function generate_pilote_page($posts)
{
    global $wp, $wp_query, $wpof;

    $url_slug = $wpof->url_pilote; // slug de la page du pilote

    if (!defined('PILOTE_PAGE') && (strtolower($wp->request) == $url_slug))
    {
        define( 'PILOTE_PAGE', true );

        // create a fake virtual page
        $post = new stdClass;
        $post->post_author    = 1;
        $post->post_name      = $url_slug;
        $post->guid           = home_url() . '/' . $url_slug;
        $post->post_title     = $wpof->title_pilote;
        $post->post_content   = get_pilote_content();
        $post->ID             = -998;
        $post->post_type      = 'page';
        $post->post_status    = 'static';
        $post->comment_status = 'closed';
        $post->ping_status    = 'closed';
        $post->comment_count  = 0;
        $post->post_date      = current_time( 'mysql' );
        $post->post_date_gmt  = current_time( 'mysql', 1 );
        $posts                = NULL;
        $posts[]              = $post;

        // make wpQuery believe this is a real page too
        $wp_query->is_page             = true;
        $wp_query->is_singular         = true;
        $wp_query->is_home             = false;
        $wp_query->is_archive          = false;
        $wp_query->is_category         = false;
        unset( $wp_query->query[ 'error' ] );
        $wp_query->query_vars[ 'error' ] = '';
        $wp_query->is_404 = false;
    }

    return $posts;
}

function get_pilote_content()
{
    $role = wpof_get_role(get_current_user_id());
    $html = "<div id='pilote-content'>";
    
    if ($role == "admin")
        $html .= "<div id='pilote_dialog'></div>";
        
    $annee_defaut = get_user_meta(get_current_user_id(), "annee_comptable", true);
    if (empty($annee_defaut))
        $annee_defaut = -1;
    
    if (in_array($role, array("um_responsable", "admin")))
    {
        $html .= get_top_bloc_pilote();
        $html .= "<div id='pilote' data-annee='$annee_defaut'>";
        $html .= get_pilote($annee_defaut);
        $html .= "</div>";
    }
    $html .= "</div>";
    
    return $html;
}

function get_top_bloc_pilote()
{
    ob_start();
    ?>
    <div id="top-bloc-pilote">
    <div id='annee_choix' data-id='pilote'><?php echo get_choix_annee_comptable(); ?></div>
    <?php echo get_fullscreen_mode("#pilote-content"); ?>
    <?php echo get_edition_mode("#table-editable"); ?>
    </div>
    <?php
    return ob_get_clean();
}

function get_pilote($annee)
{
    global $session_keys;
    global $client_keys;
    global $wpof;
    global $SessionFormation;
    $role = wpof_get_role(get_current_user_id());
    
    set_session_annee_comptable($annee);
    
    $liste = array();
    
    foreach($SessionFormation as $s)
    {
        if ($s->numero != "")
            $liste[$s->numero] = $s;
        else
            $liste[$s->id] = $s;
    }
    ksort($liste);
    
    ob_start();
    ?>
    <table id="table-editable" class="pilote_clients pilote">
    <thead>
    <tr><?php echo join(array_map("pilote_array_map_th", $client_keys)); ?></tr>
    </thead>
    
    
    <tbody>
    <?php foreach($liste as $s) : ?>
    <?php $s->init_clients(); $s->init_stagiaires(); $s->calcule_budget_global(); ?>
    <tr data-sessionid="<?php echo $s->id; ?>" class="session session<?php echo $s->id; ?>" id="session<?php echo $s->id; ?>">
    <td colspan="<?php echo count($client_keys); ?>">
    <div>
        <p class="delete-entity icone-bouton" data-objectclass="<?php echo get_class($s); ?>" data-id="<?php echo $s->id; ?>" data-parent=".session<?php echo $s->id; ?>">
        <span class="dashicons dashicons-dismiss" ></span>
        </p>
        <?php foreach($session_keys as $k => $val) : ?>
        <div class="<?php echo $k; ?>"><span class='legende'><?php echo $val['text']; ?> : </span> <?php
            if (isset($s->$k))
            {
                switch($k)
                {
                    case 'titre_session':
                        echo "<a href='{$s->permalien}'>".$s->$k."</a>";
                        break;
                    case 'inscrits':
                        $erreur = false;
                        echo "<p>".count($s->$k)."</p>";
                        if ($s->st_nb_stagiaire == 0 && count($s->$k) == 0)
                            $erreur = true;
                        if ($erreur)
                            echo "<p class='erreur'>".__("Stagiaire(s) manquant(s)")."</p>";
                        break;
                    case 'formateur':
                        if (!isset($wpof->formateur))
                            init_term_list("formateur");
                        echo get_input_jpost($s, $k, array('select' => 'multiple'));
                        break;
                    case 'dates_texte':
                    case 'budget_global':
                    case 'id':
                        echo $s->$k;
                        break;
                    case 'nb_heure_estime_decimal':
                        echo get_input_jpost($s, $k, array('input' => 'number', 'step' => 0.01, 'min' => 0));
                        break;
                    default:
                        echo get_input_jpost($s, $k, array('input' => 'text'));
                        break;
                }
            }
        ?>
        </div>
        <?php endforeach; ?>
    </div>
    </td>
    </tr>
    <?php echo get_pilote_clients($s); ?>
    <?php endforeach; ?>
    </tbody>
    </table>
    <?php
    return ob_get_clean();
}

function get_pilote_clients($session)
{
    if (count($session->clients) == 0) return "";
    
    global $wpof;
    global $client_keys;
    $session_id = $session->id;
    $role = wpof_get_role(get_current_user_id());
    
    ob_start();
    ?>
    <?php foreach($session->clients as $cid) : ?>
    <?php $client = get_client_by_id($session_id, $cid); ?>
    <tr class="client <?php echo $client->financement; ?> session<?php echo $session->id; ?>" data-sessionid="<?php echo $session_id; ?>" data-clientid="<?php echo $cid; ?>">
        <?php foreach($client_keys as $k => $val) : ?>
        <td class="<?php echo $k; ?>">
        <?php
            if (isset($client->$k))
            {
                switch($k)
                {
                    case 'nom':
                        echo get_input_jpost($client, $k, array('input' => 'text'));
                        break;
                    case 'exe_comptable':
                        echo get_pilote_exe_comptable($session);
                        break;
                    case 'nature_formation':
                    case 'financement':
                        echo get_input_jpost($client, $k, array('select' => '', 'first' => __("Choisir")));
                        break;
                    case 'stagiaires':
                        $erreur = false;
                        echo "<p>".count($client->$k)."</p>";
                        if (count($client->$k) == 0)
                            $erreur = true;
                        echo "<p><span class='toggle-stagiaires bouton' data-id='c{$client->id}'>".__("Stagiaires")."</span></p>";
                        if ($erreur)
                            echo "<p class='erreur'>".__("Stagiaire(s) manquant(s)")."</p>";
                        break;
                    case 'tarif_total_chiffre':
                    case 'tarif_total_autres_chiffre':
                        echo get_input_jpost($client, $k, array('input' => 'number', 'step' => '0.01', 'min' => '0'));
                        break;
                    case 'tarif_heure':
                    default:
                        echo $client->$k;
                        break;
                }
            }
        ?>
        </td>
        
        <?php endforeach; ?>
    </tr>
    <tr class='pilote-tableau-stagiaires' id='c<?php echo $client->id;?>'>
    <td colspan="<?php echo count($client_keys); ?>">
    <?php echo get_pilote_stagiaires($client); ?>
    </td>
    </tr>
    <?php endforeach; ?>
    <?php 
    return ob_get_clean();
}

function get_pilote_stagiaires($client)
{
    if (count($client->stagiaires) == 0) return "";
    
    global $wpof;
    global $stagiaire_keys;
    $session_id = $client->session_formation_id;
    $session = get_session_by_id($session_id);
    $stagiaires = $client->stagiaires;
    $role = wpof_get_role(get_current_user_id());
    
    ob_start();
    ?>
    <table class="pilote_stagiaires">
    <thead>
    <tr><?php echo join(array_map("pilote_array_map_th", $stagiaire_keys)); ?></tr>
    </thead>
    
    <?php foreach($stagiaires as $sid) : ?>
    <?php $s = get_stagiaire_by_id($session_id, $sid); ?>
    <tr data-clientid="<?php echo $client->id; ?>" data-stagiaireid="<?php echo $s->user_id; ?>">
        <?php foreach($stagiaire_keys as $k => $val) : ?>
        <td class="<?php echo $k; ?>">
        <?php
            if (isset($s->$k))
            {
                switch($k)
                {
                    case 'user_id':
                        echo get_displayname($s->$k);
                        if ($role == 'admin')
                            echo "<br />".$session->get_sql_select_button($s->user_id);
                        break;
                    case 'statut_stagiaire':
                        echo $s->get_select_jpost($wpof->$k, $k, $s->$k, "", __("Choisir"));
                        break;
                    case 'emarge':
                        echo $s->get_input_jpost("checkbox", $k);
                        break;
                    case 'nb_heure_estime_decimal':
                        if (empty($s->$k))
                            $s->$k = $s->nb_heure_decimal;
                    default:
                        echo $s->$k;
                        break;
                }
            }
        ?>
        </td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
    
    </table>
    <?php
    return ob_get_clean();
}

function get_pilote_exe_comptable($session)
{
    $exe_array = array();
    foreach($session->exe_comptable as $y => $b)
        $exe_array[] = "$y → $b €";
    
    return join('<br />', $exe_array);
}

?>
