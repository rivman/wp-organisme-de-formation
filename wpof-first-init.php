<?php
/*
 * wpof-first-init.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

/*
 * Initialisation du plugin lors de l'installation
 */
/*
 * Ne fonctionne pas.
 * Raison indéterminée :(
 * Pas important, à résoudre plus tard ou jamais
 *  
// Page des formations
$formation_page_arr = array
(
	'post_type' => 'page',
	'post_status' => 'publish',
	'post_title' => __('Formations', 'wpof'),
	'post_content' => '[formation_list]',
	'post_author' => 1,
);

wp_insert_post($formation_page_arr, true);


// Page des formateurs
$formateur_page_arr = array
(
	'post_type' => 'page',
	'post_status' => 'publish',
	'post_title' => __('Formateurs', 'wpof'),
	'post_content' => '[formateur_list]',
	'post_author' => 1,
);

wp_insert_post($formateur_page_arr, true);
*/


require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

function first_init()
{
    global $wpdb;
    $table_session_stagiaire = $wpdb->prefix."wpof_session_stagiaire";
    $table_documents = $wpdb->prefix."wpof_documents";
    $table_quiz = $wpdb->prefix."wpof_quiz";
    $table_creneaux = $wpdb->prefix."wpof_creneaux";
    $table_client = $wpdb->prefix."wpof_client";

    // ajout de tables dans la base de données
    // {$prefix}wpof_session_stagiaire
    // {$prefix}wpof_documents

    $charset_collate = $wpdb->get_charset_collate();
    $wpof_sql = "CREATE TABLE IF NOT EXISTS $table_documents
    (
        id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        session_id bigint(20) unsigned DEFAULT NULL,
        contexte smallint(5) unsigned DEFAULT NULL,
        contexte_id bigint(20) unsigned DEFAULT NULL,
        document varchar(64) DEFAULT NULL,
        meta_key varchar(128) DEFAULT NULL,
        meta_value longtext DEFAULT NULL,
        PRIMARY KEY (id),
        UNIQUE (session_id,contexte,contexte_id,document,meta_key)
    ) $charset_collate;";
    
    $res = dbDelta($wpof_sql);
    //debug_info($res, "SQL1");
    
    // Note : je ne sais pas pourquoi, l'envoi des deux requêtes de création de table en une fois à dbDelta ne fonctionne pas, seule la seconde est créée...
    $wpof_sql = "CREATE TABLE IF NOT EXISTS $table_session_stagiaire
    (
        id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        session_id bigint(20) unsigned DEFAULT NULL,
        user_id bigint(20) unsigned DEFAULT NULL,
        meta_key varchar(128) DEFAULT NULL,
        meta_value longtext DEFAULT NULL,
        PRIMARY KEY (id),
        UNIQUE (session_id,user_id,meta_key)
    ) $charset_collate;";

    $res = dbDelta($wpof_sql);
    //debug_info($res, "SQL2");
    
    $wpof_sql = "CREATE TABLE IF NOT EXISTS $table_quiz
    (
        id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        quiz_id bigint(20) unsigned DEFAULT NULL,
        parent_id bigint(20) unsigned DEFAULT NULL,
        type varchar(2) DEFAULT NULL,
        subject varchar(64) DEFAULT NULL,
        occurrence tinyint(4) DEFAULT NULL,
        title varchar(128) DEFAULT NULL,
        meta_key smallint unsigned DEFAULT NULL,
        meta_value longtext DEFAULT NULL,
        PRIMARY KEY (id),
        UNIQUE (quiz_id,parent_id,subject,occurrence,meta_key)
    ) $charset_collate;";

    $res = dbDelta($wpof_sql);
    
    $wpof_sql = "CREATE TABLE IF NOT EXISTS $table_creneaux
    (
        id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        session_id bigint(20) unsigned NOT NULL,
        activite_id bigint(20) DEFAULT NULL,
        module_id bigint(20) DEFAULT NULL,
        date_debut datetime,
        date_fin datetime,
        type varchar(20) DEFAULT NULL,
        lieu_id bigint(20) DEFAULT NULL,
        salle_id bigint(20) DEFAULT NULL,
        PRIMARY KEY (id),
        UNIQUE (date_debut, session_id, lieu_id, salle_id)
    ) $charset_collate;";
    
    $res = dbDelta($wpof_sql);
    
    $wpof_sql = "CREATE TABLE IF NOT EXISTS $table_client
    (
        id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        session_id bigint(20) unsigned DEFAULT NULL,
        client_id bigint(20) unsigned DEFAULT NULL,
        meta_key varchar(128) DEFAULT NULL,
        meta_value longtext DEFAULT NULL,
        PRIMARY KEY (id),
        UNIQUE (session_id,client_id,meta_key)
    ) $charset_collate;";

    $res = dbDelta($wpof_sql);
    
    add_option("wpof_version", WPOF_VERSION);
    
    $wpof_sql = file_get_contents(wpof_path . "/init/wpof_options.sql");
    $wpof_sql = str_replace("{table_prefix}", $wpdb->prefix, $wpof_sql);
    $res = $wpdb->query($wpof_sql);
    
    // Rôles ultimate-member
    $wpof_sql = file_get_contents(wpof_path . "/init/roles.sql");
    $wpof_sql = str_replace("{table_prefix}", $wpdb->prefix, $wpof_sql);
    $res = $wpdb->query($wpof_sql);
    
    // Pages prédéfinies (Formations, équipe péda, calendrier, accueil)
    $wpof_sql = file_get_contents(wpof_path . "/init/pages.sql");
    $wpof_sql = str_replace("{table_prefix}", $wpdb->prefix, $wpof_sql);
    $res = $wpdb->query($wpof_sql);

    // Modèles de documents
    $wpof_sql = file_get_contents(wpof_path . "/init/modeles_documents.sql");
    $wpof_sql = str_replace("{table_prefix}", $wpdb->prefix, $wpof_sql);
    $res = $wpdb->query($wpof_sql);
    
    init_um_options();
    update_option("wpof_annee1", date('Y'));
}

add_action('wp_ajax_init_um_options', 'init_um_options');
function init_um_options()
{
    // modèles de mails
    $mail_options = array
    (
        "welcome_email_on" => "1",
        "checkmail_email_on" => "1",
        "pending_email_on" => "1",
        "approved_email_on" => "1",
        "rejected_email_on" => "1",
        "inactive_email_on" => "1",
        "deletion_email_on" => "1",
        "resetpw_email_on" => "1",
        "changedpw_email_on" => "1",
        "changedaccount_email_on" => "1",
        "notification_new_user_on" => "1",
        "notification_review_on" => "1",
        "notification_deletion_on" => "1",
        "validation_docs_email_on" => "1",

        "welcome_email_sub" => "Bienvenue sur {site_name}",
        "checkmail_email_sub" => "{site_name} – Confirmez votre inscription",
        "pending_email_sub" => "{site_name} – En attente de validation",
        "approved_email_sub" => "{site_name} Accès confirmé",
        "rejected_email_sub" => "{site_name} – Accès refusé",
        "inactive_email_sub" => "{site_name} – Votre accès a été désactivé",
        "deletion_email_sub" => "{site_name} – Compte supprimé",
        "resetpw_email_sub" => "{site_name} – Demande de réinitialisation de votre mot de passe",
        "changedpw_email_sub" => "{site_name} – Nouveau mot de passe",
        "changedaccount_email_sub" => "Votre compte sur {site_name} a été mis à jour",
        "notification_new_user_sub" => "{site_name} Nouveau compte créé pour {display_name}",
        "notification_review_sub" => "{site_name} – Validation requise pour {display_name}",
        "notification_deletion_sub" => "{site_name} – Suppression du compte de {display_name}",
        "validation_docs_email_sub" => "{site_name} - Vous avez des documents à valider",
    );
    $profile_options = array
    (
        "profile_area_max_width" => "1000px",
        "profile_cover_enabled" => "0",
        "profile_show_bio" => "0",
        "profile_empty_text" => "0",
        "profile_tab_main" => "0",
        "profile_tab_posts" => "0",
        "profile_tab_comments" => "0",
        "profile_menu_default_tab" => "oftab",
        "profile_tab_oftab_privacy" => "3",
        "register_role" => "um_stagiaire",
        "login_secondary_btn" => "0",
        "members_page" => "",
        "permalink_base" => "name",
    );
    
    $um_options = get_option("um_options");
    update_option("um_options", array_merge($um_options, $mail_options, $profile_options));
    
    if (isset($_POST['action']) && $_POST['action'] == "init_um_options")
    {
        ?>
        <dl>
        <?php foreach(get_option("um_options") as $k => $v) : ?>
            <dt><?php echo $k; ?></dt>
            <dd><?php var_dump($v); ?></dd>
        <?php endforeach; ?>
        </dl>
        <?php
        die();
    }
}
//init_um_options();

?>
