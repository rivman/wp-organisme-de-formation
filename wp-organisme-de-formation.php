<?php
/**
 * Plugin Name: Organisme de formation
 * Plugin URI: http://formations.artefacts.coop
 * Description: Plugin permettant de gérer l'administration de la formation professionnelle selon la loi française. Gestion des inscriptions, création automatique des documents obligatoires, suivi des stagiaires
 * Version: 0.1
 * Author: Dimitri Robert – CAE Artefacts
 * Author URI:  http://formation-logiciel-libre.com
 * License: GPL3 license
 * Depends: Ultimate Member, Custom Post Type UI, Advanced Custom Fields, WP-Pro-Quiz
 *
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

    error_reporting(E_ALL);
    ini_set('display_errors','On');

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

define( 'WPOF_VERSION', '0.1' );
define( 'WPOF_WP_VERSION', get_bloginfo( 'version' ) );
define( 'wpof_url', plugin_dir_url(__FILE__ ));
define( 'wpof_path', plugin_dir_path(__FILE__ ));
define( 'debug', true );

require_once(wpof_path . "/wpof-first-init.php");

function activate_wpof()
{
    update_option('Activated_Plugin', 'OPAGA');
}
register_activation_hook(__FILE__, 'activate_wpof');

function load_plugin()
{
    if (is_admin() && get_option('Activated_Plugin') == 'OPAGA')
    {
        if (is_plugin_active('ultimate-member/ultimate-member.php'))
        {
            delete_option('Activated_Plugin');

            if (get_option("wpof_version") == "")
                first_init();
        }
    }
}
add_action( 'admin_init', 'load_plugin' );

function check_um_active_notice()
{
    if (is_admin() && !is_plugin_active('ultimate-member/ultimate-member.php'))
    {
         echo '<div class="notice notice-error">
             <p>Vous devez installer et activer le plugin <a href="'.get_admin_url().'plugin-install.php?s=ultimate+member&tab=search&type=term">Ultimate Member</a></p>
         </div>';
    }
}
add_action('admin_notices', 'check_um_active_notice');

define( 'WPOF_TABLE_SUFFIX_DOCUMENTS', 'wpof_documents');

setlocale(LC_ALL, 'fr_FR@utf8');

add_action( 'wp_enqueue_scripts', 'wpof_load_scripts', 21 );
add_action( 'admin_enqueue_scripts', 'wpof_load_scripts', 21 );
function wpof_load_scripts()
{
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_script('jquery-ui-dialog');
    wp_enqueue_script('jquery-ui-tabs');
    wp_enqueue_script('jquery-ui-menu');
    wp_enqueue_script('jquery-ui-sortable');
    wp_enqueue_style('jquery-ui', wpof_url."css/jquery-ui.css");
    
    wp_enqueue_script( 'wpof', wpof_url."js/wpof.js", array('jquery') );
    wp_localize_script('wpof', 'ajaxurl', admin_url( 'admin-ajax.php' ) );
    if (is_admin())
    {
        wp_enqueue_style( 'wpof-admin', wpof_url."css/style-admin.css" );
        wp_enqueue_script( 'wpof-admin', wpof_url."js/wpof-admin.js", array('jquery') );
    }
    else
    {
        global $post;
        global $wpof;
        
        if ($post->post_type == "session" || in_array($post->post_name, $wpof->no_theme) || get_the_ID() == um_get_option('core_user'))
        {
            $theme = wp_get_theme();
            if ($parent = $theme->parent())
            {
                wp_dequeue_style($parent->get_stylesheet()."-style");
                wp_dequeue_style($parent->get_stylesheet());
            }
            wp_dequeue_style($theme->get_stylesheet()."-style");
            wp_dequeue_style($theme->get_stylesheet());
            
            if (file_exists(get_stylesheet_directory()."/opaga-no-theme.css"))
                $no_theme_css = get_stylesheet_directory_uri()."/opaga-no-theme.css";
            else
                $no_theme_css = wpof_url."css/opaga-no-theme.css";
            wp_enqueue_style('opaga_no_theme', $no_theme_css);
        }
    }
    wp_enqueue_style( 'wpof', wpof_url."css/wpof.css" );
    
    $role = wpof_get_role(get_current_user_id());
    if ($role != "um_stagiaire")
        wp_enqueue_script( 'wpof_cpt', wpof_url."cpt/cpt.js", array('jquery') );
    wp_enqueue_style( 'dashicons' );
}

add_filter('single_template', 'get_opaga_single_template');
function get_opaga_single_template($single_template)
{
    if (is_user_logged_in())
    {
        global $post;
        global $wpof;
        
        if ($post->post_type == "session" || in_array($post->post_name, $wpof->no_theme) || get_the_ID() == um_get_option('core_user'))
        {
            $single_template = wpof_path . '/template/no-theme/single.php';
        }
    }
    
    return $single_template;
}                

function wpof_session_start()
{
    if (!session_id())
        @session_start();
    // le préfixe @ permet d'ignorer les erreurs envoyés par la fonction, par exemple, si la configuration PHP ne supporte pas les sessions
}
add_action( 'init', 'wpof_session_start', 1 );

require_once(wpof_path . "/wpof-config.php");
require_once(wpof_path . "/wpof-fonctions.php");
require_once(wpof_path . "/wpof-custom-post-types.php");
require_once(wpof_path . "/wpof-session-formation.php");
require_once(wpof_path . "/wpof-utilisateur.php");
require_once(wpof_path . "/wpof-formation.php");
require_once(wpof_path . "/wpof-messages.php");
require_once(wpof_path . "/wpof-dialog.php");

require_once(wpof_path . "/pages/page-bpf.php");
require_once(wpof_path . "/pages/page-pilote.php");

// classes
require_once(wpof_path . "/class/class-formation.php");

// globales
$Formation = array();
$SessionFormation = array();
$SessionStagiaire = array();
$Client = array();
$Documents = array();

if (is_admin())
{
    require_once(wpof_path . "/wpof-admin.php");
    if (get_option("wpof_version") < WPOF_VERSION)
        require_once(wpof_path . "/wpof-mise-a-jour.php");
}

?>
