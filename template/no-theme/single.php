<?php
/**
 * Template simplifiée pour les pages spéciales d'OPAGA
 */
?>
<!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" >
        <link rel="profile" href="https://gmpg.org/xfn/11">
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
        <?php wp_body_open(); ?>
        <header id="site-header" role="banner">
            <div id="site-title">
                <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_html(get_bloginfo('name')); ?>" rel="home"><?php echo esc_html(get_bloginfo('name')); ?></a>
            </div>
            <div id="site-description"><?php bloginfo( 'description' ); ?></div>
        </header>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php
    
    if (have_posts())
        while (have_posts())
        {
            the_post();
            ?>
            <div class="entry-header">
            <h1 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
            </div>
            
            <div class="entry-content">
            <?php the_content(); ?>
            </div>
            <?php
        }
    ?>
    </article>

    <footer>
    <?php wp_footer(); ?>
    </footer>
</body>
</html>
