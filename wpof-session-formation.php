<?php
/*
 * wpof-session-formation.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

require_once(wpof_path . "/class/class-session-formation.php");
require_once(wpof_path . "/class/class-session-stagiaire.php");

/*
 * État d'avancement de l'inscription d'un stagiaire
 * C'est le formateur qui fait avancer cet état
 */
$etat_session = array
(
    'initial' => __("Demande d'inscription"),
    'inscrit' => __("Inscrit (prêt pour convention)"),
    'confirme' => __("Confirmé (convention reçue signée)"),
    'pendant' => __("En formation"),
    'apres' => __("Formation suivie"),
    'annulok' => __("Annulation dans les délais"),
    'annulhd' => __("Annulation hors délais"),
);

add_action('wp_ajax_first_contact', 'first_contact');
add_action('wp_ajax_nopriv_first_contact', 'first_contact');
function first_contact()
{
    $reponse = array();
    
    // vérification captcha
    if (!empty($_POST['identifiant']))
        return;
    
    if (strtolower($_POST['verif']) != strtolower(date_i18n("F")) && $_POST['verif'] != date("m") && $_POST['verif'] != date("n"))
        $reponse['erreur'] = __("Vous n'avez pas répondu correctement à la question de vérification.");
    else
    {
        $reponse['succes'] = __("Votre demande a été transmise, nous vous contactons très rapidement.");
        $session = get_session_by_id($_POST['session_id']);
        
        $destinataires = array();
        foreach($session->formateur as $user_id)
        {
            $user = get_userdata($user_id);
            $destinataires[] = $user->user_email;
        }
        
        $corps_email = sprintf("Bonjour,\n\n%s vient de faire une demande d'%s au sujet de la session %s.\n→ %s\n\nemail : %s\ntéléphone : %s\n\n-- \nDemande effectuée depuis le site %s.", $_POST['nom'], $_POST['choix'], $session->titre_session, $session->permalien, $_POST['email'], $_POST['telephone'], get_site_url());
        
        $sujet_email = sprintf("[Formation] Demande d'%s de %s", $_POST['choix'], $_POST['nom']);
        
        wp_mail($destinataires, $sujet_email, $corps_email);
    }
    
    echo json_encode($reponse);
    die();
}

/*
 * Traitement questionnaire de connaissances
 * $etape : preform ou bteval_postform
 * $session_data : si null, alors le questionnaire est autonome (a son propre form)
 * et sa gestion se fait intégralement ici (récup de session_data et mise à jour à la fin)
 * Sinon, on renvoit le session_data mis à jour à la fonction appelante qui s'en occupe
 *
 */
function traitement_questionnaire_connaissances($sujet)
{
    if (!isset($_POST['quiz'][$sujet])) return;
    
    $tab_quiz = $_POST['quiz'][$sujet];
    
    $quiz = new Quiz($tab_quiz['id']);
    $quiz->set_identite($sujet, $tab_quiz['parent_id']);
    
    $quiz->reponses = $_POST['quiz_'.$sujet];
    $quiz->update_reponses($_POST['user_id'], $tab_quiz['occurrence']);
}

/*
 * Fonction de Traitement des données saisies par le formateur dans l'onglet admin du tableau de bord stagiaire
 */
function enregistrer_stagiaire_tarif_input()
{
    global $SessionFormation;
    global $SessionStagiaire;

    if (!isset($SessionFormation[$_POST['session_id']]))
        $SessionFormation[$_POST['session_id']] = new SessionFormation($_POST['session_id']);
    $session_formation =& $SessionFormation[$_POST['session_id']];
    
    if (!isset($SessionStagiaire[$_POST['stagiaire_id']]))
        $SessionStagiaire[$_POST['stagiaire_id']] = new SessionStagiaire($_POST['session_id'], $_POST['stagiaire_id']);
    $session_stagiaire =& $SessionStagiaire[$_POST['stagiaire_id']];

    $response = array();
    
    if ($session_formation->type_index == "inter")
    {
        $key = 'tarif_heure';
        $session_stagiaire->$key = $_POST[$key];
        $res = $session_stagiaire->update_meta($key, $_POST[$key]);
        if (false === $res)
            $response['message'] = "<span class='alerte'>[ $key ] ".__("Erreur")." ".var_dump($res)."</span><br />";
        else
        {
            $session_stagiaire->calcule_tarif();
            $response['message'] = "<span class='succes'>[ $key ] ".__("mise à jour")."</span><br />";
            $response['total_chiffre'] = $session_stagiaire->tarif_total_chiffre;
            $response['total_lettre'] = $session_stagiaire->tarif_total_lettre;
        }
    }
    else
        $response['message'] = "<span class='alerte'>".__("Mauvais type de session : ").$session_formation->type_index."<span>";
    
    echo json_encode($response);

    die();
}
add_action( 'wp_ajax_enregistrer_stagiaire_tarif_input', 'enregistrer_stagiaire_tarif_input' );


add_action('wp_ajax_enregistrer_stagiaire_form', 'enregistrer_stagiaire_form');
function enregistrer_stagiaire_form()
{
    $reponse = array();
    $reponse['log'] = var_export($_POST, true);

    $stagiaire = get_stagiaire_by_id($_POST['session_id'], $_POST['user_id']);
    
    // champs simples, sans traitement particulier
    $fields = array
    (
        'attentes',
    );
    foreach ($fields as $f)
        if (isset($_POST[$f]))
            $stagiaire->update_meta($f, $_POST[$f]);

    if (isset($_POST['quiz_sujet']))
    {
        foreach($_POST['quiz_sujet'] as $sujet)
            traitement_questionnaire_connaissances($sujet);
    }
    $reponse['message'] = "<span class='succes'>".__("Modifications enregistrées")."</span>";
    echo json_encode($reponse);
    die();
}

/*
 * Fonction de traitement d'un document
 * Utilisée via une requête POST Ajax
 */
function traitement_doc()
{
    global $doc_nom;
    global $SessionFormation;
    global $wpof;
    
    $contexte_id = $_POST['contexte_id'];
    $contexte = $_POST['contexte'];
    $session_id = $_POST['session_id'];
    $type_doc = $_POST['type_doc'];
    $action_doc = $_POST['action_doc'];
    
    $doc = new Document($type_doc, $session_id, $contexte, $contexte_id);

    $reponse = array('message' => '', 'lignedoc' => '');
    
    switch($action_doc)
    {
        case "final":
        case "brouillon":
            if ($action_doc == "brouillon")
                $doc->valid |= Document::DRAFT;
            else
                $doc->valid &= ~Document::DRAFT;
            
            $doc->valid &= ~(Document::VALID_CLIENT_DONE | Document::VALID_STAGIAIRE_DONE | Document::VALID_RESPONSABLE_DONE);
            
            $doc->update_meta('valid', $doc->valid);
            
            if ($_POST["signataire"] == 1)
                $doc->signataire = true;
            
            if ($doc->pdf_creer())
            {
                $doc->update_meta('pdf_filename', $doc->pdf_filename);
                $doc->update_meta('html_filename', $doc->html_filename);
                $doc->update_meta('last_modif', current_time('timestamp'));
                
                $doc->init_link_name();
            }
            else
                $reponse['message'] .= "<span class='erreur'>".__("Création échouée, le document est vide !")."</span>";
            // TODO : faire un objet JSON avec champs html, erreur, demande_valid, diffuser
            break;
            
        case "demande_valid":
            $doc->update_meta('valid', $doc->valid ^ Document::VALID_RESPONSABLE_REQUEST);
            // TODO : envoi un message à la personne responsable
            break;
            
        case "diffuser":
            $doc->update_meta('visible_stagiaire', !$doc->visible_stagiaire);
            // TODO : envoi un message au stagiaire
            break;
        case "supprimer":
            $doc->supprimer();
            $doc = new Document($type_doc, $session_id, $contexte, $contexte_id);
            // TODO : éventuellement supprimer le fichier
            break;
    }
    $reponse['lignedoc'] = $doc->get_html_ligne((isset($_POST['cols'])) ? $_POST['cols'] : Document::COL_ALL);
    
    echo json_encode($reponse);

    die();
}
add_action( 'wp_ajax_traitement_doc', 'traitement_doc' );

// Inclure dompdf installé via composer
require_once wpof_path . '/vendor/autoload.php';
use Dompdf\Dompdf;
use Dompdf\Options;

/*
 * Fonction de traitement d'un document
 * Utilisée via une requête POST Ajax
 */
function export_pdf()
{
    global $doc_nom;
    global $SessionFormation;
    global $SessionStagiaire;
    global $Documents;
    
    $user_id = (isset($_POST['user_id'])) ? $_POST['user_id'] : -1;
    $doc_id = (isset($_POST['doc_id'])) ? $_POST['doc_id'] : "";
    $session_id = $_POST['session_id'];
    
    $suffixe = ($doc_id != "") ? "-$doc_id" : "";
    
    $session_formation = $SessionFormation[$session_id] = new SessionFormation($session_id);
    $session_formation->init_stagiaires();
    
    if ($user_id > 0)
    {
        $SessionStagiaire[$user_id]->init_docs();
        $suffixe .= "-".$SessionStagiaire[$user_id]->user->user_nicename;
    }
    else
    {
        $session_formation->init_docs();
        foreach($session_formation->inscrits as $i)
            $SessionStagiaire[$i]->init_docs();
    }
    
    $first_doc = reset($Documents);
    $html = $first_doc->html_from_model();
    
    foreach($Documents as $doc)
    {
        if ($doc_id == "" || $doc_id == $doc->type)
        {
            $file_content = file_get_contents($doc->path."/".$doc->html_filename);
            
            if ($file_content != "")
            {
                $html .= $file_content;
                $html .= "<div class='saut-de-page'></div>";
            }
        }
    }
    
    $options = new Options();
    $options->set('chroot', WP_CONTENT_DIR);
    $options->set('tempDir', WP_CONTENT_DIR."/uploads");
    $options->set('isRemoteEnabled', TRUE);
    $options->set('isPhpEnabled', TRUE);
    $options->set('defaultPaperSize', 'A4');
    $options->set('dpi', 200);
    
    $dompdf = new Dompdf($options);
    $dompdf->loadHtml($html);
    $dompdf->render();
    $pdf_content = $dompdf->output();
    
    $pdf_name = sanitize_title($session_formation->titre_session.$suffixe).".pdf";
    $pdf_file = fopen($first_doc->path."/".$pdf_name, "w");
    fwrite($pdf_file, $pdf_content);
    fclose($pdf_file);
    
    echo "<a href='{$first_doc->url}/$pdf_name'>$pdf_name</a>";
    
    die();
}
add_action( 'wp_ajax_export_pdf', 'export_pdf' );


/*
 * Mise à jour de valeur(s) d'une session stagiaire (session_data)
 * TODO : à virer
 */
function update_session_data($session_id, $user_id, $data = array())
{
    $sd = json_decode(get_user_meta($user_id, "session".$session_id, true), true);
    if ($sd)
    {
        $update = array_merge($sd, $data);
        update_user_meta($user_id, 'session'.$session_id, json_encode($update, JSON_HEX_APOS|JSON_UNESCAPED_UNICODE));
    }
    else
    {
        add_user_meta($user_id, 'session'.$session_id, json_encode($data, JSON_HEX_APOS|JSON_UNESCAPED_UNICODE));
    }
    return true;
}


function get_template_session()
{
    global $SessionFormation;
    global $SessionStagiaire;
    global $Formation;
    global $wpof;
    
    $session_id = get_the_ID();
    $user_id = get_current_user_id();
    
    $session_formation = get_session_by_id($session_id);

    // Si le visiteur n'est pas connecté, on n'affiche que la boîte de connexion/inscription
    if ($user_id == 0):
        echo $session_formation->get_html_presentation();
        ?>
            <div class="icone-bouton premier_contact dynamic-dialog" data-function="premier_contact" data-sessionid="<?php echo $session_formation->id; ?>">
                <span class="dashicons dashicons-email-alt"></span>
                <?php _e("Pour en savoir plus"); ?>
            </div>
        <?php
        /*
        echo "<p class='important'>{$wpof->terms_connexion_session}</p>";
        $login_post = $wpof->session_login_form;
        echo do_shortcode("[ultimatemember form_id=$login_post]");
        */
    
    else: // affiche tout le reste, le endif correspondant doit se trouver tout à la fin
    
        $role = wpof_get_role($user_id);

        ?>
        <?php if(($role == 'um_stagiaire' && $user_id != $session_formation->entreprise_contact_id) || 
            ($role == "um_formateur-trice" && $wpof->formateur_gest != 1 && !in_array($user_id, $session_formation->formateur))) : ?>
        
            <div id="session" class="<?php echo $role;?> session_box">
            <?php // Test si stagiaire inscrit à cette session
            
            $session_formation->init_stagiaires($user_id);
            
            if (in_array($user_id, $session_formation->inscrits))
            {
                $SessionStagiaire[$user_id]->the_board();
            }
            else
            {
                echo $session_formation->get_html_presentation(); // attention : ne pas afficher deux fois session_presentation dans une même page, pb Javascript
                    
                echo "<p class='msg-intro msg'>{$wpof->terms_msg_debut_pre_inscription}</p>";
                echo "<form id='inscription' action='".$session_formation->permalien."' method='post'>";
                echo "<p class='msg-validation msg'>{$wpof->terms_msg_validation_pre_inscription}<br />";
                echo "<input type='submit' value='".__("Validez votre pré-inscription")."'></p>";
                echo hidden_input("inscription", 1);
                echo $session_formation->get_formulaire_inscription_session($user_id);
                echo $session_formation->quizpr->get_html($user_id, $session_formation->quizpr_id, 0);
                echo $session_formation->quizobj->get_html($user_id, $session_formation->quizobj_id, 0);
                echo "<p class='msg-validation msg'>{$wpof->terms_msg_validation_pre_inscription}<br />";
                echo "<input type='submit' value='".__("Validez votre pré-inscription")."'></p>";
                echo "</form>";
            }
            
            ?>
            </div> <!-- session_box session -->
        
        <?php else: ?>
        
            <div id="session" class="<?php echo $role;?> session_box">
            
            <?php
                $session_formation->init_stagiaires();
                $session_formation->init_clients();
                $session_formation->the_board();
            ?>
            
            </div> <!-- session_box session -->
        <?php endif;
            
    endif; // user_id != 0
}


/**
 * Filter the upload size limit for non-administrators.
 *
 * @param string $size Upload size limit (in bytes).
 * @return int (maybe) Filtered size limit.
 */
function filter_session_upload_size_limit($size)
{
    // Set the upload size limit to 60 MB for users lacking the 'manage_options' capability.
    if ( ! current_user_can( 'manage_options' ) ) {
        // 60 MB.
        $size = 5 * 1024 * 1024;
    }
    return $size;
}
add_filter( 'upload_size_limit', 'filter_session_upload_size_limit', 20 );


add_action( 'wp_ajax_delete_entity', 'delete_entity' );
function delete_entity()
{
    $reponse = array();
    
    $object_class = $_POST['object_class'];
    
    switch ($object_class)
    {
        case "Client":
        case "SessionStagiaire":
            try
            {
                $object = new $object_class($_POST['session_id'], $_POST['id']);
            }
            catch(Exception $e)
            {
                $reponse['erreur'] = "Pas de $object_class ".$_POST['id']." pour la session ".$_POST['session_id'];
            }
            break;
        case "SessionFormation":
            try
            {
                $object = new $object_class($_POST['id']);
            }
            catch(Exception $e)
            {
                $reponse['erreur'] = "Pas de $object_class ".$_POST['id'];
            }
            break;
        default:
            $reponse['erreur'] = "Classe d'objet non reconnu ".$_POST['object_class'];
            break;
    }
    
    if (!isset($reponse['erreur']))
    {
        $reponse['log'] = $object->delete();
        $reponse['succes'] = "Succès";
    }
    
    echo json_encode($reponse);
    die();
}

add_action( 'wp_ajax_unsubscribe', 'unsubscribe' );
function unsubscribe()
{
    $session_id = $_POST['session_id'];
    $user_id = $_POST['user_id'];

    $inscrits = get_post_meta($session_id, 'inscrits', true);
    if (in_array($user_id, $inscrits))
    {
        $key = array_search($user_id, $inscrits);
        unset($inscrits[$key]);
        update_post_meta($session_id, 'inscrits', $inscrits);
        
        // Ajout de l'ID de la session de formation dans la meta inscription du stagiaire
        $inscription = get_user_meta($user_id, "inscription", true);
        $key = array_search($session_id, $inscription);
        unset($inscription[$key]);
        update_user_meta($user_id, "inscription", array_unique($inscription));
    }

    die();
}

add_action( 'wp_ajax_subscribe', 'subscribe' );
function subscribe()
{
    $reponse = array('message' => '', 'stagiaires_board' => '');
    $nouveaux_inscrits = array();    
    $deja_inscrits = array();    
    
    $session_id = $_POST['session_id'];
    if (isset($_POST['stagiaire_id']))
    {
        $inscrits = get_post_meta($session_id, "inscrits", true);
        if (!$inscrits) $inscrits = array();
        
        // Ajout de l'ID de la session de formation dans la meta inscription des stagiaires
        foreach($_POST['stagiaire_id'] as $user_id)
        {
            if (!in_array($user_id, $inscrits))
            {
                $inscription = get_user_meta($user_id, "inscription", true);
                if ($inscription == "")
                    $inscription = array();
                
                $inscription[] = $session_id;
                update_user_meta($user_id, "inscription", array_unique($inscription));
                
                $nouveaux_inscrits[] = get_displayname($user_id);
                
                global $SessionStagiaire;
                $session_formation = new SessionFormation($session_id);
                $SessionStagiaire[$user_id] = new SessionStagiaire($session_id, $user_id);
                ob_start();
                $session_formation->the_stagiaire_board($user_id);
                $reponse['stagiaires_board'] .= ob_get_clean();
            }
            else
                $deja_inscrits[] = get_displayname($user_id);
        }
        
        if (count($nouveaux_inscrits) > 0)
            $reponse['message'] .= "<p class='succes'>".join(", ", $nouveaux_inscrits)." ".__("désormais inscrit(s) à cette session")."</p>";
        if (count($deja_inscrits) > 0)
            $reponse['message'] .= "<p class='alerte'>".join(", ", $deja_inscrits)." ".__("ignoré(s) car déjà inscrit(s) à cette session")."</p>";
            
        update_post_meta($session_id, 'inscrits', array_unique(array_merge($inscrits, $_POST['stagiaire_id'])));
    }
    echo json_encode($reponse);
    
    die();
}

add_action( 'wp_ajax_enregistrer_session_tarif', 'enregistrer_session_tarif' );
function enregistrer_session_tarif()
{
    $reponse = array();
    
    $session_id = $_POST['session_id'];
    
    if (isset($_POST['stagiaire_id']))
        $session = new SessionStagiaire($session_id, $_POST['stagiaire_id']);
    else
        $session = new SessionFormation($session_id);
    
    foreach(array("tarif_base_total", "tarif_heure", "tarif_total_chiffre", "tarif_total_autres_chiffre", "autres_frais") as $data)
    {
        if (isset($_POST[$data]))
        {
            $$data = $_POST[$data];
            $session->$data = $$data;
            $result = $session->update_meta($data, $$data);
            $reponse['resultat'][$data] = $$data;
        }
    }
    
    $session->calcule_tarif();
    
    foreach(array('tarif_heure', 'tarif_total_chiffre', 'tarif_total_lettre', 'tarif_total_autres_chiffre', 'tarif_total_autres_lettre') as $data)
        if (isset($session->$data))
            $reponse[$data] = $session->$data;
    
    if (!isset($_POST['stagiaire_id']) && $session->tarif_total_chiffre < $session->tarif_total_autres_chiffre)
        $reponse['erreur'] = "<span class='erreur'>".__("Les frais annexes excédent le budget total !")."</span>";
    
    echo json_encode($reponse);
    
    die();
}
    

add_action( 'wp_ajax_enregistrer_session_input', 'enregistrer_session_input' );
function enregistrer_session_input()
{
    $session_id = $_POST['session_id'];
    $fields = $_POST['fields'];
    
    foreach($fields as $key => $value)
    {
        $result = update_post_meta($session_id, $key, $value);
        if ($result)
            echo "<span class='succes'>[ ".$key." ] ".__("mise à jour")."</span><br />";
        else
            echo "<span class='alerte'>[ ".$key." ] ".__("inchangée")."</span><br />";
    }
    
    die();
}
add_action( 'wp_ajax_enregistrer_horaires', 'enregistrer_horaires' );
function enregistrer_horaires()
{
    $session_id = $_POST['session_id'];
    $fields = $_POST['fields'];

    // pour chaque tranche, l'heure de début et de fin doivent être précisées
    // si l'une manque, on n'enregistre pas l'autre
    if ($fields['horaire-am-debut'] != "" && $fields['horaire-am-fin'] != "")
    {
        update_post_meta($session_id, 'horaire-am-debut', $fields['horaire-am-debut']);
        update_post_meta($session_id, 'horaire-am-fin', $fields['horaire-am-fin']);
        echo "<span class='succes'>".__("horaires du matin modifiés")."<span>";
    }
    else
    {
        update_post_meta($session_id, 'horaire-am-debut', "");
        update_post_meta($session_id, 'horaire-am-fin', "");
        echo "<span class='alerte'>".__("pas d'horaires du matin")."<span>";
    }
    echo "<br />";
    if ($fields['horaire-pm-debut'] != "" && $fields['horaire-pm-fin'] != "")
    {
        update_post_meta($session_id, 'horaire-pm-debut', $fields['horaire-pm-debut']);
        update_post_meta($session_id, 'horaire-pm-fin', $fields['horaire-pm-fin']);
        echo "<span class='succes'>".__("horaires de l'après-midi modifiés")."<span>";
    }
    else
    {
        update_post_meta($session_id, 'horaire-pm-debut', "");
        update_post_meta($session_id, 'horaire-pm-fin', "");
        echo "<span class='alerte'>".__("pas d'horaires de l'après-midi")."<span>";
    }
    
    die();
}

add_action( 'wp_ajax_change_type_emargement', 'change_type_emargement' );
function change_type_emargement()
{
    $session_id = $_POST['session_id'];
    $emarge_type = $_POST['emarge_type'];
    $value = 1 - $_POST['value'];
    
    $types_emargement = get_post_meta($session_id, "type_emargement", true);
    if ($types_emargement == "")
    {
        global $type_emargement_text;
        $types_emargement = array_fill_keys(array_keys($type_emargement_text), 0);
        $types_emargement['jour'] = 1;
    }
    
    $types_emargement[$emarge_type] = $value;
    update_post_meta($session_id, "type_emargement", $types_emargement);
    
    if ($value == 1)
        echo "<span class='succes'>".__("Type")." $emarge_type ".__("activé")."</span>";
    else
        echo "<span class='alerte'>".__("Type")." $emarge_type ".__("désactivé")."</span>";
    
    die();
}

add_action( 'wp_ajax_update_unique_value', 'update_unique_value' );
function update_unique_value()
{
    $session_id = $_POST['session_id'];
    global $SessionFormation;
    global $SessionStagiaire;

    if (isset($_POST['stagiaire_id']))
    {
        if (!isset($SessionStagiaire[$_POST['stagiaire_id']]))
            $SessionStagiaire[$_POST['stagiaire_id']] = new SessionStagiaire($_POST['session_id'], $_POST['stagiaire_id']);
        $session_stagiaire =& $SessionStagiaire[$_POST['stagiaire_id']];
        
        $session_stagiaire->update_meta($_POST['meta_key'], $_POST['meta_value']);
        echo __("Nouvelle valeur")." : ".$_POST['meta_value'];
    }
    else
    {
        // TODO : à tester !
        update_post_meta($session_id, $_POST['meta_key'], $_POST['meta_value']);
        echo $_POST['meta_value'];
    }

    die();
}

add_action('wp_ajax_add_new_date_line', 'add_new_date_line');
function add_new_date_line()
{
    if (isset($_POST['session_id']))
        $session = new SessionFormation($_POST['session_id']);
    else
        $session = new SessionFormation();
    
    $newdate = null;
    
    if (!empty($_POST['fields']))
    {
        $datebase = $_POST['fields']['datebase'];
        $decaljour = $_POST['fields']['decaljour'];
        echo $datebase;
        $newdate_obj = DateTime::createFromFormat("d/m/Y", $datebase);
        $newdate_obj->add(new DateInterval('P'.$decaljour.'D'));
        $newdate = $newdate_obj->format('d/m/Y');
        $session->creneaux[$newdate] = $session->creneaux[$datebase];
        foreach($session->creneaux[$newdate] as $creno)
        {
            $creno->id = -1;
        }
    }
    echo $session->get_html_ligne_creneaux(true, $newdate);
    
    die();
}

// TODO : à supprimer
add_action('wp_ajax_add_new_creneau', 'add_new_creneau');
function add_new_creneau()
{
    $creno = new Creneau();
    if (isset($_POST['date']))
    {
        $creno->date = $_POST['date'];
        $creno->session_id = $_POST['session_id'];
        $creno->lieu_id = $_POST['lieu_id'];
        if ($creno->lieu_id == -1)
            $creno->lieu_nom = $_POST['lieu_nom'];
        echo $creno->get_html("", true);
    }
    
    die();
}

add_action('wp_ajax_update_date', 'update_date');
function update_date()
{
    $session = get_session_by_id($_POST['session_id']);
    
    if (isset($_POST['old_date']) && array_key_exists($_POST['old_date'], $session->creneaux))
    {
        $session->creneaux[$_POST['date']] = $session->creneaux[$_POST['old_date']];
        unset($session->creneaux[$_POST['old_date']]);
    }
    else
    {
        $session->creneaux[$_POST['date']] = array();
        if (isset($_POST['creneaux']))
        {
            foreach($_POST['creneaux'] as $c_id)
            {
                $original = new Creneau($c_id);
                $copie = clone $original;
                $copie->id = -1;
                $copie->set_date($_POST['date']);
                $copie->update();
                $session->creneaux[$_POST['date']][] = $copie;
            }
        }
        $reponse['html'] = $session->get_html_ligne_creneaux(true, $_POST['date']);
    }
    
    $session->update_meta("creneaux");
    
    $reponse['log'] = var_export($_POST, true);
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_update_creneau', 'update_creneau');
function update_creneau()
{
    $creno = new Creneau();
    $session = get_session_by_id($_POST['data']['session_id']);
    if (isset($_POST['data']))
        $creno->init_from_form($_POST['data']);
    $creno->update();
    
    $session->creneaux[$_POST['data']['date']][] = $creno;
    $session->update_meta("creneaux");
    
    echo $creno->get_html("", true);
    
    die();
}

add_action('wp_ajax_active_creneau', 'active_creneau');
function active_creneau()
{
    if (isset($_POST['creno_id']) && isset($_POST['actif']) && isset($_POST['session_id']) && isset($_POST['objet_id']))
    {
        $objet = new $_POST['objet']($_POST['session_id'], $_POST['objet_id']);
        $objet->creneaux[$_POST['creno_id']] = $_POST['actif'];
        $objet->update_meta("creneaux", $objet->creneaux);
    }
    else
    {
        echo "Il manque des données !";
        var_dump($_POST);
    }
    
    die();
}

add_action('wp_ajax_get_session_stagiaire_data', 'get_session_stagiaire_data');
function get_session_stagiaire_data()
{
    if (isset($_POST['session_id']) && isset($_POST['user_id']) && isset($_POST['data']))
    {
        $session_stagiaire = new SessionStagiaire($_POST['session_id'], $_POST['user_id']);
        $data = $_POST['data'];
        
        $reponse = array();
        foreach($data as $d)
            $reponse[$d] = $session_stagiaire->$d;
        
        echo json_encode($reponse);
    }
    
    die();
}

add_action('wp_ajax_update_duree_st_session', 'update_duree_st_session');
function update_duree_st_session()
{
    $session_formation = new SessionFormation($_POST['session_id']);
    $session_formation->update_meta('nb_heure_decimal', $_POST['nb_heures']);
    
    die();
}

add_action('wp_ajax_update_jpost_value', 'update_jpost_value');
function update_jpost_value()
{
    global $wpof;
    $reponse = array();
    
    $object_class = (isset($_POST['object_class'])) ? $_POST['object_class'] : "SessionFormation" ;
    
    if (isset($_POST['stagiaire_id']) && (integer) $_POST['stagiaire_id'] > 0)
        $object_class = "SessionStagiaire";
    
    switch ($object_class)
    {
        case "SessionStagiaire":
            $session = get_stagiaire_by_id($_POST['session_id'], $_POST['stagiaire_id']);
            $session->update_meta($_POST['meta'], $_POST['value']);
            
            /*
            $session_formation = new SessionFormation($_POST['session_id']);
            $session_formation->init_stagiaires();
            
            switch ($_POST['meta'])
            {
                case 'tarif_total_chiffre':
                    $session->update_meta("tarif_base_total", 1);
                    $session->calcule_tarif();
                    $session->set_exercice_comptable();
                    $session_formation->calcule_budget_global();
                    $session->update_meta("tarif_heure", $session->tarif_heure);
                    $reponse['session_stagiaire']["tarif_heure"] = $session->tarif_heure;
                    $reponse['session_stagiaire']["exe_comptable"] = get_pilote_exe_comptable($session);
                    $reponse['session_formation']["budget_global"] = $session_formation->budget_global;
                    break;
                default:
                    break;
            }
            */
            break;
        case "Client":
            $client = new Client($_POST['session_id'], $_POST['client_id']);
            $client->update_meta($_POST['meta'], $_POST['value']);
            break;
        case "SessionFormation":
            $session = new SessionFormation($_POST['session_id']);
            $session->update_meta($_POST['meta'], $_POST['value']);
            
            switch ($_POST['meta'])
            {
                case 'tarif_total_chiffre':
                    if ($session->type_index != 'inter')
                    {
                        $session->update_meta("tarif_base_total", 1);
                        $session->calcule_tarif();
                        $session->calcule_budget_global();
                        $session->set_exercice_comptable();
                        $session->update_meta("tarif_heure", $session->tarif_heure);
                        $reponse['session_formation']["tarif_heure"] = $session->tarif_heure; //get_input_jpost("number", "tarif_heure");
                        $reponse['session_formation']["budget_global"] = $session->budget_global;
                        $reponse['session_formation']["exe_comptable"] = get_pilote_exe_comptable($session);
                    }
                    break;
    /*            case 'tarif_heure':
                    if ($session->type_index != 'inter')
                    {
                        $session->update_meta("tarif_base_total", 0);
                        $session->calcule_tarif();
                        $session->calcule_budget_global();
                        $session->update_meta("tarif_total_chiffre", $session->tarif_total_chiffre);
                        $reponse['session_formation']["tarif_total_chiffre"] = $session->get_input_jpost("number", "tarif_total_chiffre");
                        $reponse['session_formation']["budget_global"] = $session->budget_global;
                    }
                    break;*/
                default:
                    break;
            }
            break;
        default:
            $reponse['erreur'] = sprintf(__("%s n'est pas une classe d'objet reconnue"), $object_class);
            break;
    }
    
    if (!isset($reponse['erreur']))
    {
        if (isset($_POST['postprocess']))
            $reponse['postprocess_args'] = $_POST;
        
        $listname = $_POST['meta'];
        if ($_POST['nodeName'] == "select")
        {
            if (!isset($wpof->$listname))
                init_term_list($listname);
            $reponse['valeur'] = $wpof->$listname->get_term($_POST['value']);
        }
        else
            $reponse['valeur'] = $_POST['value'];
    }
    
    echo json_encode($reponse);
    die();
}


add_action('wp_ajax_update_pour_infos', 'update_pour_infos');
function update_pour_infos()
{
    $reponse = array();
    $object_class = $_POST['object_class'];
    switch ($object_class)
    {
        case "Client":
            $objet = new $object_class($_POST['session_id'], $_POST['client_id']);
            $reponse['parent'] = "#tab-c".$_POST['client_id'];
            break;
        case "SessionStagiaire":
            $objet = new $object_class($_POST['session_id'], $_POST['user_id']);
            $reponse['parent'] = "#tab-s".$_POST['user_id'];
            break;
        case "SessionFormation":
            $objet = new $object_class($_POST['session_id']);
            $reponse['parent'] = "#tab-session";
            break;
        default:
            $reponse['erreur'] = __("Classe inconnue ").$object_class;
            break;
    }
    
    if (!isset($reponse['erreur']))
        $reponse['html'] = $objet->get_pour_infos_box();
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_update_formation_value', 'update_formation_value');
function update_formation_value()
{
    update_post_meta($_POST['formation_id'], $_POST['meta'], $_POST['value']);
    
    die();
}

add_action('wp_ajax_update_lieu_details', 'update_lieu_details');
function update_lieu_details()
{
    $reponse = array();
    $session = get_session_by_id($_POST['session_id']);
    $reponse['html'] = $session->get_lieu_details();
    $reponse['log'] = var_export($_POST, true);
    
    echo json_encode($reponse);
    die();
}


add_action('wp_ajax_add_new_session', 'add_new_session');
function add_new_session()
{
    $reponse = array();
    $reponse['log'] = var_export($_POST, true);
    
    if ($_POST['formation'] == -1 && $_POST['session_unique_titre'] == "")
    {
        $reponse['erreur'] = __("Vous devez fournir un titre");
        echo json_encode($reponse);
        die();
    }
    
    $title = ($_POST['session_unique_titre'] != "") ? $_POST['session_unique_titre'] : get_the_title($_POST['formation']);
    if (empty($title))
    {
        $reponse['erreur'] = __("Titre non reconnu")." : ".__("ID formation : ").$_POST['formation'];
        echo json_encode($reponse);
        die();
    }
    $slug = sanitize_title($title)."-".date("Ymd-His");
    $session_id = wp_insert_post(array('post_title' => $title, 'post_type' => 'session', 'post_status' => 'publish', 'post_author' => get_current_user_id(), 'post_name' => $slug), true);
    if (is_wp_error($session_id))
    {
        $reponse['erreur'] = __("Erreur de création de session")." : ".__("WP Error : ").$session_id->get_error_message();
        echo json_encode($reponse);
        die();
    }

    if (!empty($_POST['formateur']))
    {
        // liste des formateurs sous forme de tableau
        $formateur = explode(',', $_POST['formateur']);
        
        // ID des formateurs dans la session
        update_post_meta($session_id, "formateur", $formateur);
    }
    
    // Accès sur invitation par défaut
    update_post_meta($session_id, "acces_session", "invite");
    
    // index de formation (-1 si pas de formation)
    update_post_meta($session_id, "formation", $_POST['formation']);
    
    if ($_POST['formation'] > 0)
    {
        // Spécialité copiée depuis la formation
        update_post_meta($session_id, "specialite", get_post_meta($_POST['formation'], "specialite", true));
        update_post_meta($session_id, "quizpr_id", get_post_meta($_POST['formation'], "quizpr_id", true));
        update_post_meta($session_id, "quizobj_id", get_post_meta($_POST['formation'], "quizobj_id", true));
        update_post_meta($session_id, "quizpr_parent_id", $_POST['formation']);
        update_post_meta($session_id, "quizobj_parent_id", $_POST['formation']);
    }
    
    //$reponse['succes'] = sprintf(__("La session <span class='bouton'><a href='%s'>%s</a></span> est créée.<br />Pour la gérer et la modifier cliquez sur le lien ci-dessus."), get_permalink($session_id), $title);
    $reponse['url'] = get_permalink($session_id);
    echo json_encode($reponse);
    
    die();
}


add_action('wp_ajax_add_new_client', 'add_new_client');
function add_new_client()
{
    $reponse = array();
    $reponse['log'] = var_export($_POST, true);
    
    if ($_POST['nom'] == "")
    {
        $reponse['erreur'] = __("Vous devez fournir un nom ou une raison sociale");
        echo json_encode($reponse);
        die();
    }
    
    if (!isset($_POST['session_id']))
    {
        $reponse['erreur'] = __("Identifiant de session absent");
    }
    $session_id = $_POST['session_id'];
    $session = new SessionFormation($session_id);
    
    if ($session == null)
    {
        $reponse['erreur'] = sprintf(__("La session dont l'ID est %d n'existe pas"), $session_id);
    }
    
    $client = new Client($session_id);
    
    $client->update_meta("nom", $_POST["nom"]);
    if (isset($_POST['contact']))
        $client->update_meta('contact', $_POST['contact']);
    
    // Si le client est un particulier, alors il est son propre contact
    if ($_POST['type_client'] == "part")
    {
        $client->update_meta('contact', $_POST['nom']);
    }
    
    if ($_POST['type_client'] != 'autre')
        $client->update_meta("financement", $_POST['type_client']);
    
    $session->clients[] = $client->id;
    $session->update_meta("clients", array_unique($session->clients));
    
    $reponse['succes'] = sprintf(__("Le client <a href='%s'>%s</a> est créé. Pour la gérer et la modifier cliquez sur le lien."), get_permalink($session_id), $_POST['nom']);
    $reponse['html'] = $session->get_tabs_clients();
    echo json_encode($reponse);
    
    die();
}


// Modification d'un créneau au niveau de la session
add_action('wp_ajax_edit_creneau', 'edit_creneau');
function edit_creneau()
{
    $reponse = array();
    $reponse['log'] = var_export($_POST, true);
    
    $session = get_session_by_id($_POST['session_id']);
    
    if (isset($_POST['creno_id']))
    {
        $data = $_POST;
        if (isset($_POST['lieu']))
            $data['lieu_id'] = $_POST['lieu'];
        
        $creno = new Creneau($_POST['creno_id']);
        $creno->init_from_form($data);
        $creno->update();
        
        $session->creneaux[$data['date']][] = $creno;
        $session->update_meta("creneaux");
        
        if ($_POST['creno_id'] < 0) // nouveau créneau
        {
            $reponse["replacewith"] = ".liste-creneau";
            $reponse["eventsfunc"] = "edit_date_add_events";
            $reponse['html'] = $session->get_html_ligne_creneaux(true, $data['date']);
        }
        else // créneau modifié
        {
            $reponse["replacewith"] = ".creneau";
            $reponse["eventsfunc"] = "edit_creneau_add_events";
            $reponse['html'] = $creno->get_html("", true);
        }
    }
    
    echo json_encode($reponse);
    
    die();
}

add_action('wp_ajax_del_date_creneau', 'del_date_creneau');
function del_date_creneau()
{
    $reponse = array();
    $reponse['log'] = var_export($_POST, true);
    
    $session = get_session_by_id($_POST['session_id']);
    
    if (isset($_POST['creneau']))
    {
        if (isset($session->creneaux))
        {
            $creno = new Creneau($_POST['creneau']);
            $creno->delete();
        }
    }
    else
    {
        if (isset($session->creneaux))
        {
            unset($session->creneaux[$_POST['date']]);
            $session->update_meta("creneaux");
        }
        else
        {
            $session->supprime_sous_entite("dates_array", $_POST['date']);
            $session->update_meta("dates_array");
        }
    }
    echo json_encode($reponse);
    
    die();
}

add_action('wp_ajax_update_session_titre', 'update_session_titre');
function update_session_titre()
{
    $reponse = array();
    $session = get_session_by_id($_POST['session_id']);
    if ($session->check_post_title())
        $reponse['titre'] = $session->titre_session;
    
    echo json_encode($reponse);
    die();
}
add_action('wp_ajax_update_exe_comptable', 'update_exe_comptable');
function update_exe_comptable()
{
    global $wpof;
    $reponse = array('message' => '');
    
    if (isset($_POST['stagiaire_id']) && (integer) $_POST['stagiaire_id'] > 0)
    {
        $session = new SessionStagiaire($_POST['session_id'], $_POST['stagiaire_id']);
    }
    else
    {
        $session = new SessionFormation($_POST['session_id']);
    }
    
    $total_tarif = $session->tarif_total_chiffre;
    $c_annee = (integer) $_POST['annee'];
    $c_tarif = (float) $_POST['tarif'];
    if ($c_tarif > $total_tarif) $c_tarif = $total_tarif;
    
    $tarif_a_repartir = $total_tarif - $c_tarif;
    
    $session->exe_comptable[$c_annee] = $c_tarif;
    
    if (count($session->exe_comptable) == 2)
        foreach($session->exe_comptable as $a => $t)
            if ($a != $c_annee)
                $session->exe_comptable[$a] = $tarif_a_repartir;
    
    $difference = array_sum($session->exe_comptable) - $total_tarif;

    if ($difference != 0)
        $reponse['message'] = "<span class='erreur'>".__("Il y a une différence de ").$difference." ".$wpof->monnaie_symbole."</span>";
    
    $reponse['inputs'] = $session->get_input_exe_comptable();
    
    $session->update_meta("exe_comptable");
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_quiz_add_line', 'quiz_add_line');
function quiz_add_line()
{
    $reponse = array();
    
    $quiz = new Quiz($_POST['quiz_id']);
    $reponse['html'] = $quiz->get_edit_line("", $_POST['type']);
    $reponse['log'] = var_export($_POST, true);
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_quiz_enregistrer', 'quiz_enregistrer');
function quiz_enregistrer()
{
    $reponse = array();
    $reponse['log'] = var_export($_POST, true);
    
    if (!empty($_POST['quiz_id']))
        $quiz = new Quiz($_POST['quiz_id']);
    else
    {
        $quiz = new Quiz();
        $quiz->quiz_id = $quiz->last_quiz_id() + 1;
        $quiz->set_identite($_POST['sujet'], -1);
    }
    
    $quiz->new_parent_id = $_POST['parent_id'];
    $quiz->parse_list($_POST['questions']);
    $quiz->update_questions();
    
    $reponse['quiz_id'] = $quiz->quiz_id; // le quiz_id peut changer suite à mise à jour, il faut le renvoyer
    $reponse['log'] = var_export($quiz->questions, true);
    
    echo json_encode($reponse);
    die();
}



add_action('wp_ajax_nopriv_set_default_tab', 'set_default_tab');
add_action('wp_ajax_set_default_tab', 'set_default_tab');
function set_default_tab()
{
    if (!session_id())
        @session_start();
    $_SESSION[$_POST['tab_name']] = $_POST['tab_id'] - 1;
    
    die();
}

add_action('wp_ajax_sql_session_formation', 'sql_session_formation');
function sql_session_formation()
{
    global $wpdb;
    
    $result = array();
    
    if (isset($_POST['stagiaire_id']) && (integer) $_POST['stagiaire_id'] > 1)
    {
        global $suffix_session_stagiaire;
        
        $query = $wpdb->prepare
        (
            "select meta_key, meta_value from ".$wpdb->prefix.$suffix_session_stagiaire." where session_id = '%d' and user_id = '%d';",
            $_POST['session_id'],
            $_POST['stagiaire_id']
        );
        
        $meta = $wpdb->get_results($query, ARRAY_A);
        foreach ($meta as $m)
            $result[$m['meta_key']] = $m['meta_value'];
        echo "<p>$query</p>";
        
        $objet = get_stagiaire_by_id($_POST['session_id'], $_POST['stagiaire_id']);
    }
    elseif (isset($_POST['client_id']) && (integer) $_POST['client_id'] > 1)
    {
        global $suffix_client;
        
        $query = $wpdb->prepare
        (
            "select meta_key, meta_value from ".$wpdb->prefix.$suffix_client." where session_id = '%d' and client_id = '%d';",
            $_POST['session_id'],
            $_POST['client_id']
        );
        
        $meta = $wpdb->get_results($query, ARRAY_A);
        foreach ($meta as $m)
            $result[$m['meta_key']] = $m['meta_value'];
        echo "<p>$query</p>";
        
        $objet = get_client_by_id($_POST['session_id'], $_POST['client_id']);
    }
    else
    {
        $query = $wpdb->prepare
        (
            "select * from ".$wpdb->prefix."posts where ID = '%d';",
            $_POST['session_id']
        );
        
        $result = $wpdb->get_results($query, ARRAY_A);
        $result = $result[0];
        echo "<p>$query</p>";
        
        $query = $wpdb->prepare
        (
            "select meta_key, meta_value from ".$wpdb->prefix."postmeta where post_id = '%d';",
            $_POST['session_id']
        );
        
        $meta = $wpdb->get_results($query, ARRAY_A);
        foreach ($meta as $m)
            $result[$m['meta_key']] = $m['meta_value'];
        echo "<p>$query</p>";
        
        $objet = get_session_by_id($_POST['session_id']);
    }
    
    echo "<h2>SQL</h2>";
    echo "<table class='sql'>";
    foreach ($result as $k => $v)
        echo "<tr><td>$k</td><td>$v</td></tr>";
    echo "</table>";
    
    echo "<h2>Objet</h2>";
    echo "<table class='sql'>";
    foreach ((array)$objet as $k => $v)
        echo "<tr><td>$k</td><td>".var_export($v, true)."</td></tr>";
    echo "</table>";
    
    die();
}

add_action('wp_ajax_archive_file', 'archive_file');
function archive_file()
{
    require_once(wpof_path . "/class/class-upload.php");
    
    $reponse = array('message' => '', 'filename' => '', 'lignedoc' => '');
    
    if (isset($_POST['session_id']) && $_POST['session_id'] > 0)
    {
        $session_id = $_POST['session_id'];
        global $wpof;
        $session_formation = get_session_by_id($session_id);
        
        if (!empty($_POST['contexte_id']) && $_POST['contexte_id'] > 0)
        {
            $contexte_id = $_POST['contexte_id'];
            $contexte = $_POST['contexte'];
        }
        else
        {
            $contexte_id = -1;
            $contexte = $wpof->doc_context->session;
        }
        
        for ($i = 0; $i < $_POST['nb_files']; $i++)
        {
            if ($_FILES["files"]["error"][$i] > 0)
            {
                $reponse['message'] .= "<span class='erreur'>".__("Erreur de téléversement code ")." ".$_FILES["files"]["error"][$i]." ".__("pour le fichier")." ".$_FILES["files"]["name"][$i]."</span>";
            }
            else
            {
                $upload = new Upload($session_id);
                
                if (!is_dir($upload->path))
                    mkdir($upload->path, 0777, true);
                
                if (isset($_POST['type_doc']))
                {
                    $type_doc = $_POST['type_doc'];
                
                    $doc = new Document($type_doc, $session_id, $contexte, $contexte_id);
                    
                    $extension = explode(".", $_FILES['files']['name'][0]);
                    $extension = end($extension);

                    $upload->filename = $doc->pdf_filename = preg_replace('/pdf$/', $extension, $doc->pdf_filename);
                    
                    $doc->update_meta('html_filename', "");
                    $doc->update_meta('last_modif', current_time('timestamp'));
                    $doc->valid |= Document::SCAN;
                    $doc->valid &= ~Document::DRAFT;
                    
                    if ($_POST["signature_responsable"] == "true")
                    {
                        $doc->valid |= Document::VALID_RESPONSABLE_DONE;
                        $doc->valid &= ~ Document::VALID_RESPONSABLE_REQUEST;
                    }
                    else
                        $doc->valid &= ~ Document::VALID_RESPONSABLE_DONE;
                    
                    if ($_POST["signature_client"] == "true")
                        $doc->valid |= Document::VALID_CLIENT_DONE;
                    else
                        $doc->valid &= ~ Document::VALID_CLIENT_DONE;
                        
                    if ($_POST["signature_stagiaire"] == "true")
                        $doc->valid |= Document::VALID_STAGIAIRE_DONE;
                    else
                        $doc->valid &= ~ Document::VALID_STAGIAIRE_DONE;
                    
                    $doc->update_meta('valid', $doc->valid);
                    
                    $doc->init_link_name();
                    
                    $reponse['lignedoc'] = $doc->get_html_ligne((isset($_POST['cols'])) ? $_POST['cols'] : Document::COL_ALL);
                }
                else
                    $upload->filename = $_FILES['files']['name'][$i];
                
                if (file_exists($upload->path . $upload->filename))
                    rename($upload->path . $upload->filename, $upload->path . preg_replace("((.*)\.)", "$1-".date("Ymd_His").".", $upload->filename));
                move_uploaded_file( $_FILES['files']['tmp_name'][$i], $upload->path . $upload->filename);
                
                if (!isset($_POST['type_doc']))
                {
                    $upload->set_md5sum();
                    
                    $upload->timestamp = time();
                    $upload->date_text = date_i18n("j F Y H:i:s", $upload->timestamp);
                    
                    $session_formation->uploads[$upload->md5sum] = $upload;
                }
                
                $reponse['message'] .= "<span class='succes'>".__("Fichier correctement copié")." ".$upload->filename."</span>";
            }
        }
        update_post_meta($session_id, "uploads", serialize($session_formation->uploads));
        foreach ($session_formation->uploads as $u)
        {
            $reponse['filename'] .= $u->get_html('tr');
        }
    }
    else
    {
        $reponse['message'] .= "<span class='erreur'>".__("Aucune session à laquelle rattacher ce fichier")."</span>";
    }
    
    $log = $_POST;
    unset($log['files']);
    $reponse['log'] = $log;
    
    echo json_encode($reponse);

    die();
}

add_action('wp_ajax_delete_scan_file', 'delete_scan_file');
function delete_scan_file()
{
    require_once(wpof_path . "/class/class-upload.php");

    $reponse = array('message' => '', 'succes' => '');
    
    if (isset($_POST['session_id']) && $_POST['session_id'] > 0)
    {
        $session_id = $_POST['session_id'];
        $md5sum = $_POST['md5sum'];
        
        global $SessionFormation;
        $SessionFormation[$session_id] = new SessionFormation($session_id);
        $session_formation =& $SessionFormation[$session_id];
        
        $upload = $session_formation->uploads[$md5sum];
        
        if (file_exists($upload->path . $upload->filename))
        {
            unlink($upload->path . $upload->filename);
            $reponse['message'] = "<span class='succes'>".sprintf(__("Supression effective de %s"), $upload->path . $upload->filename)."</span>";
        }
        else
            $reponse['message'] = "<span class='erreur'>".sprintf(__("Le fichier %s n'existe pas"), $upload->path . $upload->filename)."</span>";
            
        $reponse['succes'] = true;
        unset($session_formation->uploads[$md5sum]);
        update_post_meta($session_id, "uploads", serialize($session_formation->uploads));
    }
        
    echo json_encode($reponse);

    die();
}

?>
