<?php
/*
 * wpof-dialog.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

add_action('wp_ajax_the_dynamic_dialog', 'the_dynamic_dialog');
add_action('wp_ajax_nopriv_the_dynamic_dialog', 'the_dynamic_dialog');
function the_dynamic_dialog()
{
    $function = null;
    if (isset($_POST['function']) && function_exists($_POST['function']))
        $function = $_POST['function'];

    $param = array();
    switch ($function) 
    {
        case 'new_session':
            $titre = __("Créez une nouvelle session");
            break;
        case 'new_client':
            $titre = __("Créez un nouveau client");
            $param['session_id'] = $_POST['session_id'];
            break;
        case 'new_stagiaire':
            $titre = __("Inscrivez un⋅e stagiaire");
            $param['session_id'] = $_POST['session_id'];
            $param['client_id'] = $_POST['client_id'];
            break;
        case "add_or_edit_creneau":
            $titre = __("Modifiez un créneau de formation");
            $param = $_POST;
            break;
        case 'premier_contact':
            $titre = __("Renseignements et/ou inscription");
            $param['session_id'] = $_POST['session_id'];
            break;
        case "debug_SESSION":
            $titre = __("Variables de \$_SESSION");
            break;
        case "sql_session_formation":
            $titre = __("Requêtes SQL SELECT sur la session");
            $param['session_id'] = $_POST['session_id'];
            if (isset($_POST['client_id'])) $param['client_id'] = $_POST['client_id'];
            if (isset($_POST['stagiaire_id'])) $param['stagiaire_id'] = $_POST['stagiaire_id'];
            break;
        default:
            $titre = __("Fonction inconnue…");
            break;
    }
    
    
    ?>
    <div class="dialog edit-data" style="" title="<?php echo $titre; ?>">
    <?php
        if ($function)
        {
            echo "<form>";
            if (count($param) > 0)
                echo $function($param);
            else
                echo $function();
            echo "</form>";
            
            echo "<p class='message'></p>";
        }
        else
            echo "<p class='erreur'>".__("Aucune fonction pour afficher le contenu de cette boîte de dialogue.")."</p>";
    ?>
    </div>
    
    <?php
    die();
}

/*
 * Formulaire pour créer une nouvelle session de formation
 */
function new_session()
{
    $session = new SessionFormation();
    
    init_term_list("formation");
    echo get_input_jpost($session, "formation", array('select' => '', 'label' => __("Formation du catalogue"), 'first' => __("Session unique, sans lien avec le catalogue")));
    echo get_input_jpost($session, "session_unique_titre", array('input' => 'text', 'label' => __("Intitulé de session unique"), 'size' => '80'));
    init_term_list("formateur");
    
    $current_user_id = get_current_user_id();
    $role = wpof_get_role($current_user_id);
    if ($role == "um_formateur-trice")
        $session->formateur[] = $current_user_id;
    echo get_input_jpost($session, "formateur", array('select' => 'multiple', 'label' => __("Équipe pédagogique")));
    
    // Nom de l'action a effectuer ensuite
    echo "<input type='hidden' name='action' value='add_new_session' />";
    //echo "<input type='hidden' name='close_on_valid' value='1' />";
}

/*
 * Formulaire pour créer un nouveau client
 */
function new_client($param)
{
    $session_id = $param['session_id'];
    $client = new Client($session_id);
    
    ?>
    <p><?php _e("Le client est :"); ?></p>
    <p><input type="radio" name="type_client" id="opac" value="opac" /><label for="opac"><?php _e("Un organisme de formation (OPAC) différent du votre"); ?></label><br />
    <input type="radio" name="type_client" id="part" value="part" /><label for="part"><?php _e("Un particulier à ses frais"); ?></label><br />
    <input type="radio" name="type_client" id="autre" checked="checked" value="autre" /><label for="autre"><?php _e("Autre (vous préciserez le type de financement ultérieurement)"); ?></label></p>
    
    <?php
    echo get_input_jpost($client, "nom", array('input' => 'text', 'label' => __('Nom ou raison sociale')));
    echo get_input_jpost($client, "contact", array('input' => 'text', 'label' => __('Nom du contact')));
    echo hidden_input("session_id", $session_id);
    
    // Nom de l'action a effectuer ensuite
    ?>
    <input type='hidden' name='action' value='add_new_client' />
    <input type='hidden' name='close_on_valid' value='1' />
    <input type='hidden' name='reload_on_close' value='1' />
    <?php
}

/*
 * Formulaire pour créer de nouveaux stagiaires
 */
function new_stagiaire($param)
{
    ?>
    <input type="hidden" name="session_id" value="<?php echo $param['session_id']; ?>" />
    <input type="hidden" name="client_id" value="<?php echo $param['client_id']; ?>" />
    <input type='hidden' name='action' value='add_stagiaire' />
    
    <label class="top" for="genre"><?php _e("Genre"); ?></label><select name="genre" id="genre"><option value="Madame">Madame</option><option value="Monsieur">Monsieur</option></select>
    <label class="top" for="firstname"><?php _e("Prénom"); ?></label><input type="text" name="firstname" id="firstname" />
    <label class="top" for="lastname"><?php _e("Nom"); ?></label><input type="text" name="lastname" id="lastname" />
    <label class="top" for="email"><?php _e("Courriel, optionnel"); ?></label><input type="text" name="email" id="email" />
    <input type='hidden' name='role' value='um_stagiaire' />

    <input type='hidden' name='reload_on_close' value='1' />
    <input type='hidden' name='refresh_on_success' value='1' />
    <?php
}

/*
 * Formulaire de modification d'un créneau existant ou vide
 */
function add_or_edit_creneau($param)
{
    $creno = new Creneau((integer) $param['creno_id']);
    $creno->init_from_form($param);
    
    echo $creno->get_creneau_form();
    ?>
    <input type='hidden' name='action' value='edit_creneau' />
    <input type='hidden' name='close_on_valid' value='1' />
    <?php
}

/*
 * Créer un formulaire pour l'inscription/demande d'infos/prise de contact des stagiaires
 * La sécurité repose sur des champs cachés et une question
 */
function premier_contact($param)
{
    $session = get_session_by_id($param['session_id']);
    
    ?>
    <h3><?php echo $session->titre_session; ?></h3>
    <p><?php _e("Vous souhaitez :"); ?></p>
    <p><input id="inscription" type="radio" name="choix" value="inscription" /><label for="inscription"><?php _e("vous inscrire à cette session"); ?></label></p>
    <p><input id="informations" type="radio" name="choix" value="informations" /><label for="informations"><?php _e("seulement prendre contact pour plus d'informations"); ?></label></p>
    <label class="top" id="id" for="identifiant"><?php _e("Choisissez votre identifiant"); ?></label><input id="identifiant" type="text" name="identifiant" />
    <label class="top" id="pn" for="nom"><?php _e("Saisissez votre prénom et votre nom"); ?></label><input id="nom" type="text" name="nom" />
    <label class="top" id="em" for="email"><?php _e("Saisissez votre email"); ?></label><input id="email" type="email" name="email" />
    <label class="top" id="tel" for="telephone"><?php _e("Saisissez un numéro de téléphone pour être rappelé⋅e"); ?></label><input id="telephone" type="text" name="telephone" />
    <label class="top" id="c" for="verif"><?php _e("Quel mois de l'année sommes-nous ?"); ?></label><input id="verif" type="text" name="verif" />
    <input type='hidden' name='action' value='first_contact' />
    <input type='hidden' name='session_id' value='<?php echo $session->id; ?>' />
    <?php
}

function debug_SESSION()
{
    ?>
    <pre>
    <?php var_dump($_SESSION); ?>
    </pre>
    <?php
}


?>
