<?php
/*
 * wpof-fonctions.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

/*
 * Fonctions génériques, pour tout usage
 */

/*
 * Transforme une liste de termes en tableau
 * | sépare l'index de la valeur
 * Il est possible qu'il y ait des indications après un #, elles sont stockées dans un sous-tableau indicé par 'complement'.
 * Sauf si l'indication dit que cette valeur n'est pas visible pour le rôle de l'utilisateur courant, alors, on ne stocke rien.
 */
function text_to_array($text)
{
    $result = array();
    $role = wpof_get_role(get_current_user_id());
    
    foreach (preg_split("/\n/", $text) as $line)
    {
        $index = "";
        $visible = true;
        $complement = array();
        
        $line = explode("|", $line);
        if (isset($line[1]))
        {
            $index = sanitize_title($line[0]);
            $line = $line[1];
        }
        else
            $line = $line[0];
        
        
        $line = explode("#", $line);
        if ($index == "")
            $index = trim($line[0]);
        
        
        if (isset($line[1]) && "" != $mode = trim($line[1]))
        {
            switch ($mode)
            {
                // Affichage d'un input text additionnel
                case "T":
                    $complement['text'] = trim($line[2]);
                    break;
                // affichage conditionné au fait que l'utilisateur ait un rôle parmi ceux cités
                case "U":
                    $auth_role = explode(",", trim($line[2]));
                    if (!in_array($role, $auth_role)) $visible = false;
                    break;

                // affichage conditionné au fait que l'utilisateur ait un rôle absent de ceux cités
                case "!U":
                case "U!":
                    $not_auth_role = explode(",", trim($line[2]));
                    if (in_array($role, $not_auth_role)) $visible = false;
                    break;
            }
        }
        if ($visible)
        {
            $result[$index]['value'] = $line[0];
            if (count($complement) > 0)
                $result[$index]['complement'] = $complement;
        }
    }
    
    return $result;
}



/*
 * Crée une boîte radio ou cases à cocher à partir d'un texte simple (une ligne par bouton) ou d'un tableau
 * $name : nom du groupe
 * $type : radio ou checkbox
 * $value1 : valeur principale
 * $value2 : valeur secondaire
 */
function text_to_choices($text, $name, $type = 'radio', $value1 = "", $value2 = "")
{
    $html = "<ul class='choix-interactif'>";
    
    if (!is_array($text))
        $text = text_to_array($text);
        
    foreach ($text as $index => $value)
    {
        $second_bloc = "";
        $clic_class = "";
        
        if (checked($value1, $index, false))
        {
            $checked = "checked='checked'";
            $display = "style='display: inline;'";
        }
        else
        {
            $checked = "";
            $display = "style='display: none;'";
        }
        
        if (isset($text[$index]['complement']))
        {
            $complement = $text[$index]['complement'];
            
            foreach($complement as $mode => $compl_value)
            {
                switch ($mode)
                {
                    // Affichage d'un input text additionnel
                    case "text":
                        $second_bloc = " <span $display id='${name}_".$index."'>$compl_value <input type='text' name='${name}_complement' value='".$value2."'/></span>";
                        $clic_class = "autre";
                        break;
                }
            }
        }
        
        $html .= "<li><label class='$clic_class'><input type='$type' name='$name' value=\"".$index."\" $checked />".$value['value']."$second_bloc</label></li>";
    }
    $html .= "</ul>";
    
    return $html;
}


/*
 * Crée une liste déroulante à partir d'un type de contenu
 * Chaque option aura pour identifiant l'ID du contenu et affichera le titre du contenu
 * $name : nom de la variable renseignée
 *
 */
function select_post_by_type($type, $name, $selected = "", $first = null)
{
    $posts = get_posts( array( 'post_type' => $type, 'numberposts' => -1, 'order' => 'ASC') );
    //var_dump($posts);
    $liste = array();
    if ($posts != NULL)
    {
        foreach ($posts as $p)
        {
            $liste[$p->ID] = $p->post_title;
        }
        
        $line = "<select name='".$name."'>";
	if ($first)
            $line .= "<option value='-1' ".selected($selected, -1, false).">$first</option>";
            
	foreach ($liste as $id => $title)
	{
            $line .= "<option value=\"$id\" ".selected($selected, $id, false).">$title</option>";
	}
	$line .= "</select>";
	return $line;
    }
    else
        return null;
}

/*
 * Crée une liste déroulante à partir d'utilisateurs
 * Chaque option aura pour identifiant l'ID de l'utilisateur et affichera son display_name
 * $args : les arguments pour get_users
 * $name : nom de la variable renseignée
 * $selected : la valeur à pré-sélectionner
 * $first : texte qui apparaît par défaut en début de liste si rien de sélectionné
 * $multiple : si true, alors on peut sélectionner plusieurs utilisateurs
 *
 */
function select_user($args = array(), $name, $selected = "", $first = null, $multiple = false)
{
    $users = get_users( array_merge($args, array('numberposts' => -1, 'role') ) );
    
    if(!is_array($selected)) $selected = array($selected);
    
    $multiple = ($multiple) ? "multiple='multiple' size='10'" : "";
    
    $liste = array();
    if ($users != NULL)
    {
        $line = "<select name='".$name."' $multiple>";
	if ($first)
            $line .= "<option value='-1'>$first</option>";
            
        foreach ($users as $u)
        {
            $selected_mention = (in_array($u->ID, $selected)) ? "selected='selected'" : "";
            $line .= "<option value='".$u->ID."' $selected_mention>".$u->display_name."</option>";
        }
        
	$line .= "</select>";
	return $line;
    }
    else
        return null;
}

/*
 * Crée une liste à partir d'un tableau
 * $list : tableau de données
 * $name : nom de la liste
 * $selected : valeur sélectionnée
 * $options : ajout éventuel d'attributs data- utilisés ensuite par jQuery ou de toute autre option
 */
function select_by_list($list, $name, $selected = "", $options = "", $first = null)
{
    $html = "<select name='$name' $options>";
    
    if ($first)
        $html .= "<option value='-1' class='first'><em>$first</em></option>";
    
    foreach ($list as $key => $value)
    {
        if (is_array($value))
        {
            $html .= "<optgroup label='".$value['group']."'>";
            foreach($value as $gk => $gv)
                $html .= "<option value=\"$gk\" ".selected($selected, $gk, false).">$gv</option>";
            $html .= "</optgroup>";
        }
        else
            $html .= "<option value=\"$key\" ".selected($selected, $key, false).">$value</option>";
    }
    $html .= "</select>";
    
    return $html;
}

/*
 * Simple création d'un input hidden
 */
function hidden_input($name, $value)
{
    return "<input type='hidden' name='$name' value=\"$value\" />";
}


/*
 * Création d'un input (ou select) avec multiples options
 * L'idée est de faire un input universel
 * Affiche un div de classe input_jpost contenant
 * * un span affichant la valeur
 * * l'input demandé pour changer la valeur
 * L'un ou l'autre peut être affiché via la CSS
 *
 * Paramètres
 * * object : l'objet contenant le paramètre à modifier/afficher et doté d'une fonction update_meta
 * * name : le nom de la propriété à modifier/afficher
 * * args : tableau pouvant renseigner des paramètres comme
    input (text, number, time, checkbox)
    select (multiple ou rien),
    size,
    step, min, max,
    label,
    first (premier élément de liste, hors liste)
    before (affiché avant l'input ou le span.valeur), after (affiché après l'input ou le span.valeur)
    display : valeur de style CSS pour display
    class : classe CSS à appliquer au div global
    readonly : lecture seule
 */
function get_input_jpost($object, $name, $args = array())
{
    global $wpof;
    
    // Vérification de l'objet passé
    try
    {
        $object_class = get_class($object);
    }
    catch(Exception $e)
    {
        return "<span class='erreur'>Classe d'objet non reconnu ".$e->getMessage()."</span>";
    }
    
    // Vérification de l'existence de la propriété $name
    if (!isset($object->$name))
        return "<span class='erreur'>L'objet de classe $object_class n'a pas de propriété $name</span>";
    
    $readonly = isset($args['readonly']);
    
    $type_global = "";
    // type global d'input
    foreach(array('input', 'select', 'textarea', 'editor') as $t)
    {
        if (isset($args[$t]))
        {
            if ($t == "input")
                $type_global = $args['input'];
            else
                $type_global = $t;
        }
    }
    
    // modification de style
    $style = "";
    if (isset($args['display']))
        $style = "style='display: ".$args['display']."'";
    
    // classes additionelles
    $add_class = "";
    if (isset($args['class']))
        $add_class = $args['class'];
    
    // classe valeur pour l'affiche hors edit-data
    // ne doit pas être appliquée si readonly vaut true
    $classe_valeur = ($readonly) ? "" : "class='valeur'";
    
    // Début de la création du div input_jpost
    $html = "<div class='input_jpost $name $type_global $add_class' $style>";
    // Identifiant unique
    $input_id = $name.rand();
    
    // Affichage avant et après
    $before = (isset($args['before'])) ? $args['before'] : "";
    $after = (isset($args['after'])) ? $args['after'] : "";
    
    // Ajout d'un label si défini, contenant les icônes fonctionnelles
    $label_header = $label_input = "";
    if (isset($args['label']))
    {
        $label_input = "<label class='top input_jpost_label' for='$input_id'>{$args['label']} ".get_icons()."</label>";
        $label_header = "<h3>{$args['label']}</h3>";
    }
    
    // Tests sur le type d'input
    if (isset($args['input']))
    {
        // Affichage de la valeur de la propriété $name
        $html .= $label_input;
        $html .= "<span $classe_valeur>$before {$object->$name} $after</span>";
    
        if (!$readonly)
        {
            $type = $args['input'];
            $options = array();
            $value = $object->$name;
            
            switch($type)
            {
                case "time":
                case "number":
                    foreach(array('step', 'min', 'max') as $o)
                        $options[] = (isset($args[$o])) ? "$o='".$args[$o]."' " : "";
                    break;
                case "checkbox":
                    $value = 1;
                    $options[] = checked($object->$name, 1, false);
                    break;
                case "text":
                    $o = 'size';
                    $options[] = (isset($args[$o])) ? "$o='".$args[$o]."' " : "";
                    break;
            }
            
            $html .= "$before <input class='input_jpost_value' type='$type' ".join(" ", $options)." id='$input_id' name='$name' value='$value' /> $after";
        }
    }
    elseif (isset($args['select']))
    {
        $html .= $label_input;
        $list = $wpof->$name;
        if (get_class($list) == "TermList")
        {
            // Affichage de la valeur de la propriété $name
            $html .= "<span $classe_valeur>".$list->get_term($object->$name)."</span>";
            if (!$readonly)
            {
                $options = $supp_class = "";
                if ($args['select'] == "multiple")
                {
                    $options = "multiple='multiple'";
                    $size = "";
                    if (isset($args['rows'])) $size = $args['rows'];
                    if (isset($args['size'])) $size = $args['size'];
                    if ($size != "") $options .= " size='$size'";
                    $supp_class = "select-multiple";
                    $first = null;
                }
                else
                    $first = __("Choisissez");
                
                if (isset($args['first']))
                    $first = $args['first'];
                elseif (!is_array($object->$name) && !isset($list->term[$object->$name]) && !empty($object->$name))
                    $first = $object->$name." — terme inconnu, à changer !!";
                $html .= "<div class='select $supp_class'>$before ".$list->get_select_list($object->$name, $options, $first)." $after</div>";
            }
        }
        else
            $html .= "<span class='erreur'>$name ".__("n'est pas un objet TermList mais ").get_class($list)."</span>";
    }
    elseif (isset($args['textarea']))
    {
        $html .= $label_input;
        $options = array();
        foreach(array('rows', 'cols', 'maxlength', 'minlength', 'disabled') as $o)
            $options[] = (isset($args[$o])) ? "$o='".$args[$o]."' " : "";
        
        $html .= "<div $classe_valeur>$before {$object->$name} $after</div>";
        
        if (!$readonly)
            $html .= "$before <textarea ".join(" ", $options)." id='$input_id' name='$name'>".$object->$name."</textarea> $after";
    }
    elseif (isset($args['editor']))
    {
        switch ($args['editor'])
        {
            case "quiz":
                global $tinymce_wpof_quiz_settings;
                $tinymce_settings = $tinymce_wpof_quiz_settings;
                break;
            default:
                global $tinymce_wpof_settings;
                $tinymce_settings = $tinymce_wpof_settings;
                break;
        }
        
        ob_start();
        $html .= $label_header;
        echo "<div $classe_valeur>$before {$object->$name} $after</div>";
        
        if (!$readonly)
            wp_editor($object->$name, $name, $tinymce_settings);
        
        $html .= ob_get_clean();
    }
    
    
    // Icônes fonctionnelles en-dessous si pas de label
    if (!isset($args['label']))
        $html .= get_icons();
    
    // Infos complémentaires pour retrouver l'objet dans la fonction appelée par Ajax
    $html .= "<input type='hidden' name='object_class' value='$object_class' />";
    
    switch ($object_class)
    {
        case "SessionFormation":
            $html .= "<input type='hidden' name='session_id' value='{$object->id}' />";
            break;
        case "Client":
            $html .= "<input type='hidden' name='session_id' value='{$object->session_formation_id}' />";
            $html .= "<input type='hidden' name='client_id' value='{$object->id}' />";
            break;
        case "SessionStagiaire":
            $html .= "<input type='hidden' name='session_id' value='{$object->session_formation_id}' />";
            $html .= "<input type='hidden' name='client_id' value='{$object->client_id}' />";
            $html .= "<input type='hidden' name='stagiaire_id' value='{$object->user_id}' />";
            break;
        case "Creneau":
            $html .= "<input type='hidden' name='session_id' value='{$object->session_id}' />";
            break;
    }
    if (isset($args['postprocess']))
        $html .= "<input type='hidden' name='postprocess' value='".$args['postprocess']."' />";
        
    $html .= "</div>";
    
    return $html;
}

// ajout d'un bouton qui ajoute du contenu dans l'éditeur
// ajout d'un bouton qui efface le contenu de l'éditeur
function content_editor_buttons($editor_id)
{
    $post_ID = get_the_ID();
    if (get_post_type($post_ID) != "session") return;
    
    $id_formation = get_post_meta($post_ID, "formation", true);
    $button_add_disabled = ($id_formation == "" || $id_formation == -1) ? "disabled" : "";
    $object_class = "SessionFormation";
    if (substr_compare($editor_id, "quiz", 0, 4) == 0)
        $object_class = "Quiz";
    
    if (in_array($editor_id, array('presentation', 'objectifs', 'prerequis', 'pre_requis', 'public_cible', 'programme', 'materiel_pedagogique', 'quizpr', 'quizobj', 'ressources')))
    {
        echo "<button class='button add-content-formation' data-content='$editor_id' data-formationid='$id_formation' type='button' $button_add_disabled>".__("Ajouter texte depuis catalogue")."</button>";
    }
    echo "<button class='button del-content' data-content='$editor_id' type='button'>".__("Effacer tout")."</button>";
    echo "<button class='button save-content' data-content='$editor_id' data-class='$object_class' type='button'>".__("Enregistrer")."</button>";
    echo get_icons();
}
add_action('media_buttons', 'content_editor_buttons');

function get_icons()
{
    $html = "";
    
    // Icône d'enregistrement de l'info modifiée
    $html .= "<span class='valid dashicons dashicons-yes-alt'></span>";
    
    return $html;
}

// Création des TermList à la demande
function init_term_list($listname)
{
    global $wpof;
    
    switch ($listname)
    {
        case "formation":
        case "lieu":
            $post = get_posts(array( 'post_type' => $listname, 'posts_per_page' => -1));
            $wpof->$listname = new TermList($listname);
            foreach($post as $p)
                $wpof->$listname->add_term($p->ID, $p->post_title);
            break;
        case "formateur":
            $formateur_users = get_users(array('role__in' => array('um_formateur-trice', 'um_responsable')));
            $wpof->formateur = new TermList("formateur");
            foreach($formateur_users as $u)
            {
                $nom = $u->data->display_name;
                if ($wpof->formateur_marque)
                {
                    $marque = get_user_meta($u->ID, "formateur_marque", true);
                    if (!empty($marque))
                        $nom .= " – $marque";
                }
                $wpof->formateur->add_term($u->ID, $nom);
            }
            break;
        default:
            $wpof->$listname = new TermList($listname);
            $wpof->$listname->add_term(-1, $listname.__("n'est pas une liste !"));
            break;
    }
}

add_action('wp_ajax_add_content_formation', 'add_content_formation');
function add_content_formation()
{
    require_once(wpof_path . "/class/class-quiz.php");
    $formation_id = (isset($_POST['formation_id'])) ? $_POST['formation_id'] : -1;
    
    if ($formation_id > 0)
    {
        if (isset($_POST['content']))
        {
            $editor_id = $_POST['content'];
            
            if (substr_compare($editor_id, "quiz", 0, 4) == 0)
            {
                $quiz_id = get_post_meta($formation_id, $editor_id."_id", true);
                $quiz = new Quiz($quiz_id);
                $quiz->init_questions($quiz_id);
                echo $quiz->get_html_questions();
            }
            else
                echo wpautop(get_post_meta($formation_id, $editor_id, true));
        }
    }

    die();
}


/*
  Conversion de dates depuis une chaîne de caractères
  Il peut y avoir plusieurs dates
  Toutes doivent être sous le format JJ/MM/AAAA
  
  Retour : un tableau ordonné de dates avec en clé le format timestamp et en valeur quelque chose de lisible facilement
  */
function convert_dates($str)
{
    foreach(preg_split("/[\s]+/", $str) as $d)
    {
        $timestamp =  date_create_from_format("d/m/Y", $d)->getTimestamp();
        $date[$timestamp] = "<span class='jour'>".date_i18n("j", $timestamp)."</span> ";
        $date[$timestamp] .= "<span class='mois'>".date_i18n("F", $timestamp)."</span> ";
        $date[$timestamp] .= "<span class='annee'>".date_i18n("Y", $timestamp)."</span>";
    }
    
    // tri selon les indices (timestamp)
    ksort($date);
    return $date;
}

/*
 * Tri des dates dans l'ordre chronologique
 * $dates : tableau de dates au format JJ/MM/AAAA
 * return : le même tableau trié et expurgé des dates vides
 */
function sort_dates($dates)
{
    $tsdates = array();
    foreach($dates as $d)
    {
        if ($d != "")
            $tsdates[] = date_create_from_format("d/m/Y", $d)->getTimestamp();
    }
    sort($tsdates);
    
    $dates = array();
    foreach($tsdates as $ts)
    {
        $dates[] = date("d/m/Y", $ts);
    }
    return $dates;
}

/*
 * Fonction pour afficher les dates de manière plus jolie que simplement une liste de jj/mm/aaaa
 * En entrée, du texte avec une date par ligne au format jj/mm/aaaa
 * En sortie, ce qui doit être affiché
 */
function pretty_print_dates($dates)
{
    if (!is_array($dates))
        $dates = preg_split("/[\s]+/", $dates);
        
    $ts = array();
    foreach($dates as $d)
    {
        $ts[] = date_create_from_format("d/m/Y", $d)->getTimestamp();
    }
    
    if (count($ts) > 1)
    {
        // Tri en ordre anti-chronologique
        rsort($ts);
        
        // On sort la première date (dernière par ordre chronologique)
        $last_ts = array_shift($ts);
        $datestr = " et ".date_i18n("j F Y", $last_ts);
        
        foreach($ts as $d)
        {
            $format = "j";
            if (date_i18n("F", $d) != date_i18n("F", $last_ts)) $format .= " F";
            if (date_i18n("Y", $d) != date_i18n("Y", $last_ts)) $format .= " Y";
            $datestr = date_i18n(", $format", $d) . $datestr;
            if (date_i18n("j", $d) == 1)
                $datestr = preg_replace("/1 /", "1<sup>er</sup> ", $datestr, 1);
            $last_ts = $d;
        }
        // On doit encore enlever les caractères ", " en début de chaîne
        $datestr = substr($datestr, 2);
    }
    else
    {
        $d = reset($ts);
        $datestr = date_i18n("j F Y", $d);
        if (date_i18n("j", $d) == 1)
            $datestr = preg_replace("/1 /", "1<sup>er</sup> ", $datestr, 1);
    }
    
    return $datestr;
}


/*
 * Certaines variables sont stockées sous la forme de tableau, même si valeur unique (exemple, le genre)
 * Cette fonction permet d'analyser une variable et de retourner
 * sa valeur inchangée si c'est une string
 * sa première valeur si c'est un tableau
 */
function str_or_array($data)
{
    if (is_array($data))
        return $data[0];
    else
        return $data;
}

function array_map_td($c)
{
    return "<td>$c</td>";
}

function pilote_array_map_th($c)
{
    return "<th>".$c['text']."</th>";
}

// l'utilisateur courant (ou $user_id) peut-il signer les documents
function is_signataire($user_id = -1)
{
    global $wpof;
    if ($user_id == -1)
        $user_id = get_current_user_id();
    $role = wpof_get_role($user_id);
    return (($role == "um_responsable" && $user_id == $wpof->respform_id) || ($role == "admin" && $wpof->respform_admin == 1));
}


// retourne le texte du tarif de formation en fonction de la TVA
// tarif horaire ou global, c'est juste pour factoriser le code testant la TVA
function get_tarif_formation($ht)
{
    global $wpof;
    if ($ttc = get_ttc_prix($ht))
        return sprintf(__("%.2f %s HT (%.2f %s TTC)"), $ht, $wpof->monnaie_symbole, $ttc, $wpof->monnaie_symbole);
    else
        return sprintf(__("%.2f %s"), $ht, $wpof->monnaie_symbole);
}

// convertir un tarif en TTC
// retourne null si pas de TVA car non-assujetti ou exonéré
function get_ttc_prix($prixht)
{
    global $wpof;
    
    if ($wpof->of_hastva && !$wpof->of_exotva)
        return sprintf("%.2f", $prixht * ((100.0 + $wpof->of_tauxtva) / 100.0));
    else
        return null;
}
// idem, mais pour les frais hors-formation, donc avec TVA si l'OF est assujetti
function get_ttc_prix_autres($prixht)
{
    global $wpof;
    
    if ($wpof->of_hastva)
        return sprintf("%.2f", ($prixht * (100.0 + $wpof->of_tauxtva)) / 100.0);
    else
        return null;
}

/*
 * Retourne le nom (display_name) d'un utilisateur par son ID
 * Si $link vaut true, alors le nom est entouré d'une balise a qui pointe vers sa page de profil
 */
function get_displayname($user_id, $link = false)
{
    $user = get_userdata($user_id);
    if ($user == null)
        return "";
    $display_name = $user->display_name;
    
    if ($link)
    {
        // Recherche du permalien de l'utilisateur
        $permalink_base = um_get_option('permalink_base');
        $permalink = "/user/".get_user_meta( $user_id, "um_user_profile_url_slug_{$permalink_base}", true );
        return "<a href='$permalink'>$display_name</a>";
    }
    else
        return $display_name;
}

/*
 * Retourne la date et l'heure de dernière connexion d'un utilisateur par son ID
 */
function get_last_login($user_id)
{
    $last_login = get_user_meta($user_id, '_um_last_login', true);
    if ($last_login)
        return date("j/m/Y à H:i", $last_login);
    else
        return __("aucune");
}


/*
 * Fonction qui interroge la table _documents à la recherche de documents à signer
 * $role_request vaut , VALID_CLIENT_REQUEST ou VALID_STAGIAIRE_REQUEST (cf. constantes de la classe Document)
 * $get_user : si true alors on retroune la valeur user_id depuis la base de données
 * $get_doc : si true alors on retourne la valeur document depuis la base de données (type de document) (voir si il ne serait pas intéressant de renvoyer aussi le lien PDF et la date de last_modif)
 *
 * return: un tableau classé par session_id
 */
function get_docs_to_validated($role_request)
{
    require_once(wpof_path . "/class/class-document.php");
    
    global $wpdb;
    $table = $wpdb->prefix . WPOF_TABLE_SUFFIX_DOCUMENTS;
    
    $query = $wpdb->prepare
    ("SELECT session_id, contexte, contexte_id, document from $table
        WHERE meta_key = 'valid'
        AND meta_value & '%d'
        ORDER BY session_id;",
        $role_request);

    return $wpdb->get_results($query);
}

/* Crée un bouton pour passer un bloc en mode édition ou lecture simple */
function get_edition_mode($bloc_id)
{
    ob_start();
    ?>
    <span class="bouton edition-mode" data-id="<?php echo $bloc_id; ?>"><?php _e("Mode édition"); ?></span>    
    <?php
    return ob_get_clean();
}


function get_fullscreen_mode($element)
{
    ob_start();
    ?>
    <span class="bouton fullscreen-mode" data-id="<?php echo $element; ?>"><?php _e("Plein écran"); ?></span>    
    <?php
    return ob_get_clean();
}

function get_session_numero($session, $link = true)
{
    ob_start(); ?>
    <a href="<?php echo $session->permalien; ?>"><?php echo ($session->numero != "") ? $session->numero : $session->id; ?></a>
    <?php
    return str_replace("\r\n",'',trim(ob_get_clean()));//ob_get_clean();
}


/*
 * Forcer le téléchargement d'un fichier plutôt que l'affichage dans le navigateur
 * Le seul paramètre connu est l'URL.
 * On remplace wpof_url_pdf par wpof_path_pdf puis on extrait le nom du fichier
 */
add_action('get_header', 'force_download', 1);
function force_download($content)
{
    if (isset($_GET['download']))
    {
        global $wpof;
        $doc_type = $_GET['download'];
        $session_id = $_GET['s'];
        
        if (isset($_GET['ci']))
        {
            $contexte_id = $_GET['ci'];
            $contexte = $_GET['c'];
        }
        if (isset($_GET['m']))
            $md5sum = $_GET['m'];

        $current_user_id = get_current_user_id();
        $role = wpof_get_role($current_user_id);
        
        if ($role === false) die();
        
        $session = new SessionFormation($session_id);
        
        if (isset($contexte_id)) // document prédéfini
        {
            if (($role == "um_stagiaire") && (!in_array($current_user_id, $session->inscrits) || !in_array($current_user_id, array($user_id, -1))))
            {
                _e("Vous n'avez pas accès à cette ressource.");
                http_response_code(403);
                die();
            }
            
            if ($role == "um_formateur-trice" && !($wpof->formateur_gest) && !in_array($current_user_id, $session->formateur))
            {
                _e("Vous n'avez pas accès à cette ressource.");
                http_response_code(403);
                die();
            }
            
            $doc = new Document($doc_type, $session_id, $contexte, $contexte_id);
            
            $filepath = $doc->path."/".$doc->pdf_filename;
            $filename = $doc->pdf_filename;
        }
        elseif (isset($md5sum)) // document libre
        {
            $upload = $session->uploads[$md5sum];
            $filepath = $upload->path.$upload->filename;
            $filename = $upload->filename;
        }
        else die();
        
        if (file_exists($filepath))
        {
    //        echo "Le fichier |".$doc->path."/".$doc->pdf_filename."| existe";
    //        echo 'Content-Disposition: attachment; filename="'.$doc->pdf_filename.'"   ';
    //        echo filesize($doc->path."/".$doc->pdf_filename);
    //        die();
            header('Content-Description: File Transfer');
            header('Content-Type: application/force-download');
            header('Content-Type: application/octet-stream');
            header('Content-Type: application/download');
            header('Content-Disposition: attachment; filename="'.$filename.'"');
            header('Expires: 0');
            header("Cache-Control: no-cache, must-revalidate");
            header("Cache-Control: post-check=0,pre-check=0");
            header("Cache-Control: max-age=0");
            header("Pragma: no-cache, public");
            header('Content-Length: ' . filesize($filepath));
            //flush(); // Flush system output buffer
            readfile($filepath);
            http_response_code(200);
            
            die();
        }
        else
        {
            // par sécurité nous ne distinguons pas le cas du fichier inexistant de celui où l'utilisateur n'as pas le droit d'accès
            _e("Vous n'avez pas accès à cette ressource.");
            http_response_code(403);
            die();
        }

        die();
        
    }
}



/* @link https://anythinggraphic.net/paste-as-text-by-default-in-wordpress
/* Use Paste As Text by default in the editor
----------------------------------------------------------------------------------------*/
//add_filter('tiny_mce_before_init', 'wpof_tinymce_paste_as_text');
function wpof_tinymce_paste_as_text( $init )
{
    $init['paste_as_text'] = true;
    return $init;
}


function debug_info($info, $key)
{
    $role = wpof_get_role(get_current_user_id());
    if (debug && $role == "admin"):

    ?>
    <h3 data-id="debug<?php echo $key;?>" class="openButton">Debug <?php echo $key; ?></h3>
    <div class="blocHidden debug" id="debug<?php echo $key;?>">
    <pre>
    <?php var_dump($info); ?>
    </pre>
    </div>
    
    <?php
    endif;
}

function num_to_letter($nombre)
{
    global $wpof;
    
    if ($nombre == "0") return "zéro ".$wpof->monnaie;
    
    # Décomposition du chiffre
    # Séparation du nombre entier et des décimales
    if (preg_match("/\b,\b/i", $nombre))
        $nombre = explode(',',$nombre);
    else
        $nombre = explode('.',$nombre);
    $partie_entiere = $nombre[0];
    
    # Décomposition du nombre entier par tranche de 3 nombre (centaine, dizaine, unitaire)
    $nb_part = 0;
    while(strlen($partie_entiere)>0)
    {
        $nbtmp[$nb_part] = substr($partie_entiere,-3);
        if(strlen($partie_entiere)>3)
            $partie_entiere = substr($partie_entiere,0,strlen($partie_entiere)-3);
        else
            $partie_entiere = '';
        $nb_part++;
    }
    
    $nb_lettres = '';
    
    // Traitement du côté entier
    for($i = $nb_part - 1; $i >= 0; $i--)
    {
        $nb_lettres .= traitement_nombre_trois($nbtmp[$i], $i);
        if($i == 1 && $nbtmp[$i] != 0)
            $nb_lettres .= 'mille ';
    }

    if($nombre[0] > 0) $nb_lettres .= $wpof->monnaie;
    if($nombre[0] > 1) $nb_lettres .= 's';
    $nb_lettres .= " ";

    ## Traitement du côté décimale
    if (isset($nombre[1]))
    {
        if($nombre[0] > 0 && $nombre[1] > 0)
            $nb_lettres.=' et ';
            
        $nb_lettres .= traitement_nombre_trois($nombre[1]);
        
        if($nombre[1] != 0 && !empty($nombre[1]))
        {
            if($nombre[1] > 1)
                $nb_lettres.='centimes ';
            else
                $nb_lettres.='centime ';
        }
    }
        
    return $nb_lettres;
}

// traitement d'un nombre à trois chiffres
// conversion en lettres
function traitement_nombre_trois($nbtmp, $i = 0)
{
    $unites_termes = Array('un','deux','trois','quatre','cinq','six','sept','huit','neuf','dix','onze','douze','treize','quatorze','quinze','seize','dix-sept','dix-huit','dix-neuf');
    $dizaines_termes = Array('vingt','trente','quarante','cinquante','soixante','soixante','quatre-vingt','quatre-vingt');
    $nb_lettres = "";
    
    if(strlen(trim($nbtmp))==3)
    {
        // on retire la centaine
        $diz_unit = substr($nbtmp,1);
        
        $centaine = substr($nbtmp,0,1);

        if($centaine > 1)
        {
            $nb_lettres .= $unites_termes[$centaine - 1];
            if( $diz_unit != 0 )
                $nb_lettres .= ' cent ';
            else
                $nb_lettres .= ' cents ';
        }
        elseif ($centaine != 0)
        {
            $nb_lettres.='cent ';
        }
    }
    else
        $diz_unit = $nbtmp;

    $unite = substr($diz_unit,1,1);
    $dizaine = substr($diz_unit,0,1);
    
    if ($diz_unit > 0 && $diz_unit < 20)
    {
        if( !($i == 1 && $nbtmp == 1))
            $nb_lettres .= $unites_termes[$diz_unit-1].' ';
    }

    if ($diz_unit >= 20 && $diz_unit < 60)
    {
        switch($unite)
        {
            case 1 :    $unite_let = ' et '.$unites_termes[0];
                        break;
            case 0 :    $unite_let = '';
                        break;
            default:    $unite_let = '-'.$unites_termes[$unite - 1];
        }
        $nb_lettres .= $dizaines_termes[$dizaine - 2].$unite_let.' ';
    }

    if ($diz_unit >= 60 && $diz_unit < 100)
    {
        $nb_lettres .= $dizaines_termes[$dizaine - 2];
        if ($diz_unit == 80)
            $nb_lettres .= "s";
        
        if (($dizaine & 1) == 0) // dizaine paire
        {
            switch($unite)
            {
                case 1 :    $unite_let = ' et '.$unites_termes[0];
                            break;
                case 0 :    $unite_let = '';
                            break;
                default:    $unite_let = '-'.$unites_termes[$unite - 1];
            }
            $nb_lettres .= $unite_let.' ';
        }
        else
        {
            $nb_lettres .= '-'.$unites_termes[substr($diz_unit,1,1)+9].' ';
        }
    }

    return $nb_lettres;
}

?>
