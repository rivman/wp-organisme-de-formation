<?php

require_once(wpof_path . "/cpt/cpt-modele.php");
require_once(wpof_path . "/cpt/cpt-formation.php");
require_once(wpof_path . "/cpt/cpt-lieu.php");
require_once(wpof_path . "/cpt/cpt-session-formation.php");

/*
 * Filtre sur the_content
 */
add_filter('the_content', 'wpof_content_modifier', 5);
function wpof_content_modifier($content)
{
    if (is_single())
    {
        $post_id = get_the_ID();
        
        if (!empty($post_id))
        {
            $post_type = get_post_type($post_id);
            switch ($post_type)
            {
                case "modele":
                    $content = "";
                    break;
                case "formation":
                    $content = get_template_formation();
                    break;
                case "session":
                    $content = get_template_session();
                    break;
            }
        }
    }
    return $content;
}

/*
 * Filtre pour ajouter une classe spécifique dans le body
 */
add_filter('body_class', 'wpof_class');
function wpof_class($classes)
{
    global $wpof;
    
    if (is_single())
    {
        $post = get_post();
        
        if (!empty($post))
        {
            switch ($post->post_name)
            {
                case $wpof->url_pilote:
                case $wpof->url_bpf:
                    $classes[] = $post->post_name;
                    $classes[] = 'wpof';
                    break;
                default:
                    break;
            }
            switch ($post->post_type)
            {
                case 'formation':
                case 'session':
                case 'lieu':
                    $classes[] = 'wpof';
                    break;
                default:
                    break;
            }
            
        }
    }
            
    return $classes;
}



?>
