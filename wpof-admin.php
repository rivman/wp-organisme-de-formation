<?php
/*
 * wpof-admin.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

/*
 * Définition d'un nouveau logo pour la page de login de WordPress
 */ 
function my_login_logo() { ?>
    <style type="text/css">
body.login
{
	background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png);
	background-position: center top;
	background-repeat: no-repeat;
}
</style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

/*
// Piste à creuser, CodeMirror est intégré à WordPress depuis la version 4.9
add_action( 'admin_enqueue_scripts', 'wpof_options_load_scripts', 16 );
function wpof_options_load_scripts()
{
    wp_enqueue_script( 'code-editor' );
    wp_enqueue_script( 'csslint' );
    wp_enqueue_style( 'code-editor' );
}
*/

/*
 * Page(s) d'options
 */


// suppression de menus et sous-menus
function remove_submenu()
{
    global $submenu;
    //supprimer le sous menu "themes"
    //unset($submenu['themes.php'][10]);
}

function remove_menu()
{
    global $menu;
    global $wpof;

    $hidden_menus = get_option("wpof_hidden_menus");
    if ($hidden_menus != "")
        foreach($hidden_menus as $num => $value)
            if ($value == 1) unset($menu[$num]);
}
add_action('admin_head', 'remove_menu');
//add_action('admin_head', 'remove_submenu');

///////////////
// Création d'une page d'options
///////////////
add_action('admin_menu', 'wpof_admin_add_page');
function wpof_admin_add_page()
{
    add_menu_page(__('Organisme de formation'), __('Organisme de formation'), 'manage_options', 'wpof', 'wpof_options_page', 'dashicons-admin-site', 1);
    add_submenu_page('wpof', __('Modèles'), __('Modèles'), 'manage_options', 'edit.php?post_type=modele');
//  add_submenu_page('wpof', 'Ajout d\'utilisateurs', 'Ajout d\'utilisateurs', 'manage_options', 'wpof_add_user', 'wpof_add_user_page');
}

//// Page principale

add_action('admin_init', 'wpof_admin_init');
function wpof_admin_init()
{
    wp_enqueue_media();
    global $wpof_options;
    if (file_exists(get_stylesheet_directory()."/terms_options.csv"))
        $csv_filename = get_stylesheet_directory()."/terms_options.csv";
    else
        $csv_filename = wpof_path."/terms_options.csv";
    
    $csv_file = fopen($csv_filename, "r");
    while (($data = fgetcsv($csv_file, 1000, "|")) !== FALSE)
    {
	if (in_array(substr($data[0], 0, 1), array("#", "h"))) continue;
	$varname = trim($data[1]);
	$wpof_options[] = $varname;
    }
    fclose($csv_file);
    
    foreach ($wpof_options as $o)
    {
	register_setting('wpof', $o);
    }
}


/*
 * Options de WPOF gérées dans l'interface d'admin (wp-admin/admin.php?page=wpof)
 * options-list est la liste des options et de leurs valeurs. Elle sert pour l'import et ne doit pas être exportée en tant qu'option.
 */
$wpof_options = array
(
    'wpof_annee1',
    'wpof_monnaie',
    'wpof_monnaie_symbole',
    'wpof_ofadmin', // TODO : à supprimer
    'wpof_of_nom',
    'wpof_of_adresse',
    'wpof_of_code_postal',
    'wpof_of_ville',
    'wpof_of_telephone',
    'wpof_of_description',
    'wpof_of_logo',
    'wpof_of_siret',
    'wpof_of_noof',
    'wpof_of_datadock',
    'wpof_of_hastva',
    'wpof_of_exotva',
    'wpof_of_tauxtva',
    'wpof_formateur_gest',
    'wpof_formateur_marque',
    'wpof_tarif_inter',
    'wpof_respform_id',
    'wpof_respform_fonction',
    'wpof_respform_admin',
    'wpof_url_bpf',
    'wpof_title_bpf',
    'wpof_url_pilote',
    'wpof_title_pilote',
    'wpof_hidden_menus',
    'wpof_session_login_form',
    'wpof_eval_form',
    'wpof_pdf_header',
    'wpof_pdf_footer',
    'wpof_pdf_marge_haut',
    'wpof_pdf_marge_bas',
    'wpof_pdf_marge_gauche',
    'wpof_pdf_marge_droite',
    'wpof_pdf_hauteur_header',
    'wpof_pdf_hauteur_footer',
    'wpof_pdf_css',
    'wpof_options_list',
);

// ajout des options concernant les documents
$doc_nom;
$doc_prop = array
(
    'nom',
    'id',
    'valid_responsable',
    'valid_stagiaire',
    'valid_client',
);

foreach ($wpof->type_session as $idt => $t)
{
    $doc_prop[] = 'global_'.$idt;
    $doc_prop[] = 'stagiaire_'.$idt;
}

foreach (array_keys($doc_nom) as $d)
    foreach ($doc_prop as $p)
    {
        $wpof_options[] = "wpof_{$d}_$p";
    }


/*
 * Export des options du plugin au format JSON
 * Le fichier est enregistré dans le dossier du plugin
 */
function wpof_export_options()
{
    global $wpof;
    return json_encode($wpof, JSON_HEX_APOS|JSON_UNESCAPED_UNICODE);
}




function wpof_options_page()
{
    global $wpof;
    
    ?>
    <div class='wrap'>
    <h2><?php _e('Options — Organisme de formation'); ?></h2>

    <?php
        // traitement de l'import éventuel
        /*
        if ($wpof->options_list != "")
        {
            $options = json_decode($wpof->options_list, true);
            foreach ($options as $key => $value)
            {
                update_option($key, $value, "yes");
            }
            update_option('wpof_options_list', "");
        }*/
        
        // mise à jour ?
        if ($wpof->version < WPOF_VERSION)
            require_once(wpof_path . "/wpof-mise-a-jour.php");
            
        $editor_settings = array
        (
            'wpautop' => false,
            'media_buttons' => true,
            'tinymce' => array
                (
                'language' => 'fr',
                'spellchecker_languages' => 'French=fr',
                'toolbar1' => 'formatselect,bold,italic,underline,forecolor,backcolor,|,bullist,numlist,blockquote,|,justifyleft,justifycenter,justifyright,|,link,unlink,|,spellchecker,fullscreen,wp_adv',
                'paste_as_text' => true,
                ),
            'quicktags' => true,
            'editor_height' => 150,
        );
        
//        debug_info(get_option("um_options"), "um_options");
    ?>
	<form method="post" action="options.php">
	
    <?php
    settings_fields('wpof');
    ?>
        
        <?php echo hidden_input("default_options_tab", (isset($_SESSION['options-tabs'])) ? $_SESSION['options-tabs'] : 0); ?>
        
        <div class="wpof-menu" id="options-tabs">
        <ul>
        <li><a href="#general-options"><?php echo __("Options générales"); ?></a></li>
        <li><a href="#formations-options"><?php echo __("Options des formations"); ?></a></li>
        <li><a href="#documents-options"><?php echo __("Documents administratifs"); ?></a></li>
        <li><a href="#terms-options"><?php echo __("Textes personnalisables"); ?></a></li>
        <li><a href="#pdf-options"><?php echo __("Options PDF"); ?></a></li>
        
        <?php if (current_user_can('manage_options')): ?>
        <li><a href="#wpof-admin"><?php _e("Configuration WordPress"); ?></a></li>
        <?php endif; ?>
        
        </ul>
	
        <div id="general-options" class="tableau">
	<table class="form-table">
	<tr valign="top">
            <th scope="row"><?php _e('Organisme de formation gérant ce site'); ?></th>
            <td><input type="text" name="wpof_of_nom" value="<?php echo $wpof->of_nom; ?>" /></td>
	</tr>
	<tr valign="top">
            <th scope="row"><?php _e('Coordonnées'); ?></th>
            <td>
            <textarea name="wpof_of_adresse" placeholder="<?php _e('Adresse'); ?>" cols="60" rows="3"><?php echo $wpof->of_adresse; ?></textarea>
            <p>
            <input type="text" name="wpof_of_code_postal" placeholder="<?php _e("Code postal");?>" value="<?php echo $wpof->of_code_postal; ?>" />
            <input type="text" name="wpof_of_ville" placeholder="<?php _e("Ville");?>" value="<?php echo $wpof->of_ville; ?>" />
            <input type="text" name="wpof_of_telephone" placeholder="<?php _e("Numéro de téléphone"); ?>" value="<?php echo $wpof->of_telephone; ?>" />
            </p>
            </td>
	</tr>
	<tr valign="top">
            <th scope="row"><?php _e('Description libre'); ?></th>
            <td>
            <?php
                wp_editor($wpof->of_description, "wpof_of_description", $editor_settings);
            ?>
            </td>
	</tr>
	<tr valign="top">
            <th scope="row"><p><?php _e('Logo'); ?></p>
            <p><a href="#" class="clickButton button-add-media" data-valueid="wpof_of_logo" data-linkmedia="wpof_of_logo_link" data-image="wpof_of_logo_img"><?php _e("Téléversez une image"); ?></a></p>
            </th>
            <td>
            <?php $of_logo = ($wpof->of_logo) ? $wpof->of_logo : ""; ?>
            <input type="hidden" id="wpof_of_logo" name="wpof_of_logo" value="<?php echo $of_logo; ?>" />
            <img id="wpof_of_logo_img" style="max-width: 300px; heigth: auto;" src="<?php echo wp_get_attachment_url($of_logo); ?>" />
            <a id="wpof_of_logo_link" target="_blank" href="<?php if ($of_logo != "") echo get_attachment_link($of_logo); ?>"><?php if ($of_logo != "") echo get_the_title($of_logo); ?></a>
            </td>
	</tr>
	<tr valign="top">
            <th scope="row"><?php _e("Numéro de SIRET"); ?></th>
            <td><input type="text" name="wpof_of_siret" value="<?php echo $wpof->of_siret; ?>" /></td>
	</tr>
	<tr valign="top">
            <th scope="row"><?php _e("Numéro d'organisme de formation"); ?></th>
            <td><input type="text" name="wpof_of_noof" value="<?php echo $wpof->of_noof; ?>" /></td>
	</tr>
	<tr valign="top">
            <th scope="row"><?php _e("Lien Datadock"); ?></th>
            <td><input type="text" name="wpof_of_datadock" value="<?php echo $wpof->of_datadock; ?>" /></td>
	</tr>
	
	<tr valign="top">
            <th scope="row"><?php echo __("Responsable de formation (signataire des documents)"); ?></th>
            <td>
                <?php
                $liste = select_user(array('role__in' => array('um_responsable', 'administrator', 'admin')), "wpof_respform_id", $wpof->respform_id, __("Choisissez une personne"));
                if ($liste == null)
                    echo "<a href='/wp-admin/users.php'>".__("Créez un⋅e responsable ou affectez le rôle à une personne existante")."</a>";
                else
                    echo $liste;
                ?>
                <input type="text" name="wpof_respform_fonction" placeholder="<?php _e("Fonction"); ?>" value="<?php echo $wpof->respform_fonction; ?>" />
            </td>
	</tr>
	
	<tr valign="top">
            <th scope="row"><?php _e("TVA"); ?></th>
            <td>
            <p>
            <label for="wpof_of_hastva">
            <input class="tva" type="checkbox" id="wpof_of_hastva" name="wpof_of_hastva" value="1" <?php checked(1, $wpof->of_hastva, true); ?> />
            <?php _e("Assujettis à la TVA ?"); ?></label>
            </p>
            <p>
            <?php
            $disabled_exotva ="";
            if (!$wpof->of_hastva)
                $disabled_exotva = "disabled='disabled'";
            ?>
            <label for="wpof_of_exotva">
            <input type="checkbox" id="wpof_of_exotva" name="wpof_of_exotva" <?php echo $disabled_exotva; ?> value="1" <?php checked(1, $wpof->of_exotva, true); ?> />
            <?php _e("Formation exonérée de TVA (article 261 du CGI) ?"); ?></label>
            </p>
            <p>
            <label><?php _e("Taux de TVA"); ?></label>
            <?php
            $disabled_tva ="";
            if (!$wpof->of_hastva)
                $disabled_tva = "disabled='disabled'";
            ?>
            <input type="number" min="0" max="100" step="0.01" name="wpof_of_tauxtva" <?php echo $disabled_tva; ?> value="<?php echo $wpof->of_tauxtva; ?>" /> %
            <?php echo "(".__("utilisé notamment pour les frais hors formation").")"; ?>
            </p>
            </td>
	</tr>

	<tr valign="top">
            <th scope="row"><?php echo __("Monnaie"); ?></th>
            <td>
            <input type="text" name="wpof_monnaie" placeholder="<?php _e("Unité en lettres"); ?>" value="<?php echo $wpof->monnaie; ?>" />
            <input type="text" name="wpof_monnaie_symbole" placeholder="<?php _e("Symbole"); ?>" value="<?php echo $wpof->monnaie_symbole; ?>" /></td>
	</tr>

	<tr valign="top">
            <th scope="row"><?php echo __("Tarif inter /heure/stagiaire"); ?></th>
            <td><input type="text" name="wpof_tarif_inter" value="<?php echo $wpof->tarif_inter; ?>" /></td>
	</tr>
	
	<tr valign="top">
            <th scope="row"><?php echo __("Les formateurs⋅trices peuvent gérer les sessions d'autres formateurs⋅trices"); ?></th>
            <td><input type="checkbox" name="wpof_formateur_gest" value="1" <?php checked(1, $wpof->formateur_gest, true); ?> /></td>
	</tr>

	<tr valign="top">
            <th scope="row"><?php echo __("Les formateurs peuvent avoir et mettre en avant leur propre marque"); ?></th>
            <td><input type="checkbox" name="wpof_formateur_marque" value="1" <?php checked(1, $wpof->formateur_marque, true); ?> /></td>
	</tr>
	
	<tr valign="top">
            <th scope="row"><?php echo __("Première année d'activité gérée avec ce logiciel"); ?></th>
            <td><input type="text" name="wpof_annee1" value="<?php echo $wpof->annee1; ?>" /></td>
	</tr>

	</table>
	</div>
	

	<div id="formations-options" class="tableau">
	<table class="form-table">
	
	<tr valign="top">
		<th scope="row"><?php echo __('Formulaire de connexion pour la page de session de formation'); ?></th>
		<td>
                    <?php
                        $liste = select_post_by_type("um_form", "wpof_session_login_form", $wpof->session_login_form);
                        if ($liste == NULL)
                            echo "<a href='/wp-admin/edit.php?post_type=um_form'>".__("Problème de configuration d'Ultimate Member")."</a>";
                        else
                            echo $liste;
                    ?>
                </td>
        </tr>
        
	<tr valign="top">
            <th scope="row"><?php _e("Bilan pédagogique et financier"); ?></th>
            <td>
            <div class="label">
            <label for="wpof_url_bpf"><?php _e("URL d'accès au BPF"); ?></label>
            <input type="text" name="wpof_url_bpf" placeholder="<?php _e("URL d'accès au BPF"); ?>" value="<?php echo $wpof->url_bpf; ?>" />
            </div>
            <div class="label">
            <label for="wpof_title_bpf"><?php _e("Titre de la page"); ?></label>
            <input type="text" name="wpof_title_bpf" placeholder="<?php _e("Titre de la page"); ?>" value="<?php echo $wpof->title_bpf; ?>" /></td>
            </div>
	</tr>

	<tr valign="top">
            <th scope="row"><?php _e("Pilote, permet d'avoir une vision globale"); ?></th>
            <td>
            <div class="label">
            <label for="wpof_url_pilote"><?php _e("URL d'accès au pilote"); ?></label>
            <input type="text" name="wpof_url_pilote" placeholder="<?php _e("URL d'accès au pilote"); ?>" value="<?php echo $wpof->url_pilote; ?>" />
            </div>
            <div class="label">
            <label for="wpof_title_pilote"><?php _e("Titre de la page"); ?></label>
            <input type="text" name="wpof_title_pilote" placeholder="<?php _e("Titre de la page"); ?>" value="<?php echo $wpof->title_pilote; ?>" /></td>
            </div>
	</tr>

	</table>
	</div>
	
	<div id="terms-options" class="tableau">
	<table class="form-table">
	<?php
        if (file_exists(get_stylesheet_directory()."/terms_options.csv"))
            $csv_filename = get_stylesheet_directory()."/terms_options.csv";
        else
            $csv_filename = wpof_path."/terms_options.csv";
	$csv_file = fopen($csv_filename, "r");
	
	while (($data = fgetcsv($csv_file, 1000, "|")) !== FALSE)
	{
		$type = trim($data[0]);
		if (substr_compare($type, "#", 0, 1) == 0) continue;
		
		// Gestion des inter-titres
		if (substr_compare($type, "h", 0, 1) == 0)
		{
                    $titre = trim($data[1]);
                    ?>
                    <tr valign="top">
			<th scope="row" colspan="2"><h3><?php echo $titre; ?></h3></th>
                    </tr>
                    
                    <?php
		}
		else
		{
		$varname = trim($data[1]);
		$label = trim($data[2]);
		?>
                    <tr valign="top">
			<th scope="row"><?php echo $label; ?></th>
			<td><input type='<?php echo $type; ?>' name='<?php echo $varname; ?>' id='<?php echo $varname; ?>' class='long-text' value="<?php echo get_option($varname); ?>" />	<p><em>Nom de l'option : <?php echo $varname; ?></em></p></td>
                    </tr>
		<?php
		}

	}
	fclose($csv_file);
	?>
	</table>
	</div>

	<div id="documents-options" class="tableau">
	
	<fieldset><legend><?php echo __("Modèles de documents"); ?></legend>
	<p><?php _e("Choisissez le bon modèle pour chaque document"); ?></p>
	<table class="form-table">
	
	<?php foreach($wpof->documents->term as $index => $term) : 
                $nom = $term->text;
	?>

    <tr valign="top">
        <td scope="row"><?php echo $nom; ?></td>
        <td>
            <?php echo select_post_by_type("modele", "wpof_${index}_id", $wpof->{$index."_id"}, __("Aucun")); ?>
        </td>
    </tr>
    <?php endforeach; ?>

	</table>
	</fieldset>
	
	<fieldset><legend><?php echo __("Questionnaire pour l'évalution de la formation"); ?></legend>
	<table class="form-table">
	
	<tr><td colspan="3" scope="row"><?php echo __("Codage et formattage"); ?>
	<ul>
            <li><?php echo __("h | titre"); ?></li>
            <li><?php echo __("r | choix unique (équivalent à radio bouton)"); ?></li>
            <li><?php echo __("t | texte court (une ligne)"); ?></li>
            <li><?php echo __("ta | texte long (textarea)"); ?></li>
            <li><?php echo __("# ou ligne vide : ignorée"); ?></li>
	</ul>
	</td></tr>
	
	<tr><td colspan="3">
	<textarea name="wpof_eval_form" cols="100" rows="15"><?php echo $wpof->eval_form; ?></textarea>
	</td></tr>
	
	
	
	</table>
	</div>
	
	<div id="pdf-options" class="tableau">

	<table class="form-table">
	<tr valign="top">
            <th scope="row"><?php echo __("En-tête"); ?></th>
            <td>
            <?php
                wp_editor($wpof->pdf_header, "wpof_pdf_header", $editor_settings);
            ?>
            </td>
        </tr>
	
	<tr valign="top">
            <th scope="row"><?php echo __("Pied de page"); ?></th>
            <td>
            <?php
                wp_editor($wpof->pdf_footer, "wpof_pdf_footer", $editor_settings);
            ?>
            </td>
        </tr>
        
	<tr><td><?php echo __("Note : les dimensions sont en millimètres."); ?></td></tr>
	<tr valign="top">
            <th scope="row"><?php echo __("Marges"); ?></th>
            <td>
            <label for="pdf-marge-haut"><?php echo __("haut"); ?><input type="text" id="pdf-marge-haut" name="wpof_pdf_marge_haut" value="<?php echo $wpof->pdf_marge_haut; ?>" /></label>
            <label for="pdf-marge-bas"><?php echo __("bas"); ?><input type="text" id="pdf-marge-bas" name="wpof_pdf_marge_bas" value="<?php echo $wpof->pdf_marge_bas; ?>" /></label>
            <label for="pdf-marge-gauche"><?php echo __("gauche"); ?><input type="text" id="pdf-marge-gauche" name="wpof_pdf_marge_gauche" value="<?php echo $wpof->pdf_marge_gauche; ?>" /></label>
            <label for="pdf-marge-droite"><?php echo __("droite"); ?><input type="text" id="pdf-marge-droite" name="wpof_pdf_marge_droite" value="<?php echo $wpof->pdf_marge_droite; ?>" /></label>
            </td>
	</tr>
	
	<tr valign="top">
            <th scope="row"><?php echo __("Hauteurs"); ?></th>
            <td>
            <label for="pdf-hauteur-header"><?php echo __("En-tête"); ?><input type="text" id="pdf-hauteur-header" name="wpof_pdf_hauteur_header" value="<?php echo $wpof->pdf_hauteur_header; ?>" /></label>
            <label for="pdf-hauteur-footer"><?php echo __("Pied de page"); ?><input type="text" id="pdf-hauteur-footer" name="wpof_pdf_hauteur_footer" value="<?php echo $wpof->pdf_hauteur_footer; ?>" /></label>
            </td>
	</tr>
	<tr valign="top"><th></th>
	<td><?php
            $body_height = 297 - ($wpof->pdf_marge_haut + $wpof->pdf_marge_bas + $wpof->pdf_hauteur_header + $wpof->pdf_hauteur_footer);
            echo __("Hauteur du corps : ").$body_height."mm (".__("Hauteur du papier - (marge haute + marge basse + hauteur en-tête + hauteur pied)").")";
        ?></td>
        </tr>
	
	<tr valign="top">
            <th scope="row">
                <?php echo __("Feuille de style personnalisée (CSS)"); ?>
            </th>
            <td rowspan="2">
                <textarea id="pdf-css" name="wpof_pdf_css" cols="100" rows="20"><?php echo $wpof->pdf_css; ?></textarea>
                <script>
                // piste à creuser, CodeMirror est intégré à WordPress depuis la version 4.9
                //var cm = CodeMirror.fromTextArea(document.getElementById('pdf-css'));
                </script>
            </td>
	</tr>
	<tr valign="top"><td><?php _e("La feuille de style s'applique après les réglages ci-dessus. En cas de conflit, la feuille de style ci-contre s'applique."); ?></td></tr>
	
	
	</table>
	</div>

	<?php if (current_user_can('manage_options')): ?>
	<div id="wpof-admin" class="tableau">
	<?php global $menu; ?>
	<table class="form-table">
	<tr valign="top">
            <th scope="row"><?php echo __("Menus cachés"); ?>
            <?php
            $hiddenable_menu = array
            (
                "2" => __("Dashboard"),
                "5" => __("Posts"),
                "10" => __("Media"),
                "20" => __("Pages"),
                "25" => __("Comments"),
                "42.78578" => "Ultimate Member",
                "75" => __("Tools"),
            );
            ?>
            </th>
            <td>
            <?php
            $hidden_menus = get_option("wpof_hidden_menus");
            foreach ($hiddenable_menu as $hm => $menu_name)
            {
            ?>
                <p><label>
                <input type="checkbox" name="wpof_hidden_menus[<?php echo $hm; ?>]" value="1" <?php checked(1, (isset($hidden_menus["$hm"])) ? $hidden_menus["$hm"] : 0, true); ?> />
                <?php echo $menu_name; ?></label></p>
            <?php
            }
            ?>
            </td>
	</tr>	
	</table>
	
	<span class="bouton maj-wpof" data-log="log" data-action="maj_db_tirets"><?php _e("Mise à jour : tirets dans la BD"); ?></span>
	<span class="bouton maj-wpof" data-log="log" data-action="maj_db_client"><?php _e("Mise à jour : ajout table client"); ?></span>
	<span class="bouton maj-wpof" data-log="log" data-action="maj_db_documents"><?php _e("Mise à jour : table documents"); ?></span>
	<span class="bouton maj-wpof" data-log="log" data-action="init_um_options"><?php _e("Réinitialisation des um_options"); ?></span>
	
	<div id="log">
	<dl>
	<?php foreach(get_option("um_options") as $k => $v) : ?>
            <dt><?php echo $k; ?></dt>
            <dd><?php var_dump($v); ?></dd>
        <?php endforeach; ?>
        </dl>
	</div>
	
	<!--
	<h3><?php echo __("Importer les options"); ?></h3>
	<textarea name="wpof_options_list" id="options-list" cols="80" rows="15" placeholder="<?php _e("Collez ici les options au format JSON copiées depuis une autre instance de WPOF"); ?>"></textarea>
	
	<h3><?php echo __("Exporter les options"); ?></h3>
	<textarea class="autoselect" cols="80" rows="15" readonly><?php
        echo wpof_export_options();
	?></textarea>
	-->
	</div>
	<?php endif; ?>

	</div> <!-- #options-tabs -->
	
	<?php
	submit_button();
	?>
        </form>
<?php
}
