<?php
/*
 * wpof-admin.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

 
/**
 * Add post type modele
 */
function register_cpt_modele() {

	/**
	 * Post Type: Modèles.
	 */

	$labels = array(
		"name" => __( "Modèles", "generic" ),
		"singular_name" => __( "Modèle", "generic" ),
		"all_items" => __( "Modèles", "generic" ),
		"add_new" => __( "Ajouter un nouveau", "generic" ),
		"add_new_item" => __("Ajouter un nouveau modèle"),
		"view_item" => __("Voir le modèle"),
		"edit_item" => __("Modifier le modèle"),
		"update_item" => __("Mettre à jour le modèle"),
	);

	$args = array(
		"label" => __( "Modèles", "generic" ),
		"labels" => $labels,
		"description" => "Modèle de document",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => 'admin.php?page=wpof',
//		"show_in_menu" => 'edit.php?post_type=page',
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "modele", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-media-code",
		"supports" => array( "title", "editor" ),
		"taxonomies" => array(),
	);

	register_post_type( "modele", $args );
}

add_action( 'init', 'register_cpt_modele' );


// add meta box
add_action('add_meta_boxes','initialisation_modele_metaboxes');
function initialisation_modele_metaboxes()
{
   // add_meta_box('modele-data', __('Caractéristiques du modele'), 'modele_data_meta_box', 'modele', 'normal', 'high');
}

function modele_data_meta_box($post)
{
}        

// save meta box with update
//add_action('save_post','save_modele_metaboxes');
function save_modele_metaboxes($post_ID)
{
}
