<?php
/*
 * wpof-mise-a-jour.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

add_action('wp_ajax_maj_db_client', 'maj_db_client');
function maj_db_client()
{
    global $SessionFormation;
    
    global $wpdb;
    $table_client = $wpdb->prefix."wpof_client";
    
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    
    $charset_collate = $wpdb->get_charset_collate();
    $wpof_sql = "CREATE TABLE IF NOT EXISTS $table_client
    (
        id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        session_id bigint(20) unsigned DEFAULT NULL,
        client_id bigint(20) unsigned DEFAULT NULL,
        meta_key varchar(128) DEFAULT NULL,
        meta_value longtext DEFAULT NULL,
        PRIMARY KEY (id),
        UNIQUE (session_id,client_id,meta_key)
    ) $charset_collate;";

    $res = dbDelta($wpof_sql);
    echo "$wpof_sql\n".var_dump($res)."\n";
    
    $SessionFormation = get_formation_sessions();
    $clients = array();
    
    foreach($SessionFormation as $session)
    {
        $session->init_stagiaires();
        
        echo "<p><span class='succes'>[{$session->id}] {$session->titre_session} ({$session->type_texte})</span></p>";
        
        switch ($session->type_index)
        {
            case "inter":
                foreach ($session->stagiaires as $stagiaire)
                {
                    $client = new Client($session->id);
                    $client->update_meta("session_formation_id", $session->id);
                    $client->update_meta("stagiaires", array($stagiaire->user_id));
                    $client->update_meta("adresse", $stagiaire->adresse);
                    $client->update_meta("cp_ville", $stagiaire->cp_ville);
                    $client->update_meta("telephone", $stagiaire->telephone);
                    $client->update_meta("tarif_total_chiffre", $stagiaire->tarif_total_chiffre);
                    $client->update_meta("exe_comptable", $stagiaire->exe_comptable);
                    $client->update_meta("nature_formation", $stagiaire->nature_formation);
                    
                    if (!$stagiaire->has_employeur || $stagiaire->financement == "part" || $stagiaire->statut_stagiaire == "particulier")
                    {
                        $client->update_meta("nom", get_displayname($stagiaire->user_id, false));
                        $client->update_meta("financement", "part");
                    }
                    else
                    {
                        $client->update_meta("nom", $stagiaire->entreprise);
                        $client->update_meta("financement", $stagiaire->financement);
                        $client->update_meta("financement_complement", $stagiaire->financement_complement);
                    }
                    $session->clients[] = $client->id;
                    
                    echo "<p>".var_export($client, true)."</p>";
                }
                $session->clients = array_unique($session->clients);
                $session->update_meta("clients", $session->clients);
                break;
            case "intra":
                $client = new Client($session->id);
                $client->update_meta("session_formation_id", $session->id);
                $client->update_meta("nom", $session->entreprise_nom);
                $client->update_meta("adresse", $session->entreprise_adresse);
                $client->update_meta("cp_ville", $session->entreprise_cp_ville);
                $client->update_meta("telephone", $session->entreprise_telephone);
                $client->update_meta("contact_id", $session->entreprise_contact_id);
                $client->update_meta("nature_formation", $session->nature_formation);
                $client->update_meta("financement", $session->financement);
                $client->update_meta("financement_complement", $session->financement_complement);
                $client->update_meta("tarif_total_chiffre", $session->tarif_total_chiffre);
                $client->update_meta("tarif_total_autres_chiffre", $session->tarif_total_autres_chiffre);
                $client->update_meta("autres_frais", $session->autres_frais);
                $client->update_meta("exe_comptable", $session->exe_comptable);
                
                foreach ($session->stagiaires as $stagiaire)
                    $client->stagiaires[] = $stagiaire->user_id;
                $client->update_meta("stagiaires", $client->stagiaires);
                
                $session->update_meta("clients", array($client->id));
                echo "<p>".var_export($client, true)."</p>";
                break;
            case "sous_traitance":
                $client = new Client($session->id);
                $client->update_meta("session_formation_id", $session->id);
                $client->update_meta("nom", $session->commanditaire_nom);
                $client->update_meta("contact", $session->commanditaire_contact);
                $client->update_meta("contact_email", $session->commanditaire_contact_email);
                $client->update_meta("num_of", $session->commanditaire_num_of);
                $client->update_meta("financement", "opac");
                $client->update_meta("tarif_total_chiffre", $session->tarif_total_chiffre);
                $client->update_meta("tarif_total_autres_chiffre", $session->tarif_total_autres_chiffre);
                $client->update_meta("autres_frais", $session->autres_frais);
                $client->update_meta("exe_comptable", $session->exe_comptable);
                $client->update_meta("nb_stagiaires", $session->st_nb_stagiaire);
                $session->update_meta("clients", array($client->id));
                echo "<p>".var_export($client, true)."</p>";
                break;
            default:
                echo "<span class='erreur'>Session mal saisie ! [".$session->id."]</span>";
                break;
        }
    }
    
    die();
}

/*
 * sessions meta_key
 */
$session_tiret_meta_key = array
(
    'pre-requis',
    'horaire-am-debut',
    'horaire-am-fin',
    'horaire-pm-debut',
    'horaire-pm-fin',
    'doc-info',
    'nature-formation',
    'tarif-jour',
    'nb-jour',
    'nb-heure',
    'tarif-total-chiffre',
    'tarif-total-lettre',
    'autres-frais',
    'tarif-total-autres-chiffre',
    'tarif-total-autres-lettre',
    'session-unique-titre',
);

/*
 * session_stagiaire meta_key
 */
$session_stagiaire_tiret_meta_key = array
(
    'etat-session',
);

/*
 * options
 */
$wpof_option_tiret_meta_key = array
(
    'wpof_eval-form',
    'wpof_hidden-menus',
    'wpof_monnaie-symbole',
    'wpof_nature-formation',
    'wpof_options-list',
    'wpof_pdf-css',
    'wpof_pdf-footer',
    'wpof_pdf-hauteur-footer',
    'wpof_pdf-hauteur-header',
    'wpof_pdf-header',
    'wpof_pdf-marge-bas',
    'wpof_pdf-marge-droite',
    'wpof_pdf-marge-gauche',
    'wpof_pdf-marge-haut',
    'wpof_respform-admin',
    'wpof_respform-fonction',
    'wpof_respform-id',
    'wpof_session-login-form',
    'wpof_statut-stagiaire',
    'wpof_tarif-inter',
);

add_action('wp_ajax_maj_db_documents', 'maj_db_documents');
function maj_db_documents()
{
    global $wpof;
    global $wpdb;
    
    $table = $wpdb->prefix."wpof_documents";
    
    echo "<p>supprimer la contrainte unique : ";
    echo $wpdb->query("ALTER TABLE $table DROP INDEX session_id;");
    echo "</p>";
    
    echo "<p>renommer colonne user_id en contexte_id : ";
    echo $wpdb->query("ALTER TABLE $table CHANGE user_id contexte_id bigint(20) unsigned null;");
    echo "</p>";
    
    echo "<p>ajouter la colonne contexte après la colonne session_id : ";
    echo $wpdb->query("ALTER TABLE $table ADD COLUMN contexte smallint unsigned AFTER session_id;");
    echo "</p>";
    
    echo "<p>ajouter la nouvelle contrainte unique : ";
    echo $wpdb->query("ALTER TABLE $table ADD CONSTRAINT UNIQUE (session_id, contexte, contexte_id, document, meta_key);");
    echo "</p>";
    
    $all_docs = $wpdb->get_results("SELECT * FROM $table;");
    
    foreach($all_docs as $row)
    {
        $row->document = str_replace('-', '_', $row->document);
        if ($row->document == "undefined")
        {
            $row->document = "scan";
            $row->contexte_id = $row->session_id;
        }
        elseif (isset($wpof->documents->term[$row->document]))
        {
            $row->contexte = $wpof->documents->term[$row->document]->contexte;
            if ($row->contexte & $wpof->doc_context->session)
                $row->contexte_id = $row->session_id;
            
            if ($row->contexte & $wpof->doc_context->client)
            {
                $clients = get_post_meta($row->session_id, "clients", true);
                if ($row->contexte_id == 0 && get_post_meta($row->session_id, "type_formation", true) == "intra")
                    $row->contexte_id = $clients[0];

                foreach($clients as $cid)
                {
                    $stagiaires = unserialize(get_client_meta($cid, "stagiaires"));
                    if (!empty($stagiaires))
                    {
                        if (in_array($row->contexte_id, $stagiaires))
                        {
                            $row->contexte_id = $cid;
                        }
                    }
                    else
                    {
                        echo "<p>Client ".get_client_meta($cid, "nom")." ($cid / Session {$row->session_id}) n'a pas de stagiaires.</p>";
                    }
                }
            }
            $class = ($row->contexte_id == 0) ? 'class="erreur"' : '';
            $query = $wpdb->prepare("UPDATE $table SET contexte = '%d', contexte_id = '%d', document = '%s' WHERE id = '%d'", $row->contexte, $row->contexte_id, $row->document, $row->id);
            //echo "<p $class>$query pour session {$row->session_id} ".get_post_meta($row->session_id, "type_formation", true)."</p>";
            echo "<p>$query : ".$wpdb->query($query)."</p>";
        }
        else
        {
            $query = $wpdb->prepare("DELETE FROM $table WHERE id = '%d'", $row->id);
            //echo "<p class='alerte'>{$row->document} : $query</p>";
            echo "<p class='alerte'>{$row->document} : $query : ".$wpdb->query($query)."</p>";
        }
    }
    
    
    die();
}


add_action('wp_ajax_maj_db_tirets', 'maj_db_tirets');
function maj_db_tirets()
{
    global $wpof;
    global $wpdb;
    global $session_tiret_meta_key;
    global $wpof_option_tiret_meta_key;
    
    $table_session_stagiaire = $wpdb->prefix."wpof_session_stagiaire";
    $table_postmeta = $wpdb->prefix."postmeta";
    $table_options = $wpdb->prefix."options";
    
    foreach($session_tiret_meta_key as $k)
    {
        $query = $wpdb->prepare
        (
            "UPDATE ".$table_postmeta." SET meta_key = '%s' WHERE meta_key = '%s';",
            str_replace("-", "_", $k),
            $k
        );
        echo "<p>[$k] $query : ";
        echo $wpdb->query($query);
        echo "</p>";
        
        // suppression des doublons (on garde celui qui a l'ID le plus élevé, donc, le plus récent)
        $query = "DELETE t1 FROM $table_postmeta AS t1, $table_postmeta AS t2 WHERE t1.meta_id < t2.meta_id AND t1.meta_key = t2.meta_key AND t1.post_id = t2.post_id;";
        echo "<p>Doublons : ".$wpdb->query($query)."</p>";
    }
    
    foreach($wpof_option_tiret_meta_key as $k)
    {
        $query = $wpdb->prepare
        (
            "UPDATE ".$table_options." SET option_name = '%s' WHERE option_name = '%s';",
            str_replace("-", "_", $k),
            $k
        );
        echo "<p>[<strong>$k</strong>] $query";
        echo $wpdb->query($query);
        echo "</p>";
        
        // suppression des doublons (on garde celui qui a l'ID le plus élevé, donc, le plus récent)
        $query = "DELETE t1 FROM $table_options AS t1, $table_options AS t2 WHERE t1.option_id < t2.option_id AND t1.option_name = t2.option_name";
        echo "<p>Doublons : ".$wpdb->query($query)."</p>";
    }
    
    // statut -> statut_stagiaire
    $query = "UPDATE ".$table_session_stagiaire." SET meta_key = 'statut_stagiaire' WHERE meta_key = 'statut';";
    echo "statut → statut_stagiaire : ".$wpdb->query($query);
    
    die();
}

/*
 * Mises à jour entre versions
 */

if (WPOF_VERSION > 0.2)
{
    // ajout de deux tables dans la base de données
    // {$prefix}wpof_session_stagiaire
    // {$prefix}wpof_documents
    
    global $wpdb;
    $table_session_stagiaire = $wpdb->prefix."wpof_session_stagiaire";
    $table_documents = $wpdb->prefix."wpof_documents";
    $table_quiz = $wpdb->prefix."wpof_quiz";
    $table_creneaux = $wpdb->prefix."wpof_creneaux";
    
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    
    $charset_collate = $wpdb->get_charset_collate();
    $wpof_sql = "CREATE TABLE IF NOT EXISTS $table_creneaux
    (
        id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        session_id bigint(20) unsigned NOT NULL,
        activite_id bigint(20) DEFAULT NULL,
        module_id bigint(20) DEFAULT NULL,
        date_debut datetime,
        date_fin datetime,
        type varchar(20) DEFAULT NULL,
        lieu_id bigint(20) DEFAULT NULL,
        salle_id bigint(20) DEFAULT NULL,
        PRIMARY KEY (id),
        UNIQUE (date_debut, session_id, lieu_id, salle_id)
    ) $charset_collate;";
    
    $res = dbDelta($wpof_sql);
    
    $wpof_sql = "CREATE TABLE IF NOT EXISTS $table_documents
    (
        id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        session_id bigint(20) unsigned DEFAULT NULL,
        contexte smallint unsigned DEFAULT NULL,
        contexte_id bigint(20) unsigned DEFAULT NULL,
        document varchar(64) DEFAULT NULL,
        meta_key varchar(255) DEFAULT NULL,
        meta_value longtext DEFAULT NULL,
        PRIMARY KEY (id),
        UNIQUE (session_id,contexte,contexte_id,document,meta_key)
    ) $charset_collate;";
    
    $res = dbDelta($wpof_sql);
    
    /*
    $existing_columns = $wpdb->get_col("DESC {$table_documents}", 0);
    if (!in_array("scan", $existing_columns))
    {
        $wpdb->query("ALTER TABLE $table_documents ADD COLUMN scan tinyint DEFAULT 0 AFTER document;");
    }
    */
    
    //debug_info($res, "SQL1");
    
    // Note : je ne sais pas pourquoi, l'envoi des deux requêtes de création de table en une fois à dbDelta ne fonctionne pas, seule la seconde est créée...
    $wpof_sql = "CREATE TABLE IF NOT EXISTS $table_session_stagiaire
    (
        id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        session_id bigint(20) unsigned DEFAULT NULL,
        user_id bigint(20) unsigned DEFAULT NULL,
        meta_key varchar(255) DEFAULT NULL,
        meta_value longtext DEFAULT NULL,
        PRIMARY KEY (id),
        UNIQUE (session_id,user_id,meta_key)
    ) $charset_collate;";

    $res = dbDelta($wpof_sql);
    //debug_info($res, "SQL2");
    
    $wpof_sql = "CREATE TABLE IF NOT EXISTS $table_quiz
    (
        id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        quiz_id bigint(20) unsigned DEFAULT NULL,
        parent_id bigint(20) unsigned DEFAULT NULL,
        subject varchar(64) DEFAULT NULL,
        title varchar(255) DEFAULT NULL,
        meta_key smallint unsigned DEFAULT NULL,
        meta_value longtext DEFAULT NULL,
        PRIMARY KEY (id),
        UNIQUE (quiz_id,parent_id,subject,occurrence,meta_key)
    ) $charset_collate;";

    $res = dbDelta($wpof_sql);
    
    /*
    // Déplacement des modèles de page : post_type = page => post_type = modele
    $modele = array
    (
        'reglement-interieur-id',
        'convention-id',
        'attestation-formation-id',
        'accord-comm-id',
        'proposition-id',
        'emargement-id',
    );
    
    foreach ($modele as $m)
    {
        $page_id = get_option($m);
        wp_update_post(array('ID' => $page_id, 'post_type' => 'modele'));
    }
    */
    // Action sur les sessions
    // changement de format de date
    // nom du champ first_timestamp => first_date_timestamp
    // nom du champ contenus => presentation
    $session = get_posts(array( 'post_type' => "session", 'numberposts' => -1));
    
    foreach ($session as $s)
    {
        /*
        // Action sur les stagiaires
        $stagiaires = get_post_meta($s->ID, "inscrits", true);
        
        if (is_array($stagiaires))
        {
            foreach($stagiaires as $user_id)
            {
                $inscription = get_user_meta($user_id, "inscription", true);
                if ($inscription == "")
                    $inscription = array();
                
                $inscription[] = $s->ID;
                update_user_meta($user_id, "inscription", array_unique($inscription));
            }
        }
        
        $formateurs = get_post_meta($s->ID, "formateur", true);
        */
        $visible = get_post_meta($s->ID, "visibilite_session", true);
        if ($visible == "")
            update_post_meta($s->ID, "visibilite_session", 'public');
    }
    
    /*
    require_once(wpof_path . "/class/class-quiz.php");
    
    foreach ($session as $s)
    {
        $formation_id = get_post_meta($s->ID, "formation", true);
        if (-1 == $formation_id)
        {
            $parent_id = $s->ID;
            $parent_type = "session";
            $quizpr = new Quiz("prerequis", "Q", $parent_id, $parent_type);
            $quizpr->parse_text(get_post_meta($parent_id, "quizpr", true));
            $quizobj = new Quiz("objectifs", "Q", $parent_id, $parent_type);
            $quizobj->parse_text(get_post_meta($parent_id, "quizobj", true));
        }
        
        if ($s->post_status == "trash" || $s->post_status == "auto-draft")
            wp_delete_post($s->ID, true);
        
        $dates = get_post_meta($s->ID, 'dates', true);
        if (!is_array($dates))
        {
            $dates = preg_split("/[\s]+/", $dates);
            update_post_meta($s->ID, 'dates', $dates);
        }
        if (null == get_post_meta($s->ID, "first_date", true))
            add_post_meta($s->ID, "first_date", $dates[0], true);
        
        $data = get_post_meta($s->ID, "first_timestamp", true);
        if (isset($data))
        {
            delete_post_meta($s->ID, "first_timestamp");
            add_post_meta($s->ID, "first_date_timestamp", $data, true);
        }
        $data = get_post_meta($s->ID, "contenus", true);
        if (isset($data))
        {
            delete_post_meta($s->ID, "contenus");
            add_post_meta($s->ID, "presentation", $data, true);
        }
        $data = get_post_meta($s->ID, "quiz", true);
        if (isset($data))
        {
            delete_post_meta($s->ID, "quiz");
            add_post_meta($s->ID, "quizobj", $data, true);
        }
        
        $data = get_post_meta($s->ID, "doc-info", true);
        if (isset($data) && is_array($data))
        {
            $table = $wpdb->prefix."wpof_documents";
            foreach($data as $t => $d)
            {
                $fields = array('session_id' => $s->ID, 'document' => $t);
                if (isset($d['filename']))
                    $wpdb->replace($table, array_merge($fields, array('meta_key' => 'pdf_filename', 'meta_value' => $d['filename'])));
                if (isset($d['ready']))
                    $wpdb->replace($table, array_merge($fields, array('meta_key' => 'visible_stagiaire', 'meta_value' => $d['ready'])));
                
                $valid = get_option($t.'_valid_responsable') * Document::VALID_RESPONSABLE_NEED + get_option($t.'_valid_client') * Document::VALID_CLIENT_NEED + get_option($t.'_valid_stagiaire') * Document::VALID_STAGIAIRE_NEED;
                if (isset($d['signed']) && $d['signed'])
                    $valid += Document::VALID_RESPONSABLE_DONE;
                $wpdb->replace($table, array_merge($fields, array('meta_key' => 'valid', 'meta_value' => $valid)));

                if (isset($d['date']))
                    $wpdb->replace($table, array_merge($fields, array('meta_key' => 'last_modif', 'meta_value' => $d['date'])));
            }
            delete_post_meta($s->ID, "doc-info");
        }
        
        
        // Action sur les stagiaires
        $stagiaires = get_post_meta($s->ID, "inscrits", true);
        
        if (is_array($stagiaires)) :
        
        foreach($stagiaires as $user_id)
        {
            $inscription = get_user_meta($user_id, "inscription", true);
            if ($inscription == "")
                $inscription = array();
            
            $inscription[] = $s->ID;
            update_user_meta($user_id, "inscription", $inscription);
            
            $data = json_decode(get_user_meta($user_id, "session".$s->ID, true), true);
            
            // documents
            if (isset($data['doc-info']) && is_array($data['doc-info']))
            {
                $table = $wpdb->prefix."wpof_documents";
                foreach($data['doc-info'] as $t => $d)
                {
                    $fields = array('session_id' => $s->ID, 'user_id' => $user_id, 'document' => $t);
                    if (isset($d['filename']))
                        $wpdb->replace($table, array_merge($fields, array('meta_key' => 'pdf_filename', 'meta_value' => $d['filename'])));
                    if (isset($d['ready']))
                        $wpdb->replace($table, array_merge($fields, array('meta_key' => 'visible_stagiaire', 'meta_value' => $d['ready'])));
                    
                    $valid = (int)get_option($t.'_valid_responsable') * Document::VALID_RESPONSABLE_NEED + (int)get_option($t.'_valid_client') * Document::VALID_CLIENT_NEED + (int)get_option($t.'_valid_stagiaire') * Document::VALID_STAGIAIRE_NEED;
                    echo "<p>$t : $valid</p>";
                    if (isset($d['signed']) && $d['signed'])
                        $valid += Document::VALID_RESPONSABLE_DONE;
                    $wpdb->replace($table, array_merge($fields, array('meta_key' => 'valid', 'meta_value' => $valid)));

                    if (isset($d['date']))
                        $wpdb->replace($table, array_merge($fields, array('meta_key' => 'last_modif', 'meta_value' => $d['date'])));
                }
            }
            
            // session stagiaire
            $table = $wpdb->prefix."wpof_session_stagiaire";
            $fields = array('session_id' => $s->ID, 'user_id' => $user_id);
            
            // rappatriement des infos d'entreprise
            if (isset($data['entreprise']) && $data['entreprise'] > 0)
            {
                $eid = $data['entreprise'];
                $wpdb->replace($table, array_merge($fields, array('meta_key' => 'entreprise', 'meta_value' => get_the_title($eid))));
                if ($emeta = get_post_meta($eid, "adresse", true))
                    $wpdb->replace($table, array_merge($fields, array('meta_key' => 'entreprise_adresse', 'meta_value' => $emeta)));
                if ($emeta = get_post_meta($eid, "cp_ville", true))
                    $wpdb->replace($table, array_merge($fields, array('meta_key' => 'entreprise_cp_ville', 'meta_value' => $emeta)));
                if ($emeta = get_post_meta($eid, "telephone", true))
                    $wpdb->replace($table, array_merge($fields, array('meta_key' => 'entreprise_telephone', 'meta_value' => $emeta)));
            }
            
            // d'abord les champs qui changent de nom
            if (isset($data['fonction']))
            {
                $wpdb->replace($table, array_merge($fields, array('meta_key' => 'entreprise_fonction', 'meta_value' => $data['fonction'])));
            }
            if (isset($data['statut']))
            {
                $wpdb->replace($table, array_merge($fields, array('meta_key' => 'statut', 'meta_value' => $data['statut'])));
                if (isset($data['statut_'.sanitize_title($data['statut'])]))
                {
                    $wpdb->replace($table, array_merge($fields, array('meta_key' => 'statut_complement', 'meta_value' => $data['statut_'.sanitize_title($data['statut'])])));
                    unset($data['statut_'.sanitize_title($data['statut'])]);
                }
            }
            if (isset($data['financement']))
            {
                $wpdb->replace($table, array_merge($fields, array('meta_key' => 'financement', 'meta_value' => $data['financement'])));
                if (isset($data['financement_'.sanitize_title($data['financement'])]))
                {
                    $wpdb->replace($table, array_merge($fields, array('meta_key' => 'financement_complement', 'meta_value' => $data['financement_'.sanitize_title($data['financement'])])));
                    unset($data['financement_'.sanitize_title($data['financement'])]);
                }
            }
            
            // les questionnaires qu'on stocke au format json
            if (isset($data['eval_preform']) && $data['eval_preform'] != "")
            {
                $wpdb->replace($table, array_merge($fields, array('meta_key' => 'eval_preform', 'meta_value' => json_encode($data['eval_preform'], JSON_HEX_APOS|JSON_UNESCAPED_UNICODE))));
            }
            
            if (isset($data['eval_postform']) && $data['eval_postform'] != "")
            {
                $wpdb->replace($table, array_merge($fields, array('meta_key' => 'eval_postform', 'meta_value' => json_encode($data['eval_postform'], JSON_HEX_APOS|JSON_UNESCAPED_UNICODE))));
            }
            
            // puis les autres
            $autres = array("etat-session", "attentes", "date", "has-employeur", "entreprise-responsable", "entreprise-service", "adresse", "cp_ville", "telephone", "nature-formation", "tarif-jour", "tarif-total-chiffre", "tarif-total-lettre", "nb-jour", "nb-heure", "emarge");
            
            foreach($autres as $key)
            {
                if (isset($data[$key]) && $data[$key] != "")
                {
                    $wpdb->replace($table, array_merge($fields, array('meta_key' => str_replace('-', '_', $key), 'meta_value' => $data[$key])));
                }
            }
            
            //delete_user_meta($user_id, "session".$s->ID));
        }
        
        endif;
    }
    // Action sur les formations
    
    $formation = get_posts(array( 'post_type' => "formation", 'numberposts' => -1));
    
    foreach ($formation as $f)
    {
            $parent_id = $f->ID;
            $parent_type = "formation";
            $quizpr = new Quiz("prerequis", "Q", $parent_id, $parent_type);
            $quizpr->parse_text(get_post_meta($parent_id, "quizpr", true));
            $quizobj = new Quiz("objectifs", "Q", $parent_id, $parent_type);
            $quizobj->parse_text(get_post_meta($parent_id, "quizobj", true));
            
        $data = get_post_meta($f->ID, "contenus", true);
        if (isset($data))
        {
            delete_post_meta($f->ID, "contenus");
            add_post_meta($f->ID, "presentation", $data, true);
        }
        $data = get_post_meta($f->ID, "quiz", true);
        if (isset($data))
        {
            delete_post_meta($f->ID, "quiz");
            add_post_meta($f->ID, "quizobj", $data, true);
        }
    }
    */
    /*
    
    // Options générales
    // Préfixées par wpof_
    // Remplacer les - par des _
    $options_v1 = array
    (
        'monnaie',
        'monnaie-symbole',
        'ofadmin',
        'exotva',
        'tarif-inter',
        'respform-id',
        'respform-fonction',
        'respform-admin',
        'hidden-menus',
        'nature-formation',
        'statut-stagiaire',
        'session-login-form',
        'financement',
        'reglement-interieur-id',
        'reglement-interieur-etape-formateur',
        'reglement-interieur-etape-stagiaire',
        'convention-id',
        'convention-etape-formateur',
        'convention-etape-stagiaire',
        'attestation-formation-id',
        'attestation-formation-etape-formateur',
        'attestation-formation-etape-stagiaire',
        'accord-comm-id',
        'accord-comm-etape-formateur',
        'accord-comm-etape-stagiaire',
        'proposition-id',
        'proposition-etape-formateur',
        'proposition-etape-stagiaire',
        'emargement-id',
        'emargement-etape-formateur',
        'emargement-etape-stagiaire',
        'eval-form',
        'pdf-header',
        'pdf-footer',
        'pdf-marge-haut',
        'pdf-marge-bas',
        'pdf-marge-gauche',
        'pdf-marge-droite',
        'pdf-hauteur-header',
        'pdf-hauteur-footer',
        'pdf-css',
        'options-list',
    );
    
    $options_v2 = array
    (
        'monnaie',
        'monnaie-symbole',
        'ofadmin',
        'exotva',
        'tarif-inter',
        'respform-id',
        'respform-fonction',
        'respform-admin',
        'hidden-menus',
        'nature-formation',
        'statut-stagiaire',
        'session-login-form',
        'financement',
        'type-session',
        'eval-form',
        'pdf-header',
        'pdf-footer',
        'pdf-marge-haut',
        'pdf-marge-bas',
        'pdf-marge-gauche',
        'pdf-marge-droite',
        'pdf-hauteur-header',
        'pdf-hauteur-footer',
        'pdf-css',
        'options-list',
        'terms_exo_tva',
        'terms_no_session',
        'terms_tarif_inter',
        'terms_tarif_groupe',
        'terms_connexion_session',
        'terms_msg_debut_pre_inscription',
        'terms_msg_validation_pre_inscription',
        'terms_question_employeur',
        'terms_autres_frais',
        'doc_last_modif',
        'doc_alerte_creation',
        'doc_demande_valid',
    );
    global $doc_nom;
    $doc_prop = array
    (
        'nom',
        'id',
        'valid-responsable',
        'valid-stagiaire',
        'valid-client',
        'global-inter',
        'global-intra',
        'stagiaire-inter',
        'stagiaire-intra',
    );
    foreach (array_keys($doc_nom) as $d)
        foreach ($doc_prop as $p)
        {
            $options_v2[] = "$d-$p";
        }
    foreach ($options_v2 as $k)
    {
        $val = "";
        if ($val = get_option($k))
        {
            delete_option($k);
            update_option("wpof_$k", $val);
            echo "<p>$k => wpof_$k</p>";
        }
    }*/
}
