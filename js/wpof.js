var $ = jQuery;

$(document).ready(function($)
{

// Fonction pour ouvrir ou fermer un bloc
// Le bouton doit avoir pour data-id l'id du bloc à gérer
$('.openButton').click(function(e)
{
    id = "#"+$(this).attr("data-id");
    $(id).toggleClass('blocHidden');
});

// Fonction pour ouvrir ou fermer un bloc depuis un bouton radio ou checkbox
// Le bouton doit avoir pour data-id l'id du bloc à gérer
// Pas forcément utile de perdre du temps là-dessus...
$('.openCheck').change(function(e)
{
    id = "#"+$(this).attr("data-id");
    $(id).toggleClass('blocHidden');
});


$('.choix-interactif').click(function()
{
    $(this).children("li").each(function()
    {
        if ($(this).children("label").children("input").is(':checked'))
            $(this).children("label").children("span").show();
        else
            $(this).children("label").children("span").hide();
    });
});

// Gestion du menu du tableau de bord de session
$('.wpof-menu ul .onglet').click(switch_onglet);
function switch_onglet(e)
{
    id = "#" + $(this).attr("data-id");
    famille = $(this).parent().parent().attr("data-famille");
    $('.famille-' + famille + ' .tableau').addClass('blocHidden');
    $('.famille-' + famille + ' ul .onglet').removeClass('highlightButton');
    if (false === $(this).hasClass("fermer-tableau"))
    {
        $(id).removeClass('blocHidden');
        $(this).addClass('highlightButton');
    }
}

$("#main-tabs").tabs({active: $("input[name='default_main_tab']").val()});
$("#options-tabs").tabs({active: $("input[name='default_options_tab']").val()});
$("#user-tabs").tabs({active: $("input[name='default_user_tab']").val()});
$("#tabs-clients").tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
$("#tabs-clients li").removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
$("#profile-box-menu").menu();


$("#main-tabs > ul > li").click(switch_default_tab);
$("#options-tabs > ul > li").click(switch_default_tab);
$("#user-tabs > ul > li").click(switch_default_tab);
function switch_default_tab(e)
{
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'set_default_tab',
            'tab_id': $(this).attr('aria-labelledby').replace('ui-id-', ''),
            'tab_name': $(this).closest('.ui-tabs').attr('id'),
        },
    );
    if ($(this).parent().attr('data-reload') != undefined)
        document.location.reload();
}


/*
$('.wpof-menu ul li.fermer-tableau').click(function(e)
{
    id = $(this).attr("data-id");
    famille = $(this).parent().parent().attr("data-famille");
    $('.tableau' + id).addClass('blocHidden');
    $('.wpof-menu ul li.bouton').removeClass('highlightButton');
});
*/


// Champs relatifs à la TVA dans les options générales
$("input.tva").change(function(e)
{
    e.preventDefault();
    if ($("input[name='wpof_of_hastva']").prop("checked"))
    {
        $("input[name='wpof_of_exotva']").prop("disabled", false);
        $("input[name='wpof_of_tauxtva']").prop("disabled", false);
    }
    else
    {
        $("input[name='wpof_of_tauxtva']").prop("disabled", true);
        $("input[name='wpof_of_exotva']").prop("disabled", true);
    }
});

// 
$('input:checkbox.has-employeur').change(function (e)
{
    id = "#" + $(this).attr("data-id");
    $('table' + id + ' .employeur-oui').toggleClass('blocHidden');
    $('table' + id + ' .employeur-non').toggleClass('blocHidden');
});


$('textarea.autoselect').click(function (e)
{
    $(this).select();
});

/*
$('input.tarif_total_chiffre').click(function (e)
{
    id = $(this).attr("data-id");
    if ($(this).val() == "")
        $(this).val($('#nb-jour' + id).val() * $('#tarif-jour' + id).val());
});
*/

// désinscrire une personne d'une session de formation
// fonction attachée au bouton X dans le menu du tableau de bord
$(".unsubscribe").click(unsubscribe_stagiaire);
function unsubscribe_stagiaire(e)
{
    e.preventDefault();
    session = $(this).attr("data-sessionid");
    user = $(this).attr("data-userid");
    
    $("#user" + user).remove();
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'unsubscribe',
            'user_id': user,
            'session_id': session,
        },
        function(response)
        {
            //console.log(response);
        }
    );
}

// Bouton créer ou signer le document
$(".doc-creer").click(doc_creer);
function doc_creer(e)
{
    var ligne = $(this).closest(".docrow");
    contexte_id = ligne.attr("data-contexteid");
    contexte = ligne.attr("data-contexte");
    session = ligne.attr("data-sessionid");
    type_doc = ligne.attr("data-typedoc");
    doc_uid = ligne.attr("data-docuid");
    attention = $(this).hasClass("attention");
    action_doc = "brouillon";
    if ($(this).attr("data-final"))
        action_doc = "final";
    cols = ligne.attr("data-cols");
    if ($(this).attr("data-signataire"))
        signataire = 1;
    else
        signataire = 0;
    
    upload_form_name = "upload-" + doc_uid;
    
    /*
    if (attention && attention != "")
        confirm = window.confirm($(this).attr("data-alert"));
    */
    confirm = true;
    if (!attention || confirm)
    {
        bouton_text = $("#nom-" + doc_uid).html();
        $("#nom-" + doc_uid).html("Patientez");
    
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'traitement_doc',
                'contexte_id': contexte_id,
                'contexte': contexte,
                'session_id': session,
                'type_doc': type_doc,
                'cols': cols,
                'signataire': signataire,
                'action_doc': action_doc,
            },
            function(response)
            {
                param = JSON.parse(response);
                newline = $(param.lignedoc);
                newline.on('click', '.doc-creer', doc_creer);
                newline.on('click', '.doc-demande-valid', doc_demande_valid);
                newline.on('click', '.doc-diffuser', doc_diffuser);
                newline.on('click', '.doc-scan', doc_scan);
                newline.on('click', '.doc-supprimer', doc_supprimer);
                ligne.replaceWith(newline);
            }
        );
    }
}


// Bouton demander la validation (signature par le responsable de formation)
$(".doc-demande-valid").click(doc_demande_valid);
function doc_demande_valid(e)
{
    var ligne = $(this).closest(".docrow");
    contexte_id = ligne.attr("data-contexteid");
    contexte = ligne.attr("data-contexte");
    session = ligne.attr("data-sessionid");
    type_doc = ligne.attr("data-typedoc");
    doc_uid = ligne.attr("data-docuid");
    cols = ligne.attr("data-cols");
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'traitement_doc',
            'contexte_id': contexte_id,
            'contexte': contexte,
            'session_id': session,
            'type_doc': type_doc,
            'cols': cols,
            'action_doc': 'demande_valid',
        },
        function(response)
        {
            //console.log(response);
        }
    );
    
    // plutôt que toggle, vérifier le véritable état !
    $(this).toggleClass("en-cours");
}

// Bouton pour diffuser le document au stagiaire
$(".doc-diffuser").click(doc_diffuser);
function doc_diffuser(e)
{
    var ligne = $(this).closest(".docrow");
    contexte_id = ligne.attr("data-contexteid");
    contexte = ligne.attr("data-contexte");
    session = ligne.attr("data-sessionid");
    type_doc = ligne.attr("data-typedoc");
    doc_uid = ligne.attr("data-docuid");
    cols = ligne.attr("data-cols");
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'traitement_doc',
            'contexte_id': contexte_id,
            'contexte': contexte,
            'session_id': session,
            'type_doc': type_doc,
            'cols': cols,
            'action_doc': 'diffuser',
        },
        function(response)
        {
            //console.log(response);
        }
    );
    
    // plutôt que toggle, vérifier le véritable état !
    $(this).toggleClass("fait");
}

// Bouton pour supprimer le document
$(".doc-supprimer").click(doc_supprimer);
function doc_supprimer(e)
{
    var ligne = $(this).closest(".docrow");
    contexte_id = ligne.attr("data-contexteid");
    contexte = ligne.attr("data-contexte");
    session = ligne.attr("data-sessionid");
    type_doc = ligne.attr("data-typedoc");
    doc_uid = ligne.attr("data-docuid");
    cols = ligne.attr("data-cols");
    
    var supprim = true;
    yesno = $("#supprim" + doc_uid);
    yesno.dialog(
    {
        autoOpen: true,
        title: "Êtes-vous sûr ?",
        height: 200,
        width: 350,
        modal: true,
        buttons:
        {
            "Oui": function()
            {
                jQuery.post
                (
                    ajaxurl,
                    {
                        'action': 'traitement_doc',
                        'contexte_id': contexte_id,
                        'contexte': contexte,
                        'session_id': session,
                        'type_doc': type_doc,
                        'cols': cols,
                        'action_doc': 'supprimer',
                    },
                    function(response)
                    {
                        param = JSON.parse(response);
                        newline = $(param.lignedoc);
                        newline.on('click', '.doc-creer', doc_creer);
                        newline.on('click', '.doc-demande-valid', doc_demande_valid);
                        newline.on('click', '.doc-diffuser', doc_diffuser);
                        newline.on('click', '.doc-scan', doc_scan);
                        newline.on('click', '.doc-supprimer', doc_supprimer);
                        ligne.replaceWith(newline);
                    }
                );
                yesno.dialog("destroy");
            },
            "Non": function() { supprim = false; yesno.dialog("destroy");},
        },
        close: function()
        {
            supprim = false; yesno.dialog("destroy");
        }
    });
    yesno.dialog("open");
}

$(".sql_select").click(function(e)
{
    e.preventDefault();
    session_id = $(this).attr("data-sessionid");
    stagiaire_id = $(this).attr("data-stagiaireid");
    
    sql_info = $("#pilote_dialog");
    
    if (sql_info.dialog( "instance" ))
        sql_info.dialog("destroy");

    sql_info.dialog
    ({
        autoOpen: true,
        title: "Infos sur la session " + session_id,
        height: 600,
        width: 1200,
        modal: false,
        close: function()
        {
            sql_info.dialog("destroy");
        }
    });
    
    jQuery.post
    (
        ajaxurl,
        {
            'action' : 'sql_session_formation',
            'session_id' : session_id,
            'stagiaire_id' : stagiaire_id,
        },
        function (response)
        {
            sql_info.html(response);
        }
    );
});

$(".button-add-media").click(function(e)
{
    e.preventDefault();
    
    value_id = $(this).attr("data-valueid");
    link_media = $(this).attr("data-linkmedia");
    img_media = $(this).attr("data-image");
    
    if (!$(this).hasClass("inactif"))
    {
        var uploader = wp.media
        ({
            title: "Envoyer un fichier",
            button: { text: "Téléverser" },
            multiple: false,
        })
        .on('select', function()
        {
            var selection = uploader.state().get('selection');
            var attachment = selection.first().toJSON();
            //console.log(attachment);
            $("#" + link_media).attr("href", attachment.link);
            $("#" + link_media).text(attachment.title);
            $("#" + value_id).val(attachment.id);
            if (img_media != undefined)
                $("#" + img_media).attr("src", attachment.url);
        })
        .open();
    }
});

$(".doc-compile").click(function()
{
    session = $(this).attr("data-sessionid");
    user = $(this).attr("data-userid");
    doc = $(this).attr("data-docid");
    resultat = "#" + $(this).attr("data-resultat");
    
    $(resultat).html("Patientez...");
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'export_pdf',
            'user_id': user,
            'session_id': session,
            'doc_id': doc,
        },
        function(response)
        {
            $(resultat).html(response);
        }
    );
    
});

// Permuter d'utilisateur
$(".switch-user").click(function(e)
{
    user = $(this).attr("data-userid");
    url = $(this).attr("data-url");
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'switchuser',
            'user_id': user,
            'url': url,
        },
        function(response)
        {
            document.location.reload(true);
        }
    );
});


// Bouton pour ajouter une ligne d'ajout de nouveau stagiaire
var stagiaire_num = 1;
$(".new-stagiaire span#plus").click(function(e)
{
    var markup = "<tr id='" + stagiaire_num +"_new-stagiaire' data-num='" + stagiaire_num +"' class='new-stagiaire new-stagiaire-plus'>";
    markup += "<td><select name='" + stagiaire_num +"_genre'><option value='Madame'>Madame</option><option value='Monsieur'>Monsieur</option></select></td>";
    markup += "<td><input type='text' name='" + stagiaire_num +"_firstname' placeholder='Prénom' /></td>";
    markup += "<td><input type='text' name='" + stagiaire_num +"_lastname' placeholder='Nom' /></td>";
    markup += "<td><input type='text' name='" + stagiaire_num +"_email' placeholder='Courriel' /></td>";
    select_role = $(this).attr("data-selectrole");
    if (select_role !== undefined)
    {
        markup += "<td><select name='" + stagiaire_num + "_role'>";
        role_list = JSON.parse(select_role);
        $.each(role_list, function(index, value)
        {
            markup += "<option value='" + index + "'>" + value + "</option>";
        });
        markup += "</select></td>";
    }
    markup += "</tr>";
    $("table.add-stagiaire tbody").append(markup);
    stagiaire_num ++;
});

// ajouter des stagiaires (création de compte + éventuellement inscription à la session session_id)
$("#add-stagiaire-bouton").click(function(e)
{
    parent_id = "#" + $(this).parent().attr("id");
    session_id = $(parent_id + " :input[name='session_id']").val();
    var allusers = new Array();
    $(parent_id + " .new-stagiaire").each(function()
    {
        id = "#" + $(this).attr("id");
        num = $(this).attr("data-num");
        genre = $(id + " :input[name$='_genre']").val();
        firstname = $(id + " :input[name$='_firstname']").val();
        lastname = $(id + " :input[name$='_lastname']").val();
        email = $(id + " :input[name$='_email']").val();
        if (session_id == 0)
            role = $(id + " :input[name$='_role']").val();
        else
            role = "um_stagiaire";
        allusers[num] = { "genre" : genre, "firstname" : firstname, "lastname" : lastname, "email" : email, "role" : role };
    });
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'add_stagiaire',
            dataType: 'json',
            contentType: 'application/json',
            'data': JSON.stringify({ 'session_id': session_id,  'allusers' : allusers }),
        },
        function(response)
        {
            param = JSON.parse(response);
            $(parent_id + " .message").html(param.message);
            $('#board-stagiaires').append(param.stagiaires_board);
            $('#update-budget-total').show();
        }
    );
});

$("#inscrit-stagiaire-bouton").click(function(e)
{
    parent_id = "#" + $(this).parent().attr("id");
    session_id = $(parent_id + " input[name='session_id']").val();
    users = new Array();
    $(parent_id + " select option:selected").each(function()
    {
        users.push($(this).val());
    });
    //console.log(users);
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'subscribe',
            'session_id': session_id,
            'stagiaire_id': users,
        },
        function(response)
        {
            param = JSON.parse(response);
            $(parent_id + " .message").html(param.message);
            boards = $(param.stagiaires_board);
            boards.on('click', '.unsubscribe', unsubscribe_stagiaire);
            boards.on('click', '.wpof-menu ul .onglet', switch_onglet);
            boards.on('change', '.metadata input[name="tarif_heure_presentiel"]', change_tarif_stagiaire);
            boards.on('click', 'div.creneau', switch_creneau);
            boards.on('click', '.doc-creer', doc_creer);
            $('#board-stagiaires').append(boards);
            $('#update-budget-total').show();
        }
    );
});


$('.stagiaire-submit').click(function(e)
{
    e.preventDefault();
    
    message_bloc = $(this).closest('.ui-tabs-panel').children('.message');
    formData = new FormData($(this).closest("form")[0]);
    formData.set('action', 'enregistrer_stagiaire_form');
    jQuery.post
    ({
        url: ajaxurl,
        data: formData,
        processData: false,
        contentType: false,
        success: function (response)
        {
            param = JSON.parse(response);
            console.log(param.log);
            message_bloc.html(param.message);
        }
    });
    
});

// envoyer un fichier libre en ajax post
$(".ajax-save-file").click(function(e)
{
    form = $(this).closest('form');
    save_file(form);
});

// fonction qui gère le dépot de fichier, soit prédéfini (input[name='document'] est renseigné), soit libre
function save_file(form)
{
    var id_span_message = "#" + form.find("input[name='id_span_message']").val();
    var id_span_filename = "#" + form.find("input[name='id_span_filename']").val();
    var ligne_id = "#" + form.find("input[name='ligne_id']").val();
    var ligne = $(ligne_id);
    
    filelist = form.children("input[type='file']")[0].files;
    json_filelist = Array();
    
    var formData = new FormData();
//    formData.set('file', $(this).parent().children("input[type='file']").prop('files'));
    for (var i = 0; i < filelist.length; i++)
    {
        formData.append("files[]", (filelist[i]));
    }
    
    formData.set('action', form.find("input[name='action']").val());
    formData.set('session_id', form.find("input[name='session_id']").val());
    formData.set('nb_files', filelist.length);
                       
    if (ligne.attr('data-typedoc') != undefined)
    {
        formData.set('contexte_id', ligne.attr("data-contexteid"));
        formData.set('contexte', ligne.attr("data-contexte"));
        formData.set('signataire', form.find("input[name='signataire']").val());
        formData.set('signature_responsable', form.find("input[name='signature_responsable']").prop('checked'));
        formData.set('signature_client', form.find("input[name='signature_client']").prop('checked'));
        formData.set('signature_stagiaire', form.find("input[name='signature_stagiaire']").prop('checked'));
        formData.set('cols', ligne.attr("data-cols"));
        formData.set('type_doc', ligne.attr("data-typedoc"));
        formData.set('doc_uid', ligne.attr("data-docuid"));
    }
    
    $(id_span_message).html('Patientez');
    
    jQuery.post
    ({
        url: ajaxurl,
        data: formData,
        processData: false,
        contentType: false,
        success: function(response)
        {
            param = JSON.parse(response);
            console.log(param.log);
            $(id_span_message).html("<p>" + param.message + "</p>");
            if (param.lignedoc != "")
            {
                newline = $(param.lignedoc);
                newline.on('click', '.doc-creer', doc_creer);
                newline.on('click', '.doc-demande-valid', doc_demande_valid);
                newline.on('click', '.doc-diffuser', doc_diffuser);
                newline.on('click', '.doc-scan', doc_scan);
                newline.on('click', '.doc-supprimer', doc_supprimer);
                $(ligne_id).replaceWith(newline);
            }
            else
            {
                $("#liste-scan tr:nth-child(n+2)").remove();
                $("#liste-scan").append(param.filename);
                $("#liste-scan").on('click', ".del-scan", del_scan);
            }
        },
        error: function()
        {
            $(id_span_message).html("<span class='erreur'>Erreur jQuery post</span>");
        }
    });
}

// bouton pour envoyer un document basé sur un modèle prédéfini
$(".doc-scan").click(doc_scan);
function doc_scan(e)
{
    e.preventDefault();
    $(this).parent().find(".message").html("");
    dialog = $(this).parent().children(".dialog");
    dialog.dialog(
    {
        autoOpen: false,
        height: 400,
        width: 550,
        modal: true,
//        create: function() { $(this).on('click', '.ajax-save-file', save_file); },
        buttons:
        {
            "Déposer": function() { save_file(dialog.find("form")); },
            "Fermer": function() { dialog.dialog("destroy");}
        },
        close: function()
        {
            //form[ 0 ].reset();
            allFields.removeClass( "ui-state-error" );
        }
    });
    dialog.dialog("open");
}

// Supression d'un fichier téléversé
$(".del-scan").click(del_scan);
function del_scan(e)
{
    e.preventDefault();
    id_span_message = "#" + $(this).attr("data-message");
    id_conteneur = "#" + $(this).attr("data-conteneur");
    session_id = $(this).attr("data-sessionid");
    md5sum = $(this).attr("data-md5sum");
    
    jQuery.post
    ({
        url: ajaxurl,
        data:
        {
            'action': 'delete_scan_file',
            'session_id': session_id,
            'md5sum': md5sum,
        },
        success: function(response)
        {
            param = JSON.parse(response);
            $(id_span_message).append("<p>" + param.message + "</p>");
            $(id_conteneur).remove();
        },
        error: function()
        {
            $(id_span_message).html("<span class='erreur'>Erreur jQuery post</span>");
        }
    });
}

// enregistrer les champs user_meta qui sont dans des input
$(".metadata .enregistrer-user-input").click(function(e)
{
    user_id = $(this).attr("data-userid");
    parent = "#" + $(this).parent().attr("id");
    var fields = new Object();
    $(parent + " :input").each(function()
    {
        inputid = $(this).attr("name");
        if (editor = tinyMCE.get(inputid))
            fields[inputid] = editor.getContent();
        else
            fields[inputid] = $(this).val();
    });
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'enregistrer_user_input',
            'user_id': user_id,
            'fields': fields,
        },
        function(response)
        {
            $(parent + " .message").html(response);
        }
    );
});


// enregistrer les images uploadées par le responsable de formation
$("form#uploadcrimgform .enregistrer-resp-img").click(function(e)
{
    parent = "#" + $(this).parent().attr("id");
    
    user_id = $(this).attr("data-userid");
    signature = $("form#uploadcrimgform #signature_img").prop('files')[0];
    //console.log(signature);
    formData = new FormData();
    formData.append('file', signature);
    formData.append('action', 'enregistrer_resp_img');
    formData.append('user_id', user_id);
    //console.log(formData);

    jQuery.post
    ({
        url: ajaxurl,
        data: formData,
        contentType: false,
        processData: false,
        success: function(response)
        {
            //console.log(response);
            $(parent + " .message").html(response);
        }
    });
});

// enregistrer les champs post_meta qui sont dans des input
$(".metadata .enregistrer-session-input").click(function(e)
{
    session_id = $(this).attr("data-sessionid");
    action = $(this).attr("data-action");
    parent = "#" + $(this).parent().attr("id");
    var fields = new Object();
    $(parent + " :input").each(function()
    {
        inputid = $(this).attr("name");
        type = $(this).attr("type");
        if (type == "radio" && !$(this).prop("checked"))
            return;
        fields[inputid] = $(this).val();
    });
    //console.log(fields);
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': action,
            'session_id': session_id,
            'fields': fields,
        },
        function(response)
        {
            $(parent + " .message").html(response);
        }
    );
});


$(".radio-choice input[type='text']").change(radio_choice_one_param);
$(".radio-choice input[type='radio']").change(radio_choice_one_param);
function radio_choice_one_param(e)
{
    e.preventDefault();
    meta_value = $(this).val();
    meta_key = $(this).attr("name");
    session_id = $(this).closest(".radio-choice").attr("data-sessionid");
    var bouton = $(this).closest(".radio-choice").parent().children(".openButton");
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'update_unique_value',
            'session_id': session_id,
            'meta_key': meta_key,
            'meta_value': meta_value,
        },
        function(response)
        {
            if (meta_key.match('_complement') != null)
            {
                base = bouton.text().split(" – ")[0];
                bouton.text(base + " – " + response);
            }
            else
                bouton.text(response);
            bouton.removeClass("erreur");
        }
    );
}

$(".metadata select").change(session_select_change);
function session_select_change(e)
{
    e.preventDefault();
    meta_value = $(this).val();
    meta_key = $(this).attr("name");
    session_id = $(this).closest("fieldset").attr("data-sessionid");
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'update_unique_value',
            'session_id': session_id,
            'meta_key': meta_key,
            'meta_value': meta_value,
        },
        function(response)
        {
        }
    );
}

$("#update-budget-total").click(update_budget_total);
function update_budget_total()
{
    if ($("#budget_total").length)
    {
        var budget_total = 0;
        $(".tarif_total_chiffre").each(function()
        {
            if ("" != $(this).val())
                budget_total += parseFloat($(this).val());
        });
        $("#budget_total").html(budget_total);
    }
    $('#update-budget-total').hide();
}

// enregistrer le changement de tarif pour un stagiaire et mettre à jour les valeurs qui en dépendent
$(".metadata input[name='tarif_heure_presentiel']").change(change_tarif_stagiaire);
$(".metadata input[name='tarif_total_chiffre']").change(change_tarif_stagiaire);
//$(".metadata input[name='tarif_heure_presentiel']").blur(change_tarif_stagiaire);
//$(".metadata input[name='tarif_total_chiffre']").blur(change_tarif_stagiaire);
//$(".metadata input[name='tarif_heure_presentiel']").mouseout(change_tarif_stagiaire);
function change_tarif_stagiaire(e)
{
    e.preventDefault();
    var fieldset = $(this).closest("fieldset");
    
    var formData = new FormData();

    formData.set('session_id', fieldset.attr("data-sessionid"));
    formData.set('action', 'enregistrer_session_tarif');
                 
    if ($(this).attr('name') == 'tarif_heure_presentiel')
    {
        formData.set('tarif_heure', $(this).val());
        formData.set('tarif_base_total', 0);
    }
    else
    {
        formData.set('tarif_total_chiffre', $(this).val());
        formData.set('tarif_base_total', 1);
    }
    
    formData.set('stagiaire_id', fieldset.attr("data-stagiaireid"));
    formData.set('session_id', fieldset.attr("data-sessionid")); // TODO : à finir !!! TODO

    jQuery.post
    ({
        url: ajaxurl,
        data: formData,
        processData: false,
        contentType: false,
        success: function(response)
        {
            param = JSON.parse(response);
            //fieldset.children(".message").html(param.message);
            fieldset.find("input[name='tarif_total_chiffre']").val(param.tarif_total_chiffre);
            fieldset.find("input[name='tarif_heure_presentiel']").val(param.tarif_heure);
            fieldset.find(".tarif_total_lettre").text(param.tarif_total_lettre);
            $('#update-budget-total').show();
        }
    });
}

// enregistrer le changement de tarif pour un groupe et mettre à jour les valeurs qui en dépendent
$(".metadata input[name='tarif_heure_intra']").change(change_tarif_groupe);
$(".metadata input[name='tarif_total_chiffre_intra']").change(change_tarif_groupe);
$(".metadata input[name='tarif_heure_intra']").blur(change_tarif_groupe);
$(".metadata input[name='tarif_total_chiffre_intra']").blur(change_tarif_groupe);
function change_tarif_groupe(e)
{
    e.preventDefault();
    parent = $(this).parent();
    var fieldset = parent.parent();
    var fieldset_id = "#" + fieldset.attr("id");
    
    var formData = new FormData();

    formData.set('session_id', fieldset.attr("data-sessionid"));
    formData.set('action', 'enregistrer_session_tarif');
                 
    if ($(this).attr('name') == 'tarif_heure_intra')
    {
        formData.set('tarif_heure', $(this).val());
        formData.set('tarif_base_total', false);
    }
    else
    {
        formData.set('tarif_total_chiffre', $(this).val());
        formData.set('tarif_base_total', true);
    }
    
    jQuery.post
    ({
        url: ajaxurl,
        data: formData,
        processData: false,
        contentType: false,
        success: function(response)
        {
            $(fieldset_id + " .message").html("");
            param = JSON.parse(response);
            console.log(param);
            if (undefined !== param.erreur)
                $(fieldset_id + " .message").html(param.erreur);
                
            $(fieldset_id + " .tarif_total_chiffre").val(param.tarif_total_chiffre);
            $(fieldset_id + " .tarif_heure").val(param.tarif_heure);
            $(fieldset_id + " .tarif_total_lettre").text(param.tarif_total_lettre);
        }
    });
}
$(".metadata input[name='tarif_total_autres_chiffre']").change(change_tarif_autres);
//$(".metadata input[name='tarif_total_autres_chiffre']").mouseout(change_tarif_autres);
$(".metadata input[name='autres_frais']").change(change_tarif_autres);
//$(".metadata input[name='autres_frais']").mouseout(change_tarif_autres);
function change_tarif_autres(e)
{
    e.preventDefault();
    parent = $(this).parent();
    var fieldset = parent.parent();
    var fieldset_id = "#" + fieldset.attr("id");
    session_id = fieldset.attr("data-sessionid");
    
    autres_frais = $(fieldset_id + " input[name='autres_frais']").val();
    tarif_autres = $(fieldset_id + " input[name='tarif_total_autres_chiffre']").val();
    
    if (tarif_autres == 0)
        $(".tarif-autres").hide();
    else
        $(".tarif-autres").show();
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'enregistrer_session_tarif',
            'session_id': session_id,
            'tarif_total_autres_chiffre': tarif_autres,
            'autres_frais': autres_frais,
        },
        function(response)
        {
            $(fieldset_id + " .message").html("");
            param = JSON.parse(response);
            if (undefined !== param.erreur)
                $(fieldset_id + " .message").html(param.erreur);
                
            $(fieldset_id + " .tarif_total_autres_lettre").text(param.tarif_total_autres_lettre);
        }
    );
    
}

$(".notif-modif input").change(notification_modif);
$(".notif-modif select").change(notification_modif);
function notification_modif(e)
{
    e.preventDefault();
    parent_id = "#" + $(this).closest(".notif-modif").attr("id");
    bouton = $(parent_id + " .bouton").addClass("enregistrement-requis");
}

// activer ou désactiver les types de feuilles d'émargement
$("#choix-type-emargement .bouton").click(function()
{
    parent = "#" + $(this).parent().attr("id");
    emarge_type = $(this).attr("data-type");
    session_id = $(this).attr("data-sessionid");
    value = $(this).attr("data-value");
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'change_type_emargement',
            'session_id': session_id,
            'emarge_type': emarge_type,
            'value' : value,
        },
        function(response)
        {
            $(parent + " .message").html(response);
        }
    );
    
    $(this).toggleClass("fait");
    $(this).attr("data-value", 1 - value); // inversion de la valeur : soit 0, soit 1
});

$(".unique-value").change(function()
{
    var fields = new Object();
    fields["action"] = "update_unique_value";
    if (undefined !== $(this).attr("data-sessionid"))
        fields["session_id"] = $(this).attr("data-sessionid");
    if (undefined !== $(this).attr("data-stagiaireid"))
        fields["stagiaire_id"] = $(this).attr("data-stagiaireid");
    fields["meta_key"] = $(this).attr("name");
    fields["meta_value"] = $(this).val();
    
    // special checkbox non cochée
    if ($(this).is(":checkbox") && !$(this).is(":checked"))
        fields["meta_value"] = "";
    
    parent = "#" + $(this).parent().attr("id");
    
    jQuery.post
    (
        ajaxurl,
        fields,
        function(response)
        {
            $(parent + " .message").html(response);
            $(parent).addClass("succes");
   //         console.log(response);
        }
    );
});

$("select[name='stagiaire-a-copier']").change(function(e)
{
    e.preventDefault();
    
    user_id = $(this).parent().attr("data-id");
    modele_id = $(this).val();
    
    //console.log(modele_id + " -> " + user_id);
    
    if (modele_id > 0)
    {
        $("input#has-employeur" + user_id).prop("checked", $("input#has-employeur" + modele_id).prop("checked"));
        $("#entreprise" + user_id + " input[name='entreprise']").val($("#entreprise" + modele_id + " input[name='entreprise']").val());
        $("#entreprise" + user_id + " input[name='entreprise_fonction']").val($("#entreprise" + modele_id + " input[name='entreprise_fonction']").val());
        $("#entreprise" + user_id + " input[name='entreprise_service']").val($("#entreprise" + modele_id + " input[name='entreprise_service']").val());
        $("#entreprise" + user_id + " input[name='entreprise_responsable']").val($("#entreprise" + modele_id + " input[name='entreprise_responsable']").val());
        $("#entreprise" + user_id + " input[name='adresse']").val($("#entreprise" + modele_id + " input[name='adresse']").val());
        $("#entreprise" + user_id + " input[name='cp_ville']").val($("#entreprise" + modele_id + " input[name='cp_ville']").val());
        $("#entreprise" + user_id + " input[name='telephone']").val($("#entreprise" + modele_id + " input[name='telephone']").val());
        $("#inscription" + user_id + " input[name='financement']").val($("#inscription" + modele_id + " input[name='financement']").val());
        $("#inscription" + user_id + " input[name='financement_complement']").val($("#inscription" + modele_id + " input[name='financement_complement']").val());
        $("#inscription" + user_id + " input[name='nature_formation']").val($("#inscription" + modele_id + " input[name='nature_formation']").val());
        $("#inscription" + user_id + " input[name='nature_formation_complement']").val($("#inscription" + modele_id + " input[name='nature_formation_complement']").val());
        //console.log($("#inscription" + modele_id + " input[name='financement']").val());
    }
    else
    {
        $("#entreprise" + user_id + " input[name='entreprise']").val("");
        $("#entreprise" + user_id + " input[name='entreprise_fonction']").val("");
        $("#entreprise" + user_id + " input[name='entreprise_service']").val("");
        $("#entreprise" + user_id + " input[name='entreprise_responsable']").val("");
        $("#entreprise" + user_id + " input[name='adresse']").val("");
        $("#entreprise" + user_id + " input[name='cp_ville']").val("");
        $("#entreprise" + user_id + " input[name='telephone']").val("");
        $("#inscription" + user_id + " input[name='financement']").val("");
        $("#inscription" + user_id + " input[name='financement_complement']").val("");
        $("#inscription" + user_id + " input[name='nature_formation']").val("");
        $("#inscription" + user_id + " input[name='nature_formation_complement']").val("");
    }   
    
});


// Créneaux

// Activer ou désactiver un créneau pour un client
$("div.creneau").click(switch_creneau);
function switch_creneau(e)
{
    creno_id = $(this).attr("data-id");
    session_id = $(this).closest(".tableau-creneau").attr("data-sessionid");
    objet_id = $(this).closest(".tableau-creneau").attr("data-objetid");
    objet = $(this).closest(".tableau-creneau").attr("data-objet");
    
    if (undefined === objet_id) return;
    
    var creno = $(this);
    
    // actif vaut 0 si this a la classe actif (ça veut dire que ce clic le désactive) et 1 dans le cas inverse
    actif = ($(this).hasClass("actif")) ? 0 : 1;
    
    jQuery.post
    (
        ajaxurl,
        {
            'action' : 'active_creneau',
            'creno_id' : creno_id,
            'session_id' : session_id,
            'objet_id' : objet_id,
            'objet' : objet,
            'actif' : actif,
        },
        function(response)
        {
            //console.log(response);
            creno.toggleClass("actif");
            
            // Mise à jour de pour_infos_box
            params = { 'object_class': objet, 'session_id': session_id, 'client_id': objet_id, 'user_id': objet_id };
            postprocess_func['update_pour_infos_client'](params);
        }
    );
}

// sous-traitance
$("#nb_st_heures").change(update_duree_st_session);
$("#nb_st_heures").blur(update_duree_st_session);
function update_duree_st_session(e)
{
    e.preventDefault();
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'update_duree_st_session',
            'session_id': $(this).attr('data-sessionid'),
            'nb_heures': $(this).val(),
        },
        function(response)
        {
            
        },
    );
}


$(".maj-nb-stagiaires").click(function(e)
{
    e.preventDefault();
    
    nb_stagiaires = $("#nb_st_stagiaires").val();
    nb_heures = $("#nb_st_heures").val();
    session_id = $(this).attr('data-sessionid');
    
    jQuery.post
    (
        ajaxurl,
        {
            'action' : 'get_st_stagiaires_list',
            'nb_stagiaires' : nb_stagiaires,
            'nb_heures' : nb_heures,
            'session_id': session_id,
        },
        function(response)
        {
            $("#liste-stagiaires").replaceWith(response);
            $("#liste-stagiaires button.valid-st-stagiaire").on('click', '', valid_st_stagiaire);
            $("#liste-stagiaires").find("input[name='stag_nom']").on('change', '', need_valid_st_stagiaire);
            $("#liste-stagiaires").find("input[name='stag_nom']").on('focus', '', focus_nom_st_stagiaire);
            $("#liste-stagiaires").find("input[name='stag_heure']").on('change', '', need_valid_st_stagiaire);
        }
    );
});

$("#liste-stagiaires input[type=text]").focus(focus_nom_st_stagiaire);
function focus_nom_st_stagiaire()
{
   $(this).select();
}
//$("#liste-stagiaires input").blur(need_valid_st_stagiaire);
$("#liste-stagiaires input[name='stag_heure']").change(need_valid_st_stagiaire);
$("#liste-stagiaires input[name='stag_nom']").change(need_valid_st_stagiaire);
function need_valid_st_stagiaire(e)
{
    tr = $(this).closest("tr");
    tr.find(".valid-st-stagiaire").show();
}

$(".valid-st-stagiaire").click(valid_st_stagiaire);
function valid_st_stagiaire(e)
{
    e.preventDefault();
    
    parent = $(this).closest("tr");
    
    jQuery.post
    (
        ajaxurl,
        {
            'action' : 'update_st_stagiaire',
            'user_id' : parent.find("input[name='stag_id']").val(),
            'session_id' : parent.find("input[name='stag_session_id']").val(),
            'nom' : parent.find("input[name='stag_nom']").val(),
            'heures' : parent.find("input[name='stag_heure']").val(),
        },
        function(response)
        {
            param = JSON.parse(response);
            parent.replaceWith(param.html);
            if (param.message != "")
                console.log(param.message);
        }
    );
}

    var datepicker_options =
    {
        dateFormat: 'dd/mm/yy',
        onClose: datepicker_close,
        numberOfMonths: 3,
    };
    
    $('.datepicker').datepicker(datepicker_options);
    
    $(".add-date").click(add_new_date);
    function add_new_date(e)
    {
        e.preventDefault();
        parent = $(this).parent(); // TODO : à virer
        tableau = $(this).closest(".tableau-creneau");
        session_id = tableau.attr("data-sessionid");
        
        var fields = new Object();
        decaljour = $(this).attr("data-decaljour");
        if (undefined != decaljour)
        {
            // calcul du décalage en millisecondes
            decaljourMS = 1000 * 60 * 60 * 24 * decaljour;
            
            // récupération du bloc date + liste créneaux sous forme d'un objet jquery'
            dateBaseLine = $(this).closest(".liste-creneau");

            // récupération date actuelle
            dateBaseText = dateBaseLine.find("input.datepicker").val();
            // calcul de la nouvelle date
            dateArray = dateBaseText.split('/');
            newDateObj = new Date(dateArray[2] + '-' + dateArray[1] + '-' + dateArray[0]);
            newDateObj.setTime(newDateObj.getTime() + decaljourMS);
            newDateText = newDateObj.toLocaleDateString('fr-FR');
            
            // vérification si il existe déjà une ligne avec cette date-là
            double_found = false;
            tableau.find(".datepicker").each(function()
            {
                if ($(this).val() == newDateText)
                {
                    double_found = true;
                    $(this).parent().addClass("bg-alerte").delay(700).queue(function(){ $(this).removeClass("bg-alerte").dequeue(); });
                }
            });
            
            // si la nouvelle date n'existe pas encore, on peut la créer'
            if (!double_found)
            {
                crenos = Array();
                dateBaseLine.find('.creneau').each(function() { crenos.push($(this).attr('data-id')); });
                params = 
                {
                    'session_id': $(".id.session").attr('data-id'),
                    'action': 'update_date',
                    'date': newDateText,
                    'creneaux': crenos,
                };
                jQuery.post
                ({
                    url: ajaxurl,
                    data: params,
                    success: function(response)
                    {
                        param = JSON.parse(response);
                        if (param.log != undefined)
                            console.log(param.log);
                        
                        if (param.html != undefined)
                        {
                            newDateLine = $(param.html);
                            postprocess_func['edit_date_add_events'](newDateLine);
                            // si decaljour vaut 1 on ajoute la nouvelle ligne sous la ligne copiée
                            // sinon, on l'ajoute à la fin. TODO : faire un vrai tri des dates'
                            if (decaljour == 1)
                                dateBaseLine.after(newDateLine);
                            else
                                $(".empty-date").before(newDateLine);
                            
                            // on fait clignoter en couleur succes
                            newDateLine.addClass("bg-succes").delay(700).queue(function(){ $(this).removeClass("bg-succes").dequeue(); });
                        }
                        //$(document).trigger('cpt.ready');
                    },
                });
            }
            
        }
        else
        {
            bouton = $(".empty-date");
            jQuery.post
            ({
                url: ajaxurl,
                data: { 'action': 'add_new_date_line', 'session_id': session_id, 'fields': fields },
                success: function(response)
                {
                    line = $(response);
                    postprocess_func['edit_date_add_events'](line);
                    bouton.before(line);
                    datepick = line.find(".datepicker");
                    datepick.trigger('focus');
                },
            });
        }
    }
    
    // Enregistrement effectif d'une date
    function datepicker_close(date, datepicker)
    {
        date_line = $(this).closest('.liste-creneau');
        old_date = date_line.attr('data-date');
//        old_date = date_line.find("input[name$='_date']").val();
        
        if (date.match(/\d\d\/\d\d\/\d\d\d\d/))
        {
            date_line.children('.bouton').show();
            if (date != old_date)
            {
                params = 
                {
                    'session_id': $(".id.session").attr('data-id'),
                    'action': 'update_date',
                    'date': date,
                    'old_date': old_date,
                };
                jQuery.post
                (
                    ajaxurl,
                    params,
                    function (response)
                    {
                        param = JSON.parse(response);
                        //console.log(param.log);
                        date_line.attr("data-date", date);
                        date_line.children('.icone-bouton').show();
                        
                        args = Object();
                        args.session_id = $(".id.session").attr('data-id');
                        postprocess_func['update_session_titre'](args);
                    },
                );
            }
        }
        else
        {
            if (date != old_date)
            {
                date_line.find('.del-date').trigger('click');
            }
            date_line.remove();
        }
    }
    
    // Suppression d'une date
    $("span.del-date").click(del_date);
    function del_date()
    {
        date_line = $(this).closest('.liste-creneau');
        params =
        {
            'session_id': $(".id.session").attr('data-id'),
            'action': 'del_date_creneau',
            'date': date_line.find('.datepicker').val(),
        };
        
        jQuery.post
        (
            ajaxurl,
            params,
            function (response)
            {
                param = JSON.parse(response);
                //console.log(param.log);
                date_line.remove();
                postprocess_func['tabs_reload'](Array());
                
                // mise à jour pour infos session
                /*
                arg = Object();
                arg.session_id = $(".id.session").attr('data-id');
                postprocess_func['update_pour_infos_session'](arg);
                */
            },
        );
    }
    
    // Suppression effective d'un créneau
    $("span.del-creneau").click(del_creneau);
    function del_creneau()
    {
        creno_div = $(this).closest(".creneau");
        date_line = $(this).closest('.liste-creneau');
        params =
        {
            'session_id': $(".id.session").attr('data-id'),
            'action': 'del_date_creneau',
            'date': date_line.find('.datepicker').val(),
            'creneau': creno_div.attr('data-id'),
        };
        
        jQuery.post
        (
            ajaxurl,
            params,
            function (response)
            {
                param = JSON.parse(response);
                //console.log(param.log);
                creno_div.remove();
                postprocess_func['tabs_reload'](Array());
                // mise à jour pour infos session
                /*
                arg = Object();
                arg.session_id = $(".id.session").attr('data-id');
                postprocess_func['update_pour_infos_session'](arg);
                */
            },
        );
    }
    

$("input.input_jpost_value").focus(function(e)
{
    $(this).removeClass('bord-succes');
});

$(".input_jpost input").change(change_jpost_value);
$(".input_jpost select").change(change_jpost_value);
$(".select_jpost select").change(change_jpost_value);
$(".input_jpost textarea").change(change_jpost_value);
function change_jpost_value(e)
{
    e.preventDefault();
    
    input = $(this);
    global_parent = $(this).closest("tr");
    local_parent = $(this).closest(".input_jpost");
    nodeName = $(this).prop("nodeName").toLowerCase();
    object_class = local_parent.find("input[name='object_class']").val();
    session_id = local_parent.find("input[name='session_id']").val();
    client_id = local_parent.find("input[name='client_id']").val();
    stagiaire_id = local_parent.find("input[name='stagiaire_id']").val();
    valid_icon = local_parent.find(".valid");
    display_valeur = local_parent.find(".valeur");
    postprocess = local_parent.find("input[name='postprocess']").val();
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'update_jpost_value',
            'object_class': object_class,
            'session_id': session_id,
            'client_id': client_id,
            'stagiaire_id': stagiaire_id,
            'meta': $(this).attr('name'),
            'value': $(this).val(),
            'postprocess': postprocess,
            'nodeName': nodeName,
        },
        function(response)
        {
            param = JSON.parse(response);
            valid_icon.show().delay(1000).fadeOut(1000);
            display_valeur.text(param.valeur);
            
            if (postprocess != undefined)
            {
                postprocess_args = Object();
                if (param.postprocess_args != undefined)
                    postprocess_args = param.postprocess_args;
                
                postprocess_args.meta_value = input.val();
                postprocess_args.meta_key = input.attr('name');
                
                // parcours de tableau sauce Javascript plutôt que jQuery
                postprocess.split('+').forEach(function(item)
                {
                    postprocess_func[item](postprocess_args);
                });
            }
            
            if (global_parent)
            {
                if (stagiaire_id > 0)
                {
                    session_tr = $("#session" + session_id);
                    $.each(param.session_formation, function(key, value)
                    {
                        session_tr.find("." + key).html(value);
                        session_tr.find("." + key + " div ." + nodeName + "_jpost_value").on('change', '', change_jpost_value);
                        //session_tr.find("." + key + " div input[type!='hidden']").addClass('bord-succes');
                        session_tr.find("." + key).addClass("bg-succes").delay(700).queue(function(){ $(this).removeClass("bg-succes").dequeue(); });
                    });
                    $.each(param.session_stagiaire, function(key, value)
                    {
                        global_parent.find("." + key).html(value);
                        global_parent.find("." + key + " div ." + nodeName + "_jpost_value").on('change', '', change_jpost_value);
                        //global_parent.find("." + key + " div input[type!='hidden']").addClass('bord-succes');
                        global_parent.find("." + key).addClass("bg-succes").delay(700).queue(function(){ $(this).removeClass("bg-succes").dequeue(); });
                    });
                }
                else
                {
                    $.each(param.session_formation, function(key, value)
                    {
                        global_parent.find("." + key).html(value);
                        // global_parent.find("." + key + " div input[type!='hidden']").addClass('bord-succes');
                        global_parent.find("." + key).addClass("bg-succes").delay(700).queue(function(){ $(this).removeClass("bg-succes").dequeue(); });
                        global_parent.find("." + key + " div ." + nodeName + "_jpost_value").on('change', '', change_jpost_value);
                    });
                }
            }
            
        },
    );
}

var postprocess_func =
{
    'edit_creneau_add_events': function(o)
        {
            o.off('click', '.dynamic-dialog', dynamic_dialog).on('click', '.dynamic-dialog', dynamic_dialog);
            o.off('click', '.del-creneau', del_creneau).on('click', '.del-creneau', del_creneau);
            postprocess_func['tabs_reload'](Array());
        },
    'edit_date_add_events': function(o)
        {
            o.on('click', '.del-date', del_date);
            o.on('focus', '.datepicker', function() { $(this).datepicker(datepicker_options); });
            o.on('click', '.add-date', add_new_date);
            //o.find(".add-date").hide();
            postprocess_func['edit_creneau_add_events'](o);
        },
    'update_client_nom': function(params)
        {
            if (params.client_id == undefined) return;
            $('a[href="#tab-c' + params.client_id + '"]').html(params.value);
        },
    'update_pour_infos_session': function(params)
        {
            params.object_class = 'SessionFormation';
            postprocess_func['update_pour_infos'](params);
        },
    'update_pour_infos_client': function(params)
        {
            params.object_class = 'Client';
            postprocess_func['update_pour_infos'](params);
        },
    'update_pour_infos_stagiaire': function(params)
        {
            params.object_class = 'SessionStagiaire';
            postprocess_func['update_pour_infos'](params);
        },
    'update_pour_infos': function(params)
        {
            if (params == null) return;
            params.action = 'update_pour_infos';
            
            jQuery.post
            (
                ajaxurl,
                params,
                function (response)
                {
                    param = JSON.parse(response);
                    if (param.html != undefined)
                        $(param.parent).find('.pour-infos').html(param.html);
                    else
                        console.log(param.erreur);
                },
            );
        },
    'update_session_titre': function(params)
        {
            console.log(params);
            if (params.session_id != undefined)
            {
                params.action = 'update_session_titre';
                jQuery.post
                (
                    ajaxurl,
                    params,
                    function (response)
                    {
                        param = JSON.parse(response);
                        if (param.titre != undefined)
                            $("h1.entry-title a").text(param.titre);
                        if (param.log != undefined)
                            console.log(param.log);
                    }
                );
            }
        },
    'toggle_lieu_details': function(params)
        {
            if (params.meta_value != undefined)
            {
                params.action = 'update_lieu_details';
                params.session_id = $(".id.session").attr('data-id');
                
                jQuery.post
                (
                    ajaxurl,
                    params,
                    function (response)
                    {
                        param = JSON.parse(response);
                        if (param.html != undefined)
                            $("." + params.meta_key + ".details").replaceWith(param.html);
                        if (param.log != undefined)
                            console.log(param.log);
                    },
                );
            }
        },
    'toggle_opco': function(params)
        {
            if (params.meta_key == "financement")
            {
                div = $(".client-" + params.client_id + " .input_jpost.opco");
                if (params.meta_value.substr(0, 4) == "mutu")
                    div.show();
                else
                    div.hide();
            }
        },
    'tabs_reload': function(params)
        {
            $(".ui-tabs ul").attr('data-reload', 1);
        },
};

$('.lieu.details input[name="lieu_ville"]').change(function(e)
{
    args = Object();
    args.session_id = $(".id.session").attr('data-id');
    postprocess_func['update_session_titre'](args);
});

$("input.input_check_jpost_value").change(change_check_jpost_value);
function change_check_jpost_value(e)
{
    e.preventDefault();
    input = $(this);
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'update_user_value',
            'checked': $(this).is(":checked"),
            'user_id': $(this).parent().children("input[name='user_id']").val(),
            'meta': $(this).attr('name'),
        },
        function (response)
        {
            console.log(response);
        },
    );
}

$("select.select_jpost_formation_value").change(change_jpost_value_formation);
function change_jpost_value_formation(e)
{
    e.preventDefault();
    input = $(this);
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'update_formation_value',
            'formation_id': $(this).parent().children("input[name='formation_id']").val(),
            'meta': $(this).attr('name'),
            'value': $(this).val(),
        },
        function (response)
        {
            console.log(response);
            input.addClass("bg-succes").delay(700).queue(function(){ $(this).removeClass("bg-succes").dequeue(); });
        },
    );
}

$(".dynamic-dialog").click(dynamic_dialog);
function dynamic_dialog(e)
{
    e.preventDefault();
    bouton = $(this);
    
    session_id = $(this).attr('data-sessionid');
    if (session_id == undefined)
        session_id = $(".id.session").attr("data-id");

    fonction = $(this).attr('data-function');
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'the_dynamic_dialog',
            'function': fonction,
            'session_id': session_id,
            'client_id': $(this).attr('data-clientid'),
            'stagiaire_id': $(this).attr('data-stagiaireid'),
            'creno_id': $(this).attr('data-crenoid'),
            'date': $(this).closest('.liste-creneau').attr('data-date'),
        },
        function(response)
        {
            dialog_box = $(response);
            dialog_box.dialog
            ({
                autoOpen: true,
                minWidth: 400,
                width: 'auto',
                modal: true,
                buttons:
                {
                    "Valider": function()
                    {
                        dialog_box = $(this);
                        form = dialog_box.find("form")[0];
                        formData = new FormData(form);
                        dialog_box.find("select[multiple='multiple']").each(function()
                        {
                            name = $(this).attr('name');
                            values = Array();
                            //formData.set($(this).attr('name'), JSON.stringify($(this)));
                            $(this).children("option:selected").each(function()
                            {
                                values.push($(this).val());
                            });
                            formData.set(name, values);
                        });
                        jQuery.post
                        ({
                            url: ajaxurl,
                            data: formData,
                            processData: false,
                            contentType: false,
                            success: function (response)
                            {
                                param = JSON.parse(response);
                                
                                if (param.url != undefined)
                                    document.location.assign(param.url);
                                
                                if (param.erreur != undefined)
                                    dialog_box.children(".message").html("<span class='erreur'>" + param.erreur + "</span>");
                                else if (param.succes != undefined)
                                    dialog_box.children(".message").html("<span class='succes'>" + param.succes + "</span>");
                                
                                if (param.html != undefined && param.html != "")
                                {
                                    bloc_html = $(param.html);
                                    if (param.eventsfunc != undefined)
                                        postprocess_func[param.eventsfunc](bloc_html);
                                        
                                    if (param.prependto != undefined)
                                    {
                                        prependto = bouton.closest(param.prependto);
                                        bloc_html.prependTo($(prependto));
                                    }

                                    if (param.appendto != undefined)
                                    {
                                        appendto = bouton.closest(param.appendto);
                                        bloc_html.appendTo($(appendto));
                                    }
                                    
                                    if (param.replace != undefined)
                                    {
                                        replace = bouton.closest(param.replace);
                                        $(replace).html(bloc_html);
                                    }
                                    
                                    if (param.replacewith != undefined)
                                    {
                                        replace = bouton.closest(param.replacewith);
                                        $(replace).replaceWith(bloc_html);
                                    }
                                }
                                if (formData.has('close_on_valid'))
                                {
                                    dialog_box.dialog("close");
                                    if (formData.has('reload_on_close'))
                                        document.location.reload();
                                }
                                else if (formData.has('refresh_on_success'))
                                {
                                    dialog_box.find("form input[type!='hidden']").val('');
                                    //form.find("input[type!='hidden']");
                                }
                            },
                            error: function (response)
                            {
                                console.log(response);
                            },
                        });
                    },
                    "Fermer": function()
                    {
                        $(this).dialog("close");
                    },
                },
                close:function()
                {
                    formData = new FormData($(this).find("form")[0]);
                    $(this).dialog("destroy");
                    if (formData != undefined && formData.has('reload_on_close'))
                        document.location.reload(true);
                },
            });
        }
    );
}

$(".delete-entity").click(delete_entity);
function delete_entity(e)
{
    e.preventDefault();
    
    id = $(this).attr('data-id');
    session_id = $(this).attr('data-sessionid');
    object_class = $(this).attr('data-objectclass')
    parent_to_kill = $(this).attr('data-parent');
    
    yesno = $("<div>La suppression est irréversible !</div>");
    yesno.dialog(
    {
        autoOpen: true,
        title: "Êtes-vous sûr ?",
        height: 200,
        width: 350,
        modal: true,
        buttons:
        {
            "Oui": function()
            {
                jQuery.post
                ({
                    url: ajaxurl,
                    data: { 'action': 'delete_entity', 'object_class': object_class, 'id': id, 'session_id': session_id },
                    success: function (response)
                    {
                        param = JSON.parse(response);
                        
                        if (param.succes != undefined)
                            $(parent_to_kill).remove();
                    },
                });
                yesno.dialog("destroy");
            },
            "Non": function() { supprim = false; yesno.dialog("destroy");},
        },
        close: function()
        {
            supprim = false; yesno.dialog("destroy");
        }
    });
    yesno.dialog("open");
}

/* Quiz fonctions */
$(".quiz-sortable").sortable({ stop: change_quiz_order, });
$(".quiz-sortable").disableSelection();

$(".quiz-sortable li input[type='text']").change(quiz_change_value);
function quiz_change_value(e)
{
    li = $(this).closest("li");
    if (li.attr('data-new') != undefined)
        li.removeAttr('data-new');
    $(this).closest(".quiz").find(".quiz-enregistrer").addClass("enregistrement-requis");
}

function change_quiz_order(e, ui)
{
    $(this).closest(".quiz").find(".quiz-enregistrer").addClass("enregistrement-requis");
}

$(".quiz-enregistrer").click(function (e)
{
    bouton = $(this);
    parent_id = $(".id").attr('data-id');
    quiz_id = $(this).closest('.quiz').attr('data-id');
    sujet = $(this).closest('.quiz').attr('data-sujet');
    questions = Array();
    sortable = $(this).closest('.quiz').find("ul.quiz-sortable");
    sortable.children().each(function()
    {
        q = Object();
        q.type = $(this).attr('data-type');
        q.text = $(this).find("input[type='text']").val();
        questions.push(q);
    });
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'quiz_enregistrer',
            'quiz_id': quiz_id,
            'sujet': sujet,
            'parent_id': parent_id,
            'questions': questions,
        },
        function (response)
        {
            param = JSON.parse(response);
            console.log(param.log);
            bouton.removeClass("enregistrement-requis");
            bouton.closest('.quiz').attr('data-id', param.quiz_id);
        }
    );
});

// Ajouter une ligne
$(".quiz-ajouter").click(function (e)
{
    e.preventDefault();
    
    type = $(this).attr('data-type');
    list = $(this).closest('.quiz').find("ul.quiz-sortable");
    quiz_id = $(this).closest('.quiz').attr('data-id');
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'quiz_add_line',
            'type': type,
            'quiz_id': quiz_id,
        },
        function(response)
        {
            param = JSON.parse(response);
            if (param.log != undefined)
                console.log(param.log);
            if (param.html != undefined)
            {
                list.append(param.html);
                list.on('click', '.quiz-supprimer', quiz_supprimer);
                list.on('change', 'input[type="text"]', quiz_change_value);
            }
        },
    );
});

// Supprimer un ligne
$(".quiz-supprimer").click(quiz_supprimer);
function quiz_supprimer(e)
{
    e.preventDefault();
    
    li = $(this).closest("li");
    if (li.attr('data-new') == undefined)
        $(this).closest(".quiz").find(".quiz-enregistrer").addClass("enregistrement-requis");
    li.remove();
}

$("div.exe_comptable input").change(change_exe_comptable);
function change_exe_comptable(e)
{
    e.preventDefault();
    infosession = $(this).closest(".infosession");
    div_exe_comptable = $(this).parent();
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'update_exe_comptable',
            'session_id': infosession.attr("data-sessionid"),
            'stagiaire_id': infosession.attr("data-stagiaireid"),
            'annee': $(this).attr('name'),
            'tarif': $(this).val(),
        },
        function(response)
        {
            param = JSON.parse(response);
            infosession.find(".message").html(param.message);
            new_div = $(param.inputs);
            new_div.children('input').on('change', '', change_exe_comptable);
            div_exe_comptable.replaceWith(new_div);
        }
    );
}

$("select[name='annee_choix']").change(change_annee_comptable);
function change_annee_comptable(e)
{
    e.preventDefault();
    var fields = new Object();
    fields['annee_comptable'] = $(this).val();
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'enregistrer_user_input',
            'fields': fields,
            'user_id': $(this).attr('data-userid'),
        },
        function(response)
        {
            document.location.reload();
        }
    );
}

// (Dés)Active le mode fullscreen d'un bloc
$(".fullscreen-mode").click(fullscreen_mode);
function fullscreen_mode(e)
{
    bloc = $($(this).attr('data-id'));
    if (bloc.hasClass("fullscreen"))
    {
        bloc.removeClass("fullscreen");
        $(this).removeClass("fait");
        $("#wpadminbar").removeClass("blocHidden");
        $("footer").removeClass("blocHidden");
        bloc.appendTo($("div.entry-content"));
        tmp.remove();
    }
    else
    {
        bloc.addClass("fullscreen");
        $(this).addClass("fait");
        $("#wpadminbar").addClass("blocHidden");
        $("footer").addClass("blocHidden");
        tmp = $("<div id='tmp'></div>");
        bloc.appendTo(tmp);
        tmp.prependTo($('body.pilote'));
        //bloc.html("");
    }
}

// (Dés)Active le mode édition d'un bloc
$(".edition-mode").click(edition_mode);
function edition_mode(e)
{
    bloc = $($(this).attr('data-id'));
    if (bloc.hasClass("edit-data"))
    {
        bloc.removeClass("edit-data");
        $(this).removeClass("fait");
    }
    else
    {
        bloc.addClass("edit-data");
        $(this).addClass("fait");
    }
}

$('#profile-box').mouseover(function()
{
    $(this).css('z-index', 20);
});

$('#profile-box').mouseout(function()
{
    $(this).css('z-index', '');
});

/* Fonctions pour le pilote de sessions */
$('.toggle-stagiaires').click(function(e)
{
    $('#' + $(this).attr('data-id')).toggle();
});

});

