<?php
/*
 * wpof-bpf.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with wpof program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

require_once(wpof_path . "/class/class-wpof.php");
require_once(wpof_path . "/wpof-formation.php");
require_once(wpof_path . "/class/class-termlist.php");

$wpof = new WPOF();

// Pages spéciales où le thème de WordPress ne doit pas s'appliquer
$wpof->no_theme = array($wpof->url_bpf, $wpof->url_pilote);

// Type de session de formation (TODO : à virer)
$wpof->type_session = array
(
    'inter' => array('value' => __('Inter-entreprises')),
    'intra' => array('value' => __('Intra-entreprise')),
    'sous_traitance' => array('value' => __('En sous-traitance pour un autre OF'))
);
// Visibilité de la session (ancienne méthode TODO : à virer)
$wpof->visibilite_session = array
(
    'public' => array('value' => __('Publique')),
    'connecte' => array('value' => __("Nécessite d'être connecté")),
    'invite' => array('value' => __('Sur invitation')),
);

// Accès à la session (ex-Visibilité de la session) via un TermList
$wpof->acces_session = new TermList("acces_session");
$wpof->acces_session->add_term('public', __('Publique'));
$wpof->acces_session->add_term('invite', __('Sur invitation'));
$wpof->acces_session->add_term('connecte',__("Nécessite d'être connecté"));

/*
 * Types de créneau
 * correspondent aux différentes aspects d'un parcours de formation
 */
$wpof->type_creneau = array
(
    'presentiel' => 'Présentiel',
    'foad_sync' => 'À distance, synchrone',
    'foad_async' => 'À distance, asynchrone',
    'afest' => 'AFEST',
);

/*
 * Liste des champs servant à décrire précisement une formation ou une session
 */
$wpof->desc_formation = new TermList("desc_formation");
$wpof->desc_formation->add_term("presentation", "Présentation générale");
$wpof->desc_formation->add_term("objectifs_pro", "Objectifs professionnels");
$wpof->desc_formation->add_term("objectifs", "Objectifs pédagogiques");
$wpof->desc_formation->add_term("prerequis", "Pré-requis");
$wpof->desc_formation->add_term("public_cible", "Public cible");
$wpof->desc_formation->add_term("modalites_pedagogiques", "Modalités pédagogiques");
$wpof->desc_formation->add_term("ressources", "Ressources pédagogiques");
$wpof->desc_formation->add_term("organisation", "Modalités d'organisation");
$wpof->desc_formation->add_term("materiel_pedagogique", "Matériel pédagogique");
$wpof->desc_formation->add_term("accessibilite", "Accessibilité");
$wpof->desc_formation->add_term("modalites_evaluation", "Modalités d'évaluation");
$wpof->desc_formation->add_term("inscription_delai", "Modalités d'inscription et délai d'accès");
$wpof->desc_formation->add_term("programme", "Programme");

/*
 * Liste des champs servant à décrire précisement un lieu
 */
$wpof->desc_lieu = new TermList("desc_lieu");
$wpof->desc_lieu->add_term("nom", "Nom", array('type' => 'text'));
$wpof->desc_lieu->add_term("adresse", "Adresse", array('type' => 'textarea'));
$wpof->desc_lieu->add_term("code_postal", "Code postal", array('type' => 'text'));
$wpof->desc_lieu->add_term("ville", "Ville", array('type' => 'text'));
$wpof->desc_lieu->add_term("localisation", "Localisation (champ libre)", array('type' => 'editor'));
$wpof->desc_lieu->add_term("secu_erp", "PV de sécurité pour ERP", array('type' => 'image'));

/*
 * Documents administratifs
 */
// Le contexte définit dans quelle(s) partie(s) de l'interface apparaît le document
$wpof->doc_context = new stdClass();
$wpof->doc_context->direct = 0x1;
$wpof->doc_context->sous_traitance = 0x2;
$wpof->doc_context->contrat = $wpof->doc_context->direct | $wpof->doc_context->sous_traitance;
$wpof->doc_context->session = 0x4;
$wpof->doc_context->client = 0x8;
$wpof->doc_context->stagiaire = 0x10;
$wpof->doc_context->formateur = 0x20;
$wpof->doc_context->entite = $wpof->doc_context->session | $wpof->doc_context->client | $wpof->doc_context->stagiaire | $wpof->doc_context->formateur;

$wpof->doc_signature = new stdClass();
$wpof->doc_signature->stagiaire = Document::VALID_STAGIAIRE_NEED;
$wpof->doc_signature->client = Document::VALID_CLIENT_NEED;
$wpof->doc_signature->responsable = Document::VALID_RESPONSABLE_NEED;

$wpof->documents = new TermList("documents");
$wpof->documents->add_term('proposition', __("Proposition de formation : programme détaillé"),
    array('contexte' => $wpof->doc_context->direct | $wpof->doc_context->sous_traitance | $wpof->doc_context->session, 'signature' => 0)
    );
$wpof->documents->add_term('convention', __("Convention de formation professionnelle"),
    array('contexte' => $wpof->doc_context->direct | $wpof->doc_context->client, 'signature' => $wpof->doc_signature->responsable | $wpof->doc_signature->client)
    );
$wpof->documents->add_term('emargement', __("Feuille d'émargement"),
    array('contexte' => $wpof->doc_context->direct | $wpof->doc_context->session, 'signature' => $wpof->doc_signature->stagiaire)
    );
$wpof->documents->add_term('reglement_interieur', __("Réglement intérieur"),
    array('contexte' => $wpof->doc_context->direct | $wpof->doc_context->stagiaire, 'signature' => $wpof->doc_signature->stagiaire)
    );
$wpof->documents->add_term('attestation_formation', __("Attestation de suivi de formation"),
    array('contexte' => $wpof->doc_context->direct | $wpof->doc_context->stagiaire, 'signature' => $wpof->doc_signature->responsable)
    );
$wpof->documents->add_term('certificat_realisation', __("Certificat de réalisation"),
    array('contexte' => $wpof->doc_context->direct | $wpof->doc_context->stagiaire, 'signature' => $wpof->doc_signature->responsable)
    );
$wpof->documents->add_term('accord_comm', __("Accord de communication sur le partenariat"),
    array('contexte' => $wpof->doc_context->direct | $wpof->doc_context->client, 'signature' => $wpof->doc_signature->responsable | $wpof->doc_signature->client)
    );
$wpof->documents->add_term('eval_formation', __("Évaluation de la formation"),
    array('contexte' => $wpof->doc_context->direct | $wpof->doc_context->stagiaire, 'signature' => 0)
    );
$wpof->documents->add_term('quiz_connaissances', __("Évaluation des compétences sur les objectifs"),
    array('contexte' => $wpof->doc_context->direct | $wpof->doc_context->stagiaire, 'signature' => 0)
    );
$wpof->documents->add_term('pv_secu', __("PV de sécurité pour les ERP"),
    array('contexte' => $wpof->doc_context->direct | $wpof->doc_context->client, 'signature' => 0)
    );

/*
 * Pages virtuelles
 */
// URL et titre de la page BPF (par défaut)
if (!isset($wpof->url_bpf)) $wpof->url_bpf = "bpf";
if (!isset($wpof->title_bpf)) $wpof->title_bpf = __("Bilan pédagogique et financier");
// URL et titre de la page BPF (par défaut)
if (!isset($wpof->url_pilote)) $wpof->url_pilote = "pilote";
if (!isset($wpof->title_pilote)) $wpof->title_pilote = __("Pilote des sessions");

// Première année d'activité gérée ici
/*if (!isset($wpof->annee1))
{
    $sessions = get_formation_sessions(array('sort' => 'ASC'));
    if (empty($sessions))
        $wpof->annee1 = date('Y');
    else
    {
        $first_session = reset($sessions);
        $wpof->annee1 = preg_replace('/[0-9]+\/[0-9]+\//', '', $first_session->dates_array[0]);
    }
    update_option("wpof_annee1", $wpof->annee1);
}
*/
// tables supplémentaires de la base de données
$suffix_session_stagiaire = "wpof_session_stagiaire";
$suffix_client = "wpof_client";
$suffix_documents = "wpof_documents";
$suffix_quiz = "wpof_quiz";
$suffix_creneaux = "wpof_creneaux";

/*
 * Éditeur spécifique pour les wp_editor
 */
$tinymce_wpof_settings = array
(
    'tinymce' => array
    (
        'language' => 'fr',
        'spellchecker_languages' => 'French=fr',
        'paste_as_text' => true,
        'toolbar1' => 'formatselect,bold,italic,|,bullist,numlist,|,blockquote,alignleft,aligncenter,alignright,|,link,unlink,|,pastetext,removeformat,charmap,|,outdent,indent,|,undo,redo,spellchecker',
        'toolbar2' => '',
        //'content_css' => wpof_url . "/css/wp-editor.css",
    ),
    'drag_drop_upload' => true,
    'editor_height' => 180,
    'quicktags' => false,
);

/*
 * Éditeur spécifique pour rédiger les questionnaires
 */
$tinymce_wpof_quiz_settings = array
(
    'tinymce' => array
    (
        'language' => 'fr',
        'spellchecker_languages' => 'French=fr',
        'paste_as_text' => true,
        'media_buttons' => false,
        'quicktags' => false,
        'toolbar1' => 'formatselect,bold,italic',
        'toolbar2' => '',
        'dfw' => false,
        //'content_css' => wpof_url . "/css/wp-editor.css",
    ),
    'editor_height' => 180,
    'quicktags' => false,
);

/*
 * Options légales => BPF
 * Pas de localisation, ce sont les termes officiels
 */

// Types de financement
$wpof->financement = new TermList("financement");
$wpof->financement->add_group("g1", "Organismes paritaires collecteurs ou gestionnaires des fonds de la formation");
$wpof->financement->add_group("g2", "Pouvoirs publics pour la formation de publics spécifiques");
$wpof->financement->add_term("prive", "Entreprise pour la formation de ses salariés");
$wpof->financement->add_term("mutu1", "contrat d'apprentissage", array('group' => 'g1'));
$wpof->financement->add_term("mutu2", "contrat de professionnalisation", array('group' => 'g1'));
$wpof->financement->add_term("mutu3", "promotion ou reconversion en alternance", array('group' => 'g1'));
$wpof->financement->add_term("mutu4", "congé individuel de formation ou projet de transition professionnelle", array('group' => 'g1'));
$wpof->financement->add_term("mutu5", "compte personnel de formation", array('group' => 'g1'));
$wpof->financement->add_term("mutu6", "dispositif spécifique pour les personnes en recherche d'emploi", array('group' => 'g1'));
$wpof->financement->add_term("mutu7", "dispositif spécifique pour les travailleurs non-salariés", array('group' => 'g1'));
$wpof->financement->add_term("mutu8", "plan de développement des compétences ou autres dispositifs", array('group' => 'g1'));
$wpof->financement->add_term("assur", "Fonds d'assurance");
$wpof->financement->add_term("public", "Pouvoirs publics pour la formation de leurs agents (État, collectivités territoriales, établissements publics à caractère administratif)");
$wpof->financement->add_term("pubspec1", "Instances européennes", array('group' => 'g2'));
$wpof->financement->add_term("pubspec2", "État", array('group' => 'g2'));
$wpof->financement->add_term("pubspec3", "Conseils régionaux", array('group' => 'g2'));
$wpof->financement->add_term("pubspec4", "Pôle emploi", array('group' => 'g2'));
$wpof->financement->add_term("pubspec5", "Autres ressources publiques", array('group' => 'g2'));
$wpof->financement->add_term("part", "Contrats conclus avec des personnes à titre individuel et à leurs frais");
$wpof->financement->add_term("opac", "Contrats conclus avec d’autres organismes de formation");
$wpof->financement->add_term("autres", "Autres produits au titre de la formation professionnelle continue");

// Nature de la formation (ou objectif de la prestation)
$wpof->nature_formation = new TermList("nature_formation");
$wpof->nature_formation->add_group("g1", "diplôme ou titre à finalité professionnelle (hors CQP) inscrit au RNCP");
$wpof->nature_formation->add_term("form", "Autre formation professionnelle continue");
$wpof->nature_formation->add_term("bilan", "Bilan de compétences");
$wpof->nature_formation->add_term("vae", "Actions d'accompagnement à la validation des acquis d'expérience");
$wpof->nature_formation->add_term("rs", "certification (dont CQP) ou habilitation enregistrée au répertoire spécifique (RS)");
$wpof->nature_formation->add_term("nors", "CQP non enregistré au RNC ou au RS");
$wpof->nature_formation->add_term("dip1", "Niveau 6 à 8 (licence, master, diplôme d’ingénieur, doctorat)", array('group' => 'g1'));
$wpof->nature_formation->add_term("dip2", "Niveau 5 (BTS, DUT, écoles de formation sanitaire et sociale…)", array('group' => 'g1'));
$wpof->nature_formation->add_term("dip3", "Niveau 4 (BAC professionnel, BT, BP, BM…)", array('group' => 'g1'));
$wpof->nature_formation->add_term("dip4", "Niveau 3 (BEP, CAP,…)", array('group' => 'g1'));
$wpof->nature_formation->add_term("dip5", "Niveau 2", array('group' => 'g1'));
$wpof->nature_formation->add_term("dip6", "certificat de qualification professionnelle (CQP) sans niveau de qualification", array('group' => 'g1'));

// Types de stagiaires
$wpof->statut_stagiaire = new TermList("statut_stagiaire");
$wpof->statut_stagiaire->add_term("sal_prive", "Salarié d'employeur privé hors apprentis");
$wpof->statut_stagiaire->add_term("apprenti", "Apprenti");
$wpof->statut_stagiaire->add_term("rech_emploi", "Personne en recherche d'emploi formée par votre organisme de formation");
$wpof->statut_stagiaire->add_term("particulier", "Particulier à ses propres frais formé par votre organisme de formation");
$wpof->statut_stagiaire->add_term("autre", "Autres stagiaires");

// OPCO
$wpof->opco = new TermList("opco");
$wpof->opco->add_term("afdas", "AFDAS");
$wpof->opco->add_term("atlas", "ATLAS");
$wpof->opco->add_term("ocapiat", "OCAPIAT");
$wpof->opco->add_term("2i", "OPCO 2i");
$wpof->opco->add_term("construction", "OPCO de la Construction");
$wpof->opco->add_term("ep", "OPCO des entreprises de proximité (EP)");
$wpof->opco->add_term("akto", "AKTO / ESSFIMO");
$wpof->opco->add_term("mobilites", "OPCO Mobilités");
$wpof->opco->add_term("commerce", "OPCOmmerce");
$wpof->opco->add_term("sante", "OPCO Santé");
$wpof->opco->add_term("uniformation", "Uniformation – OPCO de la Cohésion sociale");

// Spécialités de formation
$wpof->specialite = new TermList("specialite");
$wpof->specialite->add_group("100", "Formations générales");
$wpof->specialite->add_term("100", "100 – Formations générales", array('group' => "100"));
$wpof->specialite->add_group("110", "Spécialités pluriscientifiques");
$wpof->specialite->add_term("110", "110 – Spécialités pluriscientifiques", array('group' => "110"));
$wpof->specialite->add_term("111", "111 – Physique-chimie", array('group' => "110"));
$wpof->specialite->add_term("112", "112 – Chimie-biologie, biochimie", array('group' => "110"));
$wpof->specialite->add_term("113", "113 – Sciences naturelles (biologie-géologie)", array('group' => "110"));
$wpof->specialite->add_term("114", "114 – Mathématiques", array('group' => "110"));
$wpof->specialite->add_term("115", "115 – Physique", array('group' => "110"));
$wpof->specialite->add_term("116", "116 – Chimie", array('group' => "110"));
$wpof->specialite->add_term("117", "117 – Sciences de la terre", array('group' => "110"));
$wpof->specialite->add_term("118", "118 – Sciences de la vie", array('group' => "110"));
$wpof->specialite->add_group("120", "Spécialités pluridisciplinaires, sciences humaines et droit");
$wpof->specialite->add_term("120", "120 – Spécialités pluridisciplinaires, sciences humaines et droit", array('group' => "120"));
$wpof->specialite->add_term("121", "121 – Géographie", array('group' => "120"));
$wpof->specialite->add_term("122", "122 – Economie", array('group' => "120"));
$wpof->specialite->add_term("123", "123 – Sciences sociales (y compris démographie, anthropologie)", array('group' => "120"));
$wpof->specialite->add_term("124", "124 – Psychologie", array('group' => "120"));
$wpof->specialite->add_term("125", "125 – Linguistique", array('group' => "120"));
$wpof->specialite->add_term("126", "126 – Histoire", array('group' => "120"));
$wpof->specialite->add_term("127", "127 – Philosophie, éthique et théologie", array('group' => "120"));
$wpof->specialite->add_term("128", "128 – Droit, sciences politiques", array('group' => "120"));
$wpof->specialite->add_group("130", "Spécialités littéraires et artistiques plurivalentes");
$wpof->specialite->add_term("130", "130 – Spécialités littéraires et artistiques plurivalentes", array('group' => "130"));
$wpof->specialite->add_term("131", "131 – Français, littérature et civilisation française", array('group' => "130"));
$wpof->specialite->add_term("132", "132 – Arts plastiques", array('group' => "130"));
$wpof->specialite->add_term("133", "133 – Musique, arts du spectacle", array('group' => "130"));
$wpof->specialite->add_term("134", "134 – Autres disciplines artistiques et spécialités artistiques plurivalentes", array('group' => "130"));
$wpof->specialite->add_term("135", "135 – Langues et civilisations anciennes", array('group' => "130"));
$wpof->specialite->add_term("136", "136 – Langues vivantes, civilisations étrangères et régionales", array('group' => "130"));
$wpof->specialite->add_group("200", "Technologies industrielles fondamentales (génie industriel, procédés de transformation, spécialités à dominante fonctionnelle)");
$wpof->specialite->add_term("200", "200 – Technologies industrielles fondamentales (génie industriel, procédés de transformation, spécialités à dominante fonctionnelle)", array('group' => "200"));
$wpof->specialite->add_term("201", "201 – Technologies de commandes des transformations industriels (automatismes et robotique industriels, informatique industrielle)", array('group' => "200"));
$wpof->specialite->add_group("210", "Spécialités plurivalentes de l'agronomie et de l'agriculture");
$wpof->specialite->add_term("210", "210 – Spécialités plurivalentes de l'agronomie et de l'agriculture", array('group' => "210"));
$wpof->specialite->add_term("211", "211 – Productions végétales, cultures spécialisées (horticulture, viticulture, arboriculture fruitière...)", array('group' => "210"));
$wpof->specialite->add_term("212", "212 – Productions animales, élevage spécialisé, aquaculture, soins aux animaux, y compris vétérinaire", array('group' => "210"));
$wpof->specialite->add_term("213", "213 – Forêts, espaces naturels, faune sauvage, pêche", array('group' => "210"));
$wpof->specialite->add_term("214", "214 – Aménagement paysager (parcs, jardins, espaces verts ...)", array('group' => "210"));
$wpof->specialite->add_group("220", "Spécialités pluritechnologiques des transformations");
$wpof->specialite->add_term("220", "220 – Spécialités pluritechnologiques des transformations", array('group' => "220"));
$wpof->specialite->add_term("221", "221 – Agro-alimentaire, alimentation, cuisine", array('group' => "220"));
$wpof->specialite->add_term("222", "222 – Transformations chimiques et apparentées (y compris industrie pharmaceutique)", array('group' => "220"));
$wpof->specialite->add_term("223", "223 – Métallurgie (y compris sidérurgie, fonderie, non ferreux...)", array('group' => "220"));
$wpof->specialite->add_term("224", "224 – Matériaux de construction, verre, céramique", array('group' => "220"));
$wpof->specialite->add_term("225", "225 – Plasturgie, matériaux composites", array('group' => "220"));
$wpof->specialite->add_term("226", "226 – Papier, carton", array('group' => "220"));
$wpof->specialite->add_term("227", "227 – Energie, génie climatique (y compris énergie nucléaire, thermique, hydraulique ; utilités : froid, climatisation, chauffage)", array('group' => "220"));
$wpof->specialite->add_group("230", "Spécialités pluritechnologiques, génie civil, construction, bois");
$wpof->specialite->add_term("230", "230 – Spécialités pluritechnologiques, génie civil, construction, bois", array('group' => "230"));
$wpof->specialite->add_term("231", "231 – Mines et carrières, génie civil, topographie", array('group' => "230"));
$wpof->specialite->add_term("232", "232 – Bâtiment : construction et couverture", array('group' => "230"));
$wpof->specialite->add_term("233", "233 – Bâtiment : finitions", array('group' => "230"));
$wpof->specialite->add_term("234", "234 – Travail du bois et de l'ameublement", array('group' => "230"));
$wpof->specialite->add_group("240", "Spécialités pluritechnologiques matériaux souples");
$wpof->specialite->add_term("240", "240 – Spécialités pluritechnologiques matériaux souples", array('group' => "240"));
$wpof->specialite->add_term("241", "241 – Textile", array('group' => "240"));
$wpof->specialite->add_term("242", "242 – Habillement (y compris mode, couture)", array('group' => "240"));
$wpof->specialite->add_term("243", "243 – Cuirs et peaux", array('group' => "240"));
$wpof->specialite->add_group("250", "Spécialités pluritechnologiques mécanique-électricité (y compris maintenance mécano-électrique)");
$wpof->specialite->add_term("250", "250 – Spécialités pluritechnologiques mécanique-électricité (y compris maintenance mécano-électrique)", array('group' => "250"));
$wpof->specialite->add_term("251", "251 – Mécanique générale et de précision, usinage", array('group' => "250"));
$wpof->specialite->add_term("252", "252 – Moteurs et mécanique auto", array('group' => "250"));
$wpof->specialite->add_term("253", "253 – Mécanique aéronautique et spatiale", array('group' => "250"));
$wpof->specialite->add_term("254", "254 – Structures métalliques (y compris soudure, carrosserie, coque bateau, cellule avion)", array('group' => "250"));
$wpof->specialite->add_term("255", "255 – Electricité, électronique (non compris automatismes, productique)", array('group' => "250"));
$wpof->specialite->add_group("300", "Spécialités plurivalentes des services");
$wpof->specialite->add_term("300", "300 – Spécialités plurivalentes des services", array('group' => "300"));
$wpof->specialite->add_group("310", "Spécialités plurivalentes des échanges et de la gestion (y compris administration générale des entreprises et des collectivités)");
$wpof->specialite->add_term("310", "310 – Spécialités plurivalentes des échanges et de la gestion (y compris administration générale des entreprises et des collectivités)" ,array('group' => "310"));
$wpof->specialite->add_term("311", "311 – Transports, manutention, magasinage", array('group' => "310"));
$wpof->specialite->add_term("312", "312 – Commerce, vente", array('group' => "310"));
$wpof->specialite->add_term("313", "313 – Finances, banque, assurances", array('group' => "310"));
$wpof->specialite->add_term("314", "314 – Comptabilité, gestion", array('group' => "310"));
$wpof->specialite->add_term("315", "315 – Ressources humaines, gestion du personnel, gestion de l'emploi", array('group' => "310"));
$wpof->specialite->add_group("320", "Spécialités plurivalentes de la communication");
$wpof->specialite->add_term("320", "320 – Spécialités plurivalentes de la communication", array('group' => "320"));
$wpof->specialite->add_term("321", "321 – Journalisme, communication (y compris communication graphique et publicité)", array('group' => "320"));
$wpof->specialite->add_term("322", "322 – Techniques de l'imprimerie et de l'édition", array('group' => "320"));
$wpof->specialite->add_term("323", "323 – Techniques de l'image et du son, métiers connexes du spectacle", array('group' => "320"));
$wpof->specialite->add_term("324", "324 – Secrétariat, bureautique", array('group' => "320"));
$wpof->specialite->add_term("325", "325 – Documentation, bibliothèques, administration des données", array('group' => "320"));
$wpof->specialite->add_term("326", "326 – Informatique, traitement de l'information, réseaux de transmission des données", array('group' => "320"));
$wpof->specialite->add_group("330", "Spécialités plurivalentes sanitaires et sociales");
$wpof->specialite->add_term("330", "330 – Spécialités plurivalentes sanitaires et sociales", array('group' => "330"));
$wpof->specialite->add_term("331", "331 – Santé", array('group' => "330"));
$wpof->specialite->add_term("332", "332 – Travail social", array('group' => "330"));
$wpof->specialite->add_term("333", "333 – Enseignement, formation", array('group' => "330"));
$wpof->specialite->add_term("334", "334 – Accueil, hôtellerie, tourisme", array('group' => "330"));
$wpof->specialite->add_term("335", "335 – Animation culturelle, sportive et de loisirs", array('group' => "330"));
$wpof->specialite->add_term("336", "336 – Coiffure, esthétique et autres spécialités des services aux personnes", array('group' => "330"));
$wpof->specialite->add_group("340", "Spécialités plurivalentes des services à la collectivité");
$wpof->specialite->add_term("340", "340 - Spécialités plurivalentes des services à la collectivité", array('group' => "340"));
$wpof->specialite->add_term("341", "341 – Aménagement du territoire, développement, urbanisme", array('group' => "340"));
$wpof->specialite->add_term("342", "342 – Protection et développement du patrimoine", array('group' => "340"));
$wpof->specialite->add_term("343", "343 – Nettoyage, assainissement, protection de l'environnement", array('group' => "340"));
$wpof->specialite->add_term("344", "344 – Sécurité des biens et des personnes, police, surveillance (y compris hygiène et sécurité)", array('group' => "340"));
$wpof->specialite->add_term("345", "345 – Application des droits et statut des personnes", array('group' => "340"));
$wpof->specialite->add_term("346", "346 – Spécialités militaires", array('group' => "340"));
$wpof->specialite->add_group("410", "Spécialités concernant plusieurs capacités");
$wpof->specialite->add_term("410", "410 – Spécialités concernant plusieurs capacités", array('group' => "410"));
$wpof->specialite->add_term("411", "411 – Pratiques sportives (y compris : arts martiaux)", array('group' => "410"));
$wpof->specialite->add_term("412", "412 – Développement des capacités mentales et apprentissages de base", array('group' => "410"));
$wpof->specialite->add_term("413", "413 – Développement des capacités comportementales et relationnelles", array('group' => "410"));
$wpof->specialite->add_term("414", "414 – Développement des capacités individuelles d'organisation", array('group' => "410"));
$wpof->specialite->add_term("415", "415 – Développement des capacités d'orientation, d'insertion ou de réinsertion sociales et professionnelles", array('group' => "410"));
$wpof->specialite->add_term("421", "421 – Jeux et activités spécifiques de loisirs", array('group' => "410"));
$wpof->specialite->add_term("422", "422 – Economie et activités domestiques", array('group' => "410"));
$wpof->specialite->add_term("423", "423 – Vie familiale, vie sociale et autres formations au développement personnel", array('group' => "410"));

?>
