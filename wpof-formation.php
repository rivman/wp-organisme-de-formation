<?php
/*
 * wpof-formation.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


function show_liste_formation( $atts )
{
    global $wpof;
    $role = wpof_get_role(get_current_user_id());
    
    ob_start();
    // Attributes
    $atts = shortcode_atts(
        array
            (
            'cat' => 'all',
            'tag' => 'all',
            'h2' => null,
            'h3' => null,
            'h4' => null,
            ),
	$atts
	);
	
    $formations = get_formations();
    
    ?>
    <div id="liste-formation">
    <?php if (isset($atts['h2'])) echo "<h2>".$atts['h2']."</h2>"; ?>
    <?php if (isset($atts['h3'])) echo "<h3>".$atts['h3']."</h3>"; ?>
    <?php if (isset($atts['h4'])) echo "<h4>".$atts['h4']."</h4>"; ?>
    
    <?php if ($formations) : ?>
    
    <ul>
    <?php
    foreach($formations as $f)
    {
        ?>
        <li><a href="<?php echo $f->permalien; ?>"><?php echo $f->titre; ?></a></li>
        <?php
            if (in_array($role, array("admin", "um_responsable")))
            {
                echo $f->get_select_jpost($wpof->specialite, "specialite", $f->specialite);
            }
    }
    ?>
    </ul>
    
    <?php else:
        _e("Aucune formation définie au catalogue");
    endif;
    ?>
    
    </div>
    <?php
    
    return ob_get_clean();
    //debug_info($formations, "formations");
}
add_shortcode( 'liste_formation', 'show_liste_formation' );


function get_formations($atts = array())
{
    $html_output = isset($atts['format']) && $atts['format'] == 'html';
    
    if (isset($atts['formation_id']))
        $formation_posts[0] = get_post($atts['formation_id']);
    else
        $formation_posts = get_posts(array( 'post_type' => 'formation', 'posts_per_page' => -1));
    
    $formations = array();
    
    foreach ($formation_posts as $fp)
    {
        $fo = new Formation($fp->ID);
        
        if (!isset($atts['formateur']) || (isset($atts['formateur']) && in_array($atts['formateur'], $fo->formateur)))
        {
            if ($html_output)
                $formations[] = "<li><a href='{$fo->permalien}'>{$fo->titre}</a></li>";
            else
                $formations[] = $fo;
        }
    }
    
    if (count($formations) == 0)
        return null;
        
    if ($html_output)
        return "<ul>".join($formations)."</ul>";
    else
        return $formations;
}

function show_calendrier($atts)
{
    ob_start();
    // Attributes
    $atts = shortcode_atts(
        array
            (
            'ville' => null,
            'formateur' => null,
            'presentation' => null,
            'sort' => "DESC",
            'passe' => false,
            'futur' => true,
            'h2' => null,
            'h3' => null,
            'h4' => null,
            ),
	$atts
	);
    
    $param = array();
    if ($atts['passe'] && !$atts['futur'])
        $param['quand'] = "passe";
    if (!$atts['passe'] && $atts['futur'])
        $param['quand'] = "futur";

    $param['sort'] = $atts['sort'];
    
    $sessions = get_formation_sessions($param);
    global $wpof;
    
    //debug_info($sessions, "sessions");
    
    if ($sessions) :
    
    ?>
    
    <div id="calendrier">
    <?php if (isset($atts['h2'])) echo "<h2>".$atts['h2']."</h2>"; ?>
    <?php if (isset($atts['h3'])) echo "<h3>".$atts['h3']."</h3>"; ?>
    <?php if (isset($atts['h4'])) echo "<h4>".$atts['h4']."</h4>"; ?>
    
    <ul>
    <?php foreach($sessions as $k => $s): ?>
    
    <li class="calendrier">
    <?php if (!empty($s->first_date)) : ?>
    <a href="<?php echo $s->permalien; ?>">
    <span class="dates"><?php echo $s->dates_texte; ?></span>
    </a>
    <?php else : ?>
    <span class="dates"><?php _e("Pas de date"); ?></span>
    <?php endif; ?>
    <?php if ($s->acces_session == "invite" || $s->acces_session == "connecte") : ?>
        <span class="visibilite <?php echo $s->acces_session; ?>">
        <?php echo $wpof->acces_session->get_term($s->acces_session); ?>
        </span>
    <?php endif; ?>
    <a href="<?php echo $s->permalien; ?>">
    <span class="formation"><?php echo $s->titre_formation; ?></span>
    </a>
    <?php if ($atts['ville']) echo "<span class='ville'>".$s->lieu_ville."</span>"; ?>
    <?php
    if ($atts['presentation'])
    {
        echo "<span class='presentation openButton' data-id='pres".$s->id."'>".__("En savoir plus")."</span>";
        echo "<div class='blocHidden' id='pres".$s->id."'>".$s->get_html_presentation()."</div>";
    }
    ?>
    </li>
    
    <?php endforeach; ?>
    </ul>
    
    </div>
    
    <?php
    else:
    
    echo "<p>".__("Pas de session programmée pour l'instant")."</p>";
    
    endif;
    return ob_get_clean();
}
add_shortcode( 'calendrier', 'show_calendrier' );


require_once(wpof_path . "class/class-session-formation.php");
require_once(wpof_path . "wpof-utilisateur.php");

function get_formation_sessions($atts = array())
{
    $user_id = get_current_user_id();
    $role = wpof_get_role($user_id);
    $meta_queries = array();
    $html_output = isset($atts['format']) && $atts['format'] == 'html';
    
    if (isset($atts['formation_id']))
    {
        $meta_queries[] = array('key' => 'formation', 'value' => $atts['formation_id']);
    }
    if ($user_id > 0)
    {
        if ($role == "um_stagiaire")
            $meta_queries[] = array('key' => 'acces_session', 'value' => 'invite', 'compare' => 'NOT IN');
    }
    else
    {
        $meta_queries[] = array('key' => 'acces_session', 'value' => 'public', 'compare' => 'IN');
    }

    $formation_session_posts = get_posts( array('post_type' => 'session', 'meta_query' => $meta_queries, 'posts_per_page' => -1));
    
    $formation_sessions = array();

    // gestion du cas où la session n'a pas encore de date
    $nodate = time() + 1000;
    foreach ($formation_session_posts as $fs)
    {
        $s = new SessionFormation($fs->ID);
        $today = time();
        
        if (!in_array($role, array('um_formateur-trice', 'um_responsable', 'admin')) && empty($s->first_date_timestamp))
            continue;
        $ts = (empty($s->first_date_timestamp)) ? $nodate++ : $s->first_date_timestamp;
        
        if ((!isset($atts['quand']) || ($atts['quand'] == "futur" && $ts >= $today) || ($atts['quand'] == "passe" && $ts < $today))
            && ((isset($atts['formateur']) && in_array($atts['formateur'], $s->formateur)) || !isset($atts['formateur'])))
        {
            if ($html_output)
                $formation_sessions[$ts] = "<li><a href='{$s->permalien}'>{$s->titre_session}</a></li>";
            else
                $formation_sessions[$ts] = $s;
        }
        unset($s);
    }
    
    if (count($formation_sessions) == 0)
        return null;
    
    if (isset($atts['sort']) && $atts['sort'] == "ASC")
        ksort($formation_sessions);
    else
        krsort($formation_sessions);

    if ($html_output)
        return "<ul>".join($formation_sessions)."</ul>";
    else
        return $formation_sessions;
}

function get_template_formation()
{
    global $wpof;
    global $Formation;
    
    $formation_id = get_the_ID();
    if (isset($Formation[$formation_id]))
        $formation = $Formation[$formation_id];
    else
        $formation = $Formation[$formation_id] = new Formation($formation_id);

    $sessions = get_formation_sessions(array('formation_id' => $formation_id, 'quand' => 'futur'));
    ob_start();
    ?>
    
    <!-- Contenu court -->
    <div class="side-content">
    <?php if ($formation->duree > 0) :?>
    <div id="formation_duree">
        <h3><?php _e("Durée conseillée"); ?></h3>
        <p><?php printf(__("%d heures"), $formation->duree); ?></p>
    </div>
    <?php endif; ?>

    <div id="formation_tarif">
        <h3><?php _e("Tarif"); ?></h3>
        <p>
        <?php
            if ($formation->duree > 0)
            {
                echo get_tarif_formation($formation->tarif * $formation->duree);
                echo " ".__("soit")." ";
            }
            echo get_tarif_formation($formation->tarif)." ".__("de l'heure");
        echo $wpof->of_exotva ? "<br />".$wpof->terms_exo_tva : "";
        ?>
        </p>
        <p><?php echo $wpof->terms_tarif_inter.". ".$wpof->terms_tarif_groupe; ?></p>
        
    </div>

    <?php if (count($formation->formateur) > 0) :?>
        <div id="formation_formateur">
            <h3><?php _e("Équipe pédagogique"); ?></h3>
            <?php the_liste_formateur(array('only' => $formation->formateur)); ?>
        </div>
    <?php endif; ?>
    
        <div id="formation_sessions">
            <h3><?php echo __("Prochaines sessions"); ?></h3>
        <?php
            if (null == $sessions)
                echo "<p>".$wpof->terms_no_session."</p>";
            else
            {
                ?>
                <ul class="list list_date">
                <?php
                foreach($sessions as $s)
                {
                    echo "<li><a href='{$s->permalien}'>{$s->dates_texte}</a></br>{$s->ville}</li>";
                }
                ?>
                </ul>
                <?php
            }
        ?>
        </div>
            
    </div>

    <!-- Contenu long -->
    <div class="main-content">

        <?php if ($formation->presentation != "") :?>
        <div id="formation_presentation">
            <?php echo $formation->presentation ?>
        </div>
        <?php endif; ?>

        <div id="formation_prerequis">
            <h3><?php echo __("Pré-requis"); ?></h3>
            <?php echo $formation->prerequis; ?>
        </div>

        <div id="formation_objectifs">
            <h3><?php echo __("Objectifs"); ?></h3>
            <?php echo $formation->objectifs; ?>
        </div>

        <div id="formation_materiel">
            <h3><?php echo __("Moyens pédagogiques"); ?></h3>
            <?php echo $formation->materiel_pedagogique; ?>
        </div>

        <div id="formation_programme">
            <h3><?php echo __("Programme"); ?></h3>
            <?php echo $formation->programme; ?>
        </div>

    </div> <!-- .main-content -->
    <?php    
    return ob_get_clean();
}


?>
