<?php
/*
 * wpof-first-init.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 

/*
 * Rensigne le tableau $SessionFormation avec les sessions concernant l'année comptable passée en argument.
 */
function set_session_annee_comptable($annee)
{
    global $SessionFormation;
    $SessionFormation = array();
    
    $meta_queries = array();
    $session_posts = get_posts( array('post_type' => 'session', 'meta_query' => $meta_queries, 'posts_per_page' => -1));
    
    foreach($session_posts as $post)
    {
        $exe_comptable = get_post_meta($post->ID, "exe_comptable", true);
        if ($annee == -1 || isset($exe_comptable[$annee]) && $exe_comptable[$annee] > 0)
            $SessionFormation[$post->ID] = new SessionFormation($post->ID);
    }
}

function get_choix_annee_comptable($annee = null)
{
    global $wpof;
    
    if ($annee == null)
    {
        $annee = get_user_meta(get_current_user_id(), "annee_comptable", true);
        if (empty($annee))
            $annee = date('Y') - 1;
    }
    
    $annee_actuelle = date('Y');
        
    $liste_annee = array("Toutes");
    for ($a = $wpof->annee1; $a <= $annee_actuelle+1; $a++)
        $liste_annee[$a] = $a;
    
    return select_by_list($liste_annee, "annee_choix", $annee, "data-userid='".get_current_user_id()."'");
}

?>
